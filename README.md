# X_Team_Sink and X_Team_Source are two Bluetooth / BLE applications involving A2DP.

X_Team_Source is an audio source. The input is music via a hardwired I2S bus. Upon power-up, 
it advertises and connects to one sink in range. The I2S input aspect is not fully functional,
but the BT / BLE aspect is.

X_Team_Sink is an audio sink with the Handsfree Profile (HFP). Upon power-up it advertises.
Connect to it with your phone. Play audio through your phone. An I2S audio DAC / Codec must be hardwired
to hear any music. 

mtb_shared is the shared folder containing the Cypress SDK and tools. 