// ../mtb_shared/wiced_btsdk/dev-kit/baselib/20706A2/release-v2.8.0/make/scripts/wiced-gen-lib-installer.pl
// args: ../mtb_shared/wiced_btsdk/dev-kit/baselib/20706A2/release-v2.8.0/libraries/prebuilt/wiced_audio_sink.a ../mtb_shared/wiced_btsdk/dev-kit/baselib/20706A2/release-v2.8.0/libraries/prebuilt/wiced_voice_path.a
// output: C:/Users/robhe/mtw/X_Team_Sink/build/CYW920706WCDEVAL/generated/lib_installer.c
#include <stdint.h>
typedef struct tag_PATCH_TABLE_ENTRY_t {
	uint32_t breakout;
	uint32_t replacement;
} PATCH_TABLE_ENTRY_t;
void patch_autoInstall(uint32_t old_address, uint32_t new_address);
void patch_autoReplace(uint32_t breakout_address, uint32_t replacement);
void patch_autoReplaceData(uint32_t breakout_address, uint32_t replacement);
void install_libs(void);

void install_libs(void)
{
	{
		extern void wiced_audio_sink_init(void);
		wiced_audio_sink_init();
	}
	{
		extern void install_patch_lite_host_proc_route_config_req(void);
		install_patch_lite_host_proc_route_config_req();
	}
	{
		extern void install_patch_addin_get_sbc_frame_len_in_jitter_buf(void);
		install_patch_addin_get_sbc_frame_len_in_jitter_buf();
	}
	{
		extern void install_patch_addin_Sample_volume_ctrl(void);
		install_patch_addin_Sample_volume_ctrl();
	}
	{
		extern void install_patch_addin_Timer2DoneInt(void);
		install_patch_addin_Timer2DoneInt();
	}
	{
		extern void install_patch_pcm_scoLinkDown(void);
		install_patch_pcm_scoLinkDown();
	}
}
#define PATCH_ENTRIES_WITH_LIBRARIES (5 + CY_PATCH_ENTRIES_BASE)
#if PATCH_ENTRIES_WITH_LIBRARIES > NUM_PATCH_ENTRIES
#error Too many patch entries for device, after adding libraries
#endif
