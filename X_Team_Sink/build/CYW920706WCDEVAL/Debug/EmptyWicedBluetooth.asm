
C:/Users/robhe/mtw/X_Team_Sink/build/CYW920706WCDEVAL/Debug/EmptyWicedBluetooth.elf:     file format elf32-littlearm


Disassembly of section .text:

00217a38 <wiced_bt_app_start>:
  217a38:	b508      	push	{r3, lr}
  217a3a:	f5f5 fa86 	bl	cf4a <wiced_hal_gpio_register_pin_for_interrupt+0x69>
  217a3e:	2004      	movs	r0, #4
  217a40:	f6c5 fb15 	bl	dd06e <wiced_bt_trace_enable_patch+0x31>
  217a44:	2100      	movs	r1, #0
  217a46:	4a0d      	ldr	r2, [pc, #52]	; (217a7c <wiced_bt_app_start+0x44>)
  217a48:	4608      	mov	r0, r1
  217a4a:	f5f6 ff63 	bl	e914 <mia_init+0xef>
  217a4e:	2300      	movs	r3, #0
  217a50:	211f      	movs	r1, #31
  217a52:	461a      	mov	r2, r3
  217a54:	2021      	movs	r0, #33	; 0x21
  217a56:	f5f5 fa81 	bl	cf5c <wiced_hal_puart_init+0x11>
  217a5a:	2100      	movs	r1, #0
  217a5c:	4a08      	ldr	r2, [pc, #32]	; (217a80 <wiced_bt_app_start+0x48>)
  217a5e:	4608      	mov	r0, r1
  217a60:	f5f5 fdfc 	bl	d65c <wiced_va_printf+0x1f7>
  217a64:	4a07      	ldr	r2, [pc, #28]	; (217a84 <wiced_bt_app_start+0x4c>)
  217a66:	4908      	ldr	r1, [pc, #32]	; (217a88 <wiced_bt_app_start+0x50>)
  217a68:	4808      	ldr	r0, [pc, #32]	; (217a8c <wiced_bt_app_start+0x54>)
  217a6a:	f6c1 f832 	bl	d8ad2 <wiced_bt_app_post_stack_init_patch+0x2ab>
  217a6e:	4b08      	ldr	r3, [pc, #32]	; (217a90 <wiced_bt_app_start+0x58>)
  217a70:	e893 0007 	ldmia.w	r3, {r0, r1, r2}
  217a74:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
  217a78:	f6c4 bc80 	b.w	dc37c <wiced_audio_sink_buff_init+0x1d>
  217a7c:	002dc6c0 	.word	0x002dc6c0
  217a80:	0021ee41 	.word	0x0021ee41
  217a84:	0021dc70 	.word	0x0021dc70
  217a88:	0021dc80 	.word	0x0021dc80
  217a8c:	0021ad35 	.word	0x0021ad35
  217a90:	0021db74 	.word	0x0021db74

00217a94 <get_sbc_frame_len_in_jitter_buf_addin>:
  217a94:	e8bd 40f0 	ldmia.w	sp!, {r4, r5, r6, r7, lr}
  217a98:	f001 bbf4 	b.w	219284 <get_sbc_frame_len_in_jitter_buf_patch>

00217a9c <Timer2DoneInt_addin>:
  217a9c:	f002 fa38 	bl	219f10 <Timer2DoneInt_C_patch>
  217aa0:	f602 be9e 	b.w	1a7e0 <A2f9e2500+0x9>

00217aa4 <wiced_audio_sink_parse_rx_acl_data>:
  217aa4:	b51c      	push	{r2, r3, r4, lr}
  217aa6:	4cff      	ldr	r4, [pc, #1020]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217aa8:	7822      	ldrb	r2, [r4, #0]
  217aaa:	2a04      	cmp	r2, #4
  217aac:	d001      	beq.n	217ab2 <wiced_audio_sink_parse_rx_acl_data+0xe>
  217aae:	2a05      	cmp	r2, #5
  217ab0:	d11b      	bne.n	217aea <wiced_audio_sink_parse_rx_acl_data+0x46>
  217ab2:	6803      	ldr	r3, [r0, #0]
  217ab4:	6842      	ldr	r2, [r0, #4]
  217ab6:	f3c3 3301 	ubfx	r3, r3, #12, #2
  217aba:	0c12      	lsrs	r2, r2, #16
  217abc:	2b02      	cmp	r3, #2
  217abe:	d106      	bne.n	217ace <wiced_audio_sink_parse_rx_acl_data+0x2a>
  217ac0:	2a01      	cmp	r2, #1
  217ac2:	d004      	beq.n	217ace <wiced_audio_sink_parse_rx_acl_data+0x2a>
  217ac4:	2a02      	cmp	r2, #2
  217ac6:	d002      	beq.n	217ace <wiced_audio_sink_parse_rx_acl_data+0x2a>
  217ac8:	88e4      	ldrh	r4, [r4, #6]
  217aca:	42a2      	cmp	r2, r4
  217acc:	d001      	beq.n	217ad2 <wiced_audio_sink_parse_rx_acl_data+0x2e>
  217ace:	2b01      	cmp	r3, #1
  217ad0:	d10b      	bne.n	217aea <wiced_audio_sink_parse_rx_acl_data+0x46>
  217ad2:	e9cd 0100 	strd	r0, r1, [sp]
  217ad6:	4669      	mov	r1, sp
  217ad8:	48f3      	ldr	r0, [pc, #972]	; (217ea8 <wiced_audio_sink_configure+0xe6>)
  217ada:	f64e ffa9 	bl	66a30 <Ab6ab9280>
  217ade:	f04f 6000 	mov.w	r0, #134217728	; 0x8000000
  217ae2:	f65a fbf0 	bl	722c6 <Ae3690800>
  217ae6:	2001      	movs	r0, #1
  217ae8:	bd1c      	pop	{r2, r3, r4, pc}
  217aea:	2000      	movs	r0, #0
  217aec:	bd1c      	pop	{r2, r3, r4, pc}

00217aee <wiced_audio_sink_send_data_to_app>:
  217aee:	b510      	push	{r4, lr}
  217af0:	4604      	mov	r4, r0
  217af2:	48ec      	ldr	r0, [pc, #944]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217af4:	49eb      	ldr	r1, [pc, #940]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217af6:	7800      	ldrb	r0, [r0, #0]
  217af8:	6a0a      	ldr	r2, [r1, #32]
  217afa:	2805      	cmp	r0, #5
  217afc:	d107      	bne.n	217b0e <wiced_audio_sink_send_data_to_app+0x20>
  217afe:	b112      	cbz	r2, 217b06 <wiced_audio_sink_send_data_to_app+0x18>
  217b00:	6821      	ldr	r1, [r4, #0]
  217b02:	6860      	ldr	r0, [r4, #4]
  217b04:	4790      	blx	r2
  217b06:	68a0      	ldr	r0, [r4, #8]
  217b08:	f64d ff7c 	bl	65a04 <A64fb9d00>
  217b0c:	e008      	b.n	217b20 <wiced_audio_sink_send_data_to_app+0x32>
  217b0e:	2803      	cmp	r0, #3
  217b10:	d106      	bne.n	217b20 <wiced_audio_sink_send_data_to_app+0x32>
  217b12:	b112      	cbz	r2, 217b1a <wiced_audio_sink_send_data_to_app+0x2c>
  217b14:	6821      	ldr	r1, [r4, #0]
  217b16:	6860      	ldr	r0, [r4, #4]
  217b18:	4790      	blx	r2
  217b1a:	6860      	ldr	r0, [r4, #4]
  217b1c:	f6b9 fb5a 	bl	d11d4 <wiced_bt_free_buffer>
  217b20:	4620      	mov	r0, r4
  217b22:	f652 f9a1 	bl	69e68 <A4402f200>
  217b26:	2000      	movs	r0, #0
  217b28:	bd10      	pop	{r4, pc}

00217b2a <wiced_audio_sink_send_decoded_audio>:
  217b2a:	b570      	push	{r4, r5, r6, lr}
  217b2c:	4ddd      	ldr	r5, [pc, #884]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217b2e:	7829      	ldrb	r1, [r5, #0]
  217b30:	b951      	cbnz	r1, 217b48 <wiced_audio_sink_send_decoded_audio+0x1e>
  217b32:	0541      	lsls	r1, r0, #21
  217b34:	d501      	bpl.n	217b3a <wiced_audio_sink_send_decoded_audio+0x10>
  217b36:	48dd      	ldr	r0, [pc, #884]	; (217eac <wiced_audio_sink_configure+0xea>)
  217b38:	e002      	b.n	217b40 <wiced_audio_sink_send_decoded_audio+0x16>
  217b3a:	0500      	lsls	r0, r0, #20
  217b3c:	d53d      	bpl.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217b3e:	48dc      	ldr	r0, [pc, #880]	; (217eb0 <wiced_audio_sink_configure+0xee>)
  217b40:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  217b44:	f650 bee6 	b.w	68914 <A28a35e00>
  217b48:	2901      	cmp	r1, #1
  217b4a:	d11d      	bne.n	217b88 <wiced_audio_sink_send_decoded_audio+0x5e>
  217b4c:	48d9      	ldr	r0, [pc, #868]	; (217eb4 <wiced_audio_sink_configure+0xf2>)
  217b4e:	6800      	ldr	r0, [r0, #0]
  217b50:	f6c4 ff07 	bl	dc962 <wiced_transport_allocate_buffer>
  217b54:	0006      	movs	r6, r0
  217b56:	d030      	beq.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217b58:	6968      	ldr	r0, [r5, #20]
  217b5a:	f6b9 fb39 	bl	d11d0 <wiced_bt_get_buffer>
  217b5e:	0004      	movs	r4, r0
  217b60:	d02b      	beq.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217b62:	4620      	mov	r0, r4
  217b64:	f650 fed6 	bl	68914 <A28a35e00>
  217b68:	4621      	mov	r1, r4
  217b6a:	4630      	mov	r0, r6
  217b6c:	696a      	ldr	r2, [r5, #20]
  217b6e:	f657 f8f0 	bl	6ed52 <mpaf_memcpy>
  217b72:	4631      	mov	r1, r6
  217b74:	f241 400a 	movw	r0, #5130	; 0x140a
  217b78:	696a      	ldr	r2, [r5, #20]
  217b7a:	f6c4 ff0d 	bl	dc998 <wiced_transport_send_buffer>
  217b7e:	4620      	mov	r0, r4
  217b80:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  217b84:	f6b9 bb26 	b.w	d11d4 <wiced_bt_free_buffer>
  217b88:	2903      	cmp	r1, #3
  217b8a:	d116      	bne.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217b8c:	6968      	ldr	r0, [r5, #20]
  217b8e:	f6b9 fb1f 	bl	d11d0 <wiced_bt_get_buffer>
  217b92:	0004      	movs	r4, r0
  217b94:	d011      	beq.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217b96:	4620      	mov	r0, r4
  217b98:	f650 febc 	bl	68914 <A28a35e00>
  217b9c:	2008      	movs	r0, #8
  217b9e:	f652 f961 	bl	69e64 <A9ee0f800>
  217ba2:	2800      	cmp	r0, #0
  217ba4:	d009      	beq.n	217bba <wiced_audio_sink_send_decoded_audio+0x90>
  217ba6:	6969      	ldr	r1, [r5, #20]
  217ba8:	6001      	str	r1, [r0, #0]
  217baa:	6044      	str	r4, [r0, #4]
  217bac:	4601      	mov	r1, r0
  217bae:	f2af 00c1 	subw	r0, pc, #193	; 0xc1
  217bb2:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  217bb6:	f678 bb1b 	b.w	901f0 <wiced_app_event_serialize>
  217bba:	bd70      	pop	{r4, r5, r6, pc}

00217bbc <wiced_audio_sink_rx_data_serialization_init>:
  217bbc:	b510      	push	{r4, lr}
  217bbe:	48b9      	ldr	r0, [pc, #740]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217bc0:	7840      	ldrb	r0, [r0, #1]
  217bc2:	00c4      	lsls	r4, r0, #3
  217bc4:	4620      	mov	r0, r4
  217bc6:	f652 f954 	bl	69e72 <cfa_mm_Sbrk>
  217bca:	4622      	mov	r2, r4
  217bcc:	4601      	mov	r1, r0
  217bce:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  217bd2:	2302      	movs	r3, #2
  217bd4:	48b4      	ldr	r0, [pc, #720]	; (217ea8 <wiced_audio_sink_configure+0xe6>)
  217bd6:	f64e bf1f 	b.w	66a18 <Ab769a400>

00217bda <wiced_audio_sink_process_media_pkt>:
  217bda:	b570      	push	{r4, r5, r6, lr}
  217bdc:	4cb1      	ldr	r4, [pc, #708]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217bde:	4616      	mov	r6, r2
  217be0:	4605      	mov	r5, r0
  217be2:	69e2      	ldr	r2, [r4, #28]
  217be4:	2000      	movs	r0, #0
  217be6:	b192      	cbz	r2, 217c0e <wiced_audio_sink_process_media_pkt+0x34>
  217be8:	8960      	ldrh	r0, [r4, #10]
  217bea:	4410      	add	r0, r2
  217bec:	8922      	ldrh	r2, [r4, #8]
  217bee:	4410      	add	r0, r2
  217bf0:	4632      	mov	r2, r6
  217bf2:	f657 f8ae 	bl	6ed52 <mpaf_memcpy>
  217bf6:	4628      	mov	r0, r5
  217bf8:	f64d ff04 	bl	65a04 <A64fb9d00>
  217bfc:	8960      	ldrh	r0, [r4, #10]
  217bfe:	4430      	add	r0, r6
  217c00:	8160      	strh	r0, [r4, #10]
  217c02:	89a0      	ldrh	r0, [r4, #12]
  217c04:	b108      	cbz	r0, 217c0a <wiced_audio_sink_process_media_pkt+0x30>
  217c06:	2002      	movs	r0, #2
  217c08:	bd70      	pop	{r4, r5, r6, pc}
  217c0a:	2001      	movs	r0, #1
  217c0c:	bd70      	pop	{r4, r5, r6, pc}
  217c0e:	89a1      	ldrh	r1, [r4, #12]
  217c10:	2900      	cmp	r1, #0
  217c12:	d0fb      	beq.n	217c0c <wiced_audio_sink_process_media_pkt+0x32>
  217c14:	61e5      	str	r5, [r4, #28]
  217c16:	8166      	strh	r6, [r4, #10]
  217c18:	e7f5      	b.n	217c06 <wiced_audio_sink_process_media_pkt+0x2c>

00217c1a <wiced_audio_sink_send_data_to_transport>:
  217c1a:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  217c1e:	4ea1      	ldr	r6, [pc, #644]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217c20:	4fa4      	ldr	r7, [pc, #656]	; (217eb4 <wiced_audio_sink_configure+0xf2>)
  217c22:	69f4      	ldr	r4, [r6, #28]
  217c24:	4680      	mov	r8, r0
  217c26:	460d      	mov	r5, r1
  217c28:	b924      	cbnz	r4, 217c34 <wiced_audio_sink_send_data_to_transport+0x1a>
  217c2a:	6838      	ldr	r0, [r7, #0]
  217c2c:	f6c4 fe99 	bl	dc962 <wiced_transport_allocate_buffer>
  217c30:	0004      	movs	r4, r0
  217c32:	d024      	beq.n	217c7e <wiced_audio_sink_send_data_to_transport+0x64>
  217c34:	6838      	ldr	r0, [r7, #0]
  217c36:	f6c4 fea5 	bl	dc984 <wiced_transport_get_buffer_size>
  217c3a:	8971      	ldrh	r1, [r6, #10]
  217c3c:	2700      	movs	r7, #0
  217c3e:	186a      	adds	r2, r5, r1
  217c40:	4290      	cmp	r0, r2
  217c42:	d317      	bcc.n	217c74 <wiced_audio_sink_send_data_to_transport+0x5a>
  217c44:	1860      	adds	r0, r4, r1
  217c46:	462a      	mov	r2, r5
  217c48:	4641      	mov	r1, r8
  217c4a:	f657 f882 	bl	6ed52 <mpaf_memcpy>
  217c4e:	89b0      	ldrh	r0, [r6, #12]
  217c50:	b948      	cbnz	r0, 217c66 <wiced_audio_sink_send_data_to_transport+0x4c>
  217c52:	8970      	ldrh	r0, [r6, #10]
  217c54:	4621      	mov	r1, r4
  217c56:	182a      	adds	r2, r5, r0
  217c58:	f241 400a 	movw	r0, #5130	; 0x140a
  217c5c:	f6c4 fe9c 	bl	dc998 <wiced_transport_send_buffer>
  217c60:	61f7      	str	r7, [r6, #28]
  217c62:	8177      	strh	r7, [r6, #10]
  217c64:	e003      	b.n	217c6e <wiced_audio_sink_send_data_to_transport+0x54>
  217c66:	61f4      	str	r4, [r6, #28]
  217c68:	8970      	ldrh	r0, [r6, #10]
  217c6a:	4428      	add	r0, r5
  217c6c:	8170      	strh	r0, [r6, #10]
  217c6e:	2000      	movs	r0, #0
  217c70:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
  217c74:	4620      	mov	r0, r4
  217c76:	f6c4 fec5 	bl	dca04 <wiced_transport_free_buffer>
  217c7a:	61f7      	str	r7, [r6, #28]
  217c7c:	8177      	strh	r7, [r6, #10]
  217c7e:	2004      	movs	r0, #4
  217c80:	e7f6      	b.n	217c70 <wiced_audio_sink_send_data_to_transport+0x56>

00217c82 <wiced_audio_sink_send_data>:
  217c82:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  217c86:	4f87      	ldr	r7, [pc, #540]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217c88:	4606      	mov	r6, r0
  217c8a:	7838      	ldrb	r0, [r7, #0]
  217c8c:	460d      	mov	r5, r1
  217c8e:	4614      	mov	r4, r2
  217c90:	2804      	cmp	r0, #4
  217c92:	d108      	bne.n	217ca6 <wiced_audio_sink_send_data+0x24>
  217c94:	b11c      	cbz	r4, 217c9e <wiced_audio_sink_send_data+0x1c>
  217c96:	4621      	mov	r1, r4
  217c98:	4628      	mov	r0, r5
  217c9a:	f7ff ffbe 	bl	217c1a <wiced_audio_sink_send_data_to_transport>
  217c9e:	4630      	mov	r0, r6
  217ca0:	f64d feb0 	bl	65a04 <A64fb9d00>
  217ca4:	e00f      	b.n	217cc6 <wiced_audio_sink_send_data+0x44>
  217ca6:	4622      	mov	r2, r4
  217ca8:	4629      	mov	r1, r5
  217caa:	4630      	mov	r0, r6
  217cac:	f7ff ff95 	bl	217bda <wiced_audio_sink_process_media_pkt>
  217cb0:	2801      	cmp	r0, #1
  217cb2:	d106      	bne.n	217cc2 <wiced_audio_sink_send_data+0x40>
  217cb4:	8938      	ldrh	r0, [r7, #8]
  217cb6:	69fe      	ldr	r6, [r7, #28]
  217cb8:	897c      	ldrh	r4, [r7, #10]
  217cba:	1835      	adds	r5, r6, r0
  217cbc:	2000      	movs	r0, #0
  217cbe:	61f8      	str	r0, [r7, #28]
  217cc0:	e003      	b.n	217cca <wiced_audio_sink_send_data+0x48>
  217cc2:	2802      	cmp	r0, #2
  217cc4:	d101      	bne.n	217cca <wiced_audio_sink_send_data+0x48>
  217cc6:	2001      	movs	r0, #1
  217cc8:	e7d2      	b.n	217c70 <wiced_audio_sink_send_data_to_transport+0x56>
  217cca:	b164      	cbz	r4, 217ce6 <wiced_audio_sink_send_data+0x64>
  217ccc:	200c      	movs	r0, #12
  217cce:	f652 f8c9 	bl	69e64 <A9ee0f800>
  217cd2:	b140      	cbz	r0, 217ce6 <wiced_audio_sink_send_data+0x64>
  217cd4:	6004      	str	r4, [r0, #0]
  217cd6:	6045      	str	r5, [r0, #4]
  217cd8:	6086      	str	r6, [r0, #8]
  217cda:	4601      	mov	r1, r0
  217cdc:	f2af 10f1 	subw	r0, pc, #497	; 0x1f1
  217ce0:	f678 fa86 	bl	901f0 <wiced_app_event_serialize>
  217ce4:	e7ef      	b.n	217cc6 <wiced_audio_sink_send_data+0x44>
  217ce6:	2000      	movs	r0, #0
  217ce8:	e7c2      	b.n	217c70 <wiced_audio_sink_send_data_to_transport+0x56>

00217cea <wiced_audio_sink_process_rx_acl_data>:
  217cea:	b57c      	push	{r2, r3, r4, r5, r6, lr}
  217cec:	2400      	movs	r4, #0
  217cee:	4d6d      	ldr	r5, [pc, #436]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217cf0:	e03a      	b.n	217d68 <wiced_audio_sink_process_rx_acl_data+0x7e>
  217cf2:	4669      	mov	r1, sp
  217cf4:	486c      	ldr	r0, [pc, #432]	; (217ea8 <wiced_audio_sink_configure+0xe6>)
  217cf6:	f64e fe98 	bl	66a2a <Aa7c76a00>
  217cfa:	9800      	ldr	r0, [sp, #0]
  217cfc:	6801      	ldr	r1, [r0, #0]
  217cfe:	f3c1 3101 	ubfx	r1, r1, #12, #2
  217d02:	2902      	cmp	r1, #2
  217d04:	d117      	bne.n	217d36 <wiced_audio_sink_process_rx_acl_data+0x4c>
  217d06:	6843      	ldr	r3, [r0, #4]
  217d08:	6880      	ldr	r0, [r0, #8]
  217d0a:	2108      	movs	r1, #8
  217d0c:	8129      	strh	r1, [r5, #8]
  217d0e:	9a01      	ldr	r2, [sp, #4]
  217d10:	9900      	ldr	r1, [sp, #0]
  217d12:	9e01      	ldr	r6, [sp, #4]
  217d14:	1f12      	subs	r2, r2, #4
  217d16:	3108      	adds	r1, #8
  217d18:	1b9b      	subs	r3, r3, r6
  217d1a:	1d1b      	adds	r3, r3, #4
  217d1c:	b29b      	uxth	r3, r3
  217d1e:	81ab      	strh	r3, [r5, #12]
  217d20:	816c      	strh	r4, [r5, #10]
  217d22:	f3c0 1040 	ubfx	r0, r0, #5, #1
  217d26:	7168      	strb	r0, [r5, #5]
  217d28:	b2c0      	uxtb	r0, r0
  217d2a:	b100      	cbz	r0, 217d2e <wiced_audio_sink_process_rx_acl_data+0x44>
  217d2c:	b1bb      	cbz	r3, 217d5e <wiced_audio_sink_process_rx_acl_data+0x74>
  217d2e:	9800      	ldr	r0, [sp, #0]
  217d30:	f7ff ffa7 	bl	217c82 <wiced_audio_sink_send_data>
  217d34:	e018      	b.n	217d68 <wiced_audio_sink_process_rx_acl_data+0x7e>
  217d36:	2901      	cmp	r1, #1
  217d38:	d116      	bne.n	217d68 <wiced_audio_sink_process_rx_acl_data+0x7e>
  217d3a:	89a8      	ldrh	r0, [r5, #12]
  217d3c:	b1a0      	cbz	r0, 217d68 <wiced_audio_sink_process_rx_acl_data+0x7e>
  217d3e:	9901      	ldr	r1, [sp, #4]
  217d40:	4281      	cmp	r1, r0
  217d42:	d901      	bls.n	217d48 <wiced_audio_sink_process_rx_acl_data+0x5e>
  217d44:	81ac      	strh	r4, [r5, #12]
  217d46:	e001      	b.n	217d4c <wiced_audio_sink_process_rx_acl_data+0x62>
  217d48:	1a40      	subs	r0, r0, r1
  217d4a:	81a8      	strh	r0, [r5, #12]
  217d4c:	e9dd 1200 	ldrd	r1, r2, [sp]
  217d50:	7968      	ldrb	r0, [r5, #5]
  217d52:	1d09      	adds	r1, r1, #4
  217d54:	2800      	cmp	r0, #0
  217d56:	d0ea      	beq.n	217d2e <wiced_audio_sink_process_rx_acl_data+0x44>
  217d58:	89a8      	ldrh	r0, [r5, #12]
  217d5a:	2800      	cmp	r0, #0
  217d5c:	d1e7      	bne.n	217d2e <wiced_audio_sink_process_rx_acl_data+0x44>
  217d5e:	1888      	adds	r0, r1, r2
  217d60:	f810 0c01 	ldrb.w	r0, [r0, #-1]
  217d64:	1a12      	subs	r2, r2, r0
  217d66:	e7e2      	b.n	217d2e <wiced_audio_sink_process_rx_acl_data+0x44>
  217d68:	484f      	ldr	r0, [pc, #316]	; (217ea8 <wiced_audio_sink_configure+0xe6>)
  217d6a:	f64e fe8d 	bl	66a88 <A265e9180>
  217d6e:	2800      	cmp	r0, #0
  217d70:	d0bf      	beq.n	217cf2 <wiced_audio_sink_process_rx_acl_data+0x8>
  217d72:	bd7c      	pop	{r2, r3, r4, r5, r6, pc}

00217d74 <wiced_audio_sink_event_handler>:
  217d74:	b510      	push	{r4, lr}
  217d76:	4604      	mov	r4, r0
  217d78:	b959      	cbnz	r1, 217d92 <wiced_audio_sink_event_handler+0x1e>
  217d7a:	03a0      	lsls	r0, r4, #14
  217d7c:	d503      	bpl.n	217d86 <wiced_audio_sink_event_handler+0x12>
  217d7e:	f001 f983 	bl	219088 <lite_host_process_rcv_l2cap_pkt_patch>
  217d82:	f424 3400 	bic.w	r4, r4, #131072	; 0x20000
  217d86:	0120      	lsls	r0, r4, #4
  217d88:	d503      	bpl.n	217d92 <wiced_audio_sink_event_handler+0x1e>
  217d8a:	f7ff ffae 	bl	217cea <wiced_audio_sink_process_rx_acl_data>
  217d8e:	f024 6400 	bic.w	r4, r4, #134217728	; 0x8000000
  217d92:	4620      	mov	r0, r4
  217d94:	bd10      	pop	{r4, pc}

00217d96 <wiced_audio_sink_config_init>:
  217d96:	b510      	push	{r4, lr}
  217d98:	4c42      	ldr	r4, [pc, #264]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217d9a:	6120      	str	r0, [r4, #16]
  217d9c:	78e0      	ldrb	r0, [r4, #3]
  217d9e:	b970      	cbnz	r0, 217dbe <wiced_audio_sink_config_init+0x28>
  217da0:	2000      	movs	r0, #0
  217da2:	f5f6 fbff 	bl	e5a4 <A49a83200>
  217da6:	f2af 0033 	subw	r0, pc, #51	; 0x33
  217daa:	4943      	ldr	r1, [pc, #268]	; (217eb8 <wiced_audio_sink_configure+0xf6>)
  217dac:	6008      	str	r0, [r1, #0]
  217dae:	f7ff ff05 	bl	217bbc <wiced_audio_sink_rx_data_serialization_init>
  217db2:	f2af 2089 	subw	r0, pc, #649	; 0x289
  217db6:	4941      	ldr	r1, [pc, #260]	; (217ebc <wiced_audio_sink_configure+0xfa>)
  217db8:	6008      	str	r0, [r1, #0]
  217dba:	2001      	movs	r0, #1
  217dbc:	70e0      	strb	r0, [r4, #3]
  217dbe:	2000      	movs	r0, #0
  217dc0:	bd10      	pop	{r4, pc}

00217dc2 <wiced_audio_sink_configure>:
  217dc2:	e92d 4fff 	stmdb	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
  217dc6:	b09d      	sub	sp, #116	; 0x74
  217dc8:	469b      	mov	fp, r3
  217dca:	b2c0      	uxtb	r0, r0
  217dcc:	9d2a      	ldr	r5, [sp, #168]	; 0xa8
  217dce:	f629 f843 	bl	40e58 <A6e80ee00>
  217dd2:	2800      	cmp	r0, #0
  217dd4:	d078      	beq.n	217ec8 <wiced_audio_sink_configure+0x106>
  217dd6:	f8d0 716c 	ldr.w	r7, [r0, #364]	; 0x16c
  217dda:	2f00      	cmp	r7, #0
  217ddc:	d0fa      	beq.n	217dd4 <wiced_audio_sink_configure+0x12>
  217dde:	f8b7 105c 	ldrh.w	r1, [r7, #92]	; 0x5c
  217de2:	2000      	movs	r0, #0
  217de4:	f654 f9d5 	bl	6c192 <Ad260cc00>
  217de8:	0006      	movs	r6, r0
  217dea:	d0f3      	beq.n	217dd4 <wiced_audio_sink_configure+0x12>
  217dec:	482d      	ldr	r0, [pc, #180]	; (217ea4 <wiced_audio_sink_configure+0xe2>)
  217dee:	f8b7 105c 	ldrh.w	r1, [r7, #92]	; 0x5c
  217df2:	80c1      	strh	r1, [r0, #6]
  217df4:	2400      	movs	r4, #0
  217df6:	8104      	strh	r4, [r0, #8]
  217df8:	8184      	strh	r4, [r0, #12]
  217dfa:	8144      	strh	r4, [r0, #10]
  217dfc:	61c4      	str	r4, [r0, #28]
  217dfe:	991f      	ldr	r1, [sp, #124]	; 0x7c
  217e00:	f04f 0a05 	mov.w	sl, #5
  217e04:	b2c9      	uxtb	r1, r1
  217e06:	7001      	strb	r1, [r0, #0]
  217e08:	782a      	ldrb	r2, [r5, #0]
  217e0a:	7102      	strb	r2, [r0, #4]
  217e0c:	f04f 0801 	mov.w	r8, #1
  217e10:	2904      	cmp	r1, #4
  217e12:	d001      	beq.n	217e18 <wiced_audio_sink_configure+0x56>
  217e14:	2905      	cmp	r1, #5
  217e16:	d105      	bne.n	217e24 <wiced_audio_sink_configure+0x62>
  217e18:	4829      	ldr	r0, [pc, #164]	; (217ec0 <wiced_audio_sink_configure+0xfe>)
  217e1a:	2108      	movs	r1, #8
  217e1c:	f880 a000 	strb.w	sl, [r0]
  217e20:	7041      	strb	r1, [r0, #1]
  217e22:	e1d9      	b.n	2181d8 <wiced_audio_sink_configure+0x416>
  217e24:	4681      	mov	r9, r0
  217e26:	7900      	ldrb	r0, [r0, #4]
  217e28:	b920      	cbnz	r0, 217e34 <wiced_audio_sink_configure+0x72>
  217e2a:	1d28      	adds	r0, r5, #4
  217e2c:	f5f6 f84c 	bl	dec8 <wiced_get_sbcdec_frame_length>
  217e30:	f8c9 0014 	str.w	r0, [r9, #20]
  217e34:	f249 0008 	movw	r0, #36872	; 0x9008
  217e38:	f8ad 0040 	strh.w	r0, [sp, #64]	; 0x40
  217e3c:	f88d 4042 	strb.w	r4, [sp, #66]	; 0x42
  217e40:	ab04      	add	r3, sp, #16
  217e42:	aa05      	add	r2, sp, #20
  217e44:	a906      	add	r1, sp, #24
  217e46:	a810      	add	r0, sp, #64	; 0x40
  217e48:	f651 f9f6 	bl	69238 <A589a6300>
  217e4c:	f249 0003 	movw	r0, #36867	; 0x9003
  217e50:	f8ad 0040 	strh.w	r0, [sp, #64]	; 0x40
  217e54:	f88d 4042 	strb.w	r4, [sp, #66]	; 0x42
  217e58:	2004      	movs	r0, #4
  217e5a:	491a      	ldr	r1, [pc, #104]	; (217ec4 <wiced_audio_sink_configure+0x102>)
  217e5c:	f8ad 0043 	strh.w	r0, [sp, #67]	; 0x43
  217e60:	f8b1 1060 	ldrh.w	r1, [r1, #96]	; 0x60
  217e64:	f8ad 1045 	strh.w	r1, [sp, #69]	; 0x45
  217e68:	f8ad 4047 	strh.w	r4, [sp, #71]	; 0x47
  217e6c:	2102      	movs	r1, #2
  217e6e:	f88d 1049 	strb.w	r1, [sp, #73]	; 0x49
  217e72:	f88d 404a 	strb.w	r4, [sp, #74]	; 0x4a
  217e76:	f88d 004b 	strb.w	r0, [sp, #75]	; 0x4b
  217e7a:	f88d 804c 	strb.w	r8, [sp, #76]	; 0x4c
  217e7e:	8a31      	ldrh	r1, [r6, #16]
  217e80:	f8ad 104d 	strh.w	r1, [sp, #77]	; 0x4d
  217e84:	8a71      	ldrh	r1, [r6, #18]
  217e86:	f8ad 104f 	strh.w	r1, [sp, #79]	; 0x4f
  217e8a:	f8b6 1070 	ldrh.w	r1, [r6, #112]	; 0x70
  217e8e:	f8ad 1051 	strh.w	r1, [sp, #81]	; 0x51
  217e92:	68f1      	ldr	r1, [r6, #12]
  217e94:	8b89      	ldrh	r1, [r1, #28]
  217e96:	f8ad 1053 	strh.w	r1, [sp, #83]	; 0x53
  217e9a:	f8ad 0055 	strh.w	r0, [sp, #85]	; 0x55
  217e9e:	f88d 8057 	strb.w	r8, [sp, #87]	; 0x57
  217ea2:	e012      	b.n	217eca <wiced_audio_sink_configure+0x108>
  217ea4:	0021fa80 	.word	0x0021fa80
  217ea8:	0021fc50 	.word	0x0021fc50
  217eac:	00326000 	.word	0x00326000
  217eb0:	00326200 	.word	0x00326200
  217eb4:	00216d40 	.word	0x00216d40
  217eb8:	00201bdc 	.word	0x00201bdc
  217ebc:	00216d38 	.word	0x00216d38
  217ec0:	0020b75c 	.word	0x0020b75c
  217ec4:	00205fa8 	.word	0x00205fa8
  217ec8:	e0be      	b.n	218048 <wiced_audio_sink_configure+0x286>
  217eca:	ab04      	add	r3, sp, #16
  217ecc:	aa05      	add	r2, sp, #20
  217ece:	a906      	add	r1, sp, #24
  217ed0:	a810      	add	r0, sp, #64	; 0x40
  217ed2:	f651 f9b1 	bl	69238 <A589a6300>
  217ed6:	f249 0006 	movw	r0, #36870	; 0x9006
  217eda:	f8ad 0040 	strh.w	r0, [sp, #64]	; 0x40
  217ede:	2008      	movs	r0, #8
  217ee0:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  217ee4:	f88d 8043 	strb.w	r8, [sp, #67]	; 0x43
  217ee8:	f8b7 005c 	ldrh.w	r0, [r7, #92]	; 0x5c
  217eec:	f8ad 0044 	strh.w	r0, [sp, #68]	; 0x44
  217ef0:	f8cd 4046 	str.w	r4, [sp, #70]	; 0x46
  217ef4:	ab04      	add	r3, sp, #16
  217ef6:	aa05      	add	r2, sp, #20
  217ef8:	a906      	add	r1, sp, #24
  217efa:	a810      	add	r0, sp, #64	; 0x40
  217efc:	f651 f99c 	bl	69238 <A589a6300>
  217f00:	f1bb 0f00 	cmp.w	fp, #0
  217f04:	d017      	beq.n	217f36 <wiced_audio_sink_configure+0x174>
  217f06:	f249 0006 	movw	r0, #36870	; 0x9006
  217f0a:	f8ad 0040 	strh.w	r0, [sp, #64]	; 0x40
  217f0e:	200a      	movs	r0, #10
  217f10:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  217f14:	f88d 8043 	strb.w	r8, [sp, #67]	; 0x43
  217f18:	8a30      	ldrh	r0, [r6, #16]
  217f1a:	f8ad 0044 	strh.w	r0, [sp, #68]	; 0x44
  217f1e:	f00b 00ff 	and.w	r0, fp, #255	; 0xff
  217f22:	f8ad 0046 	strh.w	r0, [sp, #70]	; 0x46
  217f26:	f88d 4048 	strb.w	r4, [sp, #72]	; 0x48
  217f2a:	ab04      	add	r3, sp, #16
  217f2c:	aa05      	add	r2, sp, #20
  217f2e:	a906      	add	r1, sp, #24
  217f30:	a810      	add	r0, sp, #64	; 0x40
  217f32:	f651 f981 	bl	69238 <A589a6300>
  217f36:	f249 0b05 	movw	fp, #36869	; 0x9005
  217f3a:	f8ad b040 	strh.w	fp, [sp, #64]	; 0x40
  217f3e:	2013      	movs	r0, #19
  217f40:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  217f44:	7828      	ldrb	r0, [r5, #0]
  217f46:	2800      	cmp	r0, #0
  217f48:	d17e      	bne.n	218048 <wiced_audio_sink_configure+0x286>
  217f4a:	aa03      	add	r2, sp, #12
  217f4c:	a901      	add	r1, sp, #4
  217f4e:	1d28      	adds	r0, r5, #4
  217f50:	f5f5 ffcc 	bl	deec <wiced_get_codec_info>
  217f54:	2002      	movs	r0, #2
  217f56:	f8ad 0043 	strh.w	r0, [sp, #67]	; 0x43
  217f5a:	f89d 0004 	ldrb.w	r0, [sp, #4]
  217f5e:	f88d 0045 	strb.w	r0, [sp, #69]	; 0x45
  217f62:	f89d 0005 	ldrb.w	r0, [sp, #5]
  217f66:	f88d 0046 	strb.w	r0, [sp, #70]	; 0x46
  217f6a:	f89d 0006 	ldrb.w	r0, [sp, #6]
  217f6e:	f88d 0047 	strb.w	r0, [sp, #71]	; 0x47
  217f72:	f89d 0007 	ldrb.w	r0, [sp, #7]
  217f76:	f88d 0048 	strb.w	r0, [sp, #72]	; 0x48
  217f7a:	f89d 0008 	ldrb.w	r0, [sp, #8]
  217f7e:	f88d 0049 	strb.w	r0, [sp, #73]	; 0x49
  217f82:	f89d 0009 	ldrb.w	r0, [sp, #9]
  217f86:	f88d 004a 	strb.w	r0, [sp, #74]	; 0x4a
  217f8a:	ab04      	add	r3, sp, #16
  217f8c:	aa05      	add	r2, sp, #20
  217f8e:	a906      	add	r1, sp, #24
  217f90:	a810      	add	r0, sp, #64	; 0x40
  217f92:	f651 f951 	bl	69238 <A589a6300>
  217f96:	f8ad b040 	strh.w	fp, [sp, #64]	; 0x40
  217f9a:	200b      	movs	r0, #11
  217f9c:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  217fa0:	f8b7 005c 	ldrh.w	r0, [r7, #92]	; 0x5c
  217fa4:	f8ad 0043 	strh.w	r0, [sp, #67]	; 0x43
  217fa8:	f8b6 0070 	ldrh.w	r0, [r6, #112]	; 0x70
  217fac:	f8ad 0045 	strh.w	r0, [sp, #69]	; 0x45
  217fb0:	465d      	mov	r5, fp
  217fb2:	ab04      	add	r3, sp, #16
  217fb4:	aa05      	add	r2, sp, #20
  217fb6:	a906      	add	r1, sp, #24
  217fb8:	a810      	add	r0, sp, #64	; 0x40
  217fba:	f651 f93d 	bl	69238 <A589a6300>
  217fbe:	f8d9 0010 	ldr.w	r0, [r9, #16]
  217fc2:	464e      	mov	r6, r9
  217fc4:	b3b8      	cbz	r0, 218036 <wiced_audio_sink_configure+0x274>
  217fc6:	f8ad 5040 	strh.w	r5, [sp, #64]	; 0x40
  217fca:	2145      	movs	r1, #69	; 0x45
  217fcc:	f88d 1042 	strb.w	r1, [sp, #66]	; 0x42
  217fd0:	6881      	ldr	r1, [r0, #8]
  217fd2:	f8cd 1043 	str.w	r1, [sp, #67]	; 0x43
  217fd6:	68c1      	ldr	r1, [r0, #12]
  217fd8:	f8cd 1047 	str.w	r1, [sp, #71]	; 0x47
  217fdc:	6901      	ldr	r1, [r0, #16]
  217fde:	f8cd 104b 	str.w	r1, [sp, #75]	; 0x4b
  217fe2:	6941      	ldr	r1, [r0, #20]
  217fe4:	f8cd 104f 	str.w	r1, [sp, #79]	; 0x4f
  217fe8:	6981      	ldr	r1, [r0, #24]
  217fea:	f8cd 1053 	str.w	r1, [sp, #83]	; 0x53
  217fee:	69c1      	ldr	r1, [r0, #28]
  217ff0:	f8cd 1057 	str.w	r1, [sp, #87]	; 0x57
  217ff4:	6a00      	ldr	r0, [r0, #32]
  217ff6:	f8cd 005b 	str.w	r0, [sp, #91]	; 0x5b
  217ffa:	ab04      	add	r3, sp, #16
  217ffc:	aa05      	add	r2, sp, #20
  217ffe:	a906      	add	r1, sp, #24
  218000:	a810      	add	r0, sp, #64	; 0x40
  218002:	f651 f919 	bl	69238 <A589a6300>
  218006:	f8ad 5040 	strh.w	r5, [sp, #64]	; 0x40
  21800a:	203e      	movs	r0, #62	; 0x3e
  21800c:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  218010:	6930      	ldr	r0, [r6, #16]
  218012:	ab04      	add	r3, sp, #16
  218014:	8801      	ldrh	r1, [r0, #0]
  218016:	f8ad 1043 	strh.w	r1, [sp, #67]	; 0x43
  21801a:	7881      	ldrb	r1, [r0, #2]
  21801c:	f88d 1045 	strb.w	r1, [sp, #69]	; 0x45
  218020:	78c1      	ldrb	r1, [r0, #3]
  218022:	f88d 1046 	strb.w	r1, [sp, #70]	; 0x46
  218026:	7900      	ldrb	r0, [r0, #4]
  218028:	f88d 0047 	strb.w	r0, [sp, #71]	; 0x47
  21802c:	aa05      	add	r2, sp, #20
  21802e:	a906      	add	r1, sp, #24
  218030:	a810      	add	r0, sp, #64	; 0x40
  218032:	f651 f901 	bl	69238 <A589a6300>
  218036:	981f      	ldr	r0, [sp, #124]	; 0x7c
  218038:	2720      	movs	r7, #32
  21803a:	f8df b370 	ldr.w	fp, [pc, #880]	; 2183ac <wiced_audio_set_i2s_out_sf+0x38>
  21803e:	f44f 1948 	mov.w	r9, #3276800	; 0x320000
  218042:	2800      	cmp	r0, #0
  218044:	d15e      	bne.n	218104 <wiced_audio_sink_configure+0x342>
  218046:	e000      	b.n	21804a <wiced_audio_sink_configure+0x288>
  218048:	e014      	b.n	218074 <wiced_audio_sink_configure+0x2b2>
  21804a:	f8ad 5040 	strh.w	r5, [sp, #64]	; 0x40
  21804e:	2017      	movs	r0, #23
  218050:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  218054:	9803      	ldr	r0, [sp, #12]
  218056:	f88d a043 	strb.w	sl, [sp, #67]	; 0x43
  21805a:	b2c0      	uxtb	r0, r0
  21805c:	f88d 0044 	strb.w	r0, [sp, #68]	; 0x44
  218060:	f88d 7045 	strb.w	r7, [sp, #69]	; 0x45
  218064:	f88d 0046 	strb.w	r0, [sp, #70]	; 0x46
  218068:	69b1      	ldr	r1, [r6, #24]
  21806a:	29ff      	cmp	r1, #255	; 0xff
  21806c:	d006      	beq.n	21807c <wiced_audio_sink_configure+0x2ba>
  21806e:	f88d 1047 	strb.w	r1, [sp, #71]	; 0x47
  218072:	e005      	b.n	218080 <wiced_audio_sink_configure+0x2be>
  218074:	2004      	movs	r0, #4
  218076:	b021      	add	sp, #132	; 0x84
  218078:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21807c:	f88d 0047 	strb.w	r0, [sp, #71]	; 0x47
  218080:	2006      	movs	r0, #6
  218082:	f88d 0048 	strb.w	r0, [sp, #72]	; 0x48
  218086:	ab04      	add	r3, sp, #16
  218088:	aa05      	add	r2, sp, #20
  21808a:	a906      	add	r1, sp, #24
  21808c:	a810      	add	r0, sp, #64	; 0x40
  21808e:	f651 f8d3 	bl	69238 <A589a6300>
  218092:	981e      	ldr	r0, [sp, #120]	; 0x78
  218094:	2800      	cmp	r0, #0
  218096:	d134      	bne.n	218102 <wiced_audio_sink_configure+0x340>
  218098:	49c5      	ldr	r1, [pc, #788]	; (2183b0 <wiced_audio_set_i2s_out_sf+0x3c>)
  21809a:	4620      	mov	r0, r4
  21809c:	700c      	strb	r4, [r1, #0]
  21809e:	f641 fc44 	bl	5992a <A69e93200>
  2180a2:	f109 0090 	add.w	r0, r9, #144	; 0x90
  2180a6:	6801      	ldr	r1, [r0, #0]
  2180a8:	ea01 010b 	and.w	r1, r1, fp
  2180ac:	6001      	str	r1, [r0, #0]
  2180ae:	6801      	ldr	r1, [r0, #0]
  2180b0:	4ac0      	ldr	r2, [pc, #768]	; (2183b4 <wiced_audio_set_i2s_out_sf+0x40>)
  2180b2:	ea41 0102 	orr.w	r1, r1, r2
  2180b6:	6001      	str	r1, [r0, #0]
  2180b8:	f8d9 11a8 	ldr.w	r1, [r9, #424]	; 0x1a8
  2180bc:	f421 417f 	bic.w	r1, r1, #65280	; 0xff00
  2180c0:	f8c9 11a8 	str.w	r1, [r9, #424]	; 0x1a8
  2180c4:	f8d9 11a8 	ldr.w	r1, [r9, #424]	; 0x1a8
  2180c8:	f441 41ee 	orr.w	r1, r1, #30464	; 0x7700
  2180cc:	f8c9 11a8 	str.w	r1, [r9, #424]	; 0x1a8
  2180d0:	f8d9 1110 	ldr.w	r1, [r9, #272]	; 0x110
  2180d4:	b289      	uxth	r1, r1
  2180d6:	f8c9 1110 	str.w	r1, [r9, #272]	; 0x110
  2180da:	f8d0 1080 	ldr.w	r1, [r0, #128]	; 0x80
  2180de:	4ab6      	ldr	r2, [pc, #728]	; (2183b8 <wiced_audio_set_i2s_out_sf+0x44>)
  2180e0:	ea41 0102 	orr.w	r1, r1, r2
  2180e4:	f8c0 1080 	str.w	r1, [r0, #128]	; 0x80
  2180e8:	6d41      	ldr	r1, [r0, #84]	; 0x54
  2180ea:	f021 010f 	bic.w	r1, r1, #15
  2180ee:	6541      	str	r1, [r0, #84]	; 0x54
  2180f0:	6d41      	ldr	r1, [r0, #84]	; 0x54
  2180f2:	f041 010c 	orr.w	r1, r1, #12
  2180f6:	6541      	str	r1, [r0, #84]	; 0x54
  2180f8:	48b0      	ldr	r0, [pc, #704]	; (2183bc <wiced_audio_set_i2s_out_sf+0x48>)
  2180fa:	6c01      	ldr	r1, [r0, #64]	; 0x40
  2180fc:	f041 0104 	orr.w	r1, r1, #4
  218100:	6401      	str	r1, [r0, #64]	; 0x40
  218102:	e05c      	b.n	2181be <wiced_audio_sink_configure+0x3fc>
  218104:	f65a f8ec 	bl	722e0 <Ad8303800>
  218108:	9000      	str	r0, [sp, #0]
  21810a:	f669 ffb4 	bl	82076 <Aff73a800>
  21810e:	4eac      	ldr	r6, [pc, #688]	; (2183c0 <wiced_audio_set_i2s_out_sf+0x4c>)
  218110:	4601      	mov	r1, r0
  218112:	f886 a012 	strb.w	sl, [r6, #18]
  218116:	74f7      	strb	r7, [r6, #19]
  218118:	f806 89da 	strb.w	r8, [r6], #-218
  21811c:	f109 00e4 	add.w	r0, r9, #228	; 0xe4
  218120:	6802      	ldr	r2, [r0, #0]
  218122:	f022 1207 	bic.w	r2, r2, #458759	; 0x70007
  218126:	6002      	str	r2, [r0, #0]
  218128:	6802      	ldr	r2, [r0, #0]
  21812a:	f042 1201 	orr.w	r2, r2, #65537	; 0x10001
  21812e:	6002      	str	r2, [r0, #0]
  218130:	6ac2      	ldr	r2, [r0, #44]	; 0x2c
  218132:	4ba4      	ldr	r3, [pc, #656]	; (2183c4 <wiced_audio_set_i2s_out_sf+0x50>)
  218134:	ea02 0203 	and.w	r2, r2, r3
  218138:	62c2      	str	r2, [r0, #44]	; 0x2c
  21813a:	6ac2      	ldr	r2, [r0, #44]	; 0x2c
  21813c:	4ba2      	ldr	r3, [pc, #648]	; (2183c8 <wiced_audio_set_i2s_out_sf+0x54>)
  21813e:	ea42 0203 	orr.w	r2, r2, r3
  218142:	62c2      	str	r2, [r0, #44]	; 0x2c
  218144:	f8d9 2090 	ldr.w	r2, [r9, #144]	; 0x90
  218148:	ea02 020b 	and.w	r2, r2, fp
  21814c:	f840 2c54 	str.w	r2, [r0, #-84]
  218150:	f850 2fc4 	ldr.w	r2, [r0, #196]!
  218154:	f422 427f 	bic.w	r2, r2, #65280	; 0xff00
  218158:	6002      	str	r2, [r0, #0]
  21815a:	4f9c      	ldr	r7, [pc, #624]	; (2183cc <wiced_audio_set_i2s_out_sf+0x58>)
  21815c:	f021 0203 	bic.w	r2, r1, #3
  218160:	6838      	ldr	r0, [r7, #0]
  218162:	f102 0240 	add.w	r2, r2, #64	; 0x40
  218166:	eb00 0302 	add.w	r3, r0, r2
  21816a:	9a00      	ldr	r2, [sp, #0]
  21816c:	4293      	cmp	r3, r2
  21816e:	d839      	bhi.n	2181e4 <wiced_audio_sink_configure+0x422>
  218170:	3140      	adds	r1, #64	; 0x40
  218172:	4408      	add	r0, r1
  218174:	f65a f8ad 	bl	722d2 <A19158e00>
  218178:	f8c6 0100 	str.w	r0, [r6, #256]	; 0x100
  21817c:	b388      	cbz	r0, 2181e2 <wiced_audio_sink_configure+0x420>
  21817e:	f896 00da 	ldrb.w	r0, [r6, #218]	; 0xda
  218182:	6879      	ldr	r1, [r7, #4]
  218184:	2801      	cmp	r0, #1
  218186:	d000      	beq.n	21818a <wiced_audio_sink_configure+0x3c8>
  218188:	6939      	ldr	r1, [r7, #16]
  21818a:	68ba      	ldr	r2, [r7, #8]
  21818c:	b289      	uxth	r1, r1
  21818e:	d000      	beq.n	218192 <wiced_audio_sink_configure+0x3d0>
  218190:	697a      	ldr	r2, [r7, #20]
  218192:	68fb      	ldr	r3, [r7, #12]
  218194:	b292      	uxth	r2, r2
  218196:	d000      	beq.n	21819a <wiced_audio_sink_configure+0x3d8>
  218198:	69bb      	ldr	r3, [r7, #24]
  21819a:	b29b      	uxth	r3, r3
  21819c:	f651 fd4d 	bl	69c3a <Aa862c200>
  2181a0:	2101      	movs	r1, #1
  2181a2:	2007      	movs	r0, #7
  2181a4:	f659 ff9e 	bl	720e4 <Ac60f7400>
  2181a8:	4889      	ldr	r0, [pc, #548]	; (2183d0 <wiced_audio_set_i2s_out_sf+0x5c>)
  2181aa:	9903      	ldr	r1, [sp, #12]
  2181ac:	f8b6 20a8 	ldrh.w	r2, [r6, #168]	; 0xa8
  2181b0:	f850 1021 	ldr.w	r1, [r0, r1, lsl #2]
  2181b4:	4887      	ldr	r0, [pc, #540]	; (2183d4 <wiced_audio_set_i2s_out_sf+0x60>)
  2181b6:	1e52      	subs	r2, r2, #1
  2181b8:	6800      	ldr	r0, [r0, #0]
  2181ba:	f64f f9bf 	bl	6753c <Ad6985e00>
  2181be:	f8ad 5040 	strh.w	r5, [sp, #64]	; 0x40
  2181c2:	2042      	movs	r0, #66	; 0x42
  2181c4:	f88d 0042 	strb.w	r0, [sp, #66]	; 0x42
  2181c8:	f88d 4043 	strb.w	r4, [sp, #67]	; 0x43
  2181cc:	ab04      	add	r3, sp, #16
  2181ce:	aa05      	add	r2, sp, #20
  2181d0:	a906      	add	r1, sp, #24
  2181d2:	a810      	add	r0, sp, #64	; 0x40
  2181d4:	f651 f830 	bl	69238 <A589a6300>
  2181d8:	497f      	ldr	r1, [pc, #508]	; (2183d8 <wiced_audio_set_i2s_out_sf+0x64>)
  2181da:	2000      	movs	r0, #0
  2181dc:	f8c1 8000 	str.w	r8, [r1]
  2181e0:	e749      	b.n	218076 <wiced_audio_sink_configure+0x2b4>
  2181e2:	e7ff      	b.n	2181e4 <wiced_audio_sink_configure+0x422>
  2181e4:	2008      	movs	r0, #8
  2181e6:	e746      	b.n	218076 <wiced_audio_sink_configure+0x2b4>

002181e8 <wiced_audio_sink_reset>:
  2181e8:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  2181ec:	4d7b      	ldr	r5, [pc, #492]	; (2183dc <wiced_audio_set_i2s_out_sf+0x68>)
  2181ee:	b099      	sub	sp, #100	; 0x64
  2181f0:	69e8      	ldr	r0, [r5, #28]
  2181f2:	2400      	movs	r4, #0
  2181f4:	b140      	cbz	r0, 218208 <wiced_audio_sink_reset+0x20>
  2181f6:	7829      	ldrb	r1, [r5, #0]
  2181f8:	2904      	cmp	r1, #4
  2181fa:	d102      	bne.n	218202 <wiced_audio_sink_reset+0x1a>
  2181fc:	f6c4 fc02 	bl	dca04 <wiced_transport_free_buffer>
  218200:	e001      	b.n	218206 <wiced_audio_sink_reset+0x1e>
  218202:	f64d fbff 	bl	65a04 <A64fb9d00>
  218206:	61ec      	str	r4, [r5, #28]
  218208:	616c      	str	r4, [r5, #20]
  21820a:	7828      	ldrb	r0, [r5, #0]
  21820c:	f8df 81b0 	ldr.w	r8, [pc, #432]	; 2183c0 <wiced_audio_set_i2s_out_sf+0x4c>
  218210:	2780      	movs	r7, #128	; 0x80
  218212:	f1a8 08da 	sub.w	r8, r8, #218	; 0xda
  218216:	f04f 09ff 	mov.w	r9, #255	; 0xff
  21821a:	f04f 0b03 	mov.w	fp, #3
  21821e:	2804      	cmp	r0, #4
  218220:	d001      	beq.n	218226 <wiced_audio_sink_reset+0x3e>
  218222:	2805      	cmp	r0, #5
  218224:	d107      	bne.n	218236 <wiced_audio_sink_reset+0x4e>
  218226:	f108 00ec 	add.w	r0, r8, #236	; 0xec
  21822a:	7007      	strb	r7, [r0, #0]
  21822c:	7044      	strb	r4, [r0, #1]
  21822e:	80ec      	strh	r4, [r5, #6]
  218230:	f885 9000 	strb.w	r9, [r5]
  218234:	e07e      	b.n	218334 <wiced_audio_sink_reset+0x14c>
  218236:	f249 0605 	movw	r6, #36869	; 0x9005
  21823a:	f8ad 6030 	strh.w	r6, [sp, #48]	; 0x30
  21823e:	200d      	movs	r0, #13
  218240:	f88d 0032 	strb.w	r0, [sp, #50]	; 0x32
  218244:	88e8      	ldrh	r0, [r5, #6]
  218246:	f8ad 0033 	strh.w	r0, [sp, #51]	; 0x33
  21824a:	466b      	mov	r3, sp
  21824c:	aa01      	add	r2, sp, #4
  21824e:	a902      	add	r1, sp, #8
  218250:	a80c      	add	r0, sp, #48	; 0x30
  218252:	f650 fff1 	bl	69238 <A589a6300>
  218256:	1eb0      	subs	r0, r6, #2
  218258:	f8ad 0030 	strh.w	r0, [sp, #48]	; 0x30
  21825c:	f04f 0a02 	mov.w	sl, #2
  218260:	f88d a032 	strb.w	sl, [sp, #50]	; 0x32
  218264:	f8ad 4033 	strh.w	r4, [sp, #51]	; 0x33
  218268:	2001      	movs	r0, #1
  21826a:	f88d 0035 	strb.w	r0, [sp, #53]	; 0x35
  21826e:	88e8      	ldrh	r0, [r5, #6]
  218270:	f8ad 0036 	strh.w	r0, [sp, #54]	; 0x36
  218274:	466b      	mov	r3, sp
  218276:	aa01      	add	r2, sp, #4
  218278:	a902      	add	r1, sp, #8
  21827a:	a80c      	add	r0, sp, #48	; 0x30
  21827c:	f650 ffdc 	bl	69238 <A589a6300>
  218280:	f8ad 6030 	strh.w	r6, [sp, #48]	; 0x30
  218284:	200f      	movs	r0, #15
  218286:	f88d 0032 	strb.w	r0, [sp, #50]	; 0x32
  21828a:	88e8      	ldrh	r0, [r5, #6]
  21828c:	f8ad 0033 	strh.w	r0, [sp, #51]	; 0x33
  218290:	f8b8 00b2 	ldrh.w	r0, [r8, #178]	; 0xb2
  218294:	f8ad 0035 	strh.w	r0, [sp, #53]	; 0x35
  218298:	466b      	mov	r3, sp
  21829a:	aa01      	add	r2, sp, #4
  21829c:	a902      	add	r1, sp, #8
  21829e:	a80c      	add	r0, sp, #48	; 0x30
  2182a0:	f650 ffca 	bl	69238 <A589a6300>
  2182a4:	f8ad 6030 	strh.w	r6, [sp, #48]	; 0x30
  2182a8:	2017      	movs	r0, #23
  2182aa:	f88d 0032 	strb.w	r0, [sp, #50]	; 0x32
  2182ae:	f88d 9034 	strb.w	r9, [sp, #52]	; 0x34
  2182b2:	f88d 4035 	strb.w	r4, [sp, #53]	; 0x35
  2182b6:	2006      	movs	r0, #6
  2182b8:	f88d 9036 	strb.w	r9, [sp, #54]	; 0x36
  2182bc:	f88d 0038 	strb.w	r0, [sp, #56]	; 0x38
  2182c0:	f88d 7033 	strb.w	r7, [sp, #51]	; 0x33
  2182c4:	f88d 9037 	strb.w	r9, [sp, #55]	; 0x37
  2182c8:	466b      	mov	r3, sp
  2182ca:	aa01      	add	r2, sp, #4
  2182cc:	a902      	add	r1, sp, #8
  2182ce:	a80c      	add	r0, sp, #48	; 0x30
  2182d0:	f650 ffb2 	bl	69238 <A589a6300>
  2182d4:	1cf0      	adds	r0, r6, #3
  2182d6:	f8ad 0030 	strh.w	r0, [sp, #48]	; 0x30
  2182da:	f88d a032 	strb.w	sl, [sp, #50]	; 0x32
  2182de:	466b      	mov	r3, sp
  2182e0:	aa01      	add	r2, sp, #4
  2182e2:	a902      	add	r1, sp, #8
  2182e4:	a80c      	add	r0, sp, #48	; 0x30
  2182e6:	f650 ffa7 	bl	69238 <A589a6300>
  2182ea:	80ec      	strh	r4, [r5, #6]
  2182ec:	2205      	movs	r2, #5
  2182ee:	2100      	movs	r1, #0
  2182f0:	483b      	ldr	r0, [pc, #236]	; (2183e0 <wiced_audio_set_i2s_out_sf+0x6c>)
  2182f2:	f656 fd83 	bl	6edfc <mpaf_memset>
  2182f6:	483b      	ldr	r0, [pc, #236]	; (2183e4 <wiced_audio_set_i2s_out_sf+0x70>)
  2182f8:	6801      	ldr	r1, [r0, #0]
  2182fa:	f021 1107 	bic.w	r1, r1, #458759	; 0x70007
  2182fe:	6001      	str	r1, [r0, #0]
  218300:	6801      	ldr	r1, [r0, #0]
  218302:	f041 1101 	orr.w	r1, r1, #65537	; 0x10001
  218306:	6001      	str	r1, [r0, #0]
  218308:	6ac1      	ldr	r1, [r0, #44]	; 0x2c
  21830a:	4a2e      	ldr	r2, [pc, #184]	; (2183c4 <wiced_audio_set_i2s_out_sf+0x50>)
  21830c:	ea01 0102 	and.w	r1, r1, r2
  218310:	62c1      	str	r1, [r0, #44]	; 0x2c
  218312:	6ac1      	ldr	r1, [r0, #44]	; 0x2c
  218314:	4a2c      	ldr	r2, [pc, #176]	; (2183c8 <wiced_audio_set_i2s_out_sf+0x54>)
  218316:	ea41 0102 	orr.w	r1, r1, r2
  21831a:	62c1      	str	r1, [r0, #44]	; 0x2c
  21831c:	f850 1c54 	ldr.w	r1, [r0, #-84]
  218320:	4a22      	ldr	r2, [pc, #136]	; (2183ac <wiced_audio_set_i2s_out_sf+0x38>)
  218322:	ea01 0102 	and.w	r1, r1, r2
  218326:	f840 1c54 	str.w	r1, [r0, #-84]
  21832a:	f850 1fc4 	ldr.w	r1, [r0, #196]!
  21832e:	f421 417f 	bic.w	r1, r1, #65280	; 0xff00
  218332:	6001      	str	r1, [r0, #0]
  218334:	4928      	ldr	r1, [pc, #160]	; (2183d8 <wiced_audio_set_i2s_out_sf+0x64>)
  218336:	f04f 0000 	mov.w	r0, #0
  21833a:	f8c1 b000 	str.w	fp, [r1]
  21833e:	b019      	add	sp, #100	; 0x64
  218340:	e69a      	b.n	218078 <wiced_audio_sink_configure+0x2b6>

00218342 <wiced_audio_sink_mute>:
  218342:	b500      	push	{lr}
  218344:	b099      	sub	sp, #100	; 0x64
  218346:	f249 0205 	movw	r2, #36869	; 0x9005
  21834a:	f8ad 2030 	strh.w	r2, [sp, #48]	; 0x30
  21834e:	2247      	movs	r2, #71	; 0x47
  218350:	f88d 2032 	strb.w	r2, [sp, #50]	; 0x32
  218354:	f88d 0033 	strb.w	r0, [sp, #51]	; 0x33
  218358:	f8ad 1034 	strh.w	r1, [sp, #52]	; 0x34
  21835c:	466b      	mov	r3, sp
  21835e:	aa01      	add	r2, sp, #4
  218360:	a902      	add	r1, sp, #8
  218362:	a80c      	add	r0, sp, #48	; 0x30
  218364:	f650 ff68 	bl	69238 <A589a6300>
  218368:	2000      	movs	r0, #0
  21836a:	b019      	add	sp, #100	; 0x64
  21836c:	bd00      	pop	{pc}

0021836e <wiced_audio_sink_register_data_cback>:
  21836e:	491b      	ldr	r1, [pc, #108]	; (2183dc <wiced_audio_set_i2s_out_sf+0x68>)
  218370:	6208      	str	r0, [r1, #32]
  218372:	4770      	bx	lr

00218374 <wiced_audio_set_i2s_out_sf>:
  218374:	4601      	mov	r1, r0
  218376:	2001      	movs	r0, #1
  218378:	4a18      	ldr	r2, [pc, #96]	; (2183dc <wiced_audio_set_i2s_out_sf+0x68>)
  21837a:	f5b1 5f7a 	cmp.w	r1, #16000	; 0x3e80
  21837e:	d00d      	beq.n	21839c <wiced_audio_set_i2s_out_sf+0x28>
  218380:	f5b1 4ffa 	cmp.w	r1, #32000	; 0x7d00
  218384:	d00c      	beq.n	2183a0 <wiced_audio_set_i2s_out_sf+0x2c>
  218386:	f5a1 412c 	sub.w	r1, r1, #44032	; 0xac00
  21838a:	3944      	subs	r1, #68	; 0x44
  21838c:	d00a      	beq.n	2183a4 <wiced_audio_set_i2s_out_sf+0x30>
  21838e:	f5a1 6170 	sub.w	r1, r1, #3840	; 0xf00
  218392:	393c      	subs	r1, #60	; 0x3c
  218394:	d108      	bne.n	2183a8 <wiced_audio_set_i2s_out_sf+0x34>
  218396:	2104      	movs	r1, #4
  218398:	6191      	str	r1, [r2, #24]
  21839a:	4770      	bx	lr
  21839c:	2101      	movs	r1, #1
  21839e:	e7fb      	b.n	218398 <wiced_audio_set_i2s_out_sf+0x24>
  2183a0:	2102      	movs	r1, #2
  2183a2:	e7f9      	b.n	218398 <wiced_audio_set_i2s_out_sf+0x24>
  2183a4:	2103      	movs	r1, #3
  2183a6:	e7f7      	b.n	218398 <wiced_audio_set_i2s_out_sf+0x24>
  2183a8:	2000      	movs	r0, #0
  2183aa:	4770      	bx	lr
  2183ac:	f0f0ffff 	.word	0xf0f0ffff
  2183b0:	00201bed 	.word	0x00201bed
  2183b4:	04040000 	.word	0x04040000
  2183b8:	b9b90000 	.word	0xb9b90000
  2183bc:	00327000 	.word	0x00327000
  2183c0:	0020b74a 	.word	0x0020b74a
  2183c4:	f8f8ffff 	.word	0xf8f8ffff
  2183c8:	01010000 	.word	0x01010000
  2183cc:	00201d30 	.word	0x00201d30
  2183d0:	00201cf0 	.word	0x00201cf0
  2183d4:	00201c38 	.word	0x00201c38
  2183d8:	00201bf8 	.word	0x00201bf8
  2183dc:	0021fa80 	.word	0x0021fa80
  2183e0:	0021fac0 	.word	0x0021fac0
  2183e4:	003200e4 	.word	0x003200e4

002183e8 <_lite_host_proc_route_config_req>:
  2183e8:	e92d 4ff3 	stmdb	sp!, {r0, r1, r4, r5, r6, r7, r8, r9, sl, fp, lr}
  2183ec:	4604      	mov	r4, r0
  2183ee:	2500      	movs	r5, #0
  2183f0:	7840      	ldrb	r0, [r0, #1]
  2183f2:	b087      	sub	sp, #28
  2183f4:	2701      	movs	r7, #1
  2183f6:	46ab      	mov	fp, r5
  2183f8:	4eff      	ldr	r6, [pc, #1020]	; (2187f8 <_lite_host_proc_route_config_req+0x410>)
  2183fa:	2880      	cmp	r0, #128	; 0x80
  2183fc:	d10b      	bne.n	218416 <_lite_host_proc_route_config_req+0x2e>
  2183fe:	f669 fce7 	bl	81dd0 <Aff76bc00>
  218402:	f669 fc7d 	bl	81d00 <A5c2d6c80>
  218406:	2080      	movs	r0, #128	; 0x80
  218408:	f806 0fec 	strb.w	r0, [r6, #236]!
  21840c:	f886 b001 	strb.w	fp, [r6, #1]
  218410:	f886 b002 	strb.w	fp, [r6, #2]
  218414:	e011      	b.n	21843a <_lite_host_proc_route_config_req+0x52>
  218416:	78e2      	ldrb	r2, [r4, #3]
  218418:	b98a      	cbnz	r2, 21843e <_lite_host_proc_route_config_req+0x56>
  21841a:	48f8      	ldr	r0, [pc, #992]	; (2187fc <_lite_host_proc_route_config_req+0x414>)
  21841c:	f8d0 11cc 	ldr.w	r1, [r0, #460]	; 0x1cc
  218420:	b109      	cbz	r1, 218426 <_lite_host_proc_route_config_req+0x3e>
  218422:	f8c0 b1cc 	str.w	fp, [r0, #460]	; 0x1cc
  218426:	2000      	movs	r0, #0
  218428:	f44f 61fa 	mov.w	r1, #2000	; 0x7d0
  21842c:	1c40      	adds	r0, r0, #1
  21842e:	4288      	cmp	r0, r1
  218430:	d3fc      	bcc.n	21842c <_lite_host_proc_route_config_req+0x44>
  218432:	f669 fccd 	bl	81dd0 <Aff76bc00>
  218436:	f886 b0ed 	strb.w	fp, [r6, #237]	; 0xed
  21843a:	2700      	movs	r7, #0
  21843c:	e1ce      	b.n	2187dc <_lite_host_proc_route_config_req+0x3f4>
  21843e:	f896 10ec 	ldrb.w	r1, [r6, #236]	; 0xec
  218442:	2980      	cmp	r1, #128	; 0x80
  218444:	d001      	beq.n	21844a <_lite_host_proc_route_config_req+0x62>
  218446:	4281      	cmp	r1, r0
  218448:	d1f8      	bne.n	21843c <_lite_host_proc_route_config_req+0x54>
  21844a:	46b1      	mov	r9, r6
  21844c:	f896 10ee 	ldrb.w	r1, [r6, #238]	; 0xee
  218450:	f669 fd3f 	bl	81ed2 <Ae2e25400>
  218454:	2800      	cmp	r0, #0
  218456:	d0f1      	beq.n	21843c <_lite_host_proc_route_config_req+0x54>
  218458:	48e9      	ldr	r0, [pc, #932]	; (218800 <_lite_host_proc_route_config_req+0x418>)
  21845a:	7800      	ldrb	r0, [r0, #0]
  21845c:	b108      	cbz	r0, 218462 <_lite_host_proc_route_config_req+0x7a>
  21845e:	f669 f918 	bl	81692 <A9c42a400>
  218462:	7860      	ldrb	r0, [r4, #1]
  218464:	4ee7      	ldr	r6, [pc, #924]	; (218804 <_lite_host_proc_route_config_req+0x41c>)
  218466:	2803      	cmp	r0, #3
  218468:	d109      	bne.n	21847e <_lite_host_proc_route_config_req+0x96>
  21846a:	78e0      	ldrb	r0, [r4, #3]
  21846c:	2512      	movs	r5, #18
  21846e:	07c0      	lsls	r0, r0, #31
  218470:	d108      	bne.n	218484 <_lite_host_proc_route_config_req+0x9c>
  218472:	8870      	ldrh	r0, [r6, #2]
  218474:	3828      	subs	r0, #40	; 0x28
  218476:	f8a9 00b2 	strh.w	r0, [r9, #178]	; 0xb2
  21847a:	f669 fa3d 	bl	818f8 <A29c52500>
  21847e:	78e0      	ldrb	r0, [r4, #3]
  218480:	07c0      	lsls	r0, r0, #31
  218482:	d001      	beq.n	218488 <_lite_host_proc_route_config_req+0xa0>
  218484:	f045 050b 	orr.w	r5, r5, #11
  218488:	78e0      	ldrb	r0, [r4, #3]
  21848a:	0701      	lsls	r1, r0, #28
  21848c:	d509      	bpl.n	2184a2 <_lite_host_proc_route_config_req+0xba>
  21848e:	f045 0512 	orr.w	r5, r5, #18
  218492:	07c0      	lsls	r0, r0, #31
  218494:	d105      	bne.n	2184a2 <_lite_host_proc_route_config_req+0xba>
  218496:	88b0      	ldrh	r0, [r6, #4]
  218498:	3828      	subs	r0, #40	; 0x28
  21849a:	f8a9 00b2 	strh.w	r0, [r9, #178]	; 0xb2
  21849e:	f669 fa2b 	bl	818f8 <A29c52500>
  2184a2:	7860      	ldrb	r0, [r4, #1]
  2184a4:	2805      	cmp	r0, #5
  2184a6:	d102      	bne.n	2184ae <_lite_host_proc_route_config_req+0xc6>
  2184a8:	f045 0522 	orr.w	r5, r5, #34	; 0x22
  2184ac:	e006      	b.n	2184bc <_lite_host_proc_route_config_req+0xd4>
  2184ae:	2801      	cmp	r0, #1
  2184b0:	d104      	bne.n	2184bc <_lite_host_proc_route_config_req+0xd4>
  2184b2:	78e0      	ldrb	r0, [r4, #3]
  2184b4:	0780      	lsls	r0, r0, #30
  2184b6:	d501      	bpl.n	2184bc <_lite_host_proc_route_config_req+0xd4>
  2184b8:	78a0      	ldrb	r0, [r4, #2]
  2184ba:	7120      	strb	r0, [r4, #4]
  2184bc:	07e8      	lsls	r0, r5, #31
  2184be:	d009      	beq.n	2184d4 <_lite_host_proc_route_config_req+0xec>
  2184c0:	f899 00b1 	ldrb.w	r0, [r9, #177]	; 0xb1
  2184c4:	4649      	mov	r1, r9
  2184c6:	b928      	cbnz	r0, 2184d4 <_lite_host_proc_route_config_req+0xec>
  2184c8:	4acf      	ldr	r2, [pc, #828]	; (218808 <_lite_host_proc_route_config_req+0x420>)
  2184ca:	6810      	ldr	r0, [r2, #0]
  2184cc:	f881 00b1 	strb.w	r0, [r1, #177]	; 0xb1
  2184d0:	2001      	movs	r0, #1
  2184d2:	6010      	str	r0, [r2, #0]
  2184d4:	07a8      	lsls	r0, r5, #30
  2184d6:	d507      	bpl.n	2184e8 <_lite_host_proc_route_config_req+0x100>
  2184d8:	f899 00da 	ldrb.w	r0, [r9, #218]	; 0xda
  2184dc:	280c      	cmp	r0, #12
  2184de:	d003      	beq.n	2184e8 <_lite_host_proc_route_config_req+0x100>
  2184e0:	2101      	movs	r1, #1
  2184e2:	2007      	movs	r0, #7
  2184e4:	f659 fdfe 	bl	720e4 <Ac60f7400>
  2184e8:	0728      	lsls	r0, r5, #28
  2184ea:	d50d      	bpl.n	218508 <_lite_host_proc_route_config_req+0x120>
  2184ec:	f8d9 00cc 	ldr.w	r0, [r9, #204]	; 0xcc
  2184f0:	464e      	mov	r6, r9
  2184f2:	b948      	cbnz	r0, 218508 <_lite_host_proc_route_config_req+0x120>
  2184f4:	f640 304c 	movw	r0, #2892	; 0xb4c
  2184f8:	f659 feeb 	bl	722d2 <A19158e00>
  2184fc:	f8c6 00cc 	str.w	r0, [r6, #204]	; 0xcc
  218500:	2800      	cmp	r0, #0
  218502:	d05f      	beq.n	2185c4 <_lite_host_proc_route_config_req+0x1dc>
  218504:	f650 ff2c 	bl	69360 <Ab2b90c00>
  218508:	06a8      	lsls	r0, r5, #26
  21850a:	d529      	bpl.n	218560 <_lite_host_proc_route_config_req+0x178>
  21850c:	f8d9 0100 	ldr.w	r0, [r9, #256]	; 0x100
  218510:	464e      	mov	r6, r9
  218512:	bb28      	cbnz	r0, 218560 <_lite_host_proc_route_config_req+0x178>
  218514:	f659 fee4 	bl	722e0 <Ad8303800>
  218518:	4680      	mov	r8, r0
  21851a:	f669 fdac 	bl	82076 <Aff73a800>
  21851e:	4fbb      	ldr	r7, [pc, #748]	; (21880c <_lite_host_proc_route_config_req+0x424>)
  218520:	f020 0203 	bic.w	r2, r0, #3
  218524:	6839      	ldr	r1, [r7, #0]
  218526:	3240      	adds	r2, #64	; 0x40
  218528:	440a      	add	r2, r1
  21852a:	4542      	cmp	r2, r8
  21852c:	d870      	bhi.n	218610 <_lite_host_proc_route_config_req+0x228>
  21852e:	3040      	adds	r0, #64	; 0x40
  218530:	4408      	add	r0, r1
  218532:	f659 fece 	bl	722d2 <A19158e00>
  218536:	f8c6 0100 	str.w	r0, [r6, #256]	; 0x100
  21853a:	2800      	cmp	r0, #0
  21853c:	d068      	beq.n	218610 <_lite_host_proc_route_config_req+0x228>
  21853e:	f896 00da 	ldrb.w	r0, [r6, #218]	; 0xda
  218542:	6879      	ldr	r1, [r7, #4]
  218544:	2801      	cmp	r0, #1
  218546:	d000      	beq.n	21854a <_lite_host_proc_route_config_req+0x162>
  218548:	6939      	ldr	r1, [r7, #16]
  21854a:	68ba      	ldr	r2, [r7, #8]
  21854c:	b289      	uxth	r1, r1
  21854e:	d000      	beq.n	218552 <_lite_host_proc_route_config_req+0x16a>
  218550:	697a      	ldr	r2, [r7, #20]
  218552:	68fb      	ldr	r3, [r7, #12]
  218554:	b292      	uxth	r2, r2
  218556:	d000      	beq.n	21855a <_lite_host_proc_route_config_req+0x172>
  218558:	69bb      	ldr	r3, [r7, #24]
  21855a:	b29b      	uxth	r3, r3
  21855c:	f651 fb6d 	bl	69c3a <Aa862c200>
  218560:	06e8      	lsls	r0, r5, #27
  218562:	4648      	mov	r0, r9
  218564:	d52f      	bpl.n	2185c6 <_lite_host_proc_route_config_req+0x1de>
  218566:	f8d0 00d0 	ldr.w	r0, [r0, #208]	; 0xd0
  21856a:	2800      	cmp	r0, #0
  21856c:	d150      	bne.n	218610 <_lite_host_proc_route_config_req+0x228>
  21856e:	f659 febd 	bl	722ec <Aed121a00>
  218572:	4607      	mov	r7, r0
  218574:	f669 fd7f 	bl	82076 <Aff73a800>
  218578:	eb00 0180 	add.w	r1, r0, r0, lsl #2
  21857c:	3124      	adds	r1, #36	; 0x24
  21857e:	4606      	mov	r6, r0
  218580:	42b9      	cmp	r1, r7
  218582:	d245      	bcs.n	218610 <_lite_host_proc_route_config_req+0x228>
  218584:	f027 0a03 	bic.w	sl, r7, #3
  218588:	4650      	mov	r0, sl
  21858a:	f659 fed0 	bl	7232e <A903df000>
  21858e:	464f      	mov	r7, r9
  218590:	f8c9 00d0 	str.w	r0, [r9, #208]	; 0xd0
  218594:	b1b0      	cbz	r0, 2185c4 <_lite_host_proc_route_config_req+0x1dc>
  218596:	489e      	ldr	r0, [pc, #632]	; (218810 <_lite_host_proc_route_config_req+0x428>)
  218598:	f107 07d0 	add.w	r7, r7, #208	; 0xd0
  21859c:	6800      	ldr	r0, [r0, #0]
  21859e:	00c0      	lsls	r0, r0, #3
  2185a0:	210a      	movs	r1, #10
  2185a2:	fb90 f8f1 	sdiv	r8, r0, r1
  2185a6:	7ab8      	ldrb	r0, [r7, #10]
  2185a8:	499a      	ldr	r1, [pc, #616]	; (218814 <_lite_host_proc_route_config_req+0x42c>)
  2185aa:	eb01 1040 	add.w	r0, r1, r0, lsl #5
  2185ae:	6940      	ldr	r0, [r0, #20]
  2185b0:	4780      	blx	r0
  2185b2:	683b      	ldr	r3, [r7, #0]
  2185b4:	4601      	mov	r1, r0
  2185b6:	e9cd a300 	strd	sl, r3, [sp]
  2185ba:	7aba      	ldrb	r2, [r7, #10]
  2185bc:	b2b3      	uxth	r3, r6
  2185be:	4640      	mov	r0, r8
  2185c0:	f67d fef9 	bl	963b6 <Aa3bca000>
  2185c4:	e024      	b.n	218610 <_lite_host_proc_route_config_req+0x228>
  2185c6:	f8d0 10d0 	ldr.w	r1, [r0, #208]	; 0xd0
  2185ca:	b309      	cbz	r1, 218610 <_lite_host_proc_route_config_req+0x228>
  2185cc:	f890 10ed 	ldrb.w	r1, [r0, #237]	; 0xed
  2185d0:	0709      	lsls	r1, r1, #28
  2185d2:	d51b      	bpl.n	21860c <_lite_host_proc_route_config_req+0x224>
  2185d4:	f100 00d6 	add.w	r0, r0, #214	; 0xd6
  2185d8:	8842      	ldrh	r2, [r0, #2]
  2185da:	8801      	ldrh	r1, [r0, #0]
  2185dc:	428a      	cmp	r2, r1
  2185de:	d90b      	bls.n	2185f8 <_lite_host_proc_route_config_req+0x210>
  2185e0:	488d      	ldr	r0, [pc, #564]	; (218818 <_lite_host_proc_route_config_req+0x430>)
  2185e2:	1a51      	subs	r1, r2, r1
  2185e4:	6800      	ldr	r0, [r0, #0]
  2185e6:	b28e      	uxth	r6, r1
  2185e8:	6880      	ldr	r0, [r0, #8]
  2185ea:	4631      	mov	r1, r6
  2185ec:	f64e f9bc 	bl	66968 <__aeabi_memclr>
  2185f0:	a906      	add	r1, sp, #24
  2185f2:	4630      	mov	r0, r6
  2185f4:	f67d ffba 	bl	9656c <A81370ce0>
  2185f8:	4888      	ldr	r0, [pc, #544]	; (21881c <_lite_host_proc_route_config_req+0x434>)
  2185fa:	f880 b000 	strb.w	fp, [r0]
  2185fe:	4888      	ldr	r0, [pc, #544]	; (218820 <_lite_host_proc_route_config_req+0x438>)
  218600:	68c0      	ldr	r0, [r0, #12]
  218602:	b128      	cbz	r0, 218610 <_lite_host_proc_route_config_req+0x228>
  218604:	2100      	movs	r1, #0
  218606:	f610 f8d5 	bl	287b4 <Aa9429100>
  21860a:	e001      	b.n	218610 <_lite_host_proc_route_config_req+0x228>
  21860c:	f669 fb88 	bl	81d20 <A8b530380>
  218610:	78e1      	ldrb	r1, [r4, #3]
  218612:	7860      	ldrb	r0, [r4, #1]
  218614:	f67e f8f8 	bl	96808 <A5d5ea600>
  218618:	0007      	movs	r7, r0
  21861a:	d170      	bne.n	2186fe <_lite_host_proc_route_config_req+0x316>
  21861c:	4682      	mov	sl, r0
  21861e:	f8b9 00a8 	ldrh.w	r0, [r9, #168]	; 0xa8
  218622:	2601      	movs	r6, #1
  218624:	1e40      	subs	r0, r0, #1
  218626:	9002      	str	r0, [sp, #8]
  218628:	f64f fdd7 	bl	681da <A43b25c00>
  21862c:	7860      	ldrb	r0, [r4, #1]
  21862e:	f669 fbae 	bl	81d8e <Af334c500>
  218632:	9006      	str	r0, [sp, #24]
  218634:	ab03      	add	r3, sp, #12
  218636:	aa04      	add	r2, sp, #16
  218638:	a905      	add	r1, sp, #20
  21863a:	4620      	mov	r0, r4
  21863c:	f669 fcaf 	bl	81f9e <A51cd7c00>
  218640:	7860      	ldrb	r0, [r4, #1]
  218642:	2803      	cmp	r0, #3
  218644:	d001      	beq.n	21864a <_lite_host_proc_route_config_req+0x262>
  218646:	2806      	cmp	r0, #6
  218648:	d118      	bne.n	21867c <_lite_host_proc_route_config_req+0x294>
  21864a:	78e0      	ldrb	r0, [r4, #3]
  21864c:	2801      	cmp	r0, #1
  21864e:	4648      	mov	r0, r9
  218650:	d108      	bne.n	218664 <_lite_host_proc_route_config_req+0x27c>
  218652:	f8d0 10f8 	ldr.w	r1, [r0, #248]	; 0xf8
  218656:	b9b1      	cbnz	r1, 218686 <_lite_host_proc_route_config_req+0x29e>
  218658:	f8b0 10a8 	ldrh.w	r1, [r0, #168]	; 0xa8
  21865c:	9805      	ldr	r0, [sp, #20]
  21865e:	f650 fdb0 	bl	691c2 <A4cdb6800>
  218662:	e010      	b.n	218686 <_lite_host_proc_route_config_req+0x29e>
  218664:	f8d0 00f8 	ldr.w	r0, [r0, #248]	; 0xf8
  218668:	b108      	cbz	r0, 21866e <_lite_host_proc_route_config_req+0x286>
  21866a:	f650 fdd9 	bl	69220 <Ad89ae600>
  21866e:	78e0      	ldrb	r0, [r4, #3]
  218670:	b958      	cbnz	r0, 21868a <_lite_host_proc_route_config_req+0x2a2>
  218672:	f899 00ed 	ldrb.w	r0, [r9, #237]	; 0xed
  218676:	2801      	cmp	r0, #1
  218678:	d107      	bne.n	21868a <_lite_host_proc_route_config_req+0x2a2>
  21867a:	e004      	b.n	218686 <_lite_host_proc_route_config_req+0x29e>
  21867c:	2805      	cmp	r0, #5
  21867e:	d104      	bne.n	21868a <_lite_host_proc_route_config_req+0x2a2>
  218680:	78e0      	ldrb	r0, [r4, #3]
  218682:	2808      	cmp	r0, #8
  218684:	d101      	bne.n	21868a <_lite_host_proc_route_config_req+0x2a2>
  218686:	f04f 0a01 	mov.w	sl, #1
  21868a:	f04f 0800 	mov.w	r8, #0
  21868e:	e051      	b.n	218734 <_lite_host_proc_route_config_req+0x34c>
  218690:	78e0      	ldrb	r0, [r4, #3]
  218692:	f899 10ed 	ldrb.w	r1, [r9, #237]	; 0xed
  218696:	ea10 0206 	ands.w	r2, r0, r6
  21869a:	d027      	beq.n	2186ec <_lite_host_proc_route_config_req+0x304>
  21869c:	4231      	tst	r1, r6
  21869e:	d143      	bne.n	218728 <_lite_host_proc_route_config_req+0x340>
  2186a0:	2e01      	cmp	r6, #1
  2186a2:	d104      	bne.n	2186ae <_lite_host_proc_route_config_req+0x2c6>
  2186a4:	0709      	lsls	r1, r1, #28
  2186a6:	d428      	bmi.n	2186fa <_lite_host_proc_route_config_req+0x312>
  2186a8:	0700      	lsls	r0, r0, #28
  2186aa:	d426      	bmi.n	2186fa <_lite_host_proc_route_config_req+0x312>
  2186ac:	e006      	b.n	2186bc <_lite_host_proc_route_config_req+0x2d4>
  2186ae:	2e08      	cmp	r6, #8
  2186b0:	d104      	bne.n	2186bc <_lite_host_proc_route_config_req+0x2d4>
  2186b2:	07c8      	lsls	r0, r1, #31
  2186b4:	d12d      	bne.n	218712 <_lite_host_proc_route_config_req+0x32a>
  2186b6:	7860      	ldrb	r0, [r4, #1]
  2186b8:	2805      	cmp	r0, #5
  2186ba:	d02a      	beq.n	218712 <_lite_host_proc_route_config_req+0x32a>
  2186bc:	4610      	mov	r0, r2
  2186be:	f669 fb4a 	bl	81d56 <Ac1c8df00>
  2186c2:	2301      	movs	r3, #1
  2186c4:	9a02      	ldr	r2, [sp, #8]
  2186c6:	e9cd 2300 	strd	r2, r3, [sp]
  2186ca:	9b03      	ldr	r3, [sp, #12]
  2186cc:	2803      	cmp	r0, #3
  2186ce:	d000      	beq.n	2186d2 <_lite_host_proc_route_config_req+0x2ea>
  2186d0:	9b04      	ldr	r3, [sp, #16]
  2186d2:	4602      	mov	r2, r0
  2186d4:	e9dd 1005 	ldrd	r1, r0, [sp, #20]
  2186d8:	f64f fbc9 	bl	67e6e <Ad746b000>
  2186dc:	b108      	cbz	r0, 2186e2 <_lite_host_proc_route_config_req+0x2fa>
  2186de:	2704      	movs	r7, #4
  2186e0:	e041      	b.n	218766 <_lite_host_proc_route_config_req+0x37e>
  2186e2:	4850      	ldr	r0, [pc, #320]	; (218824 <_lite_host_proc_route_config_req+0x43c>)
  2186e4:	6800      	ldr	r0, [r0, #0]
  2186e6:	f001 fc1f 	bl	219f28 <_arip_audioHandleBTA2DP2I2STx_help>
  2186ea:	e01d      	b.n	218728 <_lite_host_proc_route_config_req+0x340>
  2186ec:	ea11 0206 	ands.w	r2, r1, r6
  2186f0:	d01a      	beq.n	218728 <_lite_host_proc_route_config_req+0x340>
  2186f2:	2e01      	cmp	r6, #1
  2186f4:	d104      	bne.n	218700 <_lite_host_proc_route_config_req+0x318>
  2186f6:	0708      	lsls	r0, r1, #28
  2186f8:	d50d      	bpl.n	218716 <_lite_host_proc_route_config_req+0x32e>
  2186fa:	2602      	movs	r6, #2
  2186fc:	e015      	b.n	21872a <_lite_host_proc_route_config_req+0x342>
  2186fe:	e05f      	b.n	2187c0 <_lite_host_proc_route_config_req+0x3d8>
  218700:	2e08      	cmp	r6, #8
  218702:	d108      	bne.n	218716 <_lite_host_proc_route_config_req+0x32e>
  218704:	07c9      	lsls	r1, r1, #31
  218706:	d001      	beq.n	21870c <_lite_host_proc_route_config_req+0x324>
  218708:	07c0      	lsls	r0, r0, #31
  21870a:	d102      	bne.n	218712 <_lite_host_proc_route_config_req+0x32a>
  21870c:	7860      	ldrb	r0, [r4, #1]
  21870e:	2805      	cmp	r0, #5
  218710:	d101      	bne.n	218716 <_lite_host_proc_route_config_req+0x32e>
  218712:	2610      	movs	r6, #16
  218714:	e009      	b.n	21872a <_lite_host_proc_route_config_req+0x342>
  218716:	4610      	mov	r0, r2
  218718:	f669 fb1d 	bl	81d56 <Ac1c8df00>
  21871c:	4601      	mov	r1, r0
  21871e:	9806      	ldr	r0, [sp, #24]
  218720:	f64f fb82 	bl	67e28 <Afa37db00>
  218724:	2800      	cmp	r0, #0
  218726:	d1da      	bne.n	2186de <_lite_host_proc_route_config_req+0x2f6>
  218728:	0076      	lsls	r6, r6, #1
  21872a:	f108 0801 	add.w	r8, r8, #1
  21872e:	f1b8 0f06 	cmp.w	r8, #6
  218732:	da02      	bge.n	21873a <_lite_host_proc_route_config_req+0x352>
  218734:	f1ba 0f00 	cmp.w	sl, #0
  218738:	d0aa      	beq.n	218690 <_lite_host_proc_route_config_req+0x2a8>
  21873a:	b9a7      	cbnz	r7, 218766 <_lite_host_proc_route_config_req+0x37e>
  21873c:	7860      	ldrb	r0, [r4, #1]
  21873e:	2803      	cmp	r0, #3
  218740:	d10a      	bne.n	218758 <_lite_host_proc_route_config_req+0x370>
  218742:	f899 10da 	ldrb.w	r1, [r9, #218]	; 0xda
  218746:	4a33      	ldr	r2, [pc, #204]	; (218814 <_lite_host_proc_route_config_req+0x42c>)
  218748:	f8d9 00b4 	ldr.w	r0, [r9, #180]	; 0xb4
  21874c:	eb02 1141 	add.w	r1, r2, r1, lsl #5
  218750:	684a      	ldr	r2, [r1, #4]
  218752:	4929      	ldr	r1, [pc, #164]	; (2187f8 <_lite_host_proc_route_config_req+0x410>)
  218754:	31dc      	adds	r1, #220	; 0xdc
  218756:	4790      	blx	r2
  218758:	78e0      	ldrb	r0, [r4, #3]
  21875a:	b108      	cbz	r0, 218760 <_lite_host_proc_route_config_req+0x378>
  21875c:	2102      	movs	r1, #2
  21875e:	e000      	b.n	218762 <_lite_host_proc_route_config_req+0x37a>
  218760:	2101      	movs	r1, #1
  218762:	f889 10a4 	strb.w	r1, [r9, #164]	; 0xa4
  218766:	7860      	ldrb	r0, [r4, #1]
  218768:	b128      	cbz	r0, 218776 <_lite_host_proc_route_config_req+0x38e>
  21876a:	78e0      	ldrb	r0, [r4, #3]
  21876c:	2802      	cmp	r0, #2
  21876e:	d002      	beq.n	218776 <_lite_host_proc_route_config_req+0x38e>
  218770:	2880      	cmp	r0, #128	; 0x80
  218772:	d000      	beq.n	218776 <_lite_host_proc_route_config_req+0x38e>
  218774:	b908      	cbnz	r0, 21877a <_lite_host_proc_route_config_req+0x392>
  218776:	f64f fd3b 	bl	681f0 <A08b78700>
  21877a:	bb0f      	cbnz	r7, 2187c0 <_lite_host_proc_route_config_req+0x3d8>
  21877c:	06e8      	lsls	r0, r5, #27
  21877e:	d514      	bpl.n	2187aa <_lite_host_proc_route_config_req+0x3c2>
  218780:	78e0      	ldrb	r0, [r4, #3]
  218782:	0700      	lsls	r0, r0, #28
  218784:	d503      	bpl.n	21878e <_lite_host_proc_route_config_req+0x3a6>
  218786:	2001      	movs	r0, #1
  218788:	f60f ff5e 	bl	28648 <A42be8e00>
  21878c:	e00d      	b.n	2187aa <_lite_host_proc_route_config_req+0x3c2>
  21878e:	7860      	ldrb	r0, [r4, #1]
  218790:	0780      	lsls	r0, r0, #30
  218792:	d00a      	beq.n	2187aa <_lite_host_proc_route_config_req+0x3c2>
  218794:	2002      	movs	r0, #2
  218796:	f60f ff57 	bl	28648 <A42be8e00>
  21879a:	481f      	ldr	r0, [pc, #124]	; (218818 <_lite_host_proc_route_config_req+0x430>)
  21879c:	6800      	ldr	r0, [r0, #0]
  21879e:	6800      	ldr	r0, [r0, #0]
  2187a0:	1f00      	subs	r0, r0, #4
  2187a2:	f64f fdf8 	bl	68396 <A14df5600>
  2187a6:	f889 b0d4 	strb.w	fp, [r9, #212]	; 0xd4
  2187aa:	7861      	ldrb	r1, [r4, #1]
  2187ac:	f109 00ec 	add.w	r0, r9, #236	; 0xec
  2187b0:	7001      	strb	r1, [r0, #0]
  2187b2:	78e1      	ldrb	r1, [r4, #3]
  2187b4:	7041      	strb	r1, [r0, #1]
  2187b6:	79a0      	ldrb	r0, [r4, #6]
  2187b8:	f04f 0100 	mov.w	r1, #0
  2187bc:	f67e f99a 	bl	96af4 <A7c9c3000>
  2187c0:	78a0      	ldrb	r0, [r4, #2]
  2187c2:	2804      	cmp	r0, #4
  2187c4:	d103      	bne.n	2187ce <_lite_host_proc_route_config_req+0x3e6>
  2187c6:	2100      	movs	r1, #0
  2187c8:	f64b 3080 	movw	r0, #48000	; 0xbb80
  2187cc:	e004      	b.n	2187d8 <_lite_host_proc_route_config_req+0x3f0>
  2187ce:	2803      	cmp	r0, #3
  2187d0:	d104      	bne.n	2187dc <_lite_host_proc_route_config_req+0x3f4>
  2187d2:	2100      	movs	r1, #0
  2187d4:	f64a 4044 	movw	r0, #44100	; 0xac44
  2187d8:	f001 fb14 	bl	219e04 <arip_adjustFifoClockWithInitialFreq>
  2187dc:	9808      	ldr	r0, [sp, #32]
  2187de:	f249 0105 	movw	r1, #36869	; 0x9005
  2187e2:	80c1      	strh	r1, [r0, #6]
  2187e4:	9808      	ldr	r0, [sp, #32]
  2187e6:	2118      	movs	r1, #24
  2187e8:	7201      	strb	r1, [r0, #8]
  2187ea:	9808      	ldr	r0, [sp, #32]
  2187ec:	7247      	strb	r7, [r0, #9]
  2187ee:	2004      	movs	r0, #4
  2187f0:	b009      	add	sp, #36	; 0x24
  2187f2:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  2187f6:	0000      	.short	0x0000
  2187f8:	0020b670 	.word	0x0020b670
  2187fc:	00327000 	.word	0x00327000
  218800:	00201cdc 	.word	0x00201cdc
  218804:	0020196c 	.word	0x0020196c
  218808:	00200eb0 	.word	0x00200eb0
  21880c:	00201d30 	.word	0x00201d30
  218810:	00202470 	.word	0x00202470
  218814:	00262850 	.word	0x00262850
  218818:	00203614 	.word	0x00203614
  21881c:	00201bf1 	.word	0x00201bf1
  218820:	00207038 	.word	0x00207038
  218824:	00201c38 	.word	0x00201c38

00218828 <lite_host_proc_avdt_hook>:
  218828:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
  21882c:	4684      	mov	ip, r0
  21882e:	4689      	mov	r9, r1
  218830:	2500      	movs	r5, #0
  218832:	f89c 1002 	ldrb.w	r1, [ip, #2]
  218836:	4628      	mov	r0, r5
  218838:	290a      	cmp	r1, #10
  21883a:	d134      	bne.n	2188a6 <lite_host_proc_avdt_hook+0x7e>
  21883c:	f10c 0c02 	add.w	ip, ip, #2
  218840:	2300      	movs	r3, #0
  218842:	f8df 842c 	ldr.w	r8, [pc, #1068]	; 218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>
  218846:	e020      	b.n	21888a <lite_host_proc_avdt_hook+0x62>
  218848:	2400      	movs	r4, #0
  21884a:	eb03 0183 	add.w	r1, r3, r3, lsl #2
  21884e:	4620      	mov	r0, r4
  218850:	4461      	add	r1, ip
  218852:	eb08 1240 	add.w	r2, r8, r0, lsl #5
  218856:	7f56      	ldrb	r6, [r2, #29]
  218858:	b126      	cbz	r6, 218864 <lite_host_proc_avdt_hook+0x3c>
  21885a:	89d6      	ldrh	r6, [r2, #14]
  21885c:	884f      	ldrh	r7, [r1, #2]
  21885e:	42be      	cmp	r6, r7
  218860:	d100      	bne.n	218864 <lite_host_proc_avdt_hook+0x3c>
  218862:	1d14      	adds	r4, r2, #4
  218864:	1c40      	adds	r0, r0, #1
  218866:	b2c0      	uxtb	r0, r0
  218868:	2805      	cmp	r0, #5
  21886a:	d3f2      	bcc.n	218852 <lite_host_proc_avdt_hook+0x2a>
  21886c:	b154      	cbz	r4, 218884 <lite_host_proc_avdt_hook+0x5c>
  21886e:	8888      	ldrh	r0, [r1, #4]
  218870:	b108      	cbz	r0, 218876 <lite_host_proc_avdt_hook+0x4e>
  218872:	2802      	cmp	r0, #2
  218874:	d106      	bne.n	218884 <lite_host_proc_avdt_hook+0x5c>
  218876:	4aff      	ldr	r2, [pc, #1020]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218878:	8050      	strh	r0, [r2, #2]
  21887a:	7988      	ldrb	r0, [r1, #6]
  21887c:	7110      	strb	r0, [r2, #4]
  21887e:	8848      	ldrh	r0, [r1, #2]
  218880:	8010      	strh	r0, [r2, #0]
  218882:	e000      	b.n	218886 <lite_host_proc_avdt_hook+0x5e>
  218884:	2501      	movs	r5, #1
  218886:	1c5b      	adds	r3, r3, #1
  218888:	b2db      	uxtb	r3, r3
  21888a:	f89c 0001 	ldrb.w	r0, [ip, #1]
  21888e:	4298      	cmp	r0, r3
  218890:	d8da      	bhi.n	218848 <lite_host_proc_avdt_hook+0x20>
  218892:	f249 0106 	movw	r1, #36870	; 0x9006
  218896:	f8a9 1006 	strh.w	r1, [r9, #6]
  21889a:	210b      	movs	r1, #11
  21889c:	f889 1008 	strb.w	r1, [r9, #8]
  2188a0:	f889 5009 	strb.w	r5, [r9, #9]
  2188a4:	2004      	movs	r0, #4
  2188a6:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}

002188aa <lite_host_proc_set_audio_mute_req>:
  2188aa:	4af2      	ldr	r2, [pc, #968]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  2188ac:	7843      	ldrb	r3, [r0, #1]
  2188ae:	3a1c      	subs	r2, #28
  2188b0:	70d3      	strb	r3, [r2, #3]
  2188b2:	8840      	ldrh	r0, [r0, #2]
  2188b4:	f240 23aa 	movw	r3, #682	; 0x2aa
  2188b8:	6150      	str	r0, [r2, #20]
  2188ba:	4298      	cmp	r0, r3
  2188bc:	dd01      	ble.n	2188c2 <lite_host_proc_set_audio_mute_req+0x18>
  2188be:	6153      	str	r3, [r2, #20]
  2188c0:	e003      	b.n	2188ca <lite_host_proc_set_audio_mute_req+0x20>
  2188c2:	2801      	cmp	r0, #1
  2188c4:	da01      	bge.n	2188ca <lite_host_proc_set_audio_mute_req+0x20>
  2188c6:	2001      	movs	r0, #1
  2188c8:	6150      	str	r0, [r2, #20]
  2188ca:	f249 0005 	movw	r0, #36869	; 0x9005
  2188ce:	80c8      	strh	r0, [r1, #6]
  2188d0:	2048      	movs	r0, #72	; 0x48
  2188d2:	7208      	strb	r0, [r1, #8]
  2188d4:	78d0      	ldrb	r0, [r2, #3]
  2188d6:	7248      	strb	r0, [r1, #9]
  2188d8:	6950      	ldr	r0, [r2, #20]
  2188da:	8148      	strh	r0, [r1, #10]
  2188dc:	2006      	movs	r0, #6
  2188de:	4770      	bx	lr

002188e0 <lite_host_proc_set_pll_parameter_req>:
  2188e0:	4ae5      	ldr	r2, [pc, #916]	; (218c78 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc8>)
  2188e2:	f8d0 3001 	ldr.w	r3, [r0, #1]
  2188e6:	6013      	str	r3, [r2, #0]
  2188e8:	f8d0 3005 	ldr.w	r3, [r0, #5]
  2188ec:	6053      	str	r3, [r2, #4]
  2188ee:	f8d0 3009 	ldr.w	r3, [r0, #9]
  2188f2:	6093      	str	r3, [r2, #8]
  2188f4:	f8d0 300d 	ldr.w	r3, [r0, #13]
  2188f8:	60d3      	str	r3, [r2, #12]
  2188fa:	f8d0 3011 	ldr.w	r3, [r0, #17]
  2188fe:	6113      	str	r3, [r2, #16]
  218900:	f8d0 3015 	ldr.w	r3, [r0, #21]
  218904:	6153      	str	r3, [r2, #20]
  218906:	f8d0 3019 	ldr.w	r3, [r0, #25]
  21890a:	6193      	str	r3, [r2, #24]
  21890c:	f249 0205 	movw	r2, #36869	; 0x9005
  218910:	80ca      	strh	r2, [r1, #6]
  218912:	2246      	movs	r2, #70	; 0x46
  218914:	720a      	strb	r2, [r1, #8]
  218916:	f8d0 2001 	ldr.w	r2, [r0, #1]
  21891a:	f8c1 2009 	str.w	r2, [r1, #9]
  21891e:	f8d0 2005 	ldr.w	r2, [r0, #5]
  218922:	f8c1 200d 	str.w	r2, [r1, #13]
  218926:	f8d0 2009 	ldr.w	r2, [r0, #9]
  21892a:	f8c1 2011 	str.w	r2, [r1, #17]
  21892e:	f8d0 200d 	ldr.w	r2, [r0, #13]
  218932:	f8c1 2015 	str.w	r2, [r1, #21]
  218936:	f8d0 2011 	ldr.w	r2, [r0, #17]
  21893a:	f8c1 2019 	str.w	r2, [r1, #25]
  21893e:	f8d0 2015 	ldr.w	r2, [r0, #21]
  218942:	f8c1 201d 	str.w	r2, [r1, #29]
  218946:	f8d0 0019 	ldr.w	r0, [r0, #25]
  21894a:	f8c1 0021 	str.w	r0, [r1, #33]	; 0x21
  21894e:	201f      	movs	r0, #31
  218950:	4770      	bx	lr

00218952 <lite_host_proc_register_play_status_req>:
  218952:	7842      	ldrb	r2, [r0, #1]
  218954:	48c7      	ldr	r0, [pc, #796]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218956:	381c      	subs	r0, #28
  218958:	7042      	strb	r2, [r0, #1]
  21895a:	f249 0205 	movw	r2, #36869	; 0x9005
  21895e:	80ca      	strh	r2, [r1, #6]
  218960:	2243      	movs	r2, #67	; 0x43
  218962:	720a      	strb	r2, [r1, #8]
  218964:	7840      	ldrb	r0, [r0, #1]
  218966:	7248      	strb	r0, [r1, #9]
  218968:	2004      	movs	r0, #4
  21896a:	4770      	bx	lr

0021896c <lite_host_proc_jitter_buf_get_state_req>:
  21896c:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  218970:	460c      	mov	r4, r1
  218972:	f651 f9d9 	bl	69d28 <_tx_v7m_get_int>
  218976:	4607      	mov	r7, r0
  218978:	f651 f9d9 	bl	69d2e <_tx_v7m_disable_int>
  21897c:	48bd      	ldr	r0, [pc, #756]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  21897e:	381c      	subs	r0, #28
  218980:	88c5      	ldrh	r5, [r0, #6]
  218982:	3028      	adds	r0, #40	; 0x28
  218984:	e9d0 1000 	ldrd	r1, r0, [r0]
  218988:	180e      	adds	r6, r1, r0
  21898a:	4638      	mov	r0, r7
  21898c:	f651 f9d2 	bl	69d34 <_tx_v7m_set_int>
  218990:	48b8      	ldr	r0, [pc, #736]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218992:	49ba      	ldr	r1, [pc, #744]	; (218c7c <LiteHost_Process_Start_L2CAP_Pkt_hook+0xcc>)
  218994:	1d80      	adds	r0, r0, #6
  218996:	8800      	ldrh	r0, [r0, #0]
  218998:	6809      	ldr	r1, [r1, #0]
  21899a:	4370      	muls	r0, r6
  21899c:	fbb0 f0f1 	udiv	r0, r0, r1
  2189a0:	f249 0105 	movw	r1, #36869	; 0x9005
  2189a4:	80e1      	strh	r1, [r4, #6]
  2189a6:	2141      	movs	r1, #65	; 0x41
  2189a8:	7221      	strb	r1, [r4, #8]
  2189aa:	f8a4 0009 	strh.w	r0, [r4, #9]
  2189ae:	f8a4 500b 	strh.w	r5, [r4, #11]
  2189b2:	2007      	movs	r0, #7
  2189b4:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

002189b8 <lite_host_proc_jitter_buf_depth_config_req>:
  2189b8:	b5f0      	push	{r4, r5, r6, r7, lr}
  2189ba:	7942      	ldrb	r2, [r0, #5]
  2189bc:	b10a      	cbz	r2, 2189c2 <lite_host_proc_jitter_buf_depth_config_req+0xa>
  2189be:	2301      	movs	r3, #1
  2189c0:	e000      	b.n	2189c4 <lite_host_proc_jitter_buf_depth_config_req+0xc>
  2189c2:	2300      	movs	r3, #0
  2189c4:	4aab      	ldr	r2, [pc, #684]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  2189c6:	f44f 7796 	mov.w	r7, #300	; 0x12c
  2189ca:	1d92      	adds	r2, r2, #6
  2189cc:	7113      	strb	r3, [r2, #4]
  2189ce:	f8b0 4001 	ldrh.w	r4, [r0, #1]
  2189d2:	463b      	mov	r3, r7
  2189d4:	42bc      	cmp	r4, r7
  2189d6:	d200      	bcs.n	2189da <lite_host_proc_jitter_buf_depth_config_req+0x22>
  2189d8:	4623      	mov	r3, r4
  2189da:	8013      	strh	r3, [r2, #0]
  2189dc:	2664      	movs	r6, #100	; 0x64
  2189de:	78c4      	ldrb	r4, [r0, #3]
  2189e0:	4633      	mov	r3, r6
  2189e2:	2c64      	cmp	r4, #100	; 0x64
  2189e4:	d200      	bcs.n	2189e8 <lite_host_proc_jitter_buf_depth_config_req+0x30>
  2189e6:	4623      	mov	r3, r4
  2189e8:	7093      	strb	r3, [r2, #2]
  2189ea:	7900      	ldrb	r0, [r0, #4]
  2189ec:	4633      	mov	r3, r6
  2189ee:	2864      	cmp	r0, #100	; 0x64
  2189f0:	d200      	bcs.n	2189f4 <lite_host_proc_jitter_buf_depth_config_req+0x3c>
  2189f2:	4603      	mov	r3, r0
  2189f4:	70d3      	strb	r3, [r2, #3]
  2189f6:	8815      	ldrh	r5, [r2, #0]
  2189f8:	f242 7010 	movw	r0, #10000	; 0x2710
  2189fc:	4368      	muls	r0, r5
  2189fe:	fbb0 f0f7 	udiv	r0, r0, r7
  218a02:	4c9e      	ldr	r4, [pc, #632]	; (218c7c <LiteHost_Process_Start_L2CAP_Pkt_hook+0xcc>)
  218a04:	6020      	str	r0, [r4, #0]
  218a06:	7893      	ldrb	r3, [r2, #2]
  218a08:	fb00 f703 	mul.w	r7, r0, r3
  218a0c:	fbb7 f7f6 	udiv	r7, r7, r6
  218a10:	6067      	str	r7, [r4, #4]
  218a12:	60a7      	str	r7, [r4, #8]
  218a14:	78d7      	ldrb	r7, [r2, #3]
  218a16:	4378      	muls	r0, r7
  218a18:	fbb0 f0f6 	udiv	r0, r0, r6
  218a1c:	60e0      	str	r0, [r4, #12]
  218a1e:	f249 0005 	movw	r0, #36869	; 0x9005
  218a22:	80c8      	strh	r0, [r1, #6]
  218a24:	203f      	movs	r0, #63	; 0x3f
  218a26:	7208      	strb	r0, [r1, #8]
  218a28:	f8a1 5009 	strh.w	r5, [r1, #9]
  218a2c:	72cb      	strb	r3, [r1, #11]
  218a2e:	730f      	strb	r7, [r1, #12]
  218a30:	7910      	ldrb	r0, [r2, #4]
  218a32:	7348      	strb	r0, [r1, #13]
  218a34:	2008      	movs	r0, #8
  218a36:	bdf0      	pop	{r4, r5, r6, r7, pc}

00218a38 <lite_host_init_audioramp>:
  218a38:	488e      	ldr	r0, [pc, #568]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218a3a:	2100      	movs	r1, #0
  218a3c:	381c      	subs	r0, #28
  218a3e:	7081      	strb	r1, [r0, #2]
  218a40:	70c1      	strb	r1, [r0, #3]
  218a42:	7101      	strb	r1, [r0, #4]
  218a44:	7141      	strb	r1, [r0, #5]
  218a46:	60c1      	str	r1, [r0, #12]
  218a48:	6101      	str	r1, [r0, #16]
  218a4a:	21c8      	movs	r1, #200	; 0xc8
  218a4c:	6141      	str	r1, [r0, #20]
  218a4e:	4770      	bx	lr

00218a50 <lite_host_ramp_status_ind>:
  218a50:	b510      	push	{r4, lr}
  218a52:	4604      	mov	r4, r0
  218a54:	2107      	movs	r1, #7
  218a56:	20ff      	movs	r0, #255	; 0xff
  218a58:	f603 fc20 	bl	1c29c <A8ece6200>
  218a5c:	2800      	cmp	r0, #0
  218a5e:	d00c      	beq.n	218a7a <lite_host_ramp_status_ind+0x2a>
  218a60:	211a      	movs	r1, #26
  218a62:	7081      	strb	r1, [r0, #2]
  218a64:	f249 0105 	movw	r1, #36869	; 0x9005
  218a68:	f8a0 1003 	strh.w	r1, [r0, #3]
  218a6c:	2149      	movs	r1, #73	; 0x49
  218a6e:	7141      	strb	r1, [r0, #5]
  218a70:	7184      	strb	r4, [r0, #6]
  218a72:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  218a76:	f603 bb83 	b.w	1c180 <Aebb10c00>
  218a7a:	bd10      	pop	{r4, pc}

00218a7c <lite_host_init_samplesinqueue>:
  218a7c:	b570      	push	{r4, r5, r6, lr}
  218a7e:	f651 f953 	bl	69d28 <_tx_v7m_get_int>
  218a82:	4605      	mov	r5, r0
  218a84:	f651 f953 	bl	69d2e <_tx_v7m_disable_int>
  218a88:	487a      	ldr	r0, [pc, #488]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218a8a:	2400      	movs	r4, #0
  218a8c:	300c      	adds	r0, #12
  218a8e:	4978      	ldr	r1, [pc, #480]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218a90:	6004      	str	r4, [r0, #0]
  218a92:	f8d1 1100 	ldr.w	r1, [r1, #256]	; 0x100
  218a96:	8a89      	ldrh	r1, [r1, #20]
  218a98:	6041      	str	r1, [r0, #4]
  218a9a:	4628      	mov	r0, r5
  218a9c:	f651 f94a 	bl	69d34 <_tx_v7m_set_int>
  218aa0:	4874      	ldr	r0, [pc, #464]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218aa2:	381c      	subs	r0, #28
  218aa4:	7801      	ldrb	r1, [r0, #0]
  218aa6:	2900      	cmp	r1, #0
  218aa8:	d006      	beq.n	218ab8 <lite_host_init_samplesinqueue+0x3c>
  218aaa:	7004      	strb	r4, [r0, #0]
  218aac:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  218ab0:	f44f 3000 	mov.w	r0, #131072	; 0x20000
  218ab4:	f659 bc07 	b.w	722c6 <Ae3690800>
  218ab8:	bd70      	pop	{r4, r5, r6, pc}

00218aba <LiteHost_Handle_UIPC_Entry_hook>:
  218aba:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  218abe:	4615      	mov	r5, r2
  218ac0:	8802      	ldrh	r2, [r0, #0]
  218ac2:	461f      	mov	r7, r3
  218ac4:	2400      	movs	r4, #0
  218ac6:	f5a2 4310 	sub.w	r3, r2, #36864	; 0x9000
  218aca:	3b06      	subs	r3, #6
  218acc:	d106      	bne.n	218adc <LiteHost_Handle_UIPC_Entry_hook+0x22>
  218ace:	7883      	ldrb	r3, [r0, #2]
  218ad0:	2b0a      	cmp	r3, #10
  218ad2:	d103      	bne.n	218adc <LiteHost_Handle_UIPC_Entry_hook+0x22>
  218ad4:	f7ff fea8 	bl	218828 <lite_host_proc_avdt_hook>
  218ad8:	7028      	strb	r0, [r5, #0]
  218ada:	e067      	b.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218adc:	f5a2 4310 	sub.w	r3, r2, #36864	; 0x9000
  218ae0:	3b05      	subs	r3, #5
  218ae2:	f04f 0401 	mov.w	r4, #1
  218ae6:	d161      	bne.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218ae8:	4e62      	ldr	r6, [pc, #392]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218aea:	7882      	ldrb	r2, [r0, #2]
  218aec:	3e1c      	subs	r6, #28
  218aee:	2a3e      	cmp	r2, #62	; 0x3e
  218af0:	d033      	beq.n	218b5a <LiteHost_Handle_UIPC_Entry_hook+0xa0>
  218af2:	dc15      	bgt.n	218b20 <LiteHost_Handle_UIPC_Entry_hook+0x66>
  218af4:	2a0b      	cmp	r2, #11
  218af6:	d022      	beq.n	218b3e <LiteHost_Handle_UIPC_Entry_hook+0x84>
  218af8:	2a0d      	cmp	r2, #13
  218afa:	d029      	beq.n	218b50 <LiteHost_Handle_UIPC_Entry_hook+0x96>
  218afc:	2a0f      	cmp	r2, #15
  218afe:	d029      	beq.n	218b54 <LiteHost_Handle_UIPC_Entry_hook+0x9a>
  218b00:	2a11      	cmp	r2, #17
  218b02:	d153      	bne.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b04:	f001 f812 	bl	219b2c <rest_pll_tuner>
  218b08:	7970      	ldrb	r0, [r6, #5]
  218b0a:	b918      	cbnz	r0, 218b14 <LiteHost_Handle_UIPC_Entry_hook+0x5a>
  218b0c:	7930      	ldrb	r0, [r6, #4]
  218b0e:	78f1      	ldrb	r1, [r6, #3]
  218b10:	4288      	cmp	r0, r1
  218b12:	d04b      	beq.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b14:	2001      	movs	r0, #1
  218b16:	f7ff ff9b 	bl	218a50 <lite_host_ramp_status_ind>
  218b1a:	f7ff ff8d 	bl	218a38 <lite_host_init_audioramp>
  218b1e:	e045      	b.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b20:	2a40      	cmp	r2, #64	; 0x40
  218b22:	d01e      	beq.n	218b62 <LiteHost_Handle_UIPC_Entry_hook+0xa8>
  218b24:	2a42      	cmp	r2, #66	; 0x42
  218b26:	d021      	beq.n	218b6c <LiteHost_Handle_UIPC_Entry_hook+0xb2>
  218b28:	2a45      	cmp	r2, #69	; 0x45
  218b2a:	d023      	beq.n	218b74 <LiteHost_Handle_UIPC_Entry_hook+0xba>
  218b2c:	2a47      	cmp	r2, #71	; 0x47
  218b2e:	d13d      	bne.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b30:	78c2      	ldrb	r2, [r0, #3]
  218b32:	78f3      	ldrb	r3, [r6, #3]
  218b34:	2400      	movs	r4, #0
  218b36:	429a      	cmp	r2, r3
  218b38:	d120      	bne.n	218b7c <LiteHost_Handle_UIPC_Entry_hook+0xc2>
  218b3a:	240c      	movs	r4, #12
  218b3c:	e026      	b.n	218b8c <LiteHost_Handle_UIPC_Entry_hook+0xd2>
  218b3e:	f7ff ff7b 	bl	218a38 <lite_host_init_audioramp>
  218b42:	484c      	ldr	r0, [pc, #304]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218b44:	1d80      	adds	r0, r0, #6
  218b46:	78c1      	ldrb	r1, [r0, #3]
  218b48:	8800      	ldrh	r0, [r0, #0]
  218b4a:	f000 ffc9 	bl	219ae0 <run_pll_tuner>
  218b4e:	e02d      	b.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b50:	f000 ffec 	bl	219b2c <rest_pll_tuner>
  218b54:	f7ff ff92 	bl	218a7c <lite_host_init_samplesinqueue>
  218b58:	e028      	b.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218b5a:	1c80      	adds	r0, r0, #2
  218b5c:	f7ff ff2c 	bl	2189b8 <lite_host_proc_jitter_buf_depth_config_req>
  218b60:	e002      	b.n	218b68 <LiteHost_Handle_UIPC_Entry_hook+0xae>
  218b62:	1c80      	adds	r0, r0, #2
  218b64:	f7ff ff02 	bl	21896c <lite_host_proc_jitter_buf_get_state_req>
  218b68:	7028      	strb	r0, [r5, #0]
  218b6a:	e01d      	b.n	218ba8 <LiteHost_Handle_UIPC_Entry_hook+0xee>
  218b6c:	1c80      	adds	r0, r0, #2
  218b6e:	f7ff fef0 	bl	218952 <lite_host_proc_register_play_status_req>
  218b72:	e7f9      	b.n	218b68 <LiteHost_Handle_UIPC_Entry_hook+0xae>
  218b74:	1c80      	adds	r0, r0, #2
  218b76:	f7ff feb3 	bl	2188e0 <lite_host_proc_set_pll_parameter_req>
  218b7a:	e7f5      	b.n	218b68 <LiteHost_Handle_UIPC_Entry_hook+0xae>
  218b7c:	4a3c      	ldr	r2, [pc, #240]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218b7e:	f892 20a4 	ldrb.w	r2, [r2, #164]	; 0xa4
  218b82:	2a02      	cmp	r2, #2
  218b84:	d002      	beq.n	218b8c <LiteHost_Handle_UIPC_Entry_hook+0xd2>
  218b86:	7932      	ldrb	r2, [r6, #4]
  218b88:	240c      	movs	r4, #12
  218b8a:	70c2      	strb	r2, [r0, #3]
  218b8c:	1c80      	adds	r0, r0, #2
  218b8e:	f7ff fe8c 	bl	2188aa <lite_host_proc_set_audio_mute_req>
  218b92:	7028      	strb	r0, [r5, #0]
  218b94:	b144      	cbz	r4, 218ba8 <LiteHost_Handle_UIPC_Entry_hook+0xee>
  218b96:	2300      	movs	r3, #0
  218b98:	461a      	mov	r2, r3
  218b9a:	4621      	mov	r1, r4
  218b9c:	f64f 408b 	movw	r0, #64651	; 0xfc8b
  218ba0:	f603 fe5d 	bl	1c85e <A22160400>
  218ba4:	2000      	movs	r0, #0
  218ba6:	7038      	strb	r0, [r7, #0]
  218ba8:	2400      	movs	r4, #0
  218baa:	e7ff      	b.n	218bac <LiteHost_Handle_UIPC_Entry_hook+0xf2>
  218bac:	4620      	mov	r0, r4
  218bae:	e701      	b.n	2189b4 <lite_host_proc_jitter_buf_get_state_req+0x48>

00218bb0 <LiteHost_Process_Start_L2CAP_Pkt_hook>:
  218bb0:	e92d 4ffe 	stmdb	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
  218bb4:	4683      	mov	fp, r0
  218bb6:	2000      	movs	r0, #0
  218bb8:	9e0c      	ldr	r6, [sp, #48]	; 0x30
  218bba:	4b2d      	ldr	r3, [pc, #180]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218bbc:	9002      	str	r0, [sp, #8]
  218bbe:	9001      	str	r0, [sp, #4]
  218bc0:	f8d3 0100 	ldr.w	r0, [r3, #256]	; 0x100
  218bc4:	460d      	mov	r5, r1
  218bc6:	78c1      	ldrb	r1, [r0, #3]
  218bc8:	465c      	mov	r4, fp
  218bca:	2901      	cmp	r1, #1
  218bcc:	d002      	beq.n	218bd4 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x24>
  218bce:	2903      	cmp	r1, #3
  218bd0:	d10c      	bne.n	218bec <LiteHost_Process_Start_L2CAP_Pkt_hook+0x3c>
  218bd2:	2104      	movs	r1, #4
  218bd4:	f8d4 8008 	ldr.w	r8, [r4, #8]
  218bd8:	468a      	mov	sl, r1
  218bda:	f3c8 1440 	ubfx	r4, r8, #5, #1
  218bde:	f3c8 0103 	ubfx	r1, r8, #0, #4
  218be2:	7004      	strb	r4, [r0, #0]
  218be4:	008c      	lsls	r4, r1, #2
  218be6:	3414      	adds	r4, #20
  218be8:	2100      	movs	r1, #0
  218bea:	e005      	b.n	218bf8 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x48>
  218bec:	4658      	mov	r0, fp
  218bee:	f64c ff09 	bl	65a04 <A64fb9d00>
  218bf2:	2000      	movs	r0, #0
  218bf4:	e8bd 8ffe 	ldmia.w	sp!, {r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, fp, pc}
  218bf8:	4b1d      	ldr	r3, [pc, #116]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218bfa:	eb03 1341 	add.w	r3, r3, r1, lsl #5
  218bfe:	7f5f      	ldrb	r7, [r3, #29]
  218c00:	b1b7      	cbz	r7, 218c30 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x80>
  218c02:	7f9f      	ldrb	r7, [r3, #30]
  218c04:	b1a7      	cbz	r7, 218c30 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x80>
  218c06:	f8b3 c012 	ldrh.w	ip, [r3, #18]
  218c0a:	8ac7      	ldrh	r7, [r0, #22]
  218c0c:	f3cc 0c0b 	ubfx	ip, ip, #0, #12
  218c10:	4567      	cmp	r7, ip
  218c12:	d10d      	bne.n	218c30 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x80>
  218c14:	8b47      	ldrh	r7, [r0, #26]
  218c16:	89db      	ldrh	r3, [r3, #14]
  218c18:	429f      	cmp	r7, r3
  218c1a:	d109      	bne.n	218c30 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x80>
  218c1c:	4b15      	ldr	r3, [pc, #84]	; (218c74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc4>)
  218c1e:	8859      	ldrh	r1, [r3, #2]
  218c20:	2902      	cmp	r1, #2
  218c22:	d108      	bne.n	218c36 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x86>
  218c24:	8b41      	ldrh	r1, [r0, #26]
  218c26:	881b      	ldrh	r3, [r3, #0]
  218c28:	4299      	cmp	r1, r3
  218c2a:	d104      	bne.n	218c36 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x86>
  218c2c:	1c64      	adds	r4, r4, #1
  218c2e:	e002      	b.n	218c36 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x86>
  218c30:	1c49      	adds	r1, r1, #1
  218c32:	2905      	cmp	r1, #5
  218c34:	dbe0      	blt.n	218bf8 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x48>
  218c36:	ea4f 4118 	mov.w	r1, r8, lsr #16
  218c3a:	0209      	lsls	r1, r1, #8
  218c3c:	ea41 6118 	orr.w	r1, r1, r8, lsr #24
  218c40:	f361 481f 	bfi	r8, r1, #16, #16
  218c44:	f8cd 8000 	str.w	r8, [sp]
  218c48:	f8df 8034 	ldr.w	r8, [pc, #52]	; 218c80 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xd0>
  218c4c:	f8df 9034 	ldr.w	r9, [pc, #52]	; 218c84 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xd4>
  218c50:	b912      	cbnz	r2, 218c58 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xa8>
  218c52:	4a07      	ldr	r2, [pc, #28]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218c54:	8981      	ldrh	r1, [r0, #12]
  218c56:	b3a1      	cbz	r1, 218cc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x112>
  218c58:	4f05      	ldr	r7, [pc, #20]	; (218c70 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xc0>)
  218c5a:	8980      	ldrh	r0, [r0, #12]
  218c5c:	f650 fe44 	bl	698e8 <A604a4a00>
  218c60:	f8d7 0100 	ldr.w	r0, [r7, #256]	; 0x100
  218c64:	2101      	movs	r1, #1
  218c66:	7081      	strb	r1, [r0, #2]
  218c68:	f04f 0c00 	mov.w	ip, #0
  218c6c:	e00c      	b.n	218c88 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xd8>
  218c6e:	0000      	.short	0x0000
  218c70:	0020b670 	.word	0x0020b670
  218c74:	0021fac0 	.word	0x0021fac0
  218c78:	0021fad8 	.word	0x0021fad8
  218c7c:	00201d30 	.word	0x00201d30
  218c80:	00201bf4 	.word	0x00201bf4
  218c84:	0020b844 	.word	0x0020b844
  218c88:	49fa      	ldr	r1, [pc, #1000]	; (219074 <lite_host_send_play_status_ind+0x36>)
  218c8a:	f8a0 c00c 	strh.w	ip, [r0, #12]
  218c8e:	6809      	ldr	r1, [r1, #0]
  218c90:	b399      	cbz	r1, 218cfa <LiteHost_Process_Start_L2CAP_Pkt_hook+0x14a>
  218c92:	4ff9      	ldr	r7, [pc, #996]	; (219078 <lite_host_send_play_status_ind+0x3a>)
  218c94:	464a      	mov	r2, r9
  218c96:	6839      	ldr	r1, [r7, #0]
  218c98:	b121      	cbz	r1, 218ca4 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xf4>
  218c9a:	eb02 0281 	add.w	r2, r2, r1, lsl #2
  218c9e:	f852 2c04 	ldr.w	r2, [r2, #-4]
  218ca2:	e001      	b.n	218ca8 <LiteHost_Process_Start_L2CAP_Pkt_hook+0xf8>
  218ca4:	f8d2 20c4 	ldr.w	r2, [r2, #196]	; 0xc4
  218ca8:	6b83      	ldr	r3, [r0, #56]	; 0x38
  218caa:	429a      	cmp	r2, r3
  218cac:	d034      	beq.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218cae:	6b80      	ldr	r0, [r0, #56]	; 0x38
  218cb0:	f849 0021 	str.w	r0, [r9, r1, lsl #2]
  218cb4:	1c49      	adds	r1, r1, #1
  218cb6:	6039      	str	r1, [r7, #0]
  218cb8:	2932      	cmp	r1, #50	; 0x32
  218cba:	d32d      	bcc.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218cbc:	f8c7 c000 	str.w	ip, [r7]
  218cc0:	e02a      	b.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218cc2:	e7ff      	b.n	218cc4 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x114>
  218cc4:	49eb      	ldr	r1, [pc, #940]	; (219074 <lite_host_send_play_status_ind+0x36>)
  218cc6:	6809      	ldr	r1, [r1, #0]
  218cc8:	b1b9      	cbz	r1, 218cfa <LiteHost_Process_Start_L2CAP_Pkt_hook+0x14a>
  218cca:	8a00      	ldrh	r0, [r0, #16]
  218ccc:	4617      	mov	r7, r2
  218cce:	2801      	cmp	r0, #1
  218cd0:	d022      	beq.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218cd2:	9800      	ldr	r0, [sp, #0]
  218cd4:	f8b8 1000 	ldrh.w	r1, [r8]
  218cd8:	0c00      	lsrs	r0, r0, #16
  218cda:	f650 fdf9 	bl	698d0 <Ac9353c00>
  218cde:	b160      	cbz	r0, 218cfa <LiteHost_Process_Start_L2CAP_Pkt_hook+0x14a>
  218ce0:	f8d7 0100 	ldr.w	r0, [r7, #256]	; 0x100
  218ce4:	2101      	movs	r1, #1
  218ce6:	4fe4      	ldr	r7, [pc, #912]	; (219078 <lite_host_send_play_status_ind+0x3a>)
  218ce8:	7081      	strb	r1, [r0, #2]
  218cea:	6839      	ldr	r1, [r7, #0]
  218cec:	464a      	mov	r2, r9
  218cee:	b129      	cbz	r1, 218cfc <LiteHost_Process_Start_L2CAP_Pkt_hook+0x14c>
  218cf0:	eb02 0281 	add.w	r2, r2, r1, lsl #2
  218cf4:	f852 2c04 	ldr.w	r2, [r2, #-4]
  218cf8:	e002      	b.n	218d00 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x150>
  218cfa:	e00d      	b.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218cfc:	f8d2 20c4 	ldr.w	r2, [r2, #196]	; 0xc4
  218d00:	6b83      	ldr	r3, [r0, #56]	; 0x38
  218d02:	429a      	cmp	r2, r3
  218d04:	d008      	beq.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218d06:	6b80      	ldr	r0, [r0, #56]	; 0x38
  218d08:	f849 0021 	str.w	r0, [r9, r1, lsl #2]
  218d0c:	1c49      	adds	r1, r1, #1
  218d0e:	6039      	str	r1, [r7, #0]
  218d10:	2932      	cmp	r1, #50	; 0x32
  218d12:	d301      	bcc.n	218d18 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x168>
  218d14:	2000      	movs	r0, #0
  218d16:	6038      	str	r0, [r7, #0]
  218d18:	9800      	ldr	r0, [sp, #0]
  218d1a:	1b29      	subs	r1, r5, r4
  218d1c:	0c00      	lsrs	r0, r0, #16
  218d1e:	f8a8 0000 	strh.w	r0, [r8]
  218d22:	f8df 8358 	ldr.w	r8, [pc, #856]	; 21907c <lite_host_send_play_status_ind+0x3e>
  218d26:	eba1 070a 	sub.w	r7, r1, sl
  218d2a:	f8d8 0100 	ldr.w	r0, [r8, #256]	; 0x100
  218d2e:	1d3f      	adds	r7, r7, #4
  218d30:	78c1      	ldrb	r1, [r0, #3]
  218d32:	2901      	cmp	r1, #1
  218d34:	d102      	bne.n	218d3c <LiteHost_Process_Start_L2CAP_Pkt_hook+0x18c>
  218d36:	eb0b 0504 	add.w	r5, fp, r4
  218d3a:	e005      	b.n	218d48 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x198>
  218d3c:	2903      	cmp	r1, #3
  218d3e:	d103      	bne.n	218d48 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x198>
  218d40:	eb0b 0204 	add.w	r2, fp, r4
  218d44:	eb02 050a 	add.w	r5, r2, sl
  218d48:	7800      	ldrb	r0, [r0, #0]
  218d4a:	b120      	cbz	r0, 218d56 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x1a6>
  218d4c:	b91e      	cbnz	r6, 218d56 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x1a6>
  218d4e:	19e8      	adds	r0, r5, r7
  218d50:	f810 0c01 	ldrb.w	r0, [r0, #-1]
  218d54:	1a3f      	subs	r7, r7, r0
  218d56:	48ca      	ldr	r0, [pc, #808]	; (219080 <lite_host_send_play_status_ind+0x42>)
  218d58:	2f00      	cmp	r7, #0
  218d5a:	da08      	bge.n	218d6e <LiteHost_Process_Start_L2CAP_Pkt_hook+0x1be>
  218d5c:	6187      	str	r7, [r0, #24]
  218d5e:	4658      	mov	r0, fp
  218d60:	f64c fe50 	bl	65a04 <A64fb9d00>
  218d64:	f8d8 1100 	ldr.w	r1, [r8, #256]	; 0x100
  218d68:	2002      	movs	r0, #2
  218d6a:	7088      	strb	r0, [r1, #2]
  218d6c:	e029      	b.n	218dc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x212>
  218d6e:	f04f 0a00 	mov.w	sl, #0
  218d72:	f8c0 a018 	str.w	sl, [r0, #24]
  218d76:	2101      	movs	r1, #1
  218d78:	4638      	mov	r0, r7
  218d7a:	f650 fd22 	bl	697c2 <A2df7d400>
  218d7e:	b310      	cbz	r0, 218dc6 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x216>
  218d80:	4658      	mov	r0, fp
  218d82:	f64c fe3f 	bl	65a04 <A64fb9d00>
  218d86:	48bb      	ldr	r0, [pc, #748]	; (219074 <lite_host_send_play_status_ind+0x36>)
  218d88:	6800      	ldr	r0, [r0, #0]
  218d8a:	b1d0      	cbz	r0, 218dc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x212>
  218d8c:	49ba      	ldr	r1, [pc, #744]	; (219078 <lite_host_send_play_status_ind+0x3a>)
  218d8e:	464a      	mov	r2, r9
  218d90:	6808      	ldr	r0, [r1, #0]
  218d92:	b120      	cbz	r0, 218d9e <LiteHost_Process_Start_L2CAP_Pkt_hook+0x1ee>
  218d94:	eb02 0280 	add.w	r2, r2, r0, lsl #2
  218d98:	f852 2c04 	ldr.w	r2, [r2, #-4]
  218d9c:	e001      	b.n	218da2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x1f2>
  218d9e:	f8d2 20c4 	ldr.w	r2, [r2, #196]	; 0xc4
  218da2:	f8d8 3100 	ldr.w	r3, [r8, #256]	; 0x100
  218da6:	6b9b      	ldr	r3, [r3, #56]	; 0x38
  218da8:	429a      	cmp	r2, r3
  218daa:	d00a      	beq.n	218dc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x212>
  218dac:	f8d8 2100 	ldr.w	r2, [r8, #256]	; 0x100
  218db0:	6b93      	ldr	r3, [r2, #56]	; 0x38
  218db2:	f849 3020 	str.w	r3, [r9, r0, lsl #2]
  218db6:	1c40      	adds	r0, r0, #1
  218db8:	6008      	str	r0, [r1, #0]
  218dba:	2832      	cmp	r0, #50	; 0x32
  218dbc:	d301      	bcc.n	218dc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x212>
  218dbe:	f8c1 a000 	str.w	sl, [r1]
  218dc2:	2001      	movs	r0, #1
  218dc4:	e716      	b.n	218bf4 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x44>
  218dc6:	f8d8 0100 	ldr.w	r0, [r8, #256]	; 0x100
  218dca:	78c1      	ldrb	r1, [r0, #3]
  218dcc:	2901      	cmp	r1, #1
  218dce:	d153      	bne.n	218e78 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2c8>
  218dd0:	f815 1b01 	ldrb.w	r1, [r5], #1
  218dd4:	060a      	lsls	r2, r1, #24
  218dd6:	d40e      	bmi.n	218df6 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x246>
  218dd8:	4652      	mov	r2, sl
  218dda:	f880 a001 	strb.w	sl, [r0, #1]
  218dde:	b10e      	cbz	r6, 218de4 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x234>
  218de0:	2102      	movs	r1, #2
  218de2:	e014      	b.n	218e0e <LiteHost_Process_Start_L2CAP_Pkt_hook+0x25e>
  218de4:	8a41      	ldrh	r1, [r0, #18]
  218de6:	4439      	add	r1, r7
  218de8:	8241      	strh	r1, [r0, #18]
  218dea:	f04f 0101 	mov.w	r1, #1
  218dee:	7081      	strb	r1, [r0, #2]
  218df0:	eb01 4107 	add.w	r1, r1, r7, lsl #16
  218df4:	e03e      	b.n	218e74 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2c4>
  218df6:	064b      	lsls	r3, r1, #25
  218df8:	f04f 0210 	mov.w	r2, #16
  218dfc:	d512      	bpl.n	218e24 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x274>
  218dfe:	7883      	ldrb	r3, [r0, #2]
  218e00:	07db      	lsls	r3, r3, #31
  218e02:	d00f      	beq.n	218e24 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x274>
  218e04:	f3c1 0103 	ubfx	r1, r1, #0, #4
  218e08:	b11e      	cbz	r6, 218e12 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x262>
  218e0a:	7041      	strb	r1, [r0, #1]
  218e0c:	2104      	movs	r1, #4
  218e0e:	7081      	strb	r1, [r0, #2]
  218e10:	e006      	b.n	218e20 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x270>
  218e12:	1e49      	subs	r1, r1, #1
  218e14:	b2c9      	uxtb	r1, r1
  218e16:	7041      	strb	r1, [r0, #1]
  218e18:	2901      	cmp	r1, #1
  218e1a:	d000      	beq.n	218e1e <LiteHost_Process_Start_L2CAP_Pkt_hook+0x26e>
  218e1c:	2208      	movs	r2, #8
  218e1e:	7082      	strb	r2, [r0, #2]
  218e20:	8187      	strh	r7, [r0, #12]
  218e22:	e029      	b.n	218e78 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2c8>
  218e24:	7843      	ldrb	r3, [r0, #1]
  218e26:	f3c1 0403 	ubfx	r4, r1, #0, #4
  218e2a:	42a3      	cmp	r3, r4
  218e2c:	d10e      	bne.n	218e4c <LiteHost_Process_Start_L2CAP_Pkt_hook+0x29c>
  218e2e:	7884      	ldrb	r4, [r0, #2]
  218e30:	0724      	lsls	r4, r4, #28
  218e32:	d50b      	bpl.n	218e4c <LiteHost_Process_Start_L2CAP_Pkt_hook+0x29c>
  218e34:	b936      	cbnz	r6, 218e44 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x294>
  218e36:	1e5b      	subs	r3, r3, #1
  218e38:	b2d9      	uxtb	r1, r3
  218e3a:	7041      	strb	r1, [r0, #1]
  218e3c:	2901      	cmp	r1, #1
  218e3e:	d000      	beq.n	218e42 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x292>
  218e40:	2208      	movs	r2, #8
  218e42:	7082      	strb	r2, [r0, #2]
  218e44:	8981      	ldrh	r1, [r0, #12]
  218e46:	4439      	add	r1, r7
  218e48:	8181      	strh	r1, [r0, #12]
  218e4a:	e015      	b.n	218e78 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2c8>
  218e4c:	0689      	lsls	r1, r1, #26
  218e4e:	d51a      	bpl.n	218e86 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2d6>
  218e50:	7881      	ldrb	r1, [r0, #2]
  218e52:	06c9      	lsls	r1, r1, #27
  218e54:	d517      	bpl.n	218e86 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2d6>
  218e56:	2e00      	cmp	r6, #0
  218e58:	d1f4      	bne.n	218e44 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x294>
  218e5a:	f880 a001 	strb.w	sl, [r0, #1]
  218e5e:	8981      	ldrh	r1, [r0, #12]
  218e60:	2301      	movs	r3, #1
  218e62:	7083      	strb	r3, [r0, #2]
  218e64:	8a43      	ldrh	r3, [r0, #18]
  218e66:	4439      	add	r1, r7
  218e68:	440b      	add	r3, r1
  218e6a:	8243      	strh	r3, [r0, #18]
  218e6c:	2302      	movs	r3, #2
  218e6e:	4652      	mov	r2, sl
  218e70:	eb03 4101 	add.w	r1, r3, r1, lsl #16
  218e74:	9101      	str	r1, [sp, #4]
  218e76:	8182      	strh	r2, [r0, #12]
  218e78:	9802      	ldr	r0, [sp, #8]
  218e7a:	b920      	cbnz	r0, 218e86 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x2d6>
  218e7c:	4639      	mov	r1, r7
  218e7e:	4628      	mov	r0, r5
  218e80:	9a01      	ldr	r2, [sp, #4]
  218e82:	f650 fbf9 	bl	69678 <A90971500>
  218e86:	4658      	mov	r0, fp
  218e88:	f64c fdbc 	bl	65a04 <A64fb9d00>
  218e8c:	e799      	b.n	218dc2 <LiteHost_Process_Start_L2CAP_Pkt_hook+0x212>

00218e8e <LiteHost_Process_Continue_L2CAP_Pkt_hook>:
  218e8e:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
  218e92:	f8df a1e8 	ldr.w	sl, [pc, #488]	; 21907c <lite_host_send_play_status_ind+0x3e>
  218e96:	4681      	mov	r9, r0
  218e98:	f8da 0100 	ldr.w	r0, [sl, #256]	; 0x100
  218e9c:	2604      	movs	r6, #4
  218e9e:	7800      	ldrb	r0, [r0, #0]
  218ea0:	460c      	mov	r4, r1
  218ea2:	4615      	mov	r5, r2
  218ea4:	2700      	movs	r7, #0
  218ea6:	b130      	cbz	r0, 218eb6 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x28>
  218ea8:	b92d      	cbnz	r5, 218eb6 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x28>
  218eaa:	f109 0004 	add.w	r0, r9, #4
  218eae:	4420      	add	r0, r4
  218eb0:	f810 0c01 	ldrb.w	r0, [r0, #-1]
  218eb4:	1a24      	subs	r4, r4, r0
  218eb6:	4972      	ldr	r1, [pc, #456]	; (219080 <lite_host_send_play_status_ind+0x42>)
  218eb8:	f04f 0800 	mov.w	r8, #0
  218ebc:	6988      	ldr	r0, [r1, #24]
  218ebe:	2800      	cmp	r0, #0
  218ec0:	da04      	bge.n	218ecc <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x3e>
  218ec2:	4404      	add	r4, r0
  218ec4:	f1c0 0604 	rsb	r6, r0, #4
  218ec8:	f8c1 8018 	str.w	r8, [r1, #24]
  218ecc:	2100      	movs	r1, #0
  218ece:	4620      	mov	r0, r4
  218ed0:	f650 fc77 	bl	697c2 <A2df7d400>
  218ed4:	bbd0      	cbnz	r0, 218f4c <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xbe>
  218ed6:	f8da 0100 	ldr.w	r0, [sl, #256]	; 0x100
  218eda:	f04f 0301 	mov.w	r3, #1
  218ede:	7881      	ldrb	r1, [r0, #2]
  218ee0:	f04f 0c02 	mov.w	ip, #2
  218ee4:	078a      	lsls	r2, r1, #30
  218ee6:	d50b      	bpl.n	218f00 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x72>
  218ee8:	bbb5      	cbnz	r5, 218f58 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xca>
  218eea:	8981      	ldrh	r1, [r0, #12]
  218eec:	8a42      	ldrh	r2, [r0, #18]
  218eee:	4421      	add	r1, r4
  218ef0:	440a      	add	r2, r1
  218ef2:	8242      	strh	r2, [r0, #18]
  218ef4:	eb0c 4701 	add.w	r7, ip, r1, lsl #16
  218ef8:	7083      	strb	r3, [r0, #2]
  218efa:	4641      	mov	r1, r8
  218efc:	8181      	strh	r1, [r0, #12]
  218efe:	e01f      	b.n	218f40 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xb2>
  218f00:	78c2      	ldrb	r2, [r0, #3]
  218f02:	2a01      	cmp	r2, #1
  218f04:	d122      	bne.n	218f4c <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xbe>
  218f06:	f011 0f0c 	tst.w	r1, #12
  218f0a:	d00a      	beq.n	218f22 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x94>
  218f0c:	bb25      	cbnz	r5, 218f58 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xca>
  218f0e:	7841      	ldrb	r1, [r0, #1]
  218f10:	2208      	movs	r2, #8
  218f12:	1e49      	subs	r1, r1, #1
  218f14:	b2c9      	uxtb	r1, r1
  218f16:	7041      	strb	r1, [r0, #1]
  218f18:	2901      	cmp	r1, #1
  218f1a:	d100      	bne.n	218f1e <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x90>
  218f1c:	2210      	movs	r2, #16
  218f1e:	7082      	strb	r2, [r0, #2]
  218f20:	e01b      	b.n	218f5a <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xcc>
  218f22:	06c9      	lsls	r1, r1, #27
  218f24:	d512      	bpl.n	218f4c <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xbe>
  218f26:	b9bd      	cbnz	r5, 218f58 <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xca>
  218f28:	f880 8001 	strb.w	r8, [r0, #1]
  218f2c:	8981      	ldrh	r1, [r0, #12]
  218f2e:	7083      	strb	r3, [r0, #2]
  218f30:	4421      	add	r1, r4
  218f32:	8a43      	ldrh	r3, [r0, #18]
  218f34:	eb0c 4701 	add.w	r7, ip, r1, lsl #16
  218f38:	440b      	add	r3, r1
  218f3a:	8243      	strh	r3, [r0, #18]
  218f3c:	f8a0 800c 	strh.w	r8, [r0, #12]
  218f40:	eb09 0006 	add.w	r0, r9, r6
  218f44:	463a      	mov	r2, r7
  218f46:	4621      	mov	r1, r4
  218f48:	f650 fb96 	bl	69678 <A90971500>
  218f4c:	4648      	mov	r0, r9
  218f4e:	f64c fd59 	bl	65a04 <A64fb9d00>
  218f52:	2001      	movs	r0, #1
  218f54:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
  218f58:	e7ff      	b.n	218f5a <LiteHost_Process_Continue_L2CAP_Pkt_hook+0xcc>
  218f5a:	8981      	ldrh	r1, [r0, #12]
  218f5c:	4421      	add	r1, r4
  218f5e:	e7cd      	b.n	218efc <LiteHost_Process_Continue_L2CAP_Pkt_hook+0x6e>

00218f60 <lite_host_setPPMforHost>:
  218f60:	4947      	ldr	r1, [pc, #284]	; (219080 <lite_host_send_play_status_ind+0x42>)
  218f62:	80c8      	strh	r0, [r1, #6]
  218f64:	4770      	bx	lr

00218f66 <lite_host_get_samplesinqueue>:
  218f66:	b570      	push	{r4, r5, r6, lr}
  218f68:	4604      	mov	r4, r0
  218f6a:	f650 fedd 	bl	69d28 <_tx_v7m_get_int>
  218f6e:	4605      	mov	r5, r0
  218f70:	f650 fedd 	bl	69d2e <_tx_v7m_disable_int>
  218f74:	4842      	ldr	r0, [pc, #264]	; (219080 <lite_host_send_play_status_ind+0x42>)
  218f76:	3028      	adds	r0, #40	; 0x28
  218f78:	6800      	ldr	r0, [r0, #0]
  218f7a:	6020      	str	r0, [r4, #0]
  218f7c:	4628      	mov	r0, r5
  218f7e:	f650 fed9 	bl	69d34 <_tx_v7m_set_int>
  218f82:	493f      	ldr	r1, [pc, #252]	; (219080 <lite_host_send_play_status_ind+0x42>)
  218f84:	7808      	ldrb	r0, [r1, #0]
  218f86:	2800      	cmp	r0, #0
  218f88:	d00e      	beq.n	218fa8 <lite_host_get_samplesinqueue+0x42>
  218f8a:	483c      	ldr	r0, [pc, #240]	; (21907c <lite_host_send_play_status_ind+0x3e>)
  218f8c:	f8d0 0100 	ldr.w	r0, [r0, #256]	; 0x100
  218f90:	8a82      	ldrh	r2, [r0, #20]
  218f92:	8940      	ldrh	r0, [r0, #10]
  218f94:	4282      	cmp	r2, r0
  218f96:	d207      	bcs.n	218fa8 <lite_host_get_samplesinqueue+0x42>
  218f98:	2000      	movs	r0, #0
  218f9a:	7008      	strb	r0, [r1, #0]
  218f9c:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  218fa0:	f44f 3000 	mov.w	r0, #131072	; 0x20000
  218fa4:	f659 b98f 	b.w	722c6 <Ae3690800>
  218fa8:	bd70      	pop	{r4, r5, r6, pc}

00218faa <lite_host_get_samplerate>:
  218faa:	4934      	ldr	r1, [pc, #208]	; (21907c <lite_host_send_play_status_ind+0x3e>)
  218fac:	f64b 3080 	movw	r0, #48000	; 0xbb80
  218fb0:	f891 20da 	ldrb.w	r2, [r1, #218]	; 0xda
  218fb4:	2a01      	cmp	r2, #1
  218fb6:	d108      	bne.n	218fca <lite_host_get_samplerate+0x20>
  218fb8:	f891 10dd 	ldrb.w	r1, [r1, #221]	; 0xdd
  218fbc:	b131      	cbz	r1, 218fcc <lite_host_get_samplerate+0x22>
  218fbe:	2901      	cmp	r1, #1
  218fc0:	d007      	beq.n	218fd2 <lite_host_get_samplerate+0x28>
  218fc2:	2902      	cmp	r1, #2
  218fc4:	d101      	bne.n	218fca <lite_host_get_samplerate+0x20>
  218fc6:	f64a 4044 	movw	r0, #44100	; 0xac44
  218fca:	4770      	bx	lr
  218fcc:	f44f 507a 	mov.w	r0, #16000	; 0x3e80
  218fd0:	4770      	bx	lr
  218fd2:	f44f 40fa 	mov.w	r0, #32000	; 0x7d00
  218fd6:	4770      	bx	lr

00218fd8 <lite_host_get_channels>:
  218fd8:	4928      	ldr	r1, [pc, #160]	; (21907c <lite_host_send_play_status_ind+0x3e>)
  218fda:	2002      	movs	r0, #2
  218fdc:	f891 20da 	ldrb.w	r2, [r1, #218]	; 0xda
  218fe0:	2a01      	cmp	r2, #1
  218fe2:	d104      	bne.n	218fee <lite_host_get_channels+0x16>
  218fe4:	f891 10de 	ldrb.w	r1, [r1, #222]	; 0xde
  218fe8:	2900      	cmp	r1, #0
  218fea:	d100      	bne.n	218fee <lite_host_get_channels+0x16>
  218fec:	2001      	movs	r0, #1
  218fee:	4770      	bx	lr

00218ff0 <lite_host_get_bytesinfifo>:
  218ff0:	b570      	push	{r4, r5, r6, lr}
  218ff2:	4604      	mov	r4, r0
  218ff4:	f650 fe98 	bl	69d28 <_tx_v7m_get_int>
  218ff8:	4605      	mov	r5, r0
  218ffa:	f650 fe98 	bl	69d2e <_tx_v7m_disable_int>
  218ffe:	481f      	ldr	r0, [pc, #124]	; (21907c <lite_host_send_play_status_ind+0x3e>)
  219000:	4e1f      	ldr	r6, [pc, #124]	; (219080 <lite_host_send_play_status_ind+0x42>)
  219002:	f8d0 0100 	ldr.w	r0, [r0, #256]	; 0x100
  219006:	3628      	adds	r6, #40	; 0x28
  219008:	8a80      	ldrh	r0, [r0, #20]
  21900a:	6070      	str	r0, [r6, #4]
  21900c:	f7ff ffe4 	bl	218fd8 <lite_host_get_channels>
  219010:	4603      	mov	r3, r0
  219012:	f7ff ffca 	bl	218faa <lite_host_get_samplerate>
  219016:	4343      	muls	r3, r0
  219018:	1fb0      	subs	r0, r6, #6
  21901a:	8800      	ldrh	r0, [r0, #0]
  21901c:	6871      	ldr	r1, [r6, #4]
  21901e:	4343      	muls	r3, r0
  219020:	f44f 70fa 	mov.w	r0, #500	; 0x1f4
  219024:	fb93 f0f0 	sdiv	r0, r3, r0
  219028:	4348      	muls	r0, r1
  21902a:	4916      	ldr	r1, [pc, #88]	; (219084 <lite_host_send_play_status_ind+0x46>)
  21902c:	6809      	ldr	r1, [r1, #0]
  21902e:	fbb0 f0f1 	udiv	r0, r0, r1
  219032:	6020      	str	r0, [r4, #0]
  219034:	4628      	mov	r0, r5
  219036:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21903a:	f650 be7b 	b.w	69d34 <_tx_v7m_set_int>

0021903e <lite_host_send_play_status_ind>:
  21903e:	b510      	push	{r4, lr}
  219040:	4604      	mov	r4, r0
  219042:	480f      	ldr	r0, [pc, #60]	; (219080 <lite_host_send_play_status_ind+0x42>)
  219044:	8104      	strh	r4, [r0, #8]
  219046:	7840      	ldrb	r0, [r0, #1]
  219048:	2800      	cmp	r0, #0
  21904a:	d012      	beq.n	219072 <lite_host_send_play_status_ind+0x34>
  21904c:	2107      	movs	r1, #7
  21904e:	20ff      	movs	r0, #255	; 0xff
  219050:	f603 f924 	bl	1c29c <A8ece6200>
  219054:	2800      	cmp	r0, #0
  219056:	d00c      	beq.n	219072 <lite_host_send_play_status_ind+0x34>
  219058:	211a      	movs	r1, #26
  21905a:	7081      	strb	r1, [r0, #2]
  21905c:	f249 0105 	movw	r1, #36869	; 0x9005
  219060:	f8a0 1003 	strh.w	r1, [r0, #3]
  219064:	2144      	movs	r1, #68	; 0x44
  219066:	7141      	strb	r1, [r0, #5]
  219068:	7184      	strb	r4, [r0, #6]
  21906a:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21906e:	f603 b887 	b.w	1c180 <Aebb10c00>
  219072:	bd10      	pop	{r4, pc}
  219074:	00201c10 	.word	0x00201c10
  219078:	00201c18 	.word	0x00201c18
  21907c:	0020b670 	.word	0x0020b670
  219080:	0021faa4 	.word	0x0021faa4
  219084:	00201d30 	.word	0x00201d30

00219088 <lite_host_process_rcv_l2cap_pkt_patch>:
  219088:	e92d 47fc 	stmdb	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
  21908c:	4fa2      	ldr	r7, [pc, #648]	; (219318 <get_sbc_frame_len_in_jitter_buf_patch+0x94>)
  21908e:	f8df a284 	ldr.w	sl, [pc, #644]	; 219314 <get_sbc_frame_len_in_jitter_buf_patch+0x90>
  219092:	f04f 0901 	mov.w	r9, #1
  219096:	f04f 0802 	mov.w	r8, #2
  21909a:	f1a7 0622 	sub.w	r6, r7, #34	; 0x22
  21909e:	4d9f      	ldr	r5, [pc, #636]	; (21931c <get_sbc_frame_len_in_jitter_buf_patch+0x98>)
  2190a0:	e046      	b.n	219130 <lite_host_process_rcv_l2cap_pkt_patch+0xa8>
  2190a2:	a901      	add	r1, sp, #4
  2190a4:	489e      	ldr	r0, [pc, #632]	; (219320 <get_sbc_frame_len_in_jitter_buf_patch+0x9c>)
  2190a6:	f64d fcc0 	bl	66a2a <Aa7c76a00>
  2190aa:	f8d5 0100 	ldr.w	r0, [r5, #256]	; 0x100
  2190ae:	9c01      	ldr	r4, [sp, #4]
  2190b0:	78c0      	ldrb	r0, [r0, #3]
  2190b2:	2801      	cmp	r0, #1
  2190b4:	d112      	bne.n	2190dc <lite_host_process_rcv_l2cap_pkt_patch+0x54>
  2190b6:	7938      	ldrb	r0, [r7, #4]
  2190b8:	2801      	cmp	r0, #1
  2190ba:	d10f      	bne.n	2190dc <lite_host_process_rcv_l2cap_pkt_patch+0x54>
  2190bc:	8820      	ldrh	r0, [r4, #0]
  2190be:	2101      	movs	r1, #1
  2190c0:	f650 fb7f 	bl	697c2 <A2df7d400>
  2190c4:	b150      	cbz	r0, 2190dc <lite_host_process_rcv_l2cap_pkt_patch+0x54>
  2190c6:	a901      	add	r1, sp, #4
  2190c8:	4895      	ldr	r0, [pc, #596]	; (219320 <get_sbc_frame_len_in_jitter_buf_patch+0x9c>)
  2190ca:	f64d fcc7 	bl	66a5c <A81f15800>
  2190ce:	f8d5 1100 	ldr.w	r1, [r5, #256]	; 0x100
  2190d2:	f8a1 8010 	strh.w	r8, [r1, #16]
  2190d6:	f886 9000 	strb.w	r9, [r6]
  2190da:	e031      	b.n	219140 <lite_host_process_rcv_l2cap_pkt_patch+0xb8>
  2190dc:	88a0      	ldrh	r0, [r4, #4]
  2190de:	2802      	cmp	r0, #2
  2190e0:	d108      	bne.n	2190f4 <lite_host_process_rcv_l2cap_pkt_patch+0x6c>
  2190e2:	8923      	ldrh	r3, [r4, #8]
  2190e4:	9300      	str	r3, [sp, #0]
  2190e6:	8863      	ldrh	r3, [r4, #2]
  2190e8:	88e2      	ldrh	r2, [r4, #6]
  2190ea:	8821      	ldrh	r1, [r4, #0]
  2190ec:	68e0      	ldr	r0, [r4, #12]
  2190ee:	f650 fc0d 	bl	6990c <A55c2d000>
  2190f2:	e01a      	b.n	21912a <lite_host_process_rcv_l2cap_pkt_patch+0xa2>
  2190f4:	2801      	cmp	r0, #1
  2190f6:	d118      	bne.n	21912a <lite_host_process_rcv_l2cap_pkt_patch+0xa2>
  2190f8:	f8d5 0100 	ldr.w	r0, [r5, #256]	; 0x100
  2190fc:	8a01      	ldrh	r1, [r0, #16]
  2190fe:	0709      	lsls	r1, r1, #28
  219100:	d50e      	bpl.n	219120 <lite_host_process_rcv_l2cap_pkt_patch+0x98>
  219102:	7939      	ldrb	r1, [r7, #4]
  219104:	2901      	cmp	r1, #1
  219106:	d10b      	bne.n	219120 <lite_host_process_rcv_l2cap_pkt_patch+0x98>
  219108:	f8a0 8010 	strh.w	r8, [r0, #16]
  21910c:	8922      	ldrh	r2, [r4, #8]
  21910e:	8821      	ldrh	r1, [r4, #0]
  219110:	68e0      	ldr	r0, [r4, #12]
  219112:	f650 fb74 	bl	697fe <A9602d080>
  219116:	f8d5 1100 	ldr.w	r1, [r5, #256]	; 0x100
  21911a:	2008      	movs	r0, #8
  21911c:	8208      	strh	r0, [r1, #16]
  21911e:	e004      	b.n	21912a <lite_host_process_rcv_l2cap_pkt_patch+0xa2>
  219120:	8922      	ldrh	r2, [r4, #8]
  219122:	8821      	ldrh	r1, [r4, #0]
  219124:	68e0      	ldr	r0, [r4, #12]
  219126:	f650 fb6a 	bl	697fe <A9602d080>
  21912a:	4620      	mov	r0, r4
  21912c:	f64c fbf8 	bl	65920 <A9fb54500>
  219130:	487b      	ldr	r0, [pc, #492]	; (219320 <get_sbc_frame_len_in_jitter_buf_patch+0x9c>)
  219132:	f64d fca9 	bl	66a88 <A265e9180>
  219136:	7831      	ldrb	r1, [r6, #0]
  219138:	4308      	orrs	r0, r1
  21913a:	d0b2      	beq.n	2190a2 <lite_host_process_rcv_l2cap_pkt_patch+0x1a>
  21913c:	7830      	ldrb	r0, [r6, #0]
  21913e:	b100      	cbz	r0, 219142 <lite_host_process_rcv_l2cap_pkt_patch+0xba>
  219140:	4648      	mov	r0, r9
  219142:	f88a 0000 	strb.w	r0, [sl]
  219146:	f8d5 0100 	ldr.w	r0, [r5, #256]	; 0x100
  21914a:	8931      	ldrh	r1, [r6, #8]
  21914c:	8a00      	ldrh	r0, [r0, #16]
  21914e:	4288      	cmp	r0, r1
  219150:	d002      	beq.n	219158 <lite_host_process_rcv_l2cap_pkt_patch+0xd0>
  219152:	b2c0      	uxtb	r0, r0
  219154:	f7ff ff73 	bl	21903e <lite_host_send_play_status_ind>
  219158:	e8bd 87fc 	ldmia.w	sp!, {r2, r3, r4, r5, r6, r7, r8, r9, sl, pc}

0021915c <lite_host_audio_ramp_state_machine>:
  21915c:	b570      	push	{r4, r5, r6, lr}
  21915e:	4b6e      	ldr	r3, [pc, #440]	; (219318 <get_sbc_frame_len_in_jitter_buf_patch+0x94>)
  219160:	3b22      	subs	r3, #34	; 0x22
  219162:	7958      	ldrb	r0, [r3, #5]
  219164:	2400      	movs	r4, #0
  219166:	b148      	cbz	r0, 21917c <lite_host_audio_ramp_state_machine+0x20>
  219168:	2802      	cmp	r0, #2
  21916a:	d11c      	bne.n	2191a6 <lite_host_audio_ramp_state_machine+0x4a>
  21916c:	7918      	ldrb	r0, [r3, #4]
  21916e:	b900      	cbnz	r0, 219172 <lite_host_audio_ramp_state_machine+0x16>
  219170:	709c      	strb	r4, [r3, #2]
  219172:	715c      	strb	r4, [r3, #5]
  219174:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  219178:	2001      	movs	r0, #1
  21917a:	e469      	b.n	218a50 <lite_host_ramp_status_ind>
  21917c:	78d8      	ldrb	r0, [r3, #3]
  21917e:	7118      	strb	r0, [r3, #4]
  219180:	b2c1      	uxtb	r1, r0
  219182:	4865      	ldr	r0, [pc, #404]	; (219318 <get_sbc_frame_len_in_jitter_buf_patch+0x94>)
  219184:	2601      	movs	r6, #1
  219186:	3822      	subs	r0, #34	; 0x22
  219188:	4d66      	ldr	r5, [pc, #408]	; (219324 <get_sbc_frame_len_in_jitter_buf_patch+0xa0>)
  21918a:	7880      	ldrb	r0, [r0, #2]
  21918c:	b161      	cbz	r1, 2191a8 <lite_host_audio_ramp_state_machine+0x4c>
  21918e:	2800      	cmp	r0, #0
  219190:	d109      	bne.n	2191a6 <lite_host_audio_ramp_state_machine+0x4a>
  219192:	f7ff ff0a 	bl	218faa <lite_host_get_samplerate>
  219196:	6959      	ldr	r1, [r3, #20]
  219198:	4348      	muls	r0, r1
  21919a:	fb95 f0f0 	sdiv	r0, r5, r0
  21919e:	e9c3 0403 	strd	r0, r4, [r3, #12]
  2191a2:	715e      	strb	r6, [r3, #5]
  2191a4:	709e      	strb	r6, [r3, #2]
  2191a6:	bd70      	pop	{r4, r5, r6, pc}
  2191a8:	2800      	cmp	r0, #0
  2191aa:	d0fc      	beq.n	2191a6 <lite_host_audio_ramp_state_machine+0x4a>
  2191ac:	f7ff fefd 	bl	218faa <lite_host_get_samplerate>
  2191b0:	6959      	ldr	r1, [r3, #20]
  2191b2:	4348      	muls	r0, r1
  2191b4:	fb95 f0f0 	sdiv	r0, r5, r0
  2191b8:	e9c3 0403 	strd	r0, r4, [r3, #12]
  2191bc:	715e      	strb	r6, [r3, #5]
  2191be:	bd70      	pop	{r4, r5, r6, pc}

002191c0 <lite_host_process_event_patch>:
  2191c0:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  2191c2:	4604      	mov	r4, r0
  2191c4:	4e58      	ldr	r6, [pc, #352]	; (219328 <get_sbc_frame_len_in_jitter_buf_patch+0xa4>)
  2191c6:	4f59      	ldr	r7, [pc, #356]	; (21932c <get_sbc_frame_len_in_jitter_buf_patch+0xa8>)
  2191c8:	0540      	lsls	r0, r0, #21
  2191ca:	f04f 0500 	mov.w	r5, #0
  2191ce:	d511      	bpl.n	2191f4 <lite_host_process_event_patch+0x34>
  2191d0:	f7ff ffc4 	bl	21915c <lite_host_audio_ramp_state_machine>
  2191d4:	6831      	ldr	r1, [r6, #0]
  2191d6:	b109      	cbz	r1, 2191dc <lite_host_process_event_patch+0x1c>
  2191d8:	4620      	mov	r0, r4
  2191da:	4788      	blx	r1
  2191dc:	6838      	ldr	r0, [r7, #0]
  2191de:	b108      	cbz	r0, 2191e4 <lite_host_process_event_patch+0x24>
  2191e0:	603d      	str	r5, [r7, #0]
  2191e2:	e007      	b.n	2191f4 <lite_host_process_event_patch+0x34>
  2191e4:	2104      	movs	r1, #4
  2191e6:	2000      	movs	r0, #0
  2191e8:	f633 fd74 	bl	4ccd4 <A489df200>
  2191ec:	2104      	movs	r1, #4
  2191ee:	2000      	movs	r0, #0
  2191f0:	f61f fea1 	bl	38f36 <A9ee36800>
  2191f4:	0520      	lsls	r0, r4, #20
  2191f6:	d511      	bpl.n	21921c <lite_host_process_event_patch+0x5c>
  2191f8:	f7ff ffb0 	bl	21915c <lite_host_audio_ramp_state_machine>
  2191fc:	6831      	ldr	r1, [r6, #0]
  2191fe:	b109      	cbz	r1, 219204 <lite_host_process_event_patch+0x44>
  219200:	4620      	mov	r0, r4
  219202:	4788      	blx	r1
  219204:	6838      	ldr	r0, [r7, #0]
  219206:	b108      	cbz	r0, 21920c <lite_host_process_event_patch+0x4c>
  219208:	603d      	str	r5, [r7, #0]
  21920a:	e007      	b.n	21921c <lite_host_process_event_patch+0x5c>
  21920c:	2108      	movs	r1, #8
  21920e:	2000      	movs	r0, #0
  219210:	f633 fd60 	bl	4ccd4 <A489df200>
  219214:	2108      	movs	r1, #8
  219216:	2000      	movs	r0, #0
  219218:	f61f fe8d 	bl	38f36 <A9ee36800>
  21921c:	04e0      	lsls	r0, r4, #19
  21921e:	d50c      	bpl.n	21923a <lite_host_process_event_patch+0x7a>
  219220:	e006      	b.n	219230 <lite_host_process_event_patch+0x70>
  219222:	4669      	mov	r1, sp
  219224:	4842      	ldr	r0, [pc, #264]	; (219330 <get_sbc_frame_len_in_jitter_buf_patch+0xac>)
  219226:	f64d fc00 	bl	66a2a <Aa7c76a00>
  21922a:	9800      	ldr	r0, [sp, #0]
  21922c:	f64f fda8 	bl	68d80 <A7dbf0000>
  219230:	483f      	ldr	r0, [pc, #252]	; (219330 <get_sbc_frame_len_in_jitter_buf_patch+0xac>)
  219232:	f64d fc29 	bl	66a88 <A265e9180>
  219236:	2800      	cmp	r0, #0
  219238:	d0f3      	beq.n	219222 <lite_host_process_event_patch+0x62>
  21923a:	04a0      	lsls	r0, r4, #18
  21923c:	d501      	bpl.n	219242 <lite_host_process_event_patch+0x82>
  21923e:	f668 fd6f 	bl	81d20 <A8b530380>
  219242:	4d36      	ldr	r5, [pc, #216]	; (21931c <get_sbc_frame_len_in_jitter_buf_patch+0x98>)
  219244:	0460      	lsls	r0, r4, #17
  219246:	d506      	bpl.n	219256 <lite_host_process_event_patch+0x96>
  219248:	f895 00c3 	ldrb.w	r0, [r5, #195]	; 0xc3
  21924c:	2806      	cmp	r0, #6
  21924e:	d002      	beq.n	219256 <lite_host_process_event_patch+0x96>
  219250:	2000      	movs	r0, #0
  219252:	f64f fb5f 	bl	68914 <A28a35e00>
  219256:	4930      	ldr	r1, [pc, #192]	; (219318 <get_sbc_frame_len_in_jitter_buf_patch+0x94>)
  219258:	f8d5 0100 	ldr.w	r0, [r5, #256]	; 0x100
  21925c:	3922      	subs	r1, #34	; 0x22
  21925e:	8a00      	ldrh	r0, [r0, #16]
  219260:	8909      	ldrh	r1, [r1, #8]
  219262:	4288      	cmp	r0, r1
  219264:	d002      	beq.n	21926c <lite_host_process_event_patch+0xac>
  219266:	b2c0      	uxtb	r0, r0
  219268:	f7ff fee9 	bl	21903e <lite_host_send_play_status_ind>
  21926c:	4831      	ldr	r0, [pc, #196]	; (219334 <get_sbc_frame_len_in_jitter_buf_patch+0xb0>)
  21926e:	6880      	ldr	r0, [r0, #8]
  219270:	03c0      	lsls	r0, r0, #15
  219272:	d505      	bpl.n	219280 <lite_host_process_event_patch+0xc0>
  219274:	2050      	movs	r0, #80	; 0x50
  219276:	f635 fa22 	bl	4e6be <A50f99400>
  21927a:	4601      	mov	r1, r0
  21927c:	4620      	mov	r0, r4
  21927e:	4788      	blx	r1
  219280:	2001      	movs	r0, #1
  219282:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}

00219284 <get_sbc_frame_len_in_jitter_buf_patch>:
  219284:	b5f0      	push	{r4, r5, r6, r7, lr}
  219286:	4603      	mov	r3, r0
  219288:	7819      	ldrb	r1, [r3, #0]
  21928a:	2000      	movs	r0, #0
  21928c:	299c      	cmp	r1, #156	; 0x9c
  21928e:	d140      	bne.n	219312 <get_sbc_frame_len_in_jitter_buf_patch+0x8e>
  219290:	4822      	ldr	r0, [pc, #136]	; (21931c <get_sbc_frame_len_in_jitter_buf_patch+0x98>)
  219292:	1c5b      	adds	r3, r3, #1
  219294:	f8d0 4100 	ldr.w	r4, [r0, #256]	; 0x100
  219298:	f8d4 c030 	ldr.w	ip, [r4, #48]	; 0x30
  21929c:	459c      	cmp	ip, r3
  21929e:	d100      	bne.n	2192a2 <get_sbc_frame_len_in_jitter_buf_patch+0x1e>
  2192a0:	6ae3      	ldr	r3, [r4, #44]	; 0x2c
  2192a2:	781d      	ldrb	r5, [r3, #0]
  2192a4:	f3c5 1001 	ubfx	r0, r5, #4, #2
  2192a8:	b318      	cbz	r0, 2192f2 <get_sbc_frame_len_in_jitter_buf_patch+0x6e>
  2192aa:	2801      	cmp	r0, #1
  2192ac:	d023      	beq.n	2192f6 <get_sbc_frame_len_in_jitter_buf_patch+0x72>
  2192ae:	2802      	cmp	r0, #2
  2192b0:	d023      	beq.n	2192fa <get_sbc_frame_len_in_jitter_buf_patch+0x76>
  2192b2:	2803      	cmp	r0, #3
  2192b4:	d100      	bne.n	2192b8 <get_sbc_frame_len_in_jitter_buf_patch+0x34>
  2192b6:	2010      	movs	r0, #16
  2192b8:	2701      	movs	r7, #1
  2192ba:	f3c5 0181 	ubfx	r1, r5, #2, #2
  2192be:	463a      	mov	r2, r7
  2192c0:	b101      	cbz	r1, 2192c4 <get_sbc_frame_len_in_jitter_buf_patch+0x40>
  2192c2:	2202      	movs	r2, #2
  2192c4:	2903      	cmp	r1, #3
  2192c6:	d000      	beq.n	2192ca <get_sbc_frame_len_in_jitter_buf_patch+0x46>
  2192c8:	2700      	movs	r7, #0
  2192ca:	2608      	movs	r6, #8
  2192cc:	07ed      	lsls	r5, r5, #31
  2192ce:	d100      	bne.n	2192d2 <get_sbc_frame_len_in_jitter_buf_patch+0x4e>
  2192d0:	2604      	movs	r6, #4
  2192d2:	1c5b      	adds	r3, r3, #1
  2192d4:	459c      	cmp	ip, r3
  2192d6:	d100      	bne.n	2192da <get_sbc_frame_len_in_jitter_buf_patch+0x56>
  2192d8:	6ae3      	ldr	r3, [r4, #44]	; 0x2c
  2192da:	2902      	cmp	r1, #2
  2192dc:	781b      	ldrb	r3, [r3, #0]
  2192de:	d20e      	bcs.n	2192fe <get_sbc_frame_len_in_jitter_buf_patch+0x7a>
  2192e0:	00b1      	lsls	r1, r6, #2
  2192e2:	4351      	muls	r1, r2
  2192e4:	4342      	muls	r2, r0
  2192e6:	08c9      	lsrs	r1, r1, #3
  2192e8:	435a      	muls	r2, r3
  2192ea:	1dd2      	adds	r2, r2, #7
  2192ec:	eb01 00d2 	add.w	r0, r1, r2, lsr #3
  2192f0:	e00e      	b.n	219310 <get_sbc_frame_len_in_jitter_buf_patch+0x8c>
  2192f2:	2004      	movs	r0, #4
  2192f4:	e7e0      	b.n	2192b8 <get_sbc_frame_len_in_jitter_buf_patch+0x34>
  2192f6:	2008      	movs	r0, #8
  2192f8:	e7de      	b.n	2192b8 <get_sbc_frame_len_in_jitter_buf_patch+0x34>
  2192fa:	200c      	movs	r0, #12
  2192fc:	e7dc      	b.n	2192b8 <get_sbc_frame_len_in_jitter_buf_patch+0x34>
  2192fe:	4377      	muls	r7, r6
  219300:	fb00 7003 	mla	r0, r0, r3, r7
  219304:	1dc0      	adds	r0, r0, #7
  219306:	08c1      	lsrs	r1, r0, #3
  219308:	00b0      	lsls	r0, r6, #2
  21930a:	4350      	muls	r0, r2
  21930c:	eb01 00d0 	add.w	r0, r1, r0, lsr #3
  219310:	1d00      	adds	r0, r0, #4
  219312:	bdf0      	pop	{r4, r5, r6, r7, pc}
  219314:	0020122c 	.word	0x0020122c
  219318:	0021fac6 	.word	0x0021fac6
  21931c:	0020b670 	.word	0x0020b670
  219320:	0020b7cc 	.word	0x0020b7cc
  219324:	1387d8f0 	.word	0x1387d8f0
  219328:	00216d38 	.word	0x00216d38
  21932c:	00201c20 	.word	0x00201c20
  219330:	0020b774 	.word	0x0020b774
  219334:	0020af84 	.word	0x0020af84

00219338 <pll_tuner_get_time_cbf>:
  219338:	b51c      	push	{r2, r3, r4, lr}
  21933a:	4604      	mov	r4, r0
  21933c:	f000 fc15 	bl	219b6a <wiced_get_nanosecond_clock_value>
  219340:	e9cd 0100 	strd	r0, r1, [sp]
  219344:	2208      	movs	r2, #8
  219346:	4669      	mov	r1, sp
  219348:	4620      	mov	r0, r4
  21934a:	f656 fb91 	bl	6fa70 <__aeabi_memcpy>
  21934e:	2000      	movs	r0, #0
  219350:	bd1c      	pop	{r2, r3, r4, pc}

00219352 <pll_tuner_start_timer_cbf>:
  219352:	f000 bc35 	b.w	219bc0 <wiced_audio_timer_enable>

00219356 <pll_tuner_stop_timer_cbf>:
  219356:	f000 bc63 	b.w	219c20 <wiced_audio_timer_disable>

0021935a <pll_tuner_wait_for_period_cbf>:
  21935a:	f000 bc78 	b.w	219c4e <wiced_audio_timer_get_frame_sync>

0021935e <pll_tuner_get_buffer_level_cbf>:
  21935e:	b57c      	push	{r2, r3, r4, r5, r6, lr}
  219360:	4605      	mov	r5, r0
  219362:	460c      	mov	r4, r1
  219364:	a801      	add	r0, sp, #4
  219366:	f000 fc94 	bl	219c92 <wiced_audio_get_current_buffer_weight>
  21936a:	2800      	cmp	r0, #0
  21936c:	d10f      	bne.n	21938e <pll_tuner_get_buffer_level_cbf+0x30>
  21936e:	9801      	ldr	r0, [sp, #4]
  219370:	6028      	str	r0, [r5, #0]
  219372:	4669      	mov	r1, sp
  219374:	f104 0024 	add.w	r0, r4, #36	; 0x24
  219378:	f000 fc90 	bl	219c9c <wiced_rtos_get_queue_occupancy>
  21937c:	2800      	cmp	r0, #0
  21937e:	d106      	bne.n	21938e <pll_tuner_get_buffer_level_cbf+0x30>
  219380:	6b21      	ldr	r1, [r4, #48]	; 0x30
  219382:	9a00      	ldr	r2, [sp, #0]
  219384:	4351      	muls	r1, r2
  219386:	9100      	str	r1, [sp, #0]
  219388:	682a      	ldr	r2, [r5, #0]
  21938a:	4411      	add	r1, r2
  21938c:	6029      	str	r1, [r5, #0]
  21938e:	bd7c      	pop	{r2, r3, r4, r5, r6, pc}

00219390 <pll_tuner_set_ppb_cbf>:
  219390:	f000 bc8a 	b.w	219ca8 <wiced_audio_set_pll_fractional_divider>

00219394 <pll_tuner_updateparams>:
  219394:	493f      	ldr	r1, [pc, #252]	; (219494 <pll_tuner_rest+0x6>)
  219396:	680a      	ldr	r2, [r1, #0]
  219398:	6002      	str	r2, [r0, #0]
  21939a:	684a      	ldr	r2, [r1, #4]
  21939c:	6042      	str	r2, [r0, #4]
  21939e:	688a      	ldr	r2, [r1, #8]
  2193a0:	6082      	str	r2, [r0, #8]
  2193a2:	68ca      	ldr	r2, [r1, #12]
  2193a4:	6142      	str	r2, [r0, #20]
  2193a6:	690a      	ldr	r2, [r1, #16]
  2193a8:	6182      	str	r2, [r0, #24]
  2193aa:	694a      	ldr	r2, [r1, #20]
  2193ac:	61c2      	str	r2, [r0, #28]
  2193ae:	6989      	ldr	r1, [r1, #24]
  2193b0:	6201      	str	r1, [r0, #32]
  2193b2:	4770      	bx	lr

002193b4 <pll_tuner_create>:
  2193b4:	b570      	push	{r4, r5, r6, lr}
  2193b6:	4d37      	ldr	r5, [pc, #220]	; (219494 <pll_tuner_rest+0x6>)
  2193b8:	4604      	mov	r4, r0
  2193ba:	1f2d      	subs	r5, r5, #4
  2193bc:	7829      	ldrb	r1, [r5, #0]
  2193be:	2000      	movs	r0, #0
  2193c0:	2900      	cmp	r1, #0
  2193c2:	d026      	beq.n	219412 <pll_tuner_create+0x5e>
  2193c4:	f104 0038 	add.w	r0, r4, #56	; 0x38
  2193c8:	4606      	mov	r6, r0
  2193ca:	f7ff ffe3 	bl	219394 <pll_tuner_updateparams>
  2193ce:	2014      	movs	r0, #20
  2193d0:	6460      	str	r0, [r4, #68]	; 0x44
  2193d2:	200f      	movs	r0, #15
  2193d4:	64a0      	str	r0, [r4, #72]	; 0x48
  2193d6:	65e4      	str	r4, [r4, #92]	; 0x5c
  2193d8:	f2af 0089 	subw	r0, pc, #137	; 0x89
  2193dc:	6620      	str	r0, [r4, #96]	; 0x60
  2193de:	f2af 0089 	subw	r0, pc, #137	; 0x89
  2193e2:	6660      	str	r0, [r4, #100]	; 0x64
  2193e4:	f2af 008d 	subw	r0, pc, #141	; 0x8d
  2193e8:	66a0      	str	r0, [r4, #104]	; 0x68
  2193ea:	f2af 008d 	subw	r0, pc, #141	; 0x8d
  2193ee:	66e0      	str	r0, [r4, #108]	; 0x6c
  2193f0:	f2af 0063 	subw	r0, pc, #99	; 0x63
  2193f4:	6720      	str	r0, [r4, #112]	; 0x70
  2193f6:	f2af 00bf 	subw	r0, pc, #191	; 0xbf
  2193fa:	6760      	str	r0, [r4, #116]	; 0x74
  2193fc:	f000 fba6 	bl	219b4c <wiced_init_nanosecond_clock>
  219400:	f104 0134 	add.w	r1, r4, #52	; 0x34
  219404:	4630      	mov	r0, r6
  219406:	f000 fac3 	bl	219990 <audio_pll_tuner_init>
  21940a:	2800      	cmp	r0, #0
  21940c:	d101      	bne.n	219412 <pll_tuner_create+0x5e>
  21940e:	2100      	movs	r1, #0
  219410:	7029      	strb	r1, [r5, #0]
  219412:	bd70      	pop	{r4, r5, r6, pc}

00219414 <pll_tuner_destroy>:
  219414:	b510      	push	{r4, lr}
  219416:	6b40      	ldr	r0, [r0, #52]	; 0x34
  219418:	f000 fb09 	bl	219a2e <audio_pll_tuner_deinit>
  21941c:	4604      	mov	r4, r0
  21941e:	f000 fb9a 	bl	219b56 <wiced_deinit_nanosecond_clock>
  219422:	4620      	mov	r0, r4
  219424:	bd10      	pop	{r4, pc}

00219426 <pll_tuner_run>:
  219426:	b510      	push	{r4, lr}
  219428:	4604      	mov	r4, r0
  21942a:	f890 0088 	ldrb.w	r0, [r0, #136]	; 0x88
  21942e:	f8d4 1084 	ldr.w	r1, [r4, #132]	; 0x84
  219432:	2264      	movs	r2, #100	; 0x64
  219434:	4348      	muls	r0, r1
  219436:	fbb0 f1f2 	udiv	r1, r0, r2
  21943a:	6aa0      	ldr	r0, [r4, #40]	; 0x28
  21943c:	67e0      	str	r0, [r4, #124]	; 0x7c
  21943e:	fbb0 f0f2 	udiv	r0, r0, r2
  219442:	f894 302c 	ldrb.w	r3, [r4, #44]	; 0x2c
  219446:	f884 3080 	strb.w	r3, [r4, #128]	; 0x80
  21944a:	f894 302d 	ldrb.w	r3, [r4, #45]	; 0x2d
  21944e:	f884 3081 	strb.w	r3, [r4, #129]	; 0x81
  219452:	4348      	muls	r0, r1
  219454:	6b21      	ldr	r1, [r4, #48]	; 0x30
  219456:	4348      	muls	r0, r1
  219458:	210a      	movs	r1, #10
  21945a:	fbb0 f0f1 	udiv	r0, r0, r1
  21945e:	67a0      	str	r0, [r4, #120]	; 0x78
  219460:	480d      	ldr	r0, [pc, #52]	; (219498 <pll_tuner_rest+0xa>)
  219462:	6b61      	ldr	r1, [r4, #52]	; 0x34
  219464:	7800      	ldrb	r0, [r0, #0]
  219466:	f881 015a 	strb.w	r0, [r1, #346]	; 0x15a
  21946a:	6b61      	ldr	r1, [r4, #52]	; 0x34
  21946c:	2000      	movs	r0, #0
  21946e:	f8c1 015c 	str.w	r0, [r1, #348]	; 0x15c
  219472:	6b60      	ldr	r0, [r4, #52]	; 0x34
  219474:	f500 7088 	add.w	r0, r0, #272	; 0x110
  219478:	f7ff ff8c 	bl	219394 <pll_tuner_updateparams>
  21947c:	f000 fb70 	bl	219b60 <wiced_reset_nanosecond_clock>
  219480:	6b60      	ldr	r0, [r4, #52]	; 0x34
  219482:	f104 0178 	add.w	r1, r4, #120	; 0x78
  219486:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21948a:	f000 badf 	b.w	219a4c <audio_pll_tuner_start>

0021948e <pll_tuner_rest>:
  21948e:	6b40      	ldr	r0, [r0, #52]	; 0x34
  219490:	f000 baf9 	b.w	219a86 <audio_pll_tuner_stop>
  219494:	0021fad8 	.word	0x0021fad8
  219498:	0021fbb0 	.word	0x0021fbb0

0021949c <apply_ppm_adjustments>:
  21949c:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  2194a0:	460d      	mov	r5, r1
  2194a2:	682c      	ldr	r4, [r5, #0]
  2194a4:	2100      	movs	r1, #0
  2194a6:	1aa6      	subs	r6, r4, r2
  2194a8:	1e37      	subs	r7, r6, #0
  2194aa:	da00      	bge.n	2194ae <apply_ppm_adjustments+0x12>
  2194ac:	4277      	negs	r7, r6
  2194ae:	f5b7 7ffa 	cmp.w	r7, #500	; 0x1f4
  2194b2:	da06      	bge.n	2194c2 <apply_ppm_adjustments+0x26>
  2194b4:	f8d0 2d6c 	ldr.w	r2, [r0, #3436]	; 0xd6c
  2194b8:	bb92      	cbnz	r2, 219520 <apply_ppm_adjustments+0x84>
  2194ba:	2201      	movs	r2, #1
  2194bc:	f8c0 2d6c 	str.w	r2, [r0, #3436]	; 0xd6c
  2194c0:	e02e      	b.n	219520 <apply_ppm_adjustments+0x84>
  2194c2:	2700      	movs	r7, #0
  2194c4:	4294      	cmp	r4, r2
  2194c6:	da07      	bge.n	2194d8 <apply_ppm_adjustments+0x3c>
  2194c8:	1b12      	subs	r2, r2, r4
  2194ca:	f8c0 7d6c 	str.w	r7, [r0, #3436]	; 0xd6c
  2194ce:	4293      	cmp	r3, r2
  2194d0:	db00      	blt.n	2194d4 <apply_ppm_adjustments+0x38>
  2194d2:	4613      	mov	r3, r2
  2194d4:	441c      	add	r4, r3
  2194d6:	e007      	b.n	2194e8 <apply_ppm_adjustments+0x4c>
  2194d8:	42a2      	cmp	r2, r4
  2194da:	da05      	bge.n	2194e8 <apply_ppm_adjustments+0x4c>
  2194dc:	f8c0 7d6c 	str.w	r7, [r0, #3436]	; 0xd6c
  2194e0:	42b3      	cmp	r3, r6
  2194e2:	db00      	blt.n	2194e6 <apply_ppm_adjustments+0x4a>
  2194e4:	4633      	mov	r3, r6
  2194e6:	1ae4      	subs	r4, r4, r3
  2194e8:	f8d0 2114 	ldr.w	r2, [r0, #276]	; 0x114
  2194ec:	eb02 0342 	add.w	r3, r2, r2, lsl #1
  2194f0:	ebc3 12c2 	rsb	r2, r3, r2, lsl #7
  2194f4:	ebb4 0fc2 	cmp.w	r4, r2, lsl #3
  2194f8:	db12      	blt.n	219520 <apply_ppm_adjustments+0x84>
  2194fa:	f8d0 2110 	ldr.w	r2, [r0, #272]	; 0x110
  2194fe:	eb02 0342 	add.w	r3, r2, r2, lsl #1
  219502:	ebc3 12c2 	rsb	r2, r3, r2, lsl #7
  219506:	ebb4 0fc2 	cmp.w	r4, r2, lsl #3
  21950a:	dc09      	bgt.n	219520 <apply_ppm_adjustments+0x84>
  21950c:	f8d0 1134 	ldr.w	r1, [r0, #308]	; 0x134
  219510:	f8d0 2148 	ldr.w	r2, [r0, #328]	; 0x148
  219514:	4620      	mov	r0, r4
  219516:	4790      	blx	r2
  219518:	0001      	movs	r1, r0
  21951a:	d101      	bne.n	219520 <apply_ppm_adjustments+0x84>
  21951c:	602c      	str	r4, [r5, #0]
  21951e:	2104      	movs	r1, #4
  219520:	4608      	mov	r0, r1
  219522:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

00219526 <get_level_correction_ppb>:
  219526:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
  21952a:	4691      	mov	r9, r2
  21952c:	469a      	mov	sl, r3
  21952e:	e9dd 2308 	ldrd	r2, r3, [sp, #32]
  219532:	4604      	mov	r4, r0
  219534:	f8d4 5150 	ldr.w	r5, [r4, #336]	; 0x150
  219538:	2000      	movs	r0, #0
  21953a:	1b4d      	subs	r5, r1, r5
  21953c:	601d      	str	r5, [r3, #0]
  21953e:	d01d      	beq.n	21957c <get_level_correction_ppb+0x56>
  219540:	2700      	movs	r7, #0
  219542:	f8df 8560 	ldr.w	r8, [pc, #1376]	; 219aa4 <audio_pll_tuner_stop+0x1e>
  219546:	17ee      	asrs	r6, r5, #31
  219548:	463b      	mov	r3, r7
  21954a:	4639      	mov	r1, r7
  21954c:	4640      	mov	r0, r8
  21954e:	f6a4 fc72 	bl	bde36 <A87a833c0>
  219552:	f8d4 2154 	ldr.w	r2, [r4, #340]	; 0x154
  219556:	463b      	mov	r3, r7
  219558:	f6a4 fc6d 	bl	bde36 <A87a833c0>
  21955c:	fba0 2305 	umull	r2, r3, r0, r5
  219560:	fb01 3105 	mla	r1, r1, r5, r3
  219564:	fb00 1306 	mla	r3, r0, r6, r1
  219568:	fba8 0202 	umull	r0, r2, r8, r2
  21956c:	fb08 2103 	mla	r1, r8, r3, r2
  219570:	4653      	mov	r3, sl
  219572:	464a      	mov	r2, r9
  219574:	e8bd 47f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
  219578:	f6a4 bc5d 	b.w	bde36 <A87a833c0>
  21957c:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}

00219580 <compute_sum>:
  219580:	b5f0      	push	{r4, r5, r6, r7, lr}
  219582:	4616      	mov	r6, r2
  219584:	b087      	sub	sp, #28
  219586:	4607      	mov	r7, r0
  219588:	460d      	mov	r5, r1
  21958a:	2208      	movs	r2, #8
  21958c:	f656 fa70 	bl	6fa70 <__aeabi_memcpy>
  219590:	2401      	movs	r4, #1
  219592:	e016      	b.n	2195c2 <compute_sum+0x42>
  219594:	eb05 01c4 	add.w	r1, r5, r4, lsl #3
  219598:	2208      	movs	r2, #8
  21959a:	a805      	add	r0, sp, #20
  21959c:	f656 fa68 	bl	6fa70 <__aeabi_memcpy>
  2195a0:	2208      	movs	r2, #8
  2195a2:	4639      	mov	r1, r7
  2195a4:	a803      	add	r0, sp, #12
  2195a6:	f656 fa63 	bl	6fa70 <__aeabi_memcpy>
  2195aa:	a803      	add	r0, sp, #12
  2195ac:	c80f      	ldmia	r0, {r0, r1, r2, r3}
  2195ae:	1880      	adds	r0, r0, r2
  2195b0:	4159      	adcs	r1, r3
  2195b2:	e9cd 0101 	strd	r0, r1, [sp, #4]
  2195b6:	2208      	movs	r2, #8
  2195b8:	a901      	add	r1, sp, #4
  2195ba:	4638      	mov	r0, r7
  2195bc:	f656 fa58 	bl	6fa70 <__aeabi_memcpy>
  2195c0:	1c64      	adds	r4, r4, #1
  2195c2:	42b4      	cmp	r4, r6
  2195c4:	d3e6      	bcc.n	219594 <compute_sum+0x14>
  2195c6:	b007      	add	sp, #28
  2195c8:	bdf0      	pop	{r4, r5, r6, r7, pc}

002195ca <compute_sliding_sum>:
  2195ca:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  2195ce:	b089      	sub	sp, #36	; 0x24
  2195d0:	4689      	mov	r9, r1
  2195d2:	4606      	mov	r6, r0
  2195d4:	4692      	mov	sl, r2
  2195d6:	2400      	movs	r4, #0
  2195d8:	4601      	mov	r1, r0
  2195da:	469b      	mov	fp, r3
  2195dc:	4625      	mov	r5, r4
  2195de:	2208      	movs	r2, #8
  2195e0:	a806      	add	r0, sp, #24
  2195e2:	f8dd 8048 	ldr.w	r8, [sp, #72]	; 0x48
  2195e6:	f656 fa43 	bl	6fa70 <__aeabi_memcpy>
  2195ea:	4630      	mov	r0, r6
  2195ec:	4642      	mov	r2, r8
  2195ee:	4649      	mov	r1, r9
  2195f0:	f7ff ffc6 	bl	219580 <compute_sum>
  2195f4:	eb04 0008 	add.w	r0, r4, r8
  2195f8:	eb09 01c0 	add.w	r1, r9, r0, lsl #3
  2195fc:	2208      	movs	r2, #8
  2195fe:	a802      	add	r0, sp, #8
  219600:	f656 fa36 	bl	6fa70 <__aeabi_memcpy>
  219604:	eb06 07c5 	add.w	r7, r6, r5, lsl #3
  219608:	2208      	movs	r2, #8
  21960a:	4639      	mov	r1, r7
  21960c:	4668      	mov	r0, sp
  21960e:	f656 fa2f 	bl	6fa70 <__aeabi_memcpy>
  219612:	e9dd 2306 	ldrd	r2, r3, [sp, #24]
  219616:	e9dd 0100 	ldrd	r0, r1, [sp]
  21961a:	1a80      	subs	r0, r0, r2
  21961c:	eb61 0103 	sbc.w	r1, r1, r3
  219620:	e9dd 2302 	ldrd	r2, r3, [sp, #8]
  219624:	1880      	adds	r0, r0, r2
  219626:	4159      	adcs	r1, r3
  219628:	e9cd 0104 	strd	r0, r1, [sp, #16]
  21962c:	3708      	adds	r7, #8
  21962e:	2208      	movs	r2, #8
  219630:	4639      	mov	r1, r7
  219632:	a806      	add	r0, sp, #24
  219634:	f656 fa1c 	bl	6fa70 <__aeabi_memcpy>
  219638:	2208      	movs	r2, #8
  21963a:	a904      	add	r1, sp, #16
  21963c:	4638      	mov	r0, r7
  21963e:	f656 fa17 	bl	6fa70 <__aeabi_memcpy>
  219642:	1c6d      	adds	r5, r5, #1
  219644:	1c64      	adds	r4, r4, #1
  219646:	1c68      	adds	r0, r5, #1
  219648:	4550      	cmp	r0, sl
  21964a:	d203      	bcs.n	219654 <compute_sliding_sum+0x8a>
  21964c:	eb04 0008 	add.w	r0, r4, r8
  219650:	4558      	cmp	r0, fp
  219652:	d3cf      	bcc.n	2195f4 <compute_sliding_sum+0x2a>
  219654:	b009      	add	sp, #36	; 0x24
  219656:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

0021965a <pll_tuner_lowpass_filter>:
  21965a:	f8d0 215c 	ldr.w	r2, [r0, #348]	; 0x15c
  21965e:	4411      	add	r1, r2
  219660:	f890 215a 	ldrb.w	r2, [r0, #346]	; 0x15a
  219664:	fa21 f302 	lsr.w	r3, r1, r2
  219668:	1ac9      	subs	r1, r1, r3
  21966a:	f8c0 115c 	str.w	r1, [r0, #348]	; 0x15c
  21966e:	fa21 f002 	lsr.w	r0, r1, r2
  219672:	4770      	bx	lr

00219674 <get_average_buffer_level_bytes>:
  219674:	b5f0      	push	{r4, r5, r6, r7, lr}
  219676:	b085      	sub	sp, #20
  219678:	2400      	movs	r4, #0
  21967a:	9404      	str	r4, [sp, #16]
  21967c:	4605      	mov	r5, r0
  21967e:	f8d0 1134 	ldr.w	r1, [r0, #308]	; 0x134
  219682:	f8d0 2144 	ldr.w	r2, [r0, #324]	; 0x144
  219686:	f04f 36ff 	mov.w	r6, #4294967295
  21968a:	a804      	add	r0, sp, #16
  21968c:	4790      	blx	r2
  21968e:	b110      	cbz	r0, 219696 <get_average_buffer_level_bytes+0x22>
  219690:	f8c5 4160 	str.w	r4, [r5, #352]	; 0x160
  219694:	e047      	b.n	219726 <get_average_buffer_level_bytes+0xb2>
  219696:	f895 015a 	ldrb.w	r0, [r5, #346]	; 0x15a
  21969a:	b908      	cbnz	r0, 2196a0 <get_average_buffer_level_bytes+0x2c>
  21969c:	9804      	ldr	r0, [sp, #16]
  21969e:	e004      	b.n	2196aa <get_average_buffer_level_bytes+0x36>
  2196a0:	4628      	mov	r0, r5
  2196a2:	9904      	ldr	r1, [sp, #16]
  2196a4:	f7ff ffd9 	bl	21965a <pll_tuner_lowpass_filter>
  2196a8:	9004      	str	r0, [sp, #16]
  2196aa:	e9cd 0402 	strd	r0, r4, [sp, #8]
  2196ae:	f8d5 0160 	ldr.w	r0, [r5, #352]	; 0x160
  2196b2:	2208      	movs	r2, #8
  2196b4:	eb05 00c0 	add.w	r0, r5, r0, lsl #3
  2196b8:	f500 70b4 	add.w	r0, r0, #360	; 0x168
  2196bc:	a902      	add	r1, sp, #8
  2196be:	f656 f9d7 	bl	6fa70 <__aeabi_memcpy>
  2196c2:	f8d5 0160 	ldr.w	r0, [r5, #352]	; 0x160
  2196c6:	1c40      	adds	r0, r0, #1
  2196c8:	f8c5 0160 	str.w	r0, [r5, #352]	; 0x160
  2196cc:	f5b0 7fc0 	cmp.w	r0, #384	; 0x180
  2196d0:	d129      	bne.n	219726 <get_average_buffer_level_bytes+0xb2>
  2196d2:	9402      	str	r4, [sp, #8]
  2196d4:	9403      	str	r4, [sp, #12]
  2196d6:	f8c5 4160 	str.w	r4, [r5, #352]	; 0x160
  2196da:	4626      	mov	r6, r4
  2196dc:	2400      	movs	r4, #0
  2196de:	f505 75b4 	add.w	r5, r5, #360	; 0x168
  2196e2:	2340      	movs	r3, #64	; 0x40
  2196e4:	eba4 1284 	sub.w	r2, r4, r4, lsl #6
  2196e8:	9300      	str	r3, [sp, #0]
  2196ea:	f502 73c0 	add.w	r3, r2, #384	; 0x180
  2196ee:	f202 1241 	addw	r2, r2, #321	; 0x141
  2196f2:	4629      	mov	r1, r5
  2196f4:	4628      	mov	r0, r5
  2196f6:	f7ff ff68 	bl	2195ca <compute_sliding_sum>
  2196fa:	1c64      	adds	r4, r4, #1
  2196fc:	2c06      	cmp	r4, #6
  2196fe:	d3f0      	bcc.n	2196e2 <get_average_buffer_level_bytes+0x6e>
  219700:	eba4 1284 	sub.w	r2, r4, r4, lsl #6
  219704:	f502 72c0 	add.w	r2, r2, #384	; 0x180
  219708:	4629      	mov	r1, r5
  21970a:	a802      	add	r0, sp, #8
  21970c:	f7ff ff38 	bl	219580 <compute_sum>
  219710:	9803      	ldr	r0, [sp, #12]
  219712:	4631      	mov	r1, r6
  219714:	0940      	lsrs	r0, r0, #5
  219716:	2300      	movs	r3, #0
  219718:	2203      	movs	r2, #3
  21971a:	9002      	str	r0, [sp, #8]
  21971c:	f660 fb18 	bl	79d50 <__aeabi_uldivmod>
  219720:	4606      	mov	r6, r0
  219722:	e9cd 0102 	strd	r0, r1, [sp, #8]
  219726:	4630      	mov	r0, r6
  219728:	b005      	add	sp, #20
  21972a:	bdf0      	pop	{r4, r5, r6, r7, pc}

0021972c <clear_sum>:
  21972c:	f44f 6140 	mov.w	r1, #3072	; 0xc00
  219730:	f500 70b4 	add.w	r0, r0, #360	; 0x168
  219734:	f64d b918 	b.w	66968 <__aeabi_memclr>

00219738 <buffer_level_driven_task>:
  219738:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21973c:	f04f 0900 	mov.w	r9, #0
  219740:	b08f      	sub	sp, #60	; 0x3c
  219742:	f8cd 9024 	str.w	r9, [sp, #36]	; 0x24
  219746:	f8cd 9028 	str.w	r9, [sp, #40]	; 0x28
  21974a:	f8cd 9020 	str.w	r9, [sp, #32]
  21974e:	f8cd 901c 	str.w	r9, [sp, #28]
  219752:	f8cd 9018 	str.w	r9, [sp, #24]
  219756:	f8cd 9010 	str.w	r9, [sp, #16]
  21975a:	f8c0 9108 	str.w	r9, [r0, #264]	; 0x108
  21975e:	f8c0 910c 	str.w	r9, [r0, #268]	; 0x10c
  219762:	4604      	mov	r4, r0
  219764:	f8c0 9160 	str.w	r9, [r0, #352]	; 0x160
  219768:	464e      	mov	r6, r9
  21976a:	46ca      	mov	sl, r9
  21976c:	46cb      	mov	fp, r9
  21976e:	f44f 6140 	mov.w	r1, #3072	; 0xc00
  219772:	f500 70b4 	add.w	r0, r0, #360	; 0x168
  219776:	f64d f8f7 	bl	66968 <__aeabi_memclr>
  21977a:	f894 0159 	ldrb.w	r0, [r4, #345]	; 0x159
  21977e:	f894 1158 	ldrb.w	r1, [r4, #344]	; 0x158
  219782:	4348      	muls	r0, r1
  219784:	08c0      	lsrs	r0, r0, #3
  219786:	9005      	str	r0, [sp, #20]
  219788:	f44f 7080 	mov.w	r0, #256	; 0x100
  21978c:	f8d4 2138 	ldr.w	r2, [r4, #312]	; 0x138
  219790:	4607      	mov	r7, r0
  219792:	f8d4 1134 	ldr.w	r1, [r4, #308]	; 0x134
  219796:	4790      	blx	r2
  219798:	0005      	movs	r5, r0
  21979a:	d17e      	bne.n	21989a <buffer_level_driven_task+0x162>
  21979c:	f8d4 1118 	ldr.w	r1, [r4, #280]	; 0x118
  2197a0:	f44f 707a 	mov.w	r0, #1000	; 0x3e8
  2197a4:	17ca      	asrs	r2, r1, #31
  2197a6:	fba7 1301 	umull	r1, r3, r7, r1
  2197aa:	fba0 5101 	umull	r5, r1, r0, r1
  2197ae:	fb07 3202 	mla	r2, r7, r2, r3
  2197b2:	fb00 1102 	mla	r1, r0, r2, r1
  2197b6:	f8d4 2154 	ldr.w	r2, [r4, #340]	; 0x154
  2197ba:	4633      	mov	r3, r6
  2197bc:	4628      	mov	r0, r5
  2197be:	f6a4 fb3a 	bl	bde36 <A87a833c0>
  2197c2:	f8c4 0d68 	str.w	r0, [r4, #3432]	; 0xd68
  2197c6:	f000 f9cb 	bl	219b60 <wiced_reset_nanosecond_clock>
  2197ca:	f8d4 214c 	ldr.w	r2, [r4, #332]	; 0x14c
  2197ce:	f8d4 1134 	ldr.w	r1, [r4, #308]	; 0x134
  2197d2:	a802      	add	r0, sp, #8
  2197d4:	4790      	blx	r2
  2197d6:	0005      	movs	r5, r0
  2197d8:	d1df      	bne.n	21979a <buffer_level_driven_task+0x62>
  2197da:	e0a9      	b.n	219930 <buffer_level_driven_task+0x1f8>
  2197dc:	f8d4 2140 	ldr.w	r2, [r4, #320]	; 0x140
  2197e0:	f8d4 1134 	ldr.w	r1, [r4, #308]	; 0x134
  2197e4:	2064      	movs	r0, #100	; 0x64
  2197e6:	4790      	blx	r2
  2197e8:	4605      	mov	r5, r0
  2197ea:	2801      	cmp	r0, #1
  2197ec:	d055      	beq.n	21989a <buffer_level_driven_task+0x162>
  2197ee:	2d06      	cmp	r5, #6
  2197f0:	d0f3      	beq.n	2197da <buffer_level_driven_task+0xa2>
  2197f2:	9804      	ldr	r0, [sp, #16]
  2197f4:	b948      	cbnz	r0, 21980a <buffer_level_driven_task+0xd2>
  2197f6:	2001      	movs	r0, #1
  2197f8:	9004      	str	r0, [sp, #16]
  2197fa:	f8d4 2148 	ldr.w	r2, [r4, #328]	; 0x148
  2197fe:	f8d4 1134 	ldr.w	r1, [r4, #308]	; 0x134
  219802:	2000      	movs	r0, #0
  219804:	4790      	blx	r2
  219806:	0005      	movs	r5, r0
  219808:	d1c7      	bne.n	21979a <buffer_level_driven_task+0x62>
  21980a:	4620      	mov	r0, r4
  21980c:	f7ff ff32 	bl	219674 <get_average_buffer_level_bytes>
  219810:	f1b0 0800 	subs.w	r8, r0, #0
  219814:	da07      	bge.n	219826 <buffer_level_driven_task+0xee>
  219816:	f8d4 3d68 	ldr.w	r3, [r4, #3432]	; 0xd68
  21981a:	a909      	add	r1, sp, #36	; 0x24
  21981c:	4620      	mov	r0, r4
  21981e:	9a0a      	ldr	r2, [sp, #40]	; 0x28
  219820:	f7ff fe3c 	bl	21949c <apply_ppm_adjustments>
  219824:	e084      	b.n	219930 <buffer_level_driven_task+0x1f8>
  219826:	e9dd 6702 	ldrd	r6, r7, [sp, #8]
  21982a:	f8d4 214c 	ldr.w	r2, [r4, #332]	; 0x14c
  21982e:	f8d4 1134 	ldr.w	r1, [r4, #308]	; 0x134
  219832:	a802      	add	r0, sp, #8
  219834:	4790      	blx	r2
  219836:	0005      	movs	r5, r0
  219838:	d12f      	bne.n	21989a <buffer_level_driven_task+0x162>
  21983a:	e9dd 0102 	ldrd	r0, r1, [sp, #8]
  21983e:	1b80      	subs	r0, r0, r6
  219840:	ab07      	add	r3, sp, #28
  219842:	9a05      	ldr	r2, [sp, #20]
  219844:	e9cd 2300 	strd	r2, r3, [sp]
  219848:	eb61 0107 	sbc.w	r1, r1, r7
  21984c:	460b      	mov	r3, r1
  21984e:	4602      	mov	r2, r0
  219850:	4641      	mov	r1, r8
  219852:	4620      	mov	r0, r4
  219854:	f7ff fe67 	bl	219526 <get_level_correction_ppb>
  219858:	4680      	mov	r8, r0
  21985a:	f1b9 0f03 	cmp.w	r9, #3
  21985e:	d205      	bcs.n	21986c <buffer_level_driven_task+0x134>
  219860:	a90b      	add	r1, sp, #44	; 0x2c
  219862:	f841 0029 	str.w	r0, [r1, r9, lsl #2]
  219866:	f109 0901 	add.w	r9, r9, #1
  21986a:	e061      	b.n	219930 <buffer_level_driven_task+0x1f8>
  21986c:	2100      	movs	r1, #0
  21986e:	aa0b      	add	r2, sp, #44	; 0x2c
  219870:	eb02 0381 	add.w	r3, r2, r1, lsl #2
  219874:	685b      	ldr	r3, [r3, #4]
  219876:	f842 3021 	str.w	r3, [r2, r1, lsl #2]
  21987a:	1c49      	adds	r1, r1, #1
  21987c:	2902      	cmp	r1, #2
  21987e:	d3f7      	bcc.n	219870 <buffer_level_driven_task+0x138>
  219880:	2300      	movs	r3, #0
  219882:	f842 0021 	str.w	r0, [r2, r1, lsl #2]
  219886:	469c      	mov	ip, r3
  219888:	4619      	mov	r1, r3
  21988a:	4696      	mov	lr, r2
  21988c:	f8d4 2124 	ldr.w	r2, [r4, #292]	; 0x124
  219890:	f85e 6021 	ldr.w	r6, [lr, r1, lsl #2]
  219894:	eb02 0742 	add.w	r7, r2, r2, lsl #1
  219898:	e000      	b.n	21989c <buffer_level_driven_task+0x164>
  21989a:	e051      	b.n	219940 <buffer_level_driven_task+0x208>
  21989c:	ebc7 12c2 	rsb	r2, r7, r2, lsl #7
  2198a0:	ebb6 0fc2 	cmp.w	r6, r2, lsl #3
  2198a4:	da08      	bge.n	2198b8 <buffer_level_driven_task+0x180>
  2198a6:	f8d4 2128 	ldr.w	r2, [r4, #296]	; 0x128
  2198aa:	eb02 0742 	add.w	r7, r2, r2, lsl #1
  2198ae:	ebc7 12c2 	rsb	r2, r7, r2, lsl #7
  2198b2:	ebb6 0fc2 	cmp.w	r6, r2, lsl #3
  2198b6:	dc01      	bgt.n	2198bc <buffer_level_driven_task+0x184>
  2198b8:	1c5b      	adds	r3, r3, #1
  2198ba:	e001      	b.n	2198c0 <buffer_level_driven_task+0x188>
  2198bc:	f10c 0c01 	add.w	ip, ip, #1
  2198c0:	1c49      	adds	r1, r1, #1
  2198c2:	2903      	cmp	r1, #3
  2198c4:	d3e2      	bcc.n	21988c <buffer_level_driven_task+0x154>
  2198c6:	2b03      	cmp	r3, #3
  2198c8:	d101      	bne.n	2198ce <buffer_level_driven_task+0x196>
  2198ca:	f04f 0b01 	mov.w	fp, #1
  2198ce:	f1bc 0f03 	cmp.w	ip, #3
  2198d2:	d102      	bne.n	2198da <buffer_level_driven_task+0x1a2>
  2198d4:	f04f 0b00 	mov.w	fp, #0
  2198d8:	e002      	b.n	2198e0 <buffer_level_driven_task+0x1a8>
  2198da:	f1bb 0f00 	cmp.w	fp, #0
  2198de:	d116      	bne.n	21990e <buffer_level_driven_task+0x1d6>
  2198e0:	9807      	ldr	r0, [sp, #28]
  2198e2:	b150      	cbz	r0, 2198fa <buffer_level_driven_task+0x1c2>
  2198e4:	f8d4 012c 	ldr.w	r0, [r4, #300]	; 0x12c
  2198e8:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  2198ec:	fb00 f008 	mul.w	r0, r0, r8
  2198f0:	fb90 f0f1 	sdiv	r0, r0, r1
  2198f4:	9908      	ldr	r1, [sp, #32]
  2198f6:	4441      	add	r1, r8
  2198f8:	e000      	b.n	2198fc <buffer_level_driven_task+0x1c4>
  2198fa:	9908      	ldr	r1, [sp, #32]
  2198fc:	9106      	str	r1, [sp, #24]
  2198fe:	f8d4 2130 	ldr.w	r2, [r4, #304]	; 0x130
  219902:	434a      	muls	r2, r1
  219904:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  219908:	fb92 f1f1 	sdiv	r1, r2, r1
  21990c:	e000      	b.n	219910 <buffer_level_driven_task+0x1d8>
  21990e:	2100      	movs	r1, #0
  219910:	4408      	add	r0, r1
  219912:	900a      	str	r0, [sp, #40]	; 0x28
  219914:	f11a 0a01 	adds.w	sl, sl, #1
  219918:	d001      	beq.n	21991e <buffer_level_driven_task+0x1e6>
  21991a:	f04f 0a00 	mov.w	sl, #0
  21991e:	f8d4 3d68 	ldr.w	r3, [r4, #3432]	; 0xd68
  219922:	a909      	add	r1, sp, #36	; 0x24
  219924:	4620      	mov	r0, r4
  219926:	9a0a      	ldr	r2, [sp, #40]	; 0x28
  219928:	f7ff fdb8 	bl	21949c <apply_ppm_adjustments>
  21992c:	9806      	ldr	r0, [sp, #24]
  21992e:	9008      	str	r0, [sp, #32]
  219930:	f8d4 0108 	ldr.w	r0, [r4, #264]	; 0x108
  219934:	b920      	cbnz	r0, 219940 <buffer_level_driven_task+0x208>
  219936:	f8d4 010c 	ldr.w	r0, [r4, #268]	; 0x10c
  21993a:	2800      	cmp	r0, #0
  21993c:	f43f af4e 	beq.w	2197dc <buffer_level_driven_task+0xa4>
  219940:	f8d4 113c 	ldr.w	r1, [r4, #316]	; 0x13c
  219944:	f8d4 0134 	ldr.w	r0, [r4, #308]	; 0x134
  219948:	4788      	blx	r1
  21994a:	4628      	mov	r0, r5
  21994c:	b00f      	add	sp, #60	; 0x3c
  21994e:	e682      	b.n	219656 <compute_sliding_sum+0x8c>

00219950 <audio_pll_tuner_task>:
  219950:	b5fe      	push	{r1, r2, r3, r4, r5, r6, r7, lr}
  219952:	2500      	movs	r5, #0
  219954:	1e6e      	subs	r6, r5, #1
  219956:	4604      	mov	r4, r0
  219958:	0877      	lsrs	r7, r6, #1
  21995a:	e9cd 6501 	strd	r6, r5, [sp, #4]
  21995e:	2200      	movs	r2, #0
  219960:	9200      	str	r2, [sp, #0]
  219962:	2301      	movs	r3, #1
  219964:	aa02      	add	r2, sp, #8
  219966:	4639      	mov	r1, r7
  219968:	1d20      	adds	r0, r4, #4
  21996a:	f000 f9c5 	bl	219cf8 <wiced_rtos_wait_for_event_flags>
  21996e:	2800      	cmp	r0, #0
  219970:	d1f3      	bne.n	21995a <audio_pll_tuner_task+0xa>
  219972:	9802      	ldr	r0, [sp, #8]
  219974:	f010 0f0c 	tst.w	r0, #12
  219978:	d002      	beq.n	219980 <audio_pll_tuner_task+0x30>
  21997a:	0700      	lsls	r0, r0, #28
  21997c:	d5ed      	bpl.n	21995a <audio_pll_tuner_task+0xa>
  21997e:	bdfe      	pop	{r1, r2, r3, r4, r5, r6, r7, pc}
  219980:	0780      	lsls	r0, r0, #30
  219982:	d5ea      	bpl.n	21995a <audio_pll_tuner_task+0xa>
  219984:	4620      	mov	r0, r4
  219986:	f7ff fed7 	bl	219738 <buffer_level_driven_task>
  21998a:	2800      	cmp	r0, #0
  21998c:	d0e5      	beq.n	21995a <audio_pll_tuner_task+0xa>
  21998e:	bdfe      	pop	{r1, r2, r3, r4, r5, r6, r7, pc}

00219990 <audio_pll_tuner_init>:
  219990:	b5fe      	push	{r1, r2, r3, r4, r5, r6, r7, lr}
  219992:	0004      	movs	r4, r0
  219994:	460f      	mov	r7, r1
  219996:	d047      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  219998:	2f00      	cmp	r7, #0
  21999a:	d045      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  21999c:	2000      	movs	r0, #0
  21999e:	6038      	str	r0, [r7, #0]
  2199a0:	6aa2      	ldr	r2, [r4, #40]	; 0x28
  2199a2:	2a00      	cmp	r2, #0
  2199a4:	d040      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199a6:	6ae0      	ldr	r0, [r4, #44]	; 0x2c
  2199a8:	2800      	cmp	r0, #0
  2199aa:	d03d      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199ac:	6b20      	ldr	r0, [r4, #48]	; 0x30
  2199ae:	2800      	cmp	r0, #0
  2199b0:	d03a      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199b2:	6ba0      	ldr	r0, [r4, #56]	; 0x38
  2199b4:	2800      	cmp	r0, #0
  2199b6:	d037      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199b8:	6b60      	ldr	r0, [r4, #52]	; 0x34
  2199ba:	2800      	cmp	r0, #0
  2199bc:	d034      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199be:	6be0      	ldr	r0, [r4, #60]	; 0x3c
  2199c0:	2800      	cmp	r0, #0
  2199c2:	d031      	beq.n	219a28 <audio_pll_tuner_init+0x98>
  2199c4:	6a61      	ldr	r1, [r4, #36]	; 0x24
  2199c6:	f44f 7080 	mov.w	r0, #256	; 0x100
  2199ca:	4790      	blx	r2
  2199cc:	0006      	movs	r6, r0
  2199ce:	d12c      	bne.n	219a2a <audio_pll_tuner_init+0x9a>
  2199d0:	6ae1      	ldr	r1, [r4, #44]	; 0x2c
  2199d2:	6a60      	ldr	r0, [r4, #36]	; 0x24
  2199d4:	4788      	blx	r1
  2199d6:	4606      	mov	r6, r0
  2199d8:	f241 1078 	movw	r0, #4472	; 0x1178
  2199dc:	f000 f957 	bl	219c8e <bt_mm_sbrk>
  2199e0:	0005      	movs	r5, r0
  2199e2:	d022      	beq.n	219a2a <audio_pll_tuner_init+0x9a>
  2199e4:	2240      	movs	r2, #64	; 0x40
  2199e6:	4621      	mov	r1, r4
  2199e8:	f505 7088 	add.w	r0, r5, #272	; 0x110
  2199ec:	f656 f840 	bl	6fa70 <__aeabi_memcpy>
  2199f0:	1d28      	adds	r0, r5, #4
  2199f2:	f000 f96b 	bl	219ccc <wiced_rtos_init_event_flags>
  2199f6:	0006      	movs	r6, r0
  2199f8:	d117      	bne.n	219a2a <audio_pll_tuner_init+0x9a>
  2199fa:	f44f 6280 	mov.w	r2, #1024	; 0x400
  2199fe:	f605 5178 	addw	r1, r5, #3448	; 0xd78
  219a02:	f2af 03b3 	subw	r3, pc, #179	; 0xb3
  219a06:	e88d 0026 	stmia.w	sp, {r1, r2, r5}
  219a0a:	f105 0060 	add.w	r0, r5, #96	; 0x60
  219a0e:	a226      	add	r2, pc, #152	; (adr r2, 219aa8 <audio_pll_tuner_stop+0x22>)
  219a10:	2115      	movs	r1, #21
  219a12:	4604      	mov	r4, r0
  219a14:	f000 f987 	bl	219d26 <wiced_rtos_create_thread_with_stack>
  219a18:	0006      	movs	r6, r0
  219a1a:	d106      	bne.n	219a2a <audio_pll_tuner_init+0x9a>
  219a1c:	f8c5 4104 	str.w	r4, [r5, #260]	; 0x104
  219a20:	4825      	ldr	r0, [pc, #148]	; (219ab8 <audio_pll_tuner_stop+0x32>)
  219a22:	6028      	str	r0, [r5, #0]
  219a24:	603d      	str	r5, [r7, #0]
  219a26:	e000      	b.n	219a2a <audio_pll_tuner_init+0x9a>
  219a28:	2603      	movs	r6, #3
  219a2a:	4630      	mov	r0, r6
  219a2c:	bdfe      	pop	{r1, r2, r3, r4, r5, r6, r7, pc}

00219a2e <audio_pll_tuner_deinit>:
  219a2e:	b510      	push	{r4, lr}
  219a30:	b150      	cbz	r0, 219a48 <audio_pll_tuner_deinit+0x1a>
  219a32:	6801      	ldr	r1, [r0, #0]
  219a34:	4a20      	ldr	r2, [pc, #128]	; (219ab8 <audio_pll_tuner_stop+0x32>)
  219a36:	4291      	cmp	r1, r2
  219a38:	d106      	bne.n	219a48 <audio_pll_tuner_deinit+0x1a>
  219a3a:	2101      	movs	r1, #1
  219a3c:	f8c0 1108 	str.w	r1, [r0, #264]	; 0x108
  219a40:	2104      	movs	r1, #4
  219a42:	1d00      	adds	r0, r0, #4
  219a44:	f000 f950 	bl	219ce8 <wiced_rtos_set_event_flags>
  219a48:	2000      	movs	r0, #0
  219a4a:	bd10      	pop	{r4, pc}

00219a4c <audio_pll_tuner_start>:
  219a4c:	b1c8      	cbz	r0, 219a82 <audio_pll_tuner_start+0x36>
  219a4e:	6802      	ldr	r2, [r0, #0]
  219a50:	4b19      	ldr	r3, [pc, #100]	; (219ab8 <audio_pll_tuner_stop+0x32>)
  219a52:	429a      	cmp	r2, r3
  219a54:	d115      	bne.n	219a82 <audio_pll_tuner_start+0x36>
  219a56:	684a      	ldr	r2, [r1, #4]
  219a58:	b19a      	cbz	r2, 219a82 <audio_pll_tuner_start+0x36>
  219a5a:	7a0a      	ldrb	r2, [r1, #8]
  219a5c:	b18a      	cbz	r2, 219a82 <audio_pll_tuner_start+0x36>
  219a5e:	7a4a      	ldrb	r2, [r1, #9]
  219a60:	b17a      	cbz	r2, 219a82 <audio_pll_tuner_start+0x36>
  219a62:	680a      	ldr	r2, [r1, #0]
  219a64:	f8c0 2150 	str.w	r2, [r0, #336]	; 0x150
  219a68:	684a      	ldr	r2, [r1, #4]
  219a6a:	f8c0 2154 	str.w	r2, [r0, #340]	; 0x154
  219a6e:	7a0a      	ldrb	r2, [r1, #8]
  219a70:	f880 2158 	strb.w	r2, [r0, #344]	; 0x158
  219a74:	7a49      	ldrb	r1, [r1, #9]
  219a76:	f880 1159 	strb.w	r1, [r0, #345]	; 0x159
  219a7a:	2102      	movs	r1, #2
  219a7c:	1d00      	adds	r0, r0, #4
  219a7e:	f000 b933 	b.w	219ce8 <wiced_rtos_set_event_flags>
  219a82:	2003      	movs	r0, #3
  219a84:	4770      	bx	lr

00219a86 <audio_pll_tuner_stop>:
  219a86:	b150      	cbz	r0, 219a9e <audio_pll_tuner_stop+0x18>
  219a88:	6801      	ldr	r1, [r0, #0]
  219a8a:	4a0b      	ldr	r2, [pc, #44]	; (219ab8 <audio_pll_tuner_stop+0x32>)
  219a8c:	4291      	cmp	r1, r2
  219a8e:	d106      	bne.n	219a9e <audio_pll_tuner_stop+0x18>
  219a90:	2101      	movs	r1, #1
  219a92:	f8c0 1108 	str.w	r1, [r0, #264]	; 0x108
  219a96:	2104      	movs	r1, #4
  219a98:	1d00      	adds	r0, r0, #4
  219a9a:	f000 b925 	b.w	219ce8 <wiced_rtos_set_event_flags>
  219a9e:	2003      	movs	r0, #3
  219aa0:	4770      	bx	lr
  219aa2:	0000      	.short	0x0000
  219aa4:	3b9aca00 	.word	0x3b9aca00
  219aa8:	69647541 	.word	0x69647541
  219aac:	6c505f6f 	.word	0x6c505f6f
  219ab0:	75545f6c 	.word	0x75545f6c
  219ab4:	0072656e 	.word	0x0072656e
  219ab8:	bea8fade 	.word	0xbea8fade

00219abc <create_pll_tuner>:
  219abc:	b510      	push	{r4, lr}
  219abe:	f7fe ffdd 	bl	218a7c <lite_host_init_samplesinqueue>
  219ac2:	48aa      	ldr	r0, [pc, #680]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219ac4:	f7ff fc76 	bl	2193b4 <pll_tuner_create>
  219ac8:	2800      	cmp	r0, #0
  219aca:	d000      	beq.n	219ace <create_pll_tuner+0x12>
  219acc:	201f      	movs	r0, #31
  219ace:	bd10      	pop	{r4, pc}

00219ad0 <destroy_pll_tuner>:
  219ad0:	b510      	push	{r4, lr}
  219ad2:	48a6      	ldr	r0, [pc, #664]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219ad4:	f7ff fc9e 	bl	219414 <pll_tuner_destroy>
  219ad8:	2800      	cmp	r0, #0
  219ada:	d000      	beq.n	219ade <destroy_pll_tuner+0xe>
  219adc:	201f      	movs	r0, #31
  219ade:	bd10      	pop	{r4, pc}

00219ae0 <run_pll_tuner>:
  219ae0:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  219ae4:	4da2      	ldr	r5, [pc, #648]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219ae6:	460f      	mov	r7, r1
  219ae8:	4606      	mov	r6, r0
  219aea:	7929      	ldrb	r1, [r5, #4]
  219aec:	2000      	movs	r0, #0
  219aee:	2900      	cmp	r1, #0
  219af0:	d11a      	bne.n	219b28 <run_pll_tuner+0x48>
  219af2:	f7ff fa5a 	bl	218faa <lite_host_get_samplerate>
  219af6:	4c9d      	ldr	r4, [pc, #628]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219af8:	3428      	adds	r4, #40	; 0x28
  219afa:	6020      	str	r0, [r4, #0]
  219afc:	2010      	movs	r0, #16
  219afe:	7120      	strb	r0, [r4, #4]
  219b00:	f7ff fa6a 	bl	218fd8 <lite_host_get_channels>
  219b04:	7160      	strb	r0, [r4, #5]
  219b06:	7921      	ldrb	r1, [r4, #4]
  219b08:	b2c0      	uxtb	r0, r0
  219b0a:	4348      	muls	r0, r1
  219b0c:	08c0      	lsrs	r0, r0, #3
  219b0e:	65e6      	str	r6, [r4, #92]	; 0x5c
  219b10:	60a0      	str	r0, [r4, #8]
  219b12:	f884 7060 	strb.w	r7, [r4, #96]	; 0x60
  219b16:	4895      	ldr	r0, [pc, #596]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219b18:	f7ff fc85 	bl	219426 <pll_tuner_run>
  219b1c:	b100      	cbz	r0, 219b20 <run_pll_tuner+0x40>
  219b1e:	201f      	movs	r0, #31
  219b20:	2800      	cmp	r0, #0
  219b22:	d101      	bne.n	219b28 <run_pll_tuner+0x48>
  219b24:	2101      	movs	r1, #1
  219b26:	7129      	strb	r1, [r5, #4]
  219b28:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

00219b2c <rest_pll_tuner>:
  219b2c:	b510      	push	{r4, lr}
  219b2e:	4c90      	ldr	r4, [pc, #576]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b30:	2000      	movs	r0, #0
  219b32:	7921      	ldrb	r1, [r4, #4]
  219b34:	2901      	cmp	r1, #1
  219b36:	d108      	bne.n	219b4a <rest_pll_tuner+0x1e>
  219b38:	488c      	ldr	r0, [pc, #560]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219b3a:	f7ff fca8 	bl	21948e <pll_tuner_rest>
  219b3e:	b100      	cbz	r0, 219b42 <rest_pll_tuner+0x16>
  219b40:	201f      	movs	r0, #31
  219b42:	2800      	cmp	r0, #0
  219b44:	d101      	bne.n	219b4a <rest_pll_tuner+0x1e>
  219b46:	2100      	movs	r1, #0
  219b48:	7121      	strb	r1, [r4, #4]
  219b4a:	bd10      	pop	{r4, pc}

00219b4c <wiced_init_nanosecond_clock>:
  219b4c:	4888      	ldr	r0, [pc, #544]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b4e:	2100      	movs	r1, #0
  219b50:	6101      	str	r1, [r0, #16]
  219b52:	6141      	str	r1, [r0, #20]
  219b54:	4770      	bx	lr

00219b56 <wiced_deinit_nanosecond_clock>:
  219b56:	4886      	ldr	r0, [pc, #536]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b58:	2100      	movs	r1, #0
  219b5a:	6101      	str	r1, [r0, #16]
  219b5c:	6141      	str	r1, [r0, #20]
  219b5e:	4770      	bx	lr

00219b60 <wiced_reset_nanosecond_clock>:
  219b60:	4883      	ldr	r0, [pc, #524]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b62:	2100      	movs	r1, #0
  219b64:	6101      	str	r1, [r0, #16]
  219b66:	6141      	str	r1, [r0, #20]
  219b68:	4770      	bx	lr

00219b6a <wiced_get_nanosecond_clock_value>:
  219b6a:	4882      	ldr	r0, [pc, #520]	; (219d74 <wiced_rtos_create_thread_with_stack+0x4e>)
  219b6c:	6a40      	ldr	r0, [r0, #36]	; 0x24
  219b6e:	4a80      	ldr	r2, [pc, #512]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b70:	6891      	ldr	r1, [r2, #8]
  219b72:	6953      	ldr	r3, [r2, #20]
  219b74:	6912      	ldr	r2, [r2, #16]
  219b76:	1a08      	subs	r0, r1, r0
  219b78:	1880      	adds	r0, r0, r2
  219b7a:	f143 0100 	adc.w	r1, r3, #0
  219b7e:	f44f 727a 	mov.w	r2, #1000	; 0x3e8
  219b82:	fba2 0300 	umull	r0, r3, r2, r0
  219b86:	fb02 3101 	mla	r1, r2, r1, r3
  219b8a:	4770      	bx	lr

00219b8c <audio_pollTimerExpiredNotice>:
  219b8c:	b570      	push	{r4, r5, r6, lr}
  219b8e:	4c78      	ldr	r4, [pc, #480]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219b90:	2500      	movs	r5, #0
  219b92:	78a0      	ldrb	r0, [r4, #2]
  219b94:	2801      	cmp	r0, #1
  219b96:	d10a      	bne.n	219bae <audio_pollTimerExpiredNotice+0x22>
  219b98:	78e0      	ldrb	r0, [r4, #3]
  219b9a:	2801      	cmp	r0, #1
  219b9c:	d104      	bne.n	219ba8 <audio_pollTimerExpiredNotice+0x1c>
  219b9e:	4873      	ldr	r0, [pc, #460]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219ba0:	381c      	subs	r0, #28
  219ba2:	f607 f8ed 	bl	20d80 <A934d2500>
  219ba6:	70e5      	strb	r5, [r4, #3]
  219ba8:	68a0      	ldr	r0, [r4, #8]
  219baa:	f000 f9a3 	bl	219ef4 <hiddcfa_startHWTimer2>
  219bae:	e9d4 2304 	ldrd	r2, r3, [r4, #16]
  219bb2:	68a0      	ldr	r0, [r4, #8]
  219bb4:	1880      	adds	r0, r0, r2
  219bb6:	eb45 0103 	adc.w	r1, r5, r3
  219bba:	e9c4 0104 	strd	r0, r1, [r4, #16]
  219bbe:	bd70      	pop	{r4, r5, r6, pc}

00219bc0 <wiced_audio_timer_enable>:
  219bc0:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  219bc4:	4d6a      	ldr	r5, [pc, #424]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219bc6:	4606      	mov	r6, r0
  219bc8:	78a8      	ldrb	r0, [r5, #2]
  219bca:	2400      	movs	r4, #0
  219bcc:	bb28      	cbnz	r0, 219c1a <wiced_audio_timer_enable+0x5a>
  219bce:	7828      	ldrb	r0, [r5, #0]
  219bd0:	b150      	cbz	r0, 219be8 <wiced_audio_timer_enable+0x28>
  219bd2:	4866      	ldr	r0, [pc, #408]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219bd4:	2200      	movs	r2, #0
  219bd6:	a168      	add	r1, pc, #416	; (adr r1, 219d78 <wiced_rtos_create_thread_with_stack+0x52>)
  219bd8:	381c      	subs	r0, #28
  219bda:	f607 fcc7 	bl	2156c <A240ba400>
  219bde:	b108      	cbz	r0, 219be4 <wiced_audio_timer_enable+0x24>
  219be0:	2401      	movs	r4, #1
  219be2:	e000      	b.n	219be6 <wiced_audio_timer_enable+0x26>
  219be4:	2400      	movs	r4, #0
  219be6:	b9cc      	cbnz	r4, 219c1c <wiced_audio_timer_enable+0x5c>
  219be8:	2700      	movs	r7, #0
  219bea:	702f      	strb	r7, [r5, #0]
  219bec:	b18e      	cbz	r6, 219c12 <wiced_audio_timer_enable+0x52>
  219bee:	485f      	ldr	r0, [pc, #380]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219bf0:	4962      	ldr	r1, [pc, #392]	; (219d7c <wiced_rtos_create_thread_with_stack+0x56>)
  219bf2:	6a80      	ldr	r0, [r0, #40]	; 0x28
  219bf4:	434e      	muls	r6, r1
  219bf6:	b908      	cbnz	r0, 219bfc <wiced_audio_timer_enable+0x3c>
  219bf8:	f64b 3080 	movw	r0, #48000	; 0xbb80
  219bfc:	fbb6 f0f0 	udiv	r0, r6, r0
  219c00:	2100      	movs	r1, #0
  219c02:	60a8      	str	r0, [r5, #8]
  219c04:	f2af 007b 	subw	r0, pc, #123	; 0x7b
  219c08:	f000 f971 	bl	219eee <hiddcfa_registerforHWTimer2>
  219c0c:	68a8      	ldr	r0, [r5, #8]
  219c0e:	f000 f971 	bl	219ef4 <hiddcfa_startHWTimer2>
  219c12:	2001      	movs	r0, #1
  219c14:	70a8      	strb	r0, [r5, #2]
  219c16:	70ef      	strb	r7, [r5, #3]
  219c18:	e000      	b.n	219c1c <wiced_audio_timer_enable+0x5c>
  219c1a:	2401      	movs	r4, #1
  219c1c:	4620      	mov	r0, r4
  219c1e:	e783      	b.n	219b28 <run_pll_tuner+0x48>

00219c20 <wiced_audio_timer_disable>:
  219c20:	b570      	push	{r4, r5, r6, lr}
  219c22:	4d53      	ldr	r5, [pc, #332]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219c24:	2400      	movs	r4, #0
  219c26:	78a8      	ldrb	r0, [r5, #2]
  219c28:	2801      	cmp	r0, #1
  219c2a:	d10c      	bne.n	219c46 <wiced_audio_timer_disable+0x26>
  219c2c:	2600      	movs	r6, #0
  219c2e:	70ae      	strb	r6, [r5, #2]
  219c30:	f000 f961 	bl	219ef6 <hiddcfa_stopHWTimer2>
  219c34:	78e8      	ldrb	r0, [r5, #3]
  219c36:	b130      	cbz	r0, 219c46 <wiced_audio_timer_disable+0x26>
  219c38:	484c      	ldr	r0, [pc, #304]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219c3a:	381c      	subs	r0, #28
  219c3c:	f607 f8a0 	bl	20d80 <A934d2500>
  219c40:	b100      	cbz	r0, 219c44 <wiced_audio_timer_disable+0x24>
  219c42:	2401      	movs	r4, #1
  219c44:	70ee      	strb	r6, [r5, #3]
  219c46:	f7ff ff8b 	bl	219b60 <wiced_reset_nanosecond_clock>
  219c4a:	4620      	mov	r0, r4
  219c4c:	bd70      	pop	{r4, r5, r6, pc}

00219c4e <wiced_audio_timer_get_frame_sync>:
  219c4e:	eb00 0180 	add.w	r1, r0, r0, lsl #2
  219c52:	0049      	lsls	r1, r1, #1
  219c54:	227d      	movs	r2, #125	; 0x7d
  219c56:	fbb1 f2f2 	udiv	r2, r1, r2
  219c5a:	b510      	push	{r4, lr}
  219c5c:	4944      	ldr	r1, [pc, #272]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219c5e:	788b      	ldrb	r3, [r1, #2]
  219c60:	b19b      	cbz	r3, 219c8a <wiced_audio_timer_get_frame_sync+0x3c>
  219c62:	2301      	movs	r3, #1
  219c64:	70cb      	strb	r3, [r1, #3]
  219c66:	1e99      	subs	r1, r3, #2
  219c68:	1c40      	adds	r0, r0, #1
  219c6a:	d000      	beq.n	219c6e <wiced_audio_timer_get_frame_sync+0x20>
  219c6c:	4611      	mov	r1, r2
  219c6e:	483f      	ldr	r0, [pc, #252]	; (219d6c <wiced_rtos_create_thread_with_stack+0x46>)
  219c70:	381c      	subs	r0, #28
  219c72:	f607 f83d 	bl	20cf0 <Ac474b300>
  219c76:	2800      	cmp	r0, #0
  219c78:	d002      	beq.n	219c80 <wiced_audio_timer_get_frame_sync+0x32>
  219c7a:	280d      	cmp	r0, #13
  219c7c:	d101      	bne.n	219c82 <wiced_audio_timer_get_frame_sync+0x34>
  219c7e:	2006      	movs	r0, #6
  219c80:	bd10      	pop	{r4, pc}
  219c82:	281a      	cmp	r0, #26
  219c84:	d101      	bne.n	219c8a <wiced_audio_timer_get_frame_sync+0x3c>
  219c86:	2005      	movs	r0, #5
  219c88:	bd10      	pop	{r4, pc}
  219c8a:	2001      	movs	r0, #1
  219c8c:	bd10      	pop	{r4, pc}

00219c8e <bt_mm_sbrk>:
  219c8e:	f64b bd0f 	b.w	656b0 <A9c828200>

00219c92 <wiced_audio_get_current_buffer_weight>:
  219c92:	b510      	push	{r4, lr}
  219c94:	f7ff f9ac 	bl	218ff0 <lite_host_get_bytesinfifo>
  219c98:	2000      	movs	r0, #0
  219c9a:	bd10      	pop	{r4, pc}

00219c9c <wiced_rtos_get_queue_occupancy>:
  219c9c:	b510      	push	{r4, lr}
  219c9e:	4608      	mov	r0, r1
  219ca0:	f7ff f961 	bl	218f66 <lite_host_get_samplesinqueue>
  219ca4:	2000      	movs	r0, #0
  219ca6:	bd10      	pop	{r4, pc}

00219ca8 <wiced_audio_set_pll_fractional_divider>:
  219ca8:	b510      	push	{r4, lr}
  219caa:	4935      	ldr	r1, [pc, #212]	; (219d80 <wiced_rtos_create_thread_with_stack+0x5a>)
  219cac:	2800      	cmp	r0, #0
  219cae:	db00      	blt.n	219cb2 <wiced_audio_set_pll_fractional_divider+0xa>
  219cb0:	4249      	negs	r1, r1
  219cb2:	4408      	add	r0, r1
  219cb4:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  219cb8:	fb90 f4f1 	sdiv	r4, r0, r1
  219cbc:	4620      	mov	r0, r4
  219cbe:	f000 f867 	bl	219d90 <arip_adjustFifoClock>
  219cc2:	b220      	sxth	r0, r4
  219cc4:	f7ff f94c 	bl	218f60 <lite_host_setPPMforHost>
  219cc8:	2000      	movs	r0, #0
  219cca:	bd10      	pop	{r4, pc}

00219ccc <wiced_rtos_init_event_flags>:
  219ccc:	b510      	push	{r4, lr}
  219cce:	4c28      	ldr	r4, [pc, #160]	; (219d70 <wiced_rtos_create_thread_with_stack+0x4a>)
  219cd0:	7861      	ldrb	r1, [r4, #1]
  219cd2:	b139      	cbz	r1, 219ce4 <wiced_rtos_init_event_flags+0x18>
  219cd4:	a12b      	add	r1, pc, #172	; (adr r1, 219d84 <wiced_rtos_create_thread_with_stack+0x5e>)
  219cd6:	f5fa fe09 	bl	148ec <A83c70600>
  219cda:	b108      	cbz	r0, 219ce0 <wiced_rtos_init_event_flags+0x14>
  219cdc:	2001      	movs	r0, #1
  219cde:	bd10      	pop	{r4, pc}
  219ce0:	2000      	movs	r0, #0
  219ce2:	7060      	strb	r0, [r4, #1]
  219ce4:	2000      	movs	r0, #0
  219ce6:	bd10      	pop	{r4, pc}

00219ce8 <wiced_rtos_set_event_flags>:
  219ce8:	b510      	push	{r4, lr}
  219cea:	2200      	movs	r2, #0
  219cec:	f61e fd80 	bl	387f0 <A37b56000>
  219cf0:	2800      	cmp	r0, #0
  219cf2:	d000      	beq.n	219cf6 <wiced_rtos_set_event_flags+0xe>
  219cf4:	2001      	movs	r0, #1
  219cf6:	bd10      	pop	{r4, pc}

00219cf8 <wiced_rtos_wait_for_event_flags>:
  219cf8:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  219cfa:	4615      	mov	r5, r2
  219cfc:	461a      	mov	r2, r3
  219cfe:	e9dd 6306 	ldrd	r6, r3, [sp, #24]
  219d02:	2a01      	cmp	r2, #1
  219d04:	d101      	bne.n	219d0a <wiced_rtos_wait_for_event_flags+0x12>
  219d06:	2401      	movs	r4, #1
  219d08:	e000      	b.n	219d0c <wiced_rtos_wait_for_event_flags+0x14>
  219d0a:	2400      	movs	r4, #0
  219d0c:	2202      	movs	r2, #2
  219d0e:	2e01      	cmp	r6, #1
  219d10:	d000      	beq.n	219d14 <wiced_rtos_wait_for_event_flags+0x1c>
  219d12:	2200      	movs	r2, #0
  219d14:	9300      	str	r3, [sp, #0]
  219d16:	4322      	orrs	r2, r4
  219d18:	462b      	mov	r3, r5
  219d1a:	f5fa fe11 	bl	14940 <A69222000>
  219d1e:	2800      	cmp	r0, #0
  219d20:	d000      	beq.n	219d24 <wiced_rtos_wait_for_event_flags+0x2c>
  219d22:	2001      	movs	r0, #1
  219d24:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}

00219d26 <wiced_rtos_create_thread_with_stack>:
  219d26:	e92d 47f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
  219d2a:	b086      	sub	sp, #24
  219d2c:	e9dd 9a0f 	ldrd	r9, sl, [sp, #60]	; 0x3c
  219d30:	9d0e      	ldr	r5, [sp, #56]	; 0x38
  219d32:	4606      	mov	r6, r0
  219d34:	460c      	mov	r4, r1
  219d36:	4617      	mov	r7, r2
  219d38:	4698      	mov	r8, r3
  219d3a:	b19d      	cbz	r5, 219d64 <wiced_rtos_create_thread_with_stack+0x3e>
  219d3c:	21a4      	movs	r1, #164	; 0xa4
  219d3e:	4630      	mov	r0, r6
  219d40:	f64c fe12 	bl	66968 <__aeabi_memclr>
  219d44:	e9cd 5900 	strd	r5, r9, [sp]
  219d48:	2200      	movs	r2, #0
  219d4a:	9402      	str	r4, [sp, #8]
  219d4c:	e9cd 4203 	strd	r4, r2, [sp, #12]
  219d50:	2301      	movs	r3, #1
  219d52:	9305      	str	r3, [sp, #20]
  219d54:	4653      	mov	r3, sl
  219d56:	4642      	mov	r2, r8
  219d58:	4639      	mov	r1, r7
  219d5a:	4630      	mov	r0, r6
  219d5c:	f5fb f984 	bl	15068 <Af344f800>
  219d60:	2800      	cmp	r0, #0
  219d62:	d000      	beq.n	219d66 <wiced_rtos_create_thread_with_stack+0x40>
  219d64:	2001      	movs	r0, #1
  219d66:	b006      	add	sp, #24
  219d68:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
  219d6c:	0021fca4 	.word	0x0021fca4
  219d70:	0021faf8 	.word	0x0021faf8
  219d74:	0032a000 	.word	0x0032a000
  219d78:	00000000 	.word	0x00000000
  219d7c:	000f4240 	.word	0x000f4240
  219d80:	fffffe0c 	.word	0xfffffe0c
  219d84:	6e657645 	.word	0x6e657645
  219d88:	6c662074 	.word	0x6c662074
  219d8c:	00736761 	.word	0x00736761

00219d90 <arip_adjustFifoClock>:
  219d90:	b570      	push	{r4, r5, r6, lr}
  219d92:	4604      	mov	r4, r0
  219d94:	f5f8 fb50 	bl	12438 <Ac000e458>
  219d98:	4d1f      	ldr	r5, [pc, #124]	; (219e18 <arip_adjustFifoClockWithInitialFreq+0x14>)
  219d9a:	f8d5 1100 	ldr.w	r1, [r5, #256]	; 0x100
  219d9e:	f44f 727a 	mov.w	r2, #1000	; 0x3e8
  219da2:	4342      	muls	r2, r0
  219da4:	f04f 6380 	mov.w	r3, #67108864	; 0x4000000
  219da8:	eba3 0252 	sub.w	r2, r3, r2, lsr #1
  219dac:	4411      	add	r1, r2
  219dae:	f5a0 42ba 	sub.w	r2, r0, #23808	; 0x5d00
  219db2:	3ac0      	subs	r2, #192	; 0xc0
  219db4:	d106      	bne.n	219dc4 <arip_adjustFifoClock+0x34>
  219db6:	f44f 707a 	mov.w	r0, #1000	; 0x3e8
  219dba:	4344      	muls	r4, r0
  219dbc:	2053      	movs	r0, #83	; 0x53
  219dbe:	fb94 f0f0 	sdiv	r0, r4, r0
  219dc2:	e007      	b.n	219dd4 <arip_adjustFifoClock+0x44>
  219dc4:	f5a0 42ca 	sub.w	r2, r0, #25856	; 0x6500
  219dc8:	3a90      	subs	r2, #144	; 0x90
  219dca:	eb04 0084 	add.w	r0, r4, r4, lsl #2
  219dce:	d103      	bne.n	219dd8 <arip_adjustFifoClock+0x48>
  219dd0:	eb00 00c4 	add.w	r0, r0, r4, lsl #3
  219dd4:	1844      	adds	r4, r0, r1
  219dd6:	e001      	b.n	219ddc <arip_adjustFifoClock+0x4c>
  219dd8:	eb01 0440 	add.w	r4, r1, r0, lsl #1
  219ddc:	f64f ffa4 	bl	69d28 <_tx_v7m_get_int>
  219de0:	4606      	mov	r6, r0
  219de2:	f64f ffa4 	bl	69d2e <_tx_v7m_disable_int>
  219de6:	2001      	movs	r0, #1
  219de8:	f505 7582 	add.w	r5, r5, #260	; 0x104
  219dec:	6068      	str	r0, [r5, #4]
  219dee:	f04f 0100 	mov.w	r1, #0
  219df2:	60e9      	str	r1, [r5, #12]
  219df4:	602c      	str	r4, [r5, #0]
  219df6:	60e8      	str	r0, [r5, #12]
  219df8:	60a8      	str	r0, [r5, #8]
  219dfa:	4630      	mov	r0, r6
  219dfc:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  219e00:	f64f bf98 	b.w	69d34 <_tx_v7m_set_int>

00219e04 <arip_adjustFifoClockWithInitialFreq>:
  219e04:	4602      	mov	r2, r0
  219e06:	f44f 737a 	mov.w	r3, #1000	; 0x3e8
  219e0a:	435a      	muls	r2, r3
  219e0c:	fbb2 f2f3 	udiv	r2, r2, r3
  219e10:	4b02      	ldr	r3, [pc, #8]	; (219e1c <arip_adjustFifoClockWithInitialFreq+0x18>)
  219e12:	4608      	mov	r0, r1
  219e14:	601a      	str	r2, [r3, #0]
  219e16:	e7bb      	b.n	219d90 <arip_adjustFifoClock>
  219e18:	00327000 	.word	0x00327000
  219e1c:	00327100 	.word	0x00327100

00219e20 <Sample_volume_ctrl_patch>:
  219e20:	b530      	push	{r4, r5, lr}
  219e22:	4a1e      	ldr	r2, [pc, #120]	; (219e9c <Sample_volume_ctrl_patch+0x7c>)
  219e24:	290f      	cmp	r1, #15
  219e26:	f932 2011 	ldrsh.w	r2, [r2, r1, lsl #1]
  219e2a:	db01      	blt.n	219e30 <Sample_volume_ctrl_patch+0x10>
  219e2c:	2000      	movs	r0, #0
  219e2e:	e007      	b.n	219e40 <Sample_volume_ctrl_patch+0x20>
  219e30:	2900      	cmp	r1, #0
  219e32:	dd05      	ble.n	219e40 <Sample_volume_ctrl_patch+0x20>
  219e34:	4350      	muls	r0, r2
  219e36:	0040      	lsls	r0, r0, #1
  219e38:	f500 4000 	add.w	r0, r0, #32768	; 0x8000
  219e3c:	0c00      	lsrs	r0, r0, #16
  219e3e:	b200      	sxth	r0, r0
  219e40:	4917      	ldr	r1, [pc, #92]	; (219ea0 <Sample_volume_ctrl_patch+0x80>)
  219e42:	7809      	ldrb	r1, [r1, #0]
  219e44:	2900      	cmp	r1, #0
  219e46:	d026      	beq.n	219e96 <Sample_volume_ctrl_patch+0x76>
  219e48:	4916      	ldr	r1, [pc, #88]	; (219ea4 <Sample_volume_ctrl_patch+0x84>)
  219e4a:	4b17      	ldr	r3, [pc, #92]	; (219ea8 <Sample_volume_ctrl_patch+0x88>)
  219e4c:	780c      	ldrb	r4, [r1, #0]
  219e4e:	6819      	ldr	r1, [r3, #0]
  219e50:	220a      	movs	r2, #10
  219e52:	fbb1 f2f2 	udiv	r2, r1, r2
  219e56:	b114      	cbz	r4, 219e5e <Sample_volume_ctrl_patch+0x3e>
  219e58:	f647 74ff 	movw	r4, #32767	; 0x7fff
  219e5c:	1aa2      	subs	r2, r4, r2
  219e5e:	4342      	muls	r2, r0
  219e60:	0050      	lsls	r0, r2, #1
  219e62:	4a12      	ldr	r2, [pc, #72]	; (219eac <Sample_volume_ctrl_patch+0x8c>)
  219e64:	f500 4000 	add.w	r0, r0, #32768	; 0x8000
  219e68:	0c00      	lsrs	r0, r0, #16
  219e6a:	7814      	ldrb	r4, [r2, #0]
  219e6c:	b200      	sxth	r0, r0
  219e6e:	b19c      	cbz	r4, 219e98 <Sample_volume_ctrl_patch+0x78>
  219e70:	4c0f      	ldr	r4, [pc, #60]	; (219eb0 <Sample_volume_ctrl_patch+0x90>)
  219e72:	42a1      	cmp	r1, r4
  219e74:	d007      	beq.n	219e86 <Sample_volume_ctrl_patch+0x66>
  219e76:	4d0f      	ldr	r5, [pc, #60]	; (219eb4 <Sample_volume_ctrl_patch+0x94>)
  219e78:	682d      	ldr	r5, [r5, #0]
  219e7a:	4429      	add	r1, r5
  219e7c:	6019      	str	r1, [r3, #0]
  219e7e:	42a1      	cmp	r1, r4
  219e80:	d907      	bls.n	219e92 <Sample_volume_ctrl_patch+0x72>
  219e82:	601c      	str	r4, [r3, #0]
  219e84:	e005      	b.n	219e92 <Sample_volume_ctrl_patch+0x72>
  219e86:	490c      	ldr	r1, [pc, #48]	; (219eb8 <Sample_volume_ctrl_patch+0x98>)
  219e88:	780b      	ldrb	r3, [r1, #0]
  219e8a:	2b01      	cmp	r3, #1
  219e8c:	d101      	bne.n	219e92 <Sample_volume_ctrl_patch+0x72>
  219e8e:	2302      	movs	r3, #2
  219e90:	700b      	strb	r3, [r1, #0]
  219e92:	2100      	movs	r1, #0
  219e94:	7011      	strb	r1, [r2, #0]
  219e96:	bd30      	pop	{r4, r5, pc}
  219e98:	2101      	movs	r1, #1
  219e9a:	e7fb      	b.n	219e94 <Sample_volume_ctrl_patch+0x74>
  219e9c:	00201c3c 	.word	0x00201c3c
  219ea0:	0021faa6 	.word	0x0021faa6
  219ea4:	0021faa8 	.word	0x0021faa8
  219ea8:	0021fab4 	.word	0x0021fab4
  219eac:	0021fb10 	.word	0x0021fb10
  219eb0:	0004fff6 	.word	0x0004fff6
  219eb4:	0021fab0 	.word	0x0021fab0
  219eb8:	0021faa9 	.word	0x0021faa9

00219ebc <timer_enableTimer2Int>:
  219ebc:	4918      	ldr	r1, [pc, #96]	; (219f20 <Timer2DoneInt_C_patch+0x10>)
  219ebe:	6208      	str	r0, [r1, #32]
  219ec0:	2000      	movs	r0, #0
  219ec2:	62c8      	str	r0, [r1, #44]	; 0x2c
  219ec4:	20a3      	movs	r0, #163	; 0xa3
  219ec6:	6288      	str	r0, [r1, #40]	; 0x28
  219ec8:	2100      	movs	r1, #0
  219eca:	f44f 2080 	mov.w	r0, #262144	; 0x40000
  219ece:	f61f b832 	b.w	38f36 <A9ee36800>

00219ed2 <timer_disableTimer2Int>:
  219ed2:	b510      	push	{r4, lr}
  219ed4:	2100      	movs	r1, #0
  219ed6:	f44f 2080 	mov.w	r0, #262144	; 0x40000
  219eda:	f626 fe77 	bl	40bcc <Ad6341000>
  219ede:	4810      	ldr	r0, [pc, #64]	; (219f20 <Timer2DoneInt_C_patch+0x10>)
  219ee0:	2100      	movs	r1, #0
  219ee2:	62c1      	str	r1, [r0, #44]	; 0x2c
  219ee4:	6a81      	ldr	r1, [r0, #40]	; 0x28
  219ee6:	f021 0121 	bic.w	r1, r1, #33	; 0x21
  219eea:	6281      	str	r1, [r0, #40]	; 0x28
  219eec:	bd10      	pop	{r4, pc}

00219eee <hiddcfa_registerforHWTimer2>:
  219eee:	4a0d      	ldr	r2, [pc, #52]	; (219f24 <Timer2DoneInt_C_patch+0x14>)
  219ef0:	c203      	stmia	r2!, {r0, r1}
  219ef2:	4770      	bx	lr

00219ef4 <hiddcfa_startHWTimer2>:
  219ef4:	e7e2      	b.n	219ebc <timer_enableTimer2Int>

00219ef6 <hiddcfa_stopHWTimer2>:
  219ef6:	e7ec      	b.n	219ed2 <timer_disableTimer2Int>

00219ef8 <hiddcfa_HWTimer2IntHandler>:
  219ef8:	b510      	push	{r4, lr}
  219efa:	f7ff ffea 	bl	219ed2 <timer_disableTimer2Int>
  219efe:	4809      	ldr	r0, [pc, #36]	; (219f24 <Timer2DoneInt_C_patch+0x14>)
  219f00:	6801      	ldr	r1, [r0, #0]
  219f02:	2900      	cmp	r1, #0
  219f04:	d003      	beq.n	219f0e <hiddcfa_HWTimer2IntHandler+0x16>
  219f06:	6840      	ldr	r0, [r0, #4]
  219f08:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  219f0c:	4708      	bx	r1
  219f0e:	bd10      	pop	{r4, pc}

00219f10 <Timer2DoneInt_C_patch>:
  219f10:	b510      	push	{r4, lr}
  219f12:	f7ff fff1 	bl	219ef8 <hiddcfa_HWTimer2IntHandler>
  219f16:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  219f1a:	f607 bc5f 	b.w	217dc <A49f2e000>
  219f1e:	0000      	.short	0x0000
  219f20:	0032a000 	.word	0x0032a000
  219f24:	0021fb14 	.word	0x0021fb14

00219f28 <_arip_audioHandleBTA2DP2I2STx_help>:
  219f28:	b510      	push	{r4, lr}
  219f2a:	4604      	mov	r4, r0
  219f2c:	481b      	ldr	r0, [pc, #108]	; (219f9c <_arip_audioHandleBTA2DP2I2STx_help+0x74>)
  219f2e:	2102      	movs	r1, #2
  219f30:	6281      	str	r1, [r0, #40]	; 0x28
  219f32:	f04f 010e 	mov.w	r1, #14
  219f36:	6101      	str	r1, [r0, #16]
  219f38:	f04f 0109 	mov.w	r1, #9
  219f3c:	6141      	str	r1, [r0, #20]
  219f3e:	f04f 010a 	mov.w	r1, #10
  219f42:	6001      	str	r1, [r0, #0]
  219f44:	6960      	ldr	r0, [r4, #20]
  219f46:	f04f 0103 	mov.w	r1, #3
  219f4a:	f3c0 1001 	ubfx	r0, r0, #4, #2
  219f4e:	f63f f93b 	bl	591c8 <A82c51800>
  219f52:	6960      	ldr	r0, [r4, #20]
  219f54:	4911      	ldr	r1, [pc, #68]	; (219f9c <_arip_audioHandleBTA2DP2I2STx_help+0x74>)
  219f56:	f3c0 1001 	ubfx	r0, r0, #4, #2
  219f5a:	31a0      	adds	r1, #160	; 0xa0
  219f5c:	f63f f93d 	bl	591da <A1343e600>
  219f60:	6960      	ldr	r0, [r4, #20]
  219f62:	490e      	ldr	r1, [pc, #56]	; (219f9c <_arip_audioHandleBTA2DP2I2STx_help+0x74>)
  219f64:	f3c0 1001 	ubfx	r0, r0, #4, #2
  219f68:	319c      	adds	r1, #156	; 0x9c
  219f6a:	f63f f936 	bl	591da <A1343e600>
  219f6e:	6960      	ldr	r0, [r4, #20]
  219f70:	2109      	movs	r1, #9
  219f72:	f3c0 1081 	ubfx	r0, r0, #6, #2
  219f76:	f63f f927 	bl	591c8 <A82c51800>
  219f7a:	6960      	ldr	r0, [r4, #20]
  219f7c:	4907      	ldr	r1, [pc, #28]	; (219f9c <_arip_audioHandleBTA2DP2I2STx_help+0x74>)
  219f7e:	f3c0 1081 	ubfx	r0, r0, #6, #2
  219f82:	3198      	adds	r1, #152	; 0x98
  219f84:	f63f f929 	bl	591da <A1343e600>
  219f88:	6960      	ldr	r0, [r4, #20]
  219f8a:	4904      	ldr	r1, [pc, #16]	; (219f9c <_arip_audioHandleBTA2DP2I2STx_help+0x74>)
  219f8c:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  219f90:	f3c0 1081 	ubfx	r0, r0, #6, #2
  219f94:	3178      	adds	r1, #120	; 0x78
  219f96:	f63f b920 	b.w	591da <A1343e600>
  219f9a:	0000      	.short	0x0000
  219f9c:	003271cc 	.word	0x003271cc

00219fa0 <wiced_bt_config_sco_path>:
  219fa0:	b570      	push	{r4, r5, r6, lr}
  219fa2:	4604      	mov	r4, r0
  219fa4:	4867      	ldr	r0, [pc, #412]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  219fa6:	460d      	mov	r5, r1
  219fa8:	7800      	ldrb	r0, [r0, #0]
  219faa:	b9c8      	cbnz	r0, 219fe0 <wiced_bt_config_sco_path+0x40>
  219fac:	f000 f8d9 	bl	21a162 <wiced_hal_get_pcm_config>
  219fb0:	4964      	ldr	r1, [pc, #400]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  219fb2:	1f09      	subs	r1, r1, #4
  219fb4:	6008      	str	r0, [r1, #0]
  219fb6:	7801      	ldrb	r1, [r0, #0]
  219fb8:	b929      	cbnz	r1, 219fc6 <wiced_bt_config_sco_path+0x26>
  219fba:	7840      	ldrb	r0, [r0, #1]
  219fbc:	2105      	movs	r1, #5
  219fbe:	2801      	cmp	r0, #1
  219fc0:	d006      	beq.n	219fd0 <wiced_bt_config_sco_path+0x30>
  219fc2:	2107      	movs	r1, #7
  219fc4:	e004      	b.n	219fd0 <wiced_bt_config_sco_path+0x30>
  219fc6:	7840      	ldrb	r0, [r0, #1]
  219fc8:	2104      	movs	r1, #4
  219fca:	2801      	cmp	r0, #1
  219fcc:	d000      	beq.n	219fd0 <wiced_bt_config_sco_path+0x30>
  219fce:	2106      	movs	r1, #6
  219fd0:	485c      	ldr	r0, [pc, #368]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  219fd2:	303c      	adds	r0, #60	; 0x3c
  219fd4:	7041      	strb	r1, [r0, #1]
  219fd6:	f880 1041 	strb.w	r1, [r0, #65]	; 0x41
  219fda:	7001      	strb	r1, [r0, #0]
  219fdc:	f880 1040 	strb.w	r1, [r0, #64]	; 0x40
  219fe0:	7968      	ldrb	r0, [r5, #5]
  219fe2:	4958      	ldr	r1, [pc, #352]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  219fe4:	2801      	cmp	r0, #1
  219fe6:	f04f 0240 	mov.w	r2, #64	; 0x40
  219fea:	f101 0108 	add.w	r1, r1, #8
  219fee:	d105      	bne.n	219ffc <wiced_bt_config_sco_path+0x5c>
  219ff0:	3140      	adds	r1, #64	; 0x40
  219ff2:	4620      	mov	r0, r4
  219ff4:	f654 fead 	bl	6ed52 <mpaf_memcpy>
  219ff8:	2002      	movs	r0, #2
  219ffa:	e003      	b.n	21a004 <wiced_bt_config_sco_path+0x64>
  219ffc:	4620      	mov	r0, r4
  219ffe:	f654 fea8 	bl	6ed52 <mpaf_memcpy>
  21a002:	2000      	movs	r0, #0
  21a004:	f5ed fbd4 	bl	77b0 <A1ad70800>
  21a008:	8828      	ldrh	r0, [r5, #0]
  21a00a:	8720      	strh	r0, [r4, #56]	; 0x38
  21a00c:	7928      	ldrb	r0, [r5, #4]
  21a00e:	f884 003c 	strb.w	r0, [r4, #60]	; 0x3c
  21a012:	8868      	ldrh	r0, [r5, #2]
  21a014:	2300      	movs	r3, #0
  21a016:	8760      	strh	r0, [r4, #58]	; 0x3a
  21a018:	f894 0034 	ldrb.w	r0, [r4, #52]	; 0x34
  21a01c:	461a      	mov	r2, r3
  21a01e:	4619      	mov	r1, r3
  21a020:	f5ed fa14 	bl	744c <A9dee9400>
  21a024:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21a028:	f000 b8ee 	b.w	21a208 <wiced_hal_internal_pcm_config>

0021a02c <wiced_bt_sco_create_as_initiator>:
  21a02c:	b530      	push	{r4, r5, lr}
  21a02e:	b091      	sub	sp, #68	; 0x44
  21a030:	4604      	mov	r4, r0
  21a032:	460d      	mov	r5, r1
  21a034:	4611      	mov	r1, r2
  21a036:	a801      	add	r0, sp, #4
  21a038:	f7ff ffb2 	bl	219fa0 <wiced_bt_config_sco_path>
  21a03c:	a801      	add	r0, sp, #4
  21a03e:	f5ed fbad 	bl	779c <Ae855a000>
  21a042:	f8bd 203e 	ldrh.w	r2, [sp, #62]	; 0x3e
  21a046:	462b      	mov	r3, r5
  21a048:	2101      	movs	r1, #1
  21a04a:	4620      	mov	r0, r4
  21a04c:	f5ed fa0a 	bl	7464 <Aa1469600>
  21a050:	4604      	mov	r4, r0
  21a052:	483c      	ldr	r0, [pc, #240]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  21a054:	7800      	ldrb	r0, [r0, #0]
  21a056:	b920      	cbnz	r0, 21a062 <wiced_bt_sco_create_as_initiator+0x36>
  21a058:	493b      	ldr	r1, [pc, #236]	; (21a148 <wiced_bt_sco_turn_off_pcm_clock+0xc>)
  21a05a:	2000      	movs	r0, #0
  21a05c:	7008      	strb	r0, [r1, #0]
  21a05e:	f000 f882 	bl	21a166 <wiced_bt_pcm_pad_config>
  21a062:	4620      	mov	r0, r4
  21a064:	b011      	add	sp, #68	; 0x44
  21a066:	bd30      	pop	{r4, r5, pc}

0021a068 <wiced_bt_sco_create_as_acceptor>:
  21a068:	2200      	movs	r2, #0
  21a06a:	4603      	mov	r3, r0
  21a06c:	4611      	mov	r1, r2
  21a06e:	4610      	mov	r0, r2
  21a070:	f5ed b9f8 	b.w	7464 <Aa1469600>

0021a074 <wiced_bt_sco_remove>:
  21a074:	f687 bbb6 	b.w	a17e4 <A4ea31100>

0021a078 <wiced_bt_sco_accept_connection>:
  21a078:	b570      	push	{r4, r5, r6, lr}
  21a07a:	b090      	sub	sp, #64	; 0x40
  21a07c:	4604      	mov	r4, r0
  21a07e:	460d      	mov	r5, r1
  21a080:	4616      	mov	r6, r2
  21a082:	f000 f8f5 	bl	21a270 <is_litehost_streaming>
  21a086:	b140      	cbz	r0, 21a09a <wiced_bt_sco_accept_connection+0x22>
  21a088:	466a      	mov	r2, sp
  21a08a:	210d      	movs	r1, #13
  21a08c:	4620      	mov	r0, r4
  21a08e:	f5ed fbf5 	bl	787c <A94fbe200>
  21a092:	f641 70a9 	movw	r0, #8105	; 0x1fa9
  21a096:	b010      	add	sp, #64	; 0x40
  21a098:	bd70      	pop	{r4, r5, r6, pc}
  21a09a:	4631      	mov	r1, r6
  21a09c:	4668      	mov	r0, sp
  21a09e:	f7ff ff7f 	bl	219fa0 <wiced_bt_config_sco_path>
  21a0a2:	466a      	mov	r2, sp
  21a0a4:	4629      	mov	r1, r5
  21a0a6:	4620      	mov	r0, r4
  21a0a8:	f5ed fbe8 	bl	787c <A94fbe200>
  21a0ac:	4825      	ldr	r0, [pc, #148]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  21a0ae:	7800      	ldrb	r0, [r0, #0]
  21a0b0:	b920      	cbnz	r0, 21a0bc <wiced_bt_sco_accept_connection+0x44>
  21a0b2:	4925      	ldr	r1, [pc, #148]	; (21a148 <wiced_bt_sco_turn_off_pcm_clock+0xc>)
  21a0b4:	2000      	movs	r0, #0
  21a0b6:	7008      	strb	r0, [r1, #0]
  21a0b8:	f000 f855 	bl	21a166 <wiced_bt_pcm_pad_config>
  21a0bc:	2000      	movs	r0, #0
  21a0be:	e7ea      	b.n	21a096 <wiced_bt_sco_accept_connection+0x1e>

0021a0c0 <wiced_bt_sco_setup_voice_path>:
  21a0c0:	4920      	ldr	r1, [pc, #128]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  21a0c2:	7800      	ldrb	r0, [r0, #0]
  21a0c4:	7008      	strb	r0, [r1, #0]
  21a0c6:	b178      	cbz	r0, 21a0e8 <wiced_bt_sco_setup_voice_path+0x28>
  21a0c8:	4920      	ldr	r1, [pc, #128]	; (21a14c <wiced_bt_sco_turn_off_pcm_clock+0x10>)
  21a0ca:	6808      	ldr	r0, [r1, #0]
  21a0cc:	f420 1060 	bic.w	r0, r0, #3670016	; 0x380000
  21a0d0:	f500 2000 	add.w	r0, r0, #524288	; 0x80000
  21a0d4:	6008      	str	r0, [r1, #0]
  21a0d6:	491b      	ldr	r1, [pc, #108]	; (21a144 <wiced_bt_sco_turn_off_pcm_clock+0x8>)
  21a0d8:	2000      	movs	r0, #0
  21a0da:	313c      	adds	r1, #60	; 0x3c
  21a0dc:	7048      	strb	r0, [r1, #1]
  21a0de:	f881 0041 	strb.w	r0, [r1, #65]	; 0x41
  21a0e2:	7008      	strb	r0, [r1, #0]
  21a0e4:	f881 0040 	strb.w	r0, [r1, #64]	; 0x40
  21a0e8:	2000      	movs	r0, #0
  21a0ea:	4770      	bx	lr

0021a0ec <wiced_bt_sco_output_stream>:
  21a0ec:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  21a0f0:	4680      	mov	r8, r0
  21a0f2:	4817      	ldr	r0, [pc, #92]	; (21a150 <wiced_bt_sco_turn_off_pcm_clock+0x14>)
  21a0f4:	460d      	mov	r5, r1
  21a0f6:	6800      	ldr	r0, [r0, #0]
  21a0f8:	4916      	ldr	r1, [pc, #88]	; (21a154 <wiced_bt_sco_turn_off_pcm_clock+0x18>)
  21a0fa:	2700      	movs	r7, #0
  21a0fc:	eb00 0080 	add.w	r0, r0, r0, lsl #2
  21a100:	eb11 00c0 	adds.w	r0, r1, r0, lsl #3
  21a104:	d016      	beq.n	21a134 <wiced_bt_sco_output_stream+0x48>
  21a106:	6946      	ldr	r6, [r0, #20]
  21a108:	b1a6      	cbz	r6, 21a134 <wiced_bt_sco_output_stream+0x48>
  21a10a:	4630      	mov	r0, r6
  21a10c:	f692 fca8 	bl	aca60 <Afba46200>
  21a110:	0004      	movs	r4, r0
  21a112:	d00f      	beq.n	21a134 <wiced_bt_sco_output_stream+0x48>
  21a114:	f8b6 004c 	ldrh.w	r0, [r6, #76]	; 0x4c
  21a118:	462f      	mov	r7, r5
  21a11a:	42a8      	cmp	r0, r5
  21a11c:	d800      	bhi.n	21a120 <wiced_bt_sco_output_stream+0x34>
  21a11e:	4607      	mov	r7, r0
  21a120:	463a      	mov	r2, r7
  21a122:	4641      	mov	r1, r8
  21a124:	f104 0020 	add.w	r0, r4, #32
  21a128:	f654 fe13 	bl	6ed52 <mpaf_memcpy>
  21a12c:	4621      	mov	r1, r4
  21a12e:	4630      	mov	r0, r6
  21a130:	f692 fcb4 	bl	aca9c <A57f5d200>
  21a134:	1be8      	subs	r0, r5, r7
  21a136:	b280      	uxth	r0, r0
  21a138:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

0021a13c <wiced_bt_sco_turn_off_pcm_clock>:
  21a13c:	2100      	movs	r1, #0
  21a13e:	4608      	mov	r0, r1
  21a140:	f000 b883 	b.w	21a24a <hci_HandleOffPcmClock>
  21a144:	0021fb20 	.word	0x0021fb20
  21a148:	002024a8 	.word	0x002024a8
  21a14c:	0020203c 	.word	0x0020203c
  21a150:	00201c00 	.word	0x00201c00
  21a154:	0020d7d0 	.word	0x0020d7d0

0021a158 <wiced_hal_set_pcm_config>:
  21a158:	4601      	mov	r1, r0
  21a15a:	2208      	movs	r2, #8
  21a15c:	482b      	ldr	r0, [pc, #172]	; (21a20c <wiced_hal_internal_pcm_config+0x4>)
  21a15e:	f654 bdf8 	b.w	6ed52 <mpaf_memcpy>

0021a162 <wiced_hal_get_pcm_config>:
  21a162:	482a      	ldr	r0, [pc, #168]	; (21a20c <wiced_hal_internal_pcm_config+0x4>)
  21a164:	4770      	bx	lr

0021a166 <wiced_bt_pcm_pad_config>:
  21a166:	b530      	push	{r4, r5, lr}
  21a168:	4828      	ldr	r0, [pc, #160]	; (21a20c <wiced_hal_internal_pcm_config+0x4>)
  21a16a:	4929      	ldr	r1, [pc, #164]	; (21a210 <wiced_hal_internal_pcm_config+0x8>)
  21a16c:	7845      	ldrb	r5, [r0, #1]
  21a16e:	482c      	ldr	r0, [pc, #176]	; (21a220 <wiced_hal_internal_pcm_config+0x18>)
  21a170:	2d01      	cmp	r5, #1
  21a172:	4a28      	ldr	r2, [pc, #160]	; (21a214 <wiced_hal_internal_pcm_config+0xc>)
  21a174:	4b28      	ldr	r3, [pc, #160]	; (21a218 <wiced_hal_internal_pcm_config+0x10>)
  21a176:	4c29      	ldr	r4, [pc, #164]	; (21a21c <wiced_hal_internal_pcm_config+0x14>)
  21a178:	f8d0 5118 	ldr.w	r5, [r0, #280]	; 0x118
  21a17c:	f425 457f 	bic.w	r5, r5, #65280	; 0xff00
  21a180:	f8c0 5118 	str.w	r5, [r0, #280]	; 0x118
  21a184:	f8d0 5118 	ldr.w	r5, [r0, #280]	; 0x118
  21a188:	f445 5588 	orr.w	r5, r5, #4352	; 0x1100
  21a18c:	f8c0 5118 	str.w	r5, [r0, #280]	; 0x118
  21a190:	6805      	ldr	r5, [r0, #0]
  21a192:	ea05 0501 	and.w	r5, r5, r1
  21a196:	6005      	str	r5, [r0, #0]
  21a198:	6801      	ldr	r1, [r0, #0]
  21a19a:	ea41 0102 	orr.w	r1, r1, r2
  21a19e:	6001      	str	r1, [r0, #0]
  21a1a0:	f8d0 1080 	ldr.w	r1, [r0, #128]	; 0x80
  21a1a4:	ea01 0103 	and.w	r1, r1, r3
  21a1a8:	f8c0 1080 	str.w	r1, [r0, #128]	; 0x80
  21a1ac:	f8d0 1080 	ldr.w	r1, [r0, #128]	; 0x80
  21a1b0:	d101      	bne.n	21a1b6 <wiced_bt_pcm_pad_config+0x50>
  21a1b2:	00d2      	lsls	r2, r2, #3
  21a1b4:	e000      	b.n	21a1b8 <wiced_bt_pcm_pad_config+0x52>
  21a1b6:	4a1b      	ldr	r2, [pc, #108]	; (21a224 <wiced_hal_internal_pcm_config+0x1c>)
  21a1b8:	4311      	orrs	r1, r2
  21a1ba:	f8c0 1080 	str.w	r1, [r0, #128]	; 0x80
  21a1be:	6d41      	ldr	r1, [r0, #84]	; 0x54
  21a1c0:	f021 117f 	bic.w	r1, r1, #8323199	; 0x7f007f
  21a1c4:	6541      	str	r1, [r0, #84]	; 0x54
  21a1c6:	6d41      	ldr	r1, [r0, #84]	; 0x54
  21a1c8:	4321      	orrs	r1, r4
  21a1ca:	6541      	str	r1, [r0, #84]	; 0x54
  21a1cc:	bd30      	pop	{r4, r5, pc}

0021a1ce <wiced_hal_internal_pcm1_config>:
  21a1ce:	b570      	push	{r4, r5, r6, lr}
  21a1d0:	4d0e      	ldr	r5, [pc, #56]	; (21a20c <wiced_hal_internal_pcm_config+0x4>)
  21a1d2:	4c15      	ldr	r4, [pc, #84]	; (21a228 <wiced_hal_internal_pcm_config+0x20>)
  21a1d4:	7869      	ldrb	r1, [r5, #1]
  21a1d6:	6820      	ldr	r0, [r4, #0]
  21a1d8:	f361 0041 	bfi	r0, r1, #1, #1
  21a1dc:	6020      	str	r0, [r4, #0]
  21a1de:	7829      	ldrb	r1, [r5, #0]
  21a1e0:	2901      	cmp	r1, #1
  21a1e2:	d10d      	bne.n	21a200 <wiced_hal_internal_pcm1_config+0x32>
  21a1e4:	2300      	movs	r3, #0
  21a1e6:	1caa      	adds	r2, r5, #2
  21a1e8:	2105      	movs	r1, #5
  21a1ea:	f64f 401e 	movw	r0, #64542	; 0xfc1e
  21a1ee:	f6bd fae0 	bl	d77b2 <wiced_bt_dev_vendor_specific_command>
  21a1f2:	79e9      	ldrb	r1, [r5, #7]
  21a1f4:	6820      	ldr	r0, [r4, #0]
  21a1f6:	f361 0082 	bfi	r0, r1, #2, #1
  21a1fa:	f020 7080 	bic.w	r0, r0, #16777216	; 0x1000000
  21a1fe:	e001      	b.n	21a204 <wiced_hal_internal_pcm1_config+0x36>
  21a200:	f040 7080 	orr.w	r0, r0, #16777216	; 0x1000000
  21a204:	6020      	str	r0, [r4, #0]
  21a206:	bd70      	pop	{r4, r5, r6, pc}

0021a208 <wiced_hal_internal_pcm_config>:
  21a208:	e7e1      	b.n	21a1ce <wiced_hal_internal_pcm1_config>
  21a20a:	0000      	.short	0x0000
  21a20c:	0021fba8 	.word	0x0021fba8
  21a210:	f0f0ffff 	.word	0xf0f0ffff
  21a214:	01010000 	.word	0x01010000
  21a218:	8080ffff 	.word	0x8080ffff
  21a21c:	0009000c 	.word	0x0009000c
  21a220:	00320090 	.word	0x00320090
  21a224:	09090000 	.word	0x09090000
  21a228:	0020203c 	.word	0x0020203c

0021a22c <_pcm_scoLinkDown>:
  21a22c:	f100 031c 	add.w	r3, r0, #28
  21a230:	2201      	movs	r2, #1
  21a232:	fa02 f103 	lsl.w	r1, r2, r3
  21a236:	4082      	lsls	r2, r0
  21a238:	480b      	ldr	r0, [pc, #44]	; (21a268 <hci_HandleOffPcmClock+0x1e>)
  21a23a:	7803      	ldrb	r3, [r0, #0]
  21a23c:	4393      	bics	r3, r2
  21a23e:	4a0b      	ldr	r2, [pc, #44]	; (21a26c <hci_HandleOffPcmClock+0x22>)
  21a240:	7003      	strb	r3, [r0, #0]
  21a242:	6810      	ldr	r0, [r2, #0]
  21a244:	4388      	bics	r0, r1
  21a246:	6010      	str	r0, [r2, #0]
  21a248:	4770      	bx	lr

0021a24a <hci_HandleOffPcmClock>:
  21a24a:	4907      	ldr	r1, [pc, #28]	; (21a268 <hci_HandleOffPcmClock+0x1e>)
  21a24c:	7809      	ldrb	r1, [r1, #0]
  21a24e:	b939      	cbnz	r1, 21a260 <hci_HandleOffPcmClock+0x16>
  21a250:	4806      	ldr	r0, [pc, #24]	; (21a26c <hci_HandleOffPcmClock+0x22>)
  21a252:	2100      	movs	r1, #0
  21a254:	62c1      	str	r1, [r0, #44]	; 0x2c
  21a256:	6801      	ldr	r1, [r0, #0]
  21a258:	f021 0109 	bic.w	r1, r1, #9
  21a25c:	6001      	str	r1, [r0, #0]
  21a25e:	4770      	bx	lr
  21a260:	210c      	movs	r1, #12
  21a262:	7001      	strb	r1, [r0, #0]
  21a264:	4770      	bx	lr
  21a266:	0000      	.short	0x0000
  21a268:	002024ab 	.word	0x002024ab
  21a26c:	00351000 	.word	0x00351000

0021a270 <is_litehost_streaming>:
  21a270:	2000      	movs	r0, #0
  21a272:	4a06      	ldr	r2, [pc, #24]	; (21a28c <is_litehost_streaming+0x1c>)
  21a274:	4601      	mov	r1, r0
  21a276:	eb02 1341 	add.w	r3, r2, r1, lsl #5
  21a27a:	7f9b      	ldrb	r3, [r3, #30]
  21a27c:	b10b      	cbz	r3, 21a282 <is_litehost_streaming+0x12>
  21a27e:	2001      	movs	r0, #1
  21a280:	4770      	bx	lr
  21a282:	1c49      	adds	r1, r1, #1
  21a284:	2905      	cmp	r1, #5
  21a286:	dbf6      	blt.n	21a276 <is_litehost_streaming+0x6>
  21a288:	4770      	bx	lr
  21a28a:	0000      	.short	0x0000
  21a28c:	0020b670 	.word	0x0020b670

0021a290 <a2dp_sink_control_cback>:
  21a290:	b570      	push	{r4, r5, r6, lr}
  21a292:	460c      	mov	r4, r1
  21a294:	b086      	sub	sp, #24
  21a296:	2805      	cmp	r0, #5
  21a298:	d83f      	bhi.n	21a31a <a2dp_sink_control_cback+0x8a>
  21a29a:	e8df f000 	tbb	[pc, r0]
  21a29e:	401d      	.short	0x401d
  21a2a0:	03756a4b 	.word	0x03756a4b
  21a2a4:	2100      	movs	r1, #0
  21a2a6:	2601      	movs	r6, #1
  21a2a8:	2500      	movs	r5, #0
  21a2aa:	4608      	mov	r0, r1
  21a2ac:	4a3b      	ldr	r2, [pc, #236]	; (21a39c <a2dp_sink_control_cback+0x10c>)
  21a2ae:	f5f3 f9d5 	bl	d65c <wiced_va_printf+0x1f7>
  21a2b2:	483b      	ldr	r0, [pc, #236]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a2b4:	2210      	movs	r2, #16
  21a2b6:	7186      	strb	r6, [r0, #6]
  21a2b8:	f104 0108 	add.w	r1, r4, #8
  21a2bc:	3008      	adds	r0, #8
  21a2be:	f617 fdfd 	bl	31ebc <bsc_OpExtended+0x2d91>
  21a2c2:	88e0      	ldrh	r0, [r4, #6]
  21a2c4:	a903      	add	r1, sp, #12
  21a2c6:	f88d 500c 	strb.w	r5, [sp, #12]
  21a2ca:	f88d 6014 	strb.w	r6, [sp, #20]
  21a2ce:	f001 ff1b 	bl	21c108 <wiced_bt_a2dp_sink_update_route_config>
  21a2d2:	4a34      	ldr	r2, [pc, #208]	; (21a3a4 <a2dp_sink_control_cback+0x114>)
  21a2d4:	2100      	movs	r1, #0
  21a2d6:	e02b      	b.n	21a330 <a2dp_sink_control_cback+0xa0>
  21a2d8:	2100      	movs	r1, #0
  21a2da:	4a33      	ldr	r2, [pc, #204]	; (21a3a8 <a2dp_sink_control_cback+0x118>)
  21a2dc:	4608      	mov	r0, r1
  21a2de:	f5f3 f9bd 	bl	d65c <wiced_va_printf+0x1f7>
  21a2e2:	8825      	ldrh	r5, [r4, #0]
  21a2e4:	b99d      	cbnz	r5, 21a30e <a2dp_sink_control_cback+0x7e>
  21a2e6:	8923      	ldrh	r3, [r4, #8]
  21a2e8:	1ca6      	adds	r6, r4, #2
  21a2ea:	4c2d      	ldr	r4, [pc, #180]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a2ec:	4629      	mov	r1, r5
  21a2ee:	e9cd 6300 	strd	r6, r3, [sp]
  21a2f2:	4a2e      	ldr	r2, [pc, #184]	; (21a3ac <a2dp_sink_control_cback+0x11c>)
  21a2f4:	4b2e      	ldr	r3, [pc, #184]	; (21a3b0 <a2dp_sink_control_cback+0x120>)
  21a2f6:	4628      	mov	r0, r5
  21a2f8:	f5f3 f9b0 	bl	d65c <wiced_va_printf+0x1f7>
  21a2fc:	2206      	movs	r2, #6
  21a2fe:	4631      	mov	r1, r6
  21a300:	4620      	mov	r0, r4
  21a302:	f617 fddb 	bl	31ebc <bsc_OpExtended+0x2d91>
  21a306:	2302      	movs	r3, #2
  21a308:	4a2a      	ldr	r2, [pc, #168]	; (21a3b4 <a2dp_sink_control_cback+0x124>)
  21a30a:	71a3      	strb	r3, [r4, #6]
  21a30c:	e7e2      	b.n	21a2d4 <a2dp_sink_control_cback+0x44>
  21a30e:	462b      	mov	r3, r5
  21a310:	4a29      	ldr	r2, [pc, #164]	; (21a3b8 <a2dp_sink_control_cback+0x128>)
  21a312:	2100      	movs	r1, #0
  21a314:	4608      	mov	r0, r1
  21a316:	f5f3 f9a1 	bl	d65c <wiced_va_printf+0x1f7>
  21a31a:	b006      	add	sp, #24
  21a31c:	bd70      	pop	{r4, r5, r6, pc}
  21a31e:	2100      	movs	r1, #0
  21a320:	4a26      	ldr	r2, [pc, #152]	; (21a3bc <a2dp_sink_control_cback+0x12c>)
  21a322:	4608      	mov	r0, r1
  21a324:	f5f3 f99a 	bl	d65c <wiced_va_printf+0x1f7>
  21a328:	2100      	movs	r1, #0
  21a32a:	4b1d      	ldr	r3, [pc, #116]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a32c:	4a24      	ldr	r2, [pc, #144]	; (21a3c0 <a2dp_sink_control_cback+0x130>)
  21a32e:	7199      	strb	r1, [r3, #6]
  21a330:	4608      	mov	r0, r1
  21a332:	e01b      	b.n	21a36c <a2dp_sink_control_cback+0xdc>
  21a334:	2100      	movs	r1, #0
  21a336:	4a23      	ldr	r2, [pc, #140]	; (21a3c4 <a2dp_sink_control_cback+0x134>)
  21a338:	4608      	mov	r0, r1
  21a33a:	f5f3 f98f 	bl	d65c <wiced_va_printf+0x1f7>
  21a33e:	2100      	movs	r1, #0
  21a340:	78a3      	ldrb	r3, [r4, #2]
  21a342:	4608      	mov	r0, r1
  21a344:	9300      	str	r3, [sp, #0]
  21a346:	88a3      	ldrh	r3, [r4, #4]
  21a348:	4a1f      	ldr	r2, [pc, #124]	; (21a3c8 <a2dp_sink_control_cback+0x138>)
  21a34a:	f5f3 f987 	bl	d65c <wiced_va_printf+0x1f7>
  21a34e:	78a1      	ldrb	r1, [r4, #2]
  21a350:	2200      	movs	r2, #0
  21a352:	88a0      	ldrh	r0, [r4, #4]
  21a354:	f001 fb54 	bl	21ba00 <wiced_bt_a2dp_sink_send_start_response>
  21a358:	4601      	mov	r1, r0
  21a35a:	2800      	cmp	r0, #0
  21a35c:	d1dd      	bne.n	21a31a <a2dp_sink_control_cback+0x8a>
  21a35e:	2201      	movs	r2, #1
  21a360:	4b1a      	ldr	r3, [pc, #104]	; (21a3cc <a2dp_sink_control_cback+0x13c>)
  21a362:	701a      	strb	r2, [r3, #0]
  21a364:	2203      	movs	r2, #3
  21a366:	4b0e      	ldr	r3, [pc, #56]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a368:	719a      	strb	r2, [r3, #6]
  21a36a:	4a19      	ldr	r2, [pc, #100]	; (21a3d0 <a2dp_sink_control_cback+0x140>)
  21a36c:	f5f3 f976 	bl	d65c <wiced_va_printf+0x1f7>
  21a370:	e7d3      	b.n	21a31a <a2dp_sink_control_cback+0x8a>
  21a372:	2100      	movs	r1, #0
  21a374:	4a17      	ldr	r2, [pc, #92]	; (21a3d4 <a2dp_sink_control_cback+0x144>)
  21a376:	4608      	mov	r0, r1
  21a378:	f5f3 f970 	bl	d65c <wiced_va_printf+0x1f7>
  21a37c:	2203      	movs	r2, #3
  21a37e:	4b08      	ldr	r3, [pc, #32]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a380:	719a      	strb	r2, [r3, #6]
  21a382:	8923      	ldrh	r3, [r4, #8]
  21a384:	4a14      	ldr	r2, [pc, #80]	; (21a3d8 <a2dp_sink_control_cback+0x148>)
  21a386:	e7c4      	b.n	21a312 <a2dp_sink_control_cback+0x82>
  21a388:	2100      	movs	r1, #0
  21a38a:	4a14      	ldr	r2, [pc, #80]	; (21a3dc <a2dp_sink_control_cback+0x14c>)
  21a38c:	4608      	mov	r0, r1
  21a38e:	f5f3 f965 	bl	d65c <wiced_va_printf+0x1f7>
  21a392:	2202      	movs	r2, #2
  21a394:	4b02      	ldr	r3, [pc, #8]	; (21a3a0 <a2dp_sink_control_cback+0x110>)
  21a396:	719a      	strb	r2, [r3, #6]
  21a398:	4a11      	ldr	r2, [pc, #68]	; (21a3e0 <a2dp_sink_control_cback+0x150>)
  21a39a:	e79b      	b.n	21a2d4 <a2dp_sink_control_cback+0x44>
  21a39c:	0021e328 	.word	0x0021e328
  21a3a0:	0021fbb4 	.word	0x0021fbb4
  21a3a4:	0021e368 	.word	0x0021e368
  21a3a8:	0021e38d 	.word	0x0021e38d
  21a3ac:	0021e3c7 	.word	0x0021e3c7
  21a3b0:	0021db40 	.word	0x0021db40
  21a3b4:	0021e3f1 	.word	0x0021e3f1
  21a3b8:	0021e408 	.word	0x0021e408
  21a3bc:	0021e42a 	.word	0x0021e42a
  21a3c0:	0021e467 	.word	0x0021e467
  21a3c4:	0021e481 	.word	0x0021e481
  21a3c8:	0021e4bd 	.word	0x0021e4bd
  21a3cc:	0021f948 	.word	0x0021f948
  21a3d0:	0021e4f5 	.word	0x0021e4f5
  21a3d4:	0021e514 	.word	0x0021e514
  21a3d8:	0021e550 	.word	0x0021e550
  21a3dc:	0021e578 	.word	0x0021e578
  21a3e0:	0021e5b2 	.word	0x0021e5b2

0021a3e4 <av_app_start>:
  21a3e4:	2100      	movs	r1, #0
  21a3e6:	b508      	push	{r3, lr}
  21a3e8:	4608      	mov	r0, r1
  21a3ea:	4b05      	ldr	r3, [pc, #20]	; (21a400 <av_app_start+0x1c>)
  21a3ec:	4a05      	ldr	r2, [pc, #20]	; (21a404 <av_app_start+0x20>)
  21a3ee:	f5f3 f935 	bl	d65c <wiced_va_printf+0x1f7>
  21a3f2:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
  21a3f6:	4904      	ldr	r1, [pc, #16]	; (21a408 <av_app_start+0x24>)
  21a3f8:	4804      	ldr	r0, [pc, #16]	; (21a40c <av_app_start+0x28>)
  21a3fa:	f001 babb 	b.w	21b974 <wiced_bt_a2dp_sink_init>
  21a3fe:	bf00      	nop
  21a400:	0021db58 	.word	0x0021db58
  21a404:	0021e604 	.word	0x0021e604
  21a408:	0021a291 	.word	0x0021a291
  21a40c:	0021f94c 	.word	0x0021f94c

0021a410 <av_app_init>:
  21a410:	b510      	push	{r4, lr}
  21a412:	2400      	movs	r4, #0
  21a414:	4b05      	ldr	r3, [pc, #20]	; (21a42c <av_app_init+0x1c>)
  21a416:	719c      	strb	r4, [r3, #6]
  21a418:	f7ff ffe4 	bl	21a3e4 <av_app_start>
  21a41c:	4621      	mov	r1, r4
  21a41e:	4620      	mov	r0, r4
  21a420:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21a424:	4b02      	ldr	r3, [pc, #8]	; (21a430 <av_app_init+0x20>)
  21a426:	4a03      	ldr	r2, [pc, #12]	; (21a434 <av_app_init+0x24>)
  21a428:	f5f3 b918 	b.w	d65c <wiced_va_printf+0x1f7>
  21a42c:	0021fbb4 	.word	0x0021fbb4
  21a430:	0021db65 	.word	0x0021db65
  21a434:	0021e61e 	.word	0x0021e61e

0021a438 <wiced_app_cfg_sdp_record_get_size>:
  21a438:	20a0      	movs	r0, #160	; 0xa0
  21a43a:	4770      	bx	lr

0021a43c <hfp_timer_expiry_handler>:
  21a43c:	b510      	push	{r4, lr}
  21a43e:	4c08      	ldr	r4, [pc, #32]	; (21a460 <hfp_timer_expiry_handler+0x24>)
  21a440:	68a3      	ldr	r3, [r4, #8]
  21a442:	b163      	cbz	r3, 21a45e <hfp_timer_expiry_handler+0x22>
  21a444:	7e63      	ldrb	r3, [r4, #25]
  21a446:	b953      	cbnz	r3, 21a45e <hfp_timer_expiry_handler+0x22>
  21a448:	8aa0      	ldrh	r0, [r4, #20]
  21a44a:	f7ff fe13 	bl	21a074 <wiced_bt_sco_remove>
  21a44e:	4620      	mov	r0, r4
  21a450:	f104 0114 	add.w	r1, r4, #20
  21a454:	4a03      	ldr	r2, [pc, #12]	; (21a464 <hfp_timer_expiry_handler+0x28>)
  21a456:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21a45a:	f7ff bde7 	b.w	21a02c <wiced_bt_sco_create_as_initiator>
  21a45e:	bd10      	pop	{r4, pc}
  21a460:	0021fd30 	.word	0x0021fd30
  21a464:	0021f99c 	.word	0x0021f99c

0021a468 <hci_control_send_hf_event>:
  21a468:	f46f 7340 	mvn.w	r3, #768	; 0x300
  21a46c:	b570      	push	{r4, r5, r6, lr}
  21a46e:	b0cc      	sub	sp, #304	; 0x130
  21a470:	18c3      	adds	r3, r0, r3
  21a472:	ad01      	add	r5, sp, #4
  21a474:	4606      	mov	r6, r0
  21a476:	4614      	mov	r4, r2
  21a478:	8029      	strh	r1, [r5, #0]
  21a47a:	2b06      	cmp	r3, #6
  21a47c:	d839      	bhi.n	21a4f2 <hci_control_send_hf_event+0x8a>
  21a47e:	e8df f003 	tbb	[pc, r3]
  21a482:	5104      	.short	0x5104
  21a484:	3851511f 	.word	0x3851511f
  21a488:	2e          	.byte	0x2e
  21a489:	00          	.byte	0x00
  21a48a:	2100      	movs	r1, #0
  21a48c:	4a27      	ldr	r2, [pc, #156]	; (21a52c <hci_control_send_hf_event+0xc4>)
  21a48e:	4608      	mov	r0, r1
  21a490:	f5f3 f8e4 	bl	d65c <wiced_va_printf+0x1f7>
  21a494:	1d63      	adds	r3, r4, #5
  21a496:	f10d 0106 	add.w	r1, sp, #6
  21a49a:	7818      	ldrb	r0, [r3, #0]
  21a49c:	42a3      	cmp	r3, r4
  21a49e:	f801 0b01 	strb.w	r0, [r1], #1
  21a4a2:	f103 33ff 	add.w	r3, r3, #4294967295
  21a4a6:	d1f8      	bne.n	21a49a <hci_control_send_hf_event+0x32>
  21a4a8:	79a3      	ldrb	r3, [r4, #6]
  21a4aa:	f10d 020d 	add.w	r2, sp, #13
  21a4ae:	722b      	strb	r3, [r5, #8]
  21a4b0:	1b52      	subs	r2, r2, r5
  21a4b2:	4629      	mov	r1, r5
  21a4b4:	4630      	mov	r0, r6
  21a4b6:	b292      	uxth	r2, r2
  21a4b8:	f6c2 fabe 	bl	dca38 <wiced_transport_free_buffer+0x34>
  21a4bc:	b04c      	add	sp, #304	; 0x130
  21a4be:	bd70      	pop	{r4, r5, r6, pc}
  21a4c0:	2100      	movs	r1, #0
  21a4c2:	4a1b      	ldr	r2, [pc, #108]	; (21a530 <hci_control_send_hf_event+0xc8>)
  21a4c4:	4608      	mov	r0, r1
  21a4c6:	f5f3 f8c9 	bl	d65c <wiced_va_printf+0x1f7>
  21a4ca:	6823      	ldr	r3, [r4, #0]
  21a4cc:	8822      	ldrh	r2, [r4, #0]
  21a4ce:	806a      	strh	r2, [r5, #2]
  21a4d0:	0c1a      	lsrs	r2, r3, #16
  21a4d2:	0e1b      	lsrs	r3, r3, #24
  21a4d4:	712a      	strb	r2, [r5, #4]
  21a4d6:	716b      	strb	r3, [r5, #5]
  21a4d8:	f10d 020a 	add.w	r2, sp, #10
  21a4dc:	e7e8      	b.n	21a4b0 <hci_control_send_hf_event+0x48>
  21a4de:	2100      	movs	r1, #0
  21a4e0:	4a14      	ldr	r2, [pc, #80]	; (21a534 <hci_control_send_hf_event+0xcc>)
  21a4e2:	4608      	mov	r0, r1
  21a4e4:	f5f3 f8ba 	bl	d65c <wiced_va_printf+0x1f7>
  21a4e8:	7923      	ldrb	r3, [r4, #4]
  21a4ea:	f10d 0207 	add.w	r2, sp, #7
  21a4ee:	70ab      	strb	r3, [r5, #2]
  21a4f0:	e7de      	b.n	21a4b0 <hci_control_send_hf_event+0x48>
  21a4f2:	2100      	movs	r1, #0
  21a4f4:	4a10      	ldr	r2, [pc, #64]	; (21a538 <hci_control_send_hf_event+0xd0>)
  21a4f6:	4608      	mov	r0, r1
  21a4f8:	f5f3 f8b0 	bl	d65c <wiced_va_printf+0x1f7>
  21a4fc:	b16c      	cbz	r4, 21a51a <hci_control_send_hf_event+0xb2>
  21a4fe:	f834 3b02 	ldrh.w	r3, [r4], #2
  21a502:	a802      	add	r0, sp, #8
  21a504:	4621      	mov	r1, r4
  21a506:	806b      	strh	r3, [r5, #2]
  21a508:	f003 fa2b 	bl	21d962 <utl_strcpy>
  21a50c:	4620      	mov	r0, r4
  21a50e:	f65f fcf9 	bl	79f04 <_printf_int_dec+0x73>
  21a512:	ab02      	add	r3, sp, #8
  21a514:	1c42      	adds	r2, r0, #1
  21a516:	441a      	add	r2, r3
  21a518:	e7ca      	b.n	21a4b0 <hci_control_send_hf_event+0x48>
  21a51a:	806c      	strh	r4, [r5, #2]
  21a51c:	712c      	strb	r4, [r5, #4]
  21a51e:	f10d 0209 	add.w	r2, sp, #9
  21a522:	e7c5      	b.n	21a4b0 <hci_control_send_hf_event+0x48>
  21a524:	f10d 0206 	add.w	r2, sp, #6
  21a528:	e7c2      	b.n	21a4b0 <hci_control_send_hf_event+0x48>
  21a52a:	bf00      	nop
  21a52c:	0021e62a 	.word	0x0021e62a
  21a530:	0021e661 	.word	0x0021e661
  21a534:	0021e69d 	.word	0x0021e69d
  21a538:	0021e6dc 	.word	0x0021e6dc

0021a53c <handsfree_send_ciev_cmd>:
  21a53c:	b570      	push	{r4, r5, r6, lr}
  21a53e:	461c      	mov	r4, r3
  21a540:	460e      	mov	r6, r1
  21a542:	4615      	mov	r5, r2
  21a544:	f003 f87c 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21a548:	232c      	movs	r3, #44	; 0x2c
  21a54a:	70e3      	strb	r3, [r4, #3]
  21a54c:	2300      	movs	r3, #0
  21a54e:	3630      	adds	r6, #48	; 0x30
  21a550:	3530      	adds	r5, #48	; 0x30
  21a552:	70a6      	strb	r6, [r4, #2]
  21a554:	7125      	strb	r5, [r4, #4]
  21a556:	7163      	strb	r3, [r4, #5]
  21a558:	4622      	mov	r2, r4
  21a55a:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21a55e:	8841      	ldrh	r1, [r0, #2]
  21a560:	f240 302a 	movw	r0, #810	; 0x32a
  21a564:	f7ff bf80 	b.w	21a468 <hci_control_send_hf_event>

0021a568 <handsfree_event_callback>:
  21a568:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  21a56c:	4605      	mov	r5, r0
  21a56e:	b0c6      	sub	sp, #280	; 0x118
  21a570:	460c      	mov	r4, r1
  21a572:	f44f 7282 	mov.w	r2, #260	; 0x104
  21a576:	2100      	movs	r1, #0
  21a578:	a805      	add	r0, sp, #20
  21a57a:	f617 fca3 	bl	31ec4 <memcpy+0x7>
  21a57e:	2d13      	cmp	r5, #19
  21a580:	d84a      	bhi.n	21a618 <handsfree_event_callback+0xb0>
  21a582:	e8df f015 	tbh	[pc, r5, lsl #1]
  21a586:	0014      	.short	0x0014
  21a588:	00a40081 	.word	0x00a40081
  21a58c:	00b00116 	.word	0x00b00116
  21a590:	01300128 	.word	0x01300128
  21a594:	011f010d 	.word	0x011f010d
  21a598:	015401d0 	.word	0x015401d0
  21a59c:	020801ed 	.word	0x020801ed
  21a5a0:	01410139 	.word	0x01410139
  21a5a4:	02590149 	.word	0x02590149
  21a5a8:	004901b3 	.word	0x004901b3
  21a5ac:	026b      	.short	0x026b
  21a5ae:	2100      	movs	r1, #0
  21a5b0:	4aad      	ldr	r2, [pc, #692]	; (21a868 <handsfree_event_callback+0x300>)
  21a5b2:	4608      	mov	r0, r1
  21a5b4:	f5f3 f852 	bl	d65c <wiced_va_printf+0x1f7>
  21a5b8:	7aa5      	ldrb	r5, [r4, #10]
  21a5ba:	2d01      	cmp	r5, #1
  21a5bc:	d136      	bne.n	21a62c <handsfree_event_callback+0xc4>
  21a5be:	f104 0804 	add.w	r8, r4, #4
  21a5c2:	4640      	mov	r0, r8
  21a5c4:	f003 f858 	bl	21d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>
  21a5c8:	2206      	movs	r2, #6
  21a5ca:	4607      	mov	r7, r0
  21a5cc:	4641      	mov	r1, r8
  21a5ce:	a803      	add	r0, sp, #12
  21a5d0:	f617 fc74 	bl	31ebc <bsc_OpExtended+0x2d91>
  21a5d4:	2300      	movs	r3, #0
  21a5d6:	f88d 3012 	strb.w	r3, [sp, #18]
  21a5da:	4ea4      	ldr	r6, [pc, #656]	; (21a86c <handsfree_event_callback+0x304>)
  21a5dc:	8879      	ldrh	r1, [r7, #2]
  21a5de:	f240 3001 	movw	r0, #769	; 0x301
  21a5e2:	aa03      	add	r2, sp, #12
  21a5e4:	82f1      	strh	r1, [r6, #22]
  21a5e6:	f7ff ff3f 	bl	21a468 <hci_control_send_hf_event>
  21a5ea:	7ae3      	ldrb	r3, [r4, #11]
  21a5ec:	4634      	mov	r4, r6
  21a5ee:	4ea0      	ldr	r6, [pc, #640]	; (21a870 <handsfree_event_callback+0x308>)
  21a5f0:	b9ab      	cbnz	r3, 21a61e <handsfree_event_callback+0xb6>
  21a5f2:	7233      	strb	r3, [r6, #8]
  21a5f4:	8879      	ldrh	r1, [r7, #2]
  21a5f6:	1d32      	adds	r2, r6, #4
  21a5f8:	f240 3007 	movw	r0, #775	; 0x307
  21a5fc:	f7ff ff34 	bl	21a468 <hci_control_send_hf_event>
  21a600:	489c      	ldr	r0, [pc, #624]	; (21a874 <handsfree_event_callback+0x30c>)
  21a602:	f7ff fd31 	bl	21a068 <wiced_bt_sco_create_as_acceptor>
  21a606:	2100      	movs	r1, #0
  21a608:	8aa3      	ldrh	r3, [r4, #20]
  21a60a:	4a9b      	ldr	r2, [pc, #620]	; (21a878 <handsfree_event_callback+0x310>)
  21a60c:	e9cd 0300 	strd	r0, r3, [sp]
  21a610:	4608      	mov	r0, r1
  21a612:	4b9a      	ldr	r3, [pc, #616]	; (21a87c <handsfree_event_callback+0x314>)
  21a614:	f5f3 f822 	bl	d65c <wiced_va_printf+0x1f7>
  21a618:	b046      	add	sp, #280	; 0x118
  21a61a:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
  21a61e:	2206      	movs	r2, #6
  21a620:	4641      	mov	r1, r8
  21a622:	4620      	mov	r0, r4
  21a624:	7235      	strb	r5, [r6, #8]
  21a626:	f617 fc49 	bl	31ebc <bsc_OpExtended+0x2d91>
  21a62a:	e7e3      	b.n	21a5f4 <handsfree_event_callback+0x8c>
  21a62c:	2d02      	cmp	r5, #2
  21a62e:	d10d      	bne.n	21a64c <handsfree_event_callback+0xe4>
  21a630:	2100      	movs	r1, #0
  21a632:	3404      	adds	r4, #4
  21a634:	4608      	mov	r0, r1
  21a636:	4b91      	ldr	r3, [pc, #580]	; (21a87c <handsfree_event_callback+0x314>)
  21a638:	4a91      	ldr	r2, [pc, #580]	; (21a880 <handsfree_event_callback+0x318>)
  21a63a:	9400      	str	r4, [sp, #0]
  21a63c:	f5f3 f80e 	bl	d65c <wiced_va_printf+0x1f7>
  21a640:	2206      	movs	r2, #6
  21a642:	4621      	mov	r1, r4
  21a644:	4889      	ldr	r0, [pc, #548]	; (21a86c <handsfree_event_callback+0x304>)
  21a646:	f617 fc39 	bl	31ebc <bsc_OpExtended+0x2d91>
  21a64a:	e7e5      	b.n	21a618 <handsfree_event_callback+0xb0>
  21a64c:	2d00      	cmp	r5, #0
  21a64e:	d1e3      	bne.n	21a618 <handsfree_event_callback+0xb0>
  21a650:	f64f 76ff 	movw	r6, #65535	; 0xffff
  21a654:	4c85      	ldr	r4, [pc, #532]	; (21a86c <handsfree_event_callback+0x304>)
  21a656:	2206      	movs	r2, #6
  21a658:	4629      	mov	r1, r5
  21a65a:	4620      	mov	r0, r4
  21a65c:	f617 fc32 	bl	31ec4 <memcpy+0x7>
  21a660:	8aa0      	ldrh	r0, [r4, #20]
  21a662:	42b0      	cmp	r0, r6
  21a664:	d009      	beq.n	21a67a <handsfree_event_callback+0x112>
  21a666:	f7ff fd05 	bl	21a074 <wiced_bt_sco_remove>
  21a66a:	82a6      	strh	r6, [r4, #20]
  21a66c:	4629      	mov	r1, r5
  21a66e:	9000      	str	r0, [sp, #0]
  21a670:	4b82      	ldr	r3, [pc, #520]	; (21a87c <handsfree_event_callback+0x314>)
  21a672:	4628      	mov	r0, r5
  21a674:	4a83      	ldr	r2, [pc, #524]	; (21a884 <handsfree_event_callback+0x31c>)
  21a676:	f5f2 fff1 	bl	d65c <wiced_va_printf+0x1f7>
  21a67a:	2200      	movs	r2, #0
  21a67c:	f240 3002 	movw	r0, #770	; 0x302
  21a680:	8ae1      	ldrh	r1, [r4, #22]
  21a682:	f7ff fef1 	bl	21a468 <hci_control_send_hf_event>
  21a686:	e7c7      	b.n	21a618 <handsfree_event_callback+0xb0>
  21a688:	2100      	movs	r1, #0
  21a68a:	4a7f      	ldr	r2, [pc, #508]	; (21a888 <handsfree_event_callback+0x320>)
  21a68c:	4608      	mov	r0, r1
  21a68e:	f5f2 ffe5 	bl	d65c <wiced_va_printf+0x1f7>
  21a692:	6863      	ldr	r3, [r4, #4]
  21a694:	4a75      	ldr	r2, [pc, #468]	; (21a86c <handsfree_event_callback+0x304>)
  21a696:	9305      	str	r3, [sp, #20]
  21a698:	f3c3 03c0 	ubfx	r3, r3, #3, #1
  21a69c:	7453      	strb	r3, [r2, #17]
  21a69e:	8820      	ldrh	r0, [r4, #0]
  21a6a0:	f002 ffce 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21a6a4:	6863      	ldr	r3, [r4, #4]
  21a6a6:	f413 7f00 	tst.w	r3, #512	; 0x200
  21a6aa:	4b78      	ldr	r3, [pc, #480]	; (21a88c <handsfree_event_callback+0x324>)
  21a6ac:	d00d      	beq.n	21a6ca <handsfree_event_callback+0x162>
  21a6ae:	6ec2      	ldr	r2, [r0, #108]	; 0x6c
  21a6b0:	0611      	lsls	r1, r2, #24
  21a6b2:	d50a      	bpl.n	21a6ca <handsfree_event_callback+0x162>
  21a6b4:	2201      	movs	r2, #1
  21a6b6:	f240 3503 	movw	r5, #771	; 0x303
  21a6ba:	715a      	strb	r2, [r3, #5]
  21a6bc:	8820      	ldrh	r0, [r4, #0]
  21a6be:	f002 ffbf 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21a6c2:	aa05      	add	r2, sp, #20
  21a6c4:	8841      	ldrh	r1, [r0, #2]
  21a6c6:	4628      	mov	r0, r5
  21a6c8:	e7db      	b.n	21a682 <handsfree_event_callback+0x11a>
  21a6ca:	2200      	movs	r2, #0
  21a6cc:	e7f3      	b.n	21a6b6 <handsfree_event_callback+0x14e>
  21a6ce:	2100      	movs	r1, #0
  21a6d0:	4a6f      	ldr	r2, [pc, #444]	; (21a890 <handsfree_event_callback+0x328>)
  21a6d2:	4608      	mov	r0, r1
  21a6d4:	f5f2 ffc2 	bl	d65c <wiced_va_printf+0x1f7>
  21a6d8:	2101      	movs	r1, #1
  21a6da:	7922      	ldrb	r2, [r4, #4]
  21a6dc:	ab05      	add	r3, sp, #20
  21a6de:	8820      	ldrh	r0, [r4, #0]
  21a6e0:	f7ff ff2c 	bl	21a53c <handsfree_send_ciev_cmd>
  21a6e4:	e798      	b.n	21a618 <handsfree_event_callback+0xb0>
  21a6e6:	2100      	movs	r1, #0
  21a6e8:	4d60      	ldr	r5, [pc, #384]	; (21a86c <handsfree_event_callback+0x304>)
  21a6ea:	4a6a      	ldr	r2, [pc, #424]	; (21a894 <handsfree_event_callback+0x32c>)
  21a6ec:	4608      	mov	r0, r1
  21a6ee:	f5f2 ffb5 	bl	d65c <wiced_va_printf+0x1f7>
  21a6f2:	79a2      	ldrb	r2, [r4, #6]
  21a6f4:	68ab      	ldr	r3, [r5, #8]
  21a6f6:	4293      	cmp	r3, r2
  21a6f8:	d004      	beq.n	21a704 <handsfree_event_callback+0x19c>
  21a6fa:	2102      	movs	r1, #2
  21a6fc:	8820      	ldrh	r0, [r4, #0]
  21a6fe:	ab05      	add	r3, sp, #20
  21a700:	f7ff ff1c 	bl	21a53c <handsfree_send_ciev_cmd>
  21a704:	7962      	ldrb	r2, [r4, #5]
  21a706:	68eb      	ldr	r3, [r5, #12]
  21a708:	4293      	cmp	r3, r2
  21a70a:	d004      	beq.n	21a716 <handsfree_event_callback+0x1ae>
  21a70c:	2104      	movs	r1, #4
  21a70e:	8820      	ldrh	r0, [r4, #0]
  21a710:	ab05      	add	r3, sp, #20
  21a712:	f7ff ff13 	bl	21a53c <handsfree_send_ciev_cmd>
  21a716:	7922      	ldrb	r2, [r4, #4]
  21a718:	7c2b      	ldrb	r3, [r5, #16]
  21a71a:	4293      	cmp	r3, r2
  21a71c:	d004      	beq.n	21a728 <handsfree_event_callback+0x1c0>
  21a71e:	2103      	movs	r1, #3
  21a720:	8820      	ldrh	r0, [r4, #0]
  21a722:	ab05      	add	r3, sp, #20
  21a724:	f7ff ff0a 	bl	21a53c <handsfree_send_ciev_cmd>
  21a728:	7923      	ldrb	r3, [r4, #4]
  21a72a:	2b03      	cmp	r3, #3
  21a72c:	d81a      	bhi.n	21a764 <handsfree_event_callback+0x1fc>
  21a72e:	e8df f003 	tbb	[pc, r3]
  21a732:	020a      	.short	0x020a
  21a734:	3029      	.short	0x3029
  21a736:	2100      	movs	r1, #0
  21a738:	4a57      	ldr	r2, [pc, #348]	; (21a898 <handsfree_event_callback+0x330>)
  21a73a:	4608      	mov	r0, r1
  21a73c:	f5f2 ff8e 	bl	d65c <wiced_va_printf+0x1f7>
  21a740:	4a56      	ldr	r2, [pc, #344]	; (21a89c <handsfree_event_callback+0x334>)
  21a742:	2100      	movs	r1, #0
  21a744:	e00b      	b.n	21a75e <handsfree_event_callback+0x1f6>
  21a746:	2100      	movs	r1, #0
  21a748:	4a55      	ldr	r2, [pc, #340]	; (21a8a0 <handsfree_event_callback+0x338>)
  21a74a:	4608      	mov	r0, r1
  21a74c:	f5f2 ff86 	bl	d65c <wiced_va_printf+0x1f7>
  21a750:	79a1      	ldrb	r1, [r4, #6]
  21a752:	b999      	cbnz	r1, 21a77c <handsfree_event_callback+0x214>
  21a754:	7c2b      	ldrb	r3, [r5, #16]
  21a756:	3b01      	subs	r3, #1
  21a758:	2b02      	cmp	r3, #2
  21a75a:	d80a      	bhi.n	21a772 <handsfree_event_callback+0x20a>
  21a75c:	4a51      	ldr	r2, [pc, #324]	; (21a8a4 <handsfree_event_callback+0x33c>)
  21a75e:	4608      	mov	r0, r1
  21a760:	f5f2 ff7c 	bl	d65c <wiced_va_printf+0x1f7>
  21a764:	79a3      	ldrb	r3, [r4, #6]
  21a766:	60ab      	str	r3, [r5, #8]
  21a768:	7923      	ldrb	r3, [r4, #4]
  21a76a:	742b      	strb	r3, [r5, #16]
  21a76c:	7963      	ldrb	r3, [r4, #5]
  21a76e:	60eb      	str	r3, [r5, #12]
  21a770:	e752      	b.n	21a618 <handsfree_event_callback+0xb0>
  21a772:	68ab      	ldr	r3, [r5, #8]
  21a774:	2b01      	cmp	r3, #1
  21a776:	d1f5      	bne.n	21a764 <handsfree_event_callback+0x1fc>
  21a778:	4a4b      	ldr	r2, [pc, #300]	; (21a8a8 <handsfree_event_callback+0x340>)
  21a77a:	e7f0      	b.n	21a75e <handsfree_event_callback+0x1f6>
  21a77c:	2901      	cmp	r1, #1
  21a77e:	d1f1      	bne.n	21a764 <handsfree_event_callback+0x1fc>
  21a780:	4a4a      	ldr	r2, [pc, #296]	; (21a8ac <handsfree_event_callback+0x344>)
  21a782:	e7de      	b.n	21a742 <handsfree_event_callback+0x1da>
  21a784:	2100      	movs	r1, #0
  21a786:	4a4a      	ldr	r2, [pc, #296]	; (21a8b0 <handsfree_event_callback+0x348>)
  21a788:	4608      	mov	r0, r1
  21a78a:	f5f2 ff67 	bl	d65c <wiced_va_printf+0x1f7>
  21a78e:	4a49      	ldr	r2, [pc, #292]	; (21a8b4 <handsfree_event_callback+0x34c>)
  21a790:	e7d7      	b.n	21a742 <handsfree_event_callback+0x1da>
  21a792:	2100      	movs	r1, #0
  21a794:	4a48      	ldr	r2, [pc, #288]	; (21a8b8 <handsfree_event_callback+0x350>)
  21a796:	4608      	mov	r0, r1
  21a798:	f5f2 ff60 	bl	d65c <wiced_va_printf+0x1f7>
  21a79c:	4a47      	ldr	r2, [pc, #284]	; (21a8bc <handsfree_event_callback+0x354>)
  21a79e:	e7d0      	b.n	21a742 <handsfree_event_callback+0x1da>
  21a7a0:	2100      	movs	r1, #0
  21a7a2:	4a47      	ldr	r2, [pc, #284]	; (21a8c0 <handsfree_event_callback+0x358>)
  21a7a4:	4608      	mov	r0, r1
  21a7a6:	f5f2 ff59 	bl	d65c <wiced_va_printf+0x1f7>
  21a7aa:	2105      	movs	r1, #5
  21a7ac:	7922      	ldrb	r2, [r4, #4]
  21a7ae:	ab05      	add	r3, sp, #20
  21a7b0:	e795      	b.n	21a6de <handsfree_event_callback+0x176>
  21a7b2:	2100      	movs	r1, #0
  21a7b4:	4a43      	ldr	r2, [pc, #268]	; (21a8c4 <handsfree_event_callback+0x35c>)
  21a7b6:	4608      	mov	r0, r1
  21a7b8:	f5f2 ff50 	bl	d65c <wiced_va_printf+0x1f7>
  21a7bc:	2106      	movs	r1, #6
  21a7be:	7922      	ldrb	r2, [r4, #4]
  21a7c0:	ab05      	add	r3, sp, #20
  21a7c2:	e78c      	b.n	21a6de <handsfree_event_callback+0x176>
  21a7c4:	2100      	movs	r1, #0
  21a7c6:	4a40      	ldr	r2, [pc, #256]	; (21a8c8 <handsfree_event_callback+0x360>)
  21a7c8:	4608      	mov	r0, r1
  21a7ca:	f5f2 ff47 	bl	d65c <wiced_va_printf+0x1f7>
  21a7ce:	2107      	movs	r1, #7
  21a7d0:	7922      	ldrb	r2, [r4, #4]
  21a7d2:	ab05      	add	r3, sp, #20
  21a7d4:	e783      	b.n	21a6de <handsfree_event_callback+0x176>
  21a7d6:	2100      	movs	r1, #0
  21a7d8:	4a3c      	ldr	r2, [pc, #240]	; (21a8cc <handsfree_event_callback+0x364>)
  21a7da:	4608      	mov	r0, r1
  21a7dc:	f5f2 ff3e 	bl	d65c <wiced_va_printf+0x1f7>
  21a7e0:	f240 3523 	movw	r5, #803	; 0x323
  21a7e4:	e76a      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a7e6:	2100      	movs	r1, #0
  21a7e8:	4a39      	ldr	r2, [pc, #228]	; (21a8d0 <handsfree_event_callback+0x368>)
  21a7ea:	4608      	mov	r0, r1
  21a7ec:	f5f2 ff36 	bl	d65c <wiced_va_printf+0x1f7>
  21a7f0:	7922      	ldrb	r2, [r4, #4]
  21a7f2:	4b1e      	ldr	r3, [pc, #120]	; (21a86c <handsfree_event_callback+0x304>)
  21a7f4:	745a      	strb	r2, [r3, #17]
  21a7f6:	e70f      	b.n	21a618 <handsfree_event_callback+0xb0>
  21a7f8:	2100      	movs	r1, #0
  21a7fa:	4a36      	ldr	r2, [pc, #216]	; (21a8d4 <handsfree_event_callback+0x36c>)
  21a7fc:	4608      	mov	r0, r1
  21a7fe:	f5f2 ff2d 	bl	d65c <wiced_va_printf+0x1f7>
  21a802:	f44f 7548 	mov.w	r5, #800	; 0x320
  21a806:	e759      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a808:	2100      	movs	r1, #0
  21a80a:	4a33      	ldr	r2, [pc, #204]	; (21a8d8 <handsfree_event_callback+0x370>)
  21a80c:	4608      	mov	r0, r1
  21a80e:	f5f2 ff25 	bl	d65c <wiced_va_printf+0x1f7>
  21a812:	f240 3521 	movw	r5, #801	; 0x321
  21a816:	e751      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a818:	2100      	movs	r1, #0
  21a81a:	4a30      	ldr	r2, [pc, #192]	; (21a8dc <handsfree_event_callback+0x374>)
  21a81c:	4608      	mov	r0, r1
  21a81e:	f5f2 ff1d 	bl	d65c <wiced_va_printf+0x1f7>
  21a822:	7923      	ldrb	r3, [r4, #4]
  21a824:	f240 3522 	movw	r5, #802	; 0x322
  21a828:	f8ad 3014 	strh.w	r3, [sp, #20]
  21a82c:	e746      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a82e:	2100      	movs	r1, #0
  21a830:	4a2b      	ldr	r2, [pc, #172]	; (21a8e0 <handsfree_event_callback+0x378>)
  21a832:	4608      	mov	r0, r1
  21a834:	f5f2 ff12 	bl	d65c <wiced_va_printf+0x1f7>
  21a838:	1d25      	adds	r5, r4, #4
  21a83a:	f894 3024 	ldrb.w	r3, [r4, #36]	; 0x24
  21a83e:	4629      	mov	r1, r5
  21a840:	22ff      	movs	r2, #255	; 0xff
  21a842:	f10d 0016 	add.w	r0, sp, #22
  21a846:	f8ad 3014 	strh.w	r3, [sp, #20]
  21a84a:	f698 f83d 	bl	b28c8 <A30fb2800+0x52b0>
  21a84e:	2100      	movs	r1, #0
  21a850:	f894 3024 	ldrb.w	r3, [r4, #36]	; 0x24
  21a854:	4608      	mov	r0, r1
  21a856:	e9cd 5300 	strd	r5, r3, [sp]
  21a85a:	4a22      	ldr	r2, [pc, #136]	; (21a8e4 <handsfree_event_callback+0x37c>)
  21a85c:	4b22      	ldr	r3, [pc, #136]	; (21a8e8 <handsfree_event_callback+0x380>)
  21a85e:	f5f2 fefd 	bl	d65c <wiced_va_printf+0x1f7>
  21a862:	f240 3529 	movw	r5, #809	; 0x329
  21a866:	e729      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a868:	0021e709 	.word	0x0021e709
  21a86c:	0021fd30 	.word	0x0021fd30
  21a870:	0021fd4c 	.word	0x0021fd4c
  21a874:	0021fd44 	.word	0x0021fd44
  21a878:	0021e74a 	.word	0x0021e74a
  21a87c:	0021dcf6 	.word	0x0021dcf6
  21a880:	0021e76c 	.word	0x0021e76c
  21a884:	0021e784 	.word	0x0021e784
  21a888:	0021e7a2 	.word	0x0021e7a2
  21a88c:	0021f99c 	.word	0x0021f99c
  21a890:	0021e7e5 	.word	0x0021e7e5
  21a894:	0021e823 	.word	0x0021e823
  21a898:	0021e85e 	.word	0x0021e85e
  21a89c:	0021e8ad 	.word	0x0021e8ad
  21a8a0:	0021e8c9 	.word	0x0021e8c9
  21a8a4:	0021e914 	.word	0x0021e914
  21a8a8:	0021e938 	.word	0x0021e938
  21a8ac:	0021e94a 	.word	0x0021e94a
  21a8b0:	0021e96b 	.word	0x0021e96b
  21a8b4:	0021e9b9 	.word	0x0021e9b9
  21a8b8:	0021e9d5 	.word	0x0021e9d5
  21a8bc:	0021ea24 	.word	0x0021ea24
  21a8c0:	0021ea3f 	.word	0x0021ea3f
  21a8c4:	0021ea78 	.word	0x0021ea78
  21a8c8:	0021eab5 	.word	0x0021eab5
  21a8cc:	0021eaf8 	.word	0x0021eaf8
  21a8d0:	0021eb2d 	.word	0x0021eb2d
  21a8d4:	0021eb6f 	.word	0x0021eb6f
  21a8d8:	0021eba2 	.word	0x0021eba2
  21a8dc:	0021ebd8 	.word	0x0021ebd8
  21a8e0:	0021ec12 	.word	0x0021ec12
  21a8e4:	0021ec4b 	.word	0x0021ec4b
  21a8e8:	0021dd19 	.word	0x0021dd19
  21a8ec:	2100      	movs	r1, #0
  21a8ee:	4a65      	ldr	r2, [pc, #404]	; (21aa84 <handsfree_event_callback+0x51c>)
  21a8f0:	4608      	mov	r0, r1
  21a8f2:	f5f2 feb3 	bl	d65c <wiced_va_printf+0x1f7>
  21a8f6:	1d25      	adds	r5, r4, #4
  21a8f8:	f894 3024 	ldrb.w	r3, [r4, #36]	; 0x24
  21a8fc:	4629      	mov	r1, r5
  21a8fe:	22ff      	movs	r2, #255	; 0xff
  21a900:	f10d 0016 	add.w	r0, sp, #22
  21a904:	f8ad 3014 	strh.w	r3, [sp, #20]
  21a908:	f697 ffde 	bl	b28c8 <A30fb2800+0x52b0>
  21a90c:	2100      	movs	r1, #0
  21a90e:	f894 3024 	ldrb.w	r3, [r4, #36]	; 0x24
  21a912:	4608      	mov	r0, r1
  21a914:	e9cd 5300 	strd	r5, r3, [sp]
  21a918:	4a5b      	ldr	r2, [pc, #364]	; (21aa88 <handsfree_event_callback+0x520>)
  21a91a:	4b5c      	ldr	r3, [pc, #368]	; (21aa8c <handsfree_event_callback+0x524>)
  21a91c:	f5f2 fe9e 	bl	d65c <wiced_va_printf+0x1f7>
  21a920:	f240 352b 	movw	r5, #811	; 0x32b
  21a924:	e6ca      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a926:	2100      	movs	r1, #0
  21a928:	4a59      	ldr	r2, [pc, #356]	; (21aa90 <handsfree_event_callback+0x528>)
  21a92a:	4608      	mov	r0, r1
  21a92c:	f5f2 fe96 	bl	d65c <wiced_va_printf+0x1f7>
  21a930:	7963      	ldrb	r3, [r4, #5]
  21a932:	7921      	ldrb	r1, [r4, #4]
  21a934:	4a57      	ldr	r2, [pc, #348]	; (21aa94 <handsfree_event_callback+0x52c>)
  21a936:	9300      	str	r3, [sp, #0]
  21a938:	4b57      	ldr	r3, [pc, #348]	; (21aa98 <handsfree_event_callback+0x530>)
  21a93a:	f240 3525 	movw	r5, #805	; 0x325
  21a93e:	2900      	cmp	r1, #0
  21a940:	bf18      	it	ne
  21a942:	4613      	movne	r3, r2
  21a944:	2100      	movs	r1, #0
  21a946:	4a55      	ldr	r2, [pc, #340]	; (21aa9c <handsfree_event_callback+0x534>)
  21a948:	4608      	mov	r0, r1
  21a94a:	f5f2 fe87 	bl	d65c <wiced_va_printf+0x1f7>
  21a94e:	7923      	ldrb	r3, [r4, #4]
  21a950:	2b01      	cmp	r3, #1
  21a952:	7963      	ldrb	r3, [r4, #5]
  21a954:	bf18      	it	ne
  21a956:	f44f 7549 	movne.w	r5, #804	; 0x324
  21a95a:	f8ad 3014 	strh.w	r3, [sp, #20]
  21a95e:	e6ad      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a960:	2100      	movs	r1, #0
  21a962:	4a4f      	ldr	r2, [pc, #316]	; (21aaa0 <handsfree_event_callback+0x538>)
  21a964:	4608      	mov	r0, r1
  21a966:	f5f2 fe79 	bl	d65c <wiced_va_printf+0x1f7>
  21a96a:	7923      	ldrb	r3, [r4, #4]
  21a96c:	4d4d      	ldr	r5, [pc, #308]	; (21aaa4 <handsfree_event_callback+0x53c>)
  21a96e:	1e9a      	subs	r2, r3, #2
  21a970:	4251      	negs	r1, r2
  21a972:	f8ad 3014 	strh.w	r3, [sp, #20]
  21a976:	7e2b      	ldrb	r3, [r5, #24]
  21a978:	4151      	adcs	r1, r2
  21a97a:	4a4b      	ldr	r2, [pc, #300]	; (21aaa8 <handsfree_event_callback+0x540>)
  21a97c:	2b01      	cmp	r3, #1
  21a97e:	7151      	strb	r1, [r2, #5]
  21a980:	d106      	bne.n	21a990 <handsfree_event_callback+0x428>
  21a982:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  21a986:	4849      	ldr	r0, [pc, #292]	; (21aaac <handsfree_event_callback+0x544>)
  21a988:	f6c3 fea8 	bl	de6dc <wiced_deinit_timer+0x19>
  21a98c:	2300      	movs	r3, #0
  21a98e:	762b      	strb	r3, [r5, #24]
  21a990:	f240 3533 	movw	r5, #819	; 0x333
  21a994:	e692      	b.n	21a6bc <handsfree_event_callback+0x154>
  21a996:	2100      	movs	r1, #0
  21a998:	4a45      	ldr	r2, [pc, #276]	; (21aab0 <handsfree_event_callback+0x548>)
  21a99a:	4608      	mov	r0, r1
  21a99c:	f5f2 fe5e 	bl	d65c <wiced_va_printf+0x1f7>
  21a9a0:	8820      	ldrh	r0, [r4, #0]
  21a9a2:	f002 fe4d 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21a9a6:	252c      	movs	r5, #44	; 0x2c
  21a9a8:	7923      	ldrb	r3, [r4, #4]
  21a9aa:	4606      	mov	r6, r0
  21a9ac:	3330      	adds	r3, #48	; 0x30
  21a9ae:	f88d 3016 	strb.w	r3, [sp, #22]
  21a9b2:	7963      	ldrb	r3, [r4, #5]
  21a9b4:	f88d 5017 	strb.w	r5, [sp, #23]
  21a9b8:	3330      	adds	r3, #48	; 0x30
  21a9ba:	f88d 3018 	strb.w	r3, [sp, #24]
  21a9be:	79a3      	ldrb	r3, [r4, #6]
  21a9c0:	f88d 5019 	strb.w	r5, [sp, #25]
  21a9c4:	3330      	adds	r3, #48	; 0x30
  21a9c6:	f88d 301a 	strb.w	r3, [sp, #26]
  21a9ca:	79e3      	ldrb	r3, [r4, #7]
  21a9cc:	f88d 501b 	strb.w	r5, [sp, #27]
  21a9d0:	3330      	adds	r3, #48	; 0x30
  21a9d2:	f88d 301c 	strb.w	r3, [sp, #28]
  21a9d6:	7a23      	ldrb	r3, [r4, #8]
  21a9d8:	f88d 501d 	strb.w	r5, [sp, #29]
  21a9dc:	3330      	adds	r3, #48	; 0x30
  21a9de:	f88d 301e 	strb.w	r3, [sp, #30]
  21a9e2:	f894 3029 	ldrb.w	r3, [r4, #41]	; 0x29
  21a9e6:	b32b      	cbz	r3, 21aa34 <handsfree_event_callback+0x4cc>
  21a9e8:	f104 0709 	add.w	r7, r4, #9
  21a9ec:	4638      	mov	r0, r7
  21a9ee:	f88d 501f 	strb.w	r5, [sp, #31]
  21a9f2:	f65f fa87 	bl	79f04 <_printf_int_dec+0x73>
  21a9f6:	4639      	mov	r1, r7
  21a9f8:	4602      	mov	r2, r0
  21a9fa:	a808      	add	r0, sp, #32
  21a9fc:	f617 fa5e 	bl	31ebc <bsc_OpExtended+0x2d91>
  21aa00:	4638      	mov	r0, r7
  21aa02:	f65f fa7f 	bl	79f04 <_printf_int_dec+0x73>
  21aa06:	ab05      	add	r3, sp, #20
  21aa08:	4403      	add	r3, r0
  21aa0a:	731d      	strb	r5, [r3, #12]
  21aa0c:	f100 010d 	add.w	r1, r0, #13
  21aa10:	ab05      	add	r3, sp, #20
  21aa12:	f100 070b 	add.w	r7, r0, #11
  21aa16:	4419      	add	r1, r3
  21aa18:	f894 0029 	ldrb.w	r0, [r4, #41]	; 0x29
  21aa1c:	f002 ffc8 	bl	21d9b0 <utl_itoa>
  21aa20:	19c3      	adds	r3, r0, r7
  21aa22:	aa05      	add	r2, sp, #20
  21aa24:	4413      	add	r3, r2
  21aa26:	2200      	movs	r2, #0
  21aa28:	709a      	strb	r2, [r3, #2]
  21aa2a:	f240 3031 	movw	r0, #817	; 0x331
  21aa2e:	8871      	ldrh	r1, [r6, #2]
  21aa30:	aa05      	add	r2, sp, #20
  21aa32:	e626      	b.n	21a682 <handsfree_event_callback+0x11a>
  21aa34:	2309      	movs	r3, #9
  21aa36:	e7f4      	b.n	21aa22 <handsfree_event_callback+0x4ba>
  21aa38:	2100      	movs	r1, #0
  21aa3a:	4a1e      	ldr	r2, [pc, #120]	; (21aab4 <handsfree_event_callback+0x54c>)
  21aa3c:	4608      	mov	r0, r1
  21aa3e:	1d25      	adds	r5, r4, #4
  21aa40:	f5f2 fe0c 	bl	d65c <wiced_va_printf+0x1f7>
  21aa44:	4628      	mov	r0, r5
  21aa46:	f65f fa5d 	bl	79f04 <_printf_int_dec+0x73>
  21aa4a:	4629      	mov	r1, r5
  21aa4c:	4602      	mov	r2, r0
  21aa4e:	f10d 0016 	add.w	r0, sp, #22
  21aa52:	f617 fa33 	bl	31ebc <bsc_OpExtended+0x2d91>
  21aa56:	f240 352e 	movw	r5, #814	; 0x32e
  21aa5a:	e62f      	b.n	21a6bc <handsfree_event_callback+0x154>
  21aa5c:	2100      	movs	r1, #0
  21aa5e:	4a16      	ldr	r2, [pc, #88]	; (21aab8 <handsfree_event_callback+0x550>)
  21aa60:	4608      	mov	r0, r1
  21aa62:	f5f2 fdfb 	bl	d65c <wiced_va_printf+0x1f7>
  21aa66:	7923      	ldrb	r3, [r4, #4]
  21aa68:	f240 3532 	movw	r5, #818	; 0x332
  21aa6c:	3330      	adds	r3, #48	; 0x30
  21aa6e:	f88d 3016 	strb.w	r3, [sp, #22]
  21aa72:	232c      	movs	r3, #44	; 0x2c
  21aa74:	f88d 3017 	strb.w	r3, [sp, #23]
  21aa78:	7963      	ldrb	r3, [r4, #5]
  21aa7a:	3330      	adds	r3, #48	; 0x30
  21aa7c:	f88d 3018 	strb.w	r3, [sp, #24]
  21aa80:	e61c      	b.n	21a6bc <handsfree_event_callback+0x154>
  21aa82:	bf00      	nop
  21aa84:	0021ec6b 	.word	0x0021ec6b
  21aa88:	0021eca0 	.word	0x0021eca0
  21aa8c:	0021dd19 	.word	0x0021dd19
  21aa90:	0021ecc0 	.word	0x0021ecc0
  21aa94:	0021e705 	.word	0x0021e705
  21aa98:	0021e701 	.word	0x0021e701
  21aa9c:	0021ecfe 	.word	0x0021ecfe
  21aaa0:	0021ed10 	.word	0x0021ed10
  21aaa4:	0021fd30 	.word	0x0021fd30
  21aaa8:	0021f99c 	.word	0x0021f99c
  21aaac:	0021fd58 	.word	0x0021fd58
  21aab0:	0021ed4b 	.word	0x0021ed4b
  21aab4:	0021ed88 	.word	0x0021ed88
  21aab8:	0021edbd 	.word	0x0021edbd

0021aabc <handsfree_init_context_data>:
  21aabc:	f640 0108 	movw	r1, #2056	; 0x808
  21aac0:	4b05      	ldr	r3, [pc, #20]	; (21aad8 <handsfree_init_context_data+0x1c>)
  21aac2:	2200      	movs	r2, #0
  21aac4:	8259      	strh	r1, [r3, #18]
  21aac6:	f64f 71ff 	movw	r1, #65535	; 0xffff
  21aaca:	e9c3 2202 	strd	r2, r2, [r3, #8]
  21aace:	741a      	strb	r2, [r3, #16]
  21aad0:	719a      	strb	r2, [r3, #6]
  21aad2:	8299      	strh	r1, [r3, #20]
  21aad4:	761a      	strb	r2, [r3, #24]
  21aad6:	4770      	bx	lr
  21aad8:	0021fd30 	.word	0x0021fd30

0021aadc <handsfree_hfp_init>:
  21aadc:	b510      	push	{r4, lr}
  21aade:	b086      	sub	sp, #24
  21aae0:	f7ff ffec 	bl	21aabc <handsfree_init_context_data>
  21aae4:	2104      	movs	r1, #4
  21aae6:	f44f 702f 	mov.w	r0, #700	; 0x2bc
  21aaea:	f6be fbdf 	bl	d92ac <wiced_bt_rfcomm_on_num_complete+0x5>
  21aaee:	b284      	uxth	r4, r0
  21aaf0:	b134      	cbz	r4, 21ab00 <handsfree_hfp_init+0x24>
  21aaf2:	2100      	movs	r1, #0
  21aaf4:	4a10      	ldr	r2, [pc, #64]	; (21ab38 <handsfree_hfp_init+0x5c>)
  21aaf6:	4608      	mov	r0, r1
  21aaf8:	f5f2 fdb0 	bl	d65c <wiced_va_printf+0x1f7>
  21aafc:	b006      	add	sp, #24
  21aafe:	bd10      	pop	{r4, pc}
  21ab00:	4b0e      	ldr	r3, [pc, #56]	; (21ab3c <handsfree_hfp_init+0x60>)
  21ab02:	490f      	ldr	r1, [pc, #60]	; (21ab40 <handsfree_hfp_init+0x64>)
  21ab04:	8a5b      	ldrh	r3, [r3, #18]
  21ab06:	a802      	add	r0, sp, #8
  21ab08:	f8ad 3008 	strh.w	r3, [sp, #8]
  21ab0c:	f44f 73ff 	mov.w	r3, #510	; 0x1fe
  21ab10:	9303      	str	r3, [sp, #12]
  21ab12:	f240 1301 	movw	r3, #257	; 0x101
  21ab16:	f8ad 3010 	strh.w	r3, [sp, #16]
  21ab1a:	f241 131e 	movw	r3, #4382	; 0x111e
  21ab1e:	f8ad 3014 	strh.w	r3, [sp, #20]
  21ab22:	f001 fe3f 	bl	21c7a4 <wiced_bt_hfp_hf_init>
  21ab26:	4621      	mov	r1, r4
  21ab28:	9000      	str	r0, [sp, #0]
  21ab2a:	4b06      	ldr	r3, [pc, #24]	; (21ab44 <handsfree_hfp_init+0x68>)
  21ab2c:	4620      	mov	r0, r4
  21ab2e:	4a06      	ldr	r2, [pc, #24]	; (21ab48 <handsfree_hfp_init+0x6c>)
  21ab30:	f5f2 fd94 	bl	d65c <wiced_va_printf+0x1f7>
  21ab34:	e7e2      	b.n	21aafc <handsfree_hfp_init+0x20>
  21ab36:	bf00      	nop
  21ab38:	0021edf2 	.word	0x0021edf2
  21ab3c:	0021fd30 	.word	0x0021fd30
  21ab40:	0021a569 	.word	0x0021a569
  21ab44:	0021dd32 	.word	0x0021dd32
  21ab48:	0021ee1b 	.word	0x0021ee1b

0021ab4c <a2dp_sink_hf_write_eir>:
  21ab4c:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  21ab4e:	f44f 7084 	mov.w	r0, #264	; 0x108
  21ab52:	f6b6 fb3d 	bl	d11d0 <wiced_bt_get_buffer>
  21ab56:	2100      	movs	r1, #0
  21ab58:	4605      	mov	r5, r0
  21ab5a:	4603      	mov	r3, r0
  21ab5c:	4a1b      	ldr	r2, [pc, #108]	; (21abcc <a2dp_sink_hf_write_eir+0x80>)
  21ab5e:	4608      	mov	r0, r1
  21ab60:	f5f2 fd7c 	bl	d65c <wiced_va_printf+0x1f7>
  21ab64:	b38d      	cbz	r5, 21abca <a2dp_sink_hf_write_eir+0x7e>
  21ab66:	4b1a      	ldr	r3, [pc, #104]	; (21abd0 <a2dp_sink_hf_write_eir+0x84>)
  21ab68:	462e      	mov	r6, r5
  21ab6a:	681f      	ldr	r7, [r3, #0]
  21ab6c:	4638      	mov	r0, r7
  21ab6e:	f65f f9c9 	bl	79f04 <_printf_int_dec+0x73>
  21ab72:	1c43      	adds	r3, r0, #1
  21ab74:	f806 3b02 	strb.w	r3, [r6], #2
  21ab78:	2309      	movs	r3, #9
  21ab7a:	b2c4      	uxtb	r4, r0
  21ab7c:	4622      	mov	r2, r4
  21ab7e:	4639      	mov	r1, r7
  21ab80:	706b      	strb	r3, [r5, #1]
  21ab82:	4630      	mov	r0, r6
  21ab84:	f617 f99a 	bl	31ebc <bsc_OpExtended+0x2d91>
  21ab88:	2303      	movs	r3, #3
  21ab8a:	1931      	adds	r1, r6, r4
  21ab8c:	5533      	strb	r3, [r6, r4]
  21ab8e:	220b      	movs	r2, #11
  21ab90:	704b      	strb	r3, [r1, #1]
  21ab92:	718b      	strb	r3, [r1, #6]
  21ab94:	2312      	movs	r3, #18
  21ab96:	708a      	strb	r2, [r1, #2]
  21ab98:	71cb      	strb	r3, [r1, #7]
  21ab9a:	2211      	movs	r2, #17
  21ab9c:	2300      	movs	r3, #0
  21ab9e:	201e      	movs	r0, #30
  21aba0:	70ca      	strb	r2, [r1, #3]
  21aba2:	7108      	strb	r0, [r1, #4]
  21aba4:	714a      	strb	r2, [r1, #5]
  21aba6:	720b      	strb	r3, [r1, #8]
  21aba8:	3109      	adds	r1, #9
  21abaa:	1b4c      	subs	r4, r1, r5
  21abac:	2c64      	cmp	r4, #100	; 0x64
  21abae:	4622      	mov	r2, r4
  21abb0:	bfa8      	it	ge
  21abb2:	2264      	movge	r2, #100	; 0x64
  21abb4:	1c69      	adds	r1, r5, #1
  21abb6:	4807      	ldr	r0, [pc, #28]	; (21abd4 <a2dp_sink_hf_write_eir+0x88>)
  21abb8:	b292      	uxth	r2, r2
  21abba:	f5f2 fbaf 	bl	d31c <wiced_get_debug_uart+0x67>
  21abbe:	4628      	mov	r0, r5
  21abc0:	b2a1      	uxth	r1, r4
  21abc2:	e8bd 40f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, lr}
  21abc6:	f6bc be1c 	b.w	d7802 <wiced_bt_dev_read_rssi+0x3>
  21abca:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
  21abcc:	0021ee78 	.word	0x0021ee78
  21abd0:	0021dc80 	.word	0x0021dc80
  21abd4:	0021ee94 	.word	0x0021ee94

0021abd8 <hf_sco_management_callback>:
  21abd8:	b573      	push	{r0, r1, r4, r5, r6, lr}
  21abda:	4604      	mov	r4, r0
  21abdc:	482e      	ldr	r0, [pc, #184]	; (21ac98 <hf_sco_management_callback+0xc0>)
  21abde:	460d      	mov	r5, r1
  21abe0:	f002 fd4a 	bl	21d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>
  21abe4:	f1a4 031b 	sub.w	r3, r4, #27
  21abe8:	4606      	mov	r6, r0
  21abea:	2b03      	cmp	r3, #3
  21abec:	d819      	bhi.n	21ac22 <hf_sco_management_callback+0x4a>
  21abee:	e8df f003 	tbb	[pc, r3]
  21abf2:	1a02      	.short	0x1a02
  21abf4:	4b34      	.short	0x4b34
  21abf6:	2100      	movs	r1, #0
  21abf8:	4a28      	ldr	r2, [pc, #160]	; (21ac9c <hf_sco_management_callback+0xc4>)
  21abfa:	4608      	mov	r0, r1
  21abfc:	f5f2 fd2e 	bl	d65c <wiced_va_printf+0x1f7>
  21ac00:	2200      	movs	r2, #0
  21ac02:	f44f 7041 	mov.w	r0, #772	; 0x304
  21ac06:	8871      	ldrh	r1, [r6, #2]
  21ac08:	f7ff fc2e 	bl	21a468 <hci_control_send_hf_event>
  21ac0c:	2100      	movs	r1, #0
  21ac0e:	4c22      	ldr	r4, [pc, #136]	; (21ac98 <hf_sco_management_callback+0xc0>)
  21ac10:	4608      	mov	r0, r1
  21ac12:	8aa3      	ldrh	r3, [r4, #20]
  21ac14:	4a22      	ldr	r2, [pc, #136]	; (21aca0 <hf_sco_management_callback+0xc8>)
  21ac16:	9300      	str	r3, [sp, #0]
  21ac18:	882b      	ldrh	r3, [r5, #0]
  21ac1a:	f5f2 fd1f 	bl	d65c <wiced_va_printf+0x1f7>
  21ac1e:	2301      	movs	r3, #1
  21ac20:	7663      	strb	r3, [r4, #25]
  21ac22:	b002      	add	sp, #8
  21ac24:	bd70      	pop	{r4, r5, r6, pc}
  21ac26:	2100      	movs	r1, #0
  21ac28:	4a1c      	ldr	r2, [pc, #112]	; (21ac9c <hf_sco_management_callback+0xc4>)
  21ac2a:	4608      	mov	r0, r1
  21ac2c:	f5f2 fd16 	bl	d65c <wiced_va_printf+0x1f7>
  21ac30:	2200      	movs	r2, #0
  21ac32:	8871      	ldrh	r1, [r6, #2]
  21ac34:	f240 3005 	movw	r0, #773	; 0x305
  21ac38:	f7ff fc16 	bl	21a468 <hci_control_send_hf_event>
  21ac3c:	4819      	ldr	r0, [pc, #100]	; (21aca4 <hf_sco_management_callback+0xcc>)
  21ac3e:	f7ff fa13 	bl	21a068 <wiced_bt_sco_create_as_acceptor>
  21ac42:	2100      	movs	r1, #0
  21ac44:	4c14      	ldr	r4, [pc, #80]	; (21ac98 <hf_sco_management_callback+0xc0>)
  21ac46:	4a18      	ldr	r2, [pc, #96]	; (21aca8 <hf_sco_management_callback+0xd0>)
  21ac48:	8aa3      	ldrh	r3, [r4, #20]
  21ac4a:	e9cd 0300 	strd	r0, r3, [sp]
  21ac4e:	4b17      	ldr	r3, [pc, #92]	; (21acac <hf_sco_management_callback+0xd4>)
  21ac50:	4608      	mov	r0, r1
  21ac52:	f5f2 fd03 	bl	d65c <wiced_va_printf+0x1f7>
  21ac56:	2300      	movs	r3, #0
  21ac58:	e7e2      	b.n	21ac20 <hf_sco_management_callback+0x48>
  21ac5a:	2100      	movs	r1, #0
  21ac5c:	4a0f      	ldr	r2, [pc, #60]	; (21ac9c <hf_sco_management_callback+0xc4>)
  21ac5e:	4608      	mov	r0, r1
  21ac60:	f5f2 fcfc 	bl	d65c <wiced_va_printf+0x1f7>
  21ac64:	4812      	ldr	r0, [pc, #72]	; (21acb0 <hf_sco_management_callback+0xd8>)
  21ac66:	f5f2 ff49 	bl	dafc <wiced_trans_get_available_buffers+0xe3>
  21ac6a:	b110      	cbz	r0, 21ac72 <hf_sco_management_callback+0x9a>
  21ac6c:	4810      	ldr	r0, [pc, #64]	; (21acb0 <hf_sco_management_callback+0xd8>)
  21ac6e:	f6c3 fcff 	bl	de670 <wiced_init_timer+0x33>
  21ac72:	4b10      	ldr	r3, [pc, #64]	; (21acb4 <hf_sco_management_callback+0xdc>)
  21ac74:	7a19      	ldrb	r1, [r3, #8]
  21ac76:	2900      	cmp	r1, #0
  21ac78:	d1d3      	bne.n	21ac22 <hf_sco_management_callback+0x4a>
  21ac7a:	4a0f      	ldr	r2, [pc, #60]	; (21acb8 <hf_sco_management_callback+0xe0>)
  21ac7c:	8828      	ldrh	r0, [r5, #0]
  21ac7e:	b002      	add	sp, #8
  21ac80:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21ac84:	f7ff b9f8 	b.w	21a078 <wiced_bt_sco_accept_connection>
  21ac88:	2100      	movs	r1, #0
  21ac8a:	4608      	mov	r0, r1
  21ac8c:	4a0b      	ldr	r2, [pc, #44]	; (21acbc <hf_sco_management_callback+0xe4>)
  21ac8e:	b002      	add	sp, #8
  21ac90:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21ac94:	f5f2 bce2 	b.w	d65c <wiced_va_printf+0x1f7>
  21ac98:	0021fd30 	.word	0x0021fd30
  21ac9c:	0021ee9a 	.word	0x0021ee9a
  21aca0:	0021eece 	.word	0x0021eece
  21aca4:	0021fd44 	.word	0x0021fd44
  21aca8:	0021e74a 	.word	0x0021e74a
  21acac:	0021dd45 	.word	0x0021dd45
  21acb0:	0021fd58 	.word	0x0021fd58
  21acb4:	0021fd4c 	.word	0x0021fd4c
  21acb8:	0021f99c 	.word	0x0021f99c
  21acbc:	0021ef0e 	.word	0x0021ef0e

0021acc0 <handsfree_write_nvram>:
  21acc0:	b530      	push	{r4, r5, lr}
  21acc2:	b085      	sub	sp, #20
  21acc4:	4605      	mov	r5, r0
  21acc6:	f10d 030e 	add.w	r3, sp, #14
  21acca:	b2c9      	uxtb	r1, r1
  21accc:	b2c0      	uxtb	r0, r0
  21acce:	f6c2 ffb9 	bl	ddc44 <wiced_hal_sflash_detected+0x29>
  21acd2:	2100      	movs	r1, #0
  21acd4:	4604      	mov	r4, r0
  21acd6:	f8bd 300e 	ldrh.w	r3, [sp, #14]
  21acda:	4a05      	ldr	r2, [pc, #20]	; (21acf0 <handsfree_write_nvram+0x30>)
  21acdc:	e9cd 0300 	strd	r0, r3, [sp]
  21ace0:	462b      	mov	r3, r5
  21ace2:	4608      	mov	r0, r1
  21ace4:	f5f2 fcba 	bl	d65c <wiced_va_printf+0x1f7>
  21ace8:	4620      	mov	r0, r4
  21acea:	b005      	add	sp, #20
  21acec:	bd30      	pop	{r4, r5, pc}
  21acee:	bf00      	nop
  21acf0:	0021ef4a 	.word	0x0021ef4a

0021acf4 <handsfree_read_nvram>:
  21acf4:	b530      	push	{r4, r5, lr}
  21acf6:	2a89      	cmp	r2, #137	; 0x89
  21acf8:	4604      	mov	r4, r0
  21acfa:	b087      	sub	sp, #28
  21acfc:	d916      	bls.n	21ad2c <handsfree_read_nvram+0x38>
  21acfe:	460a      	mov	r2, r1
  21ad00:	f10d 0316 	add.w	r3, sp, #22
  21ad04:	218a      	movs	r1, #138	; 0x8a
  21ad06:	b2c0      	uxtb	r0, r0
  21ad08:	f6c3 f85a 	bl	dddc0 <wiced_hal_delete_nvram+0x85>
  21ad0c:	f8bd 3016 	ldrh.w	r3, [sp, #22]
  21ad10:	2100      	movs	r1, #0
  21ad12:	e9cd 0301 	strd	r0, r3, [sp, #4]
  21ad16:	238a      	movs	r3, #138	; 0x8a
  21ad18:	b285      	uxth	r5, r0
  21ad1a:	9300      	str	r3, [sp, #0]
  21ad1c:	4608      	mov	r0, r1
  21ad1e:	4623      	mov	r3, r4
  21ad20:	4a03      	ldr	r2, [pc, #12]	; (21ad30 <handsfree_read_nvram+0x3c>)
  21ad22:	f5f2 fc9b 	bl	d65c <wiced_va_printf+0x1f7>
  21ad26:	4628      	mov	r0, r5
  21ad28:	b007      	add	sp, #28
  21ad2a:	bd30      	pop	{r4, r5, pc}
  21ad2c:	2500      	movs	r5, #0
  21ad2e:	e7fa      	b.n	21ad26 <handsfree_read_nvram+0x32>
  21ad30:	0021ef74 	.word	0x0021ef74

0021ad34 <app_bt_management_callback>:
  21ad34:	b530      	push	{r4, r5, lr}
  21ad36:	4605      	mov	r5, r0
  21ad38:	460c      	mov	r4, r1
  21ad3a:	b091      	sub	sp, #68	; 0x44
  21ad3c:	281e      	cmp	r0, #30
  21ad3e:	d820      	bhi.n	21ad82 <app_bt_management_callback+0x4e>
  21ad40:	e8df f010 	tbh	[pc, r0, lsl #1]
  21ad44:	005e0023 	.word	0x005e0023
  21ad48:	00850127 	.word	0x00850127
  21ad4c:	00a10097 	.word	0x00a10097
  21ad50:	001f001f 	.word	0x001f001f
  21ad54:	001f00ba 	.word	0x001f00ba
  21ad58:	00ca00af 	.word	0x00ca00af
  21ad5c:	007600da 	.word	0x007600da
  21ad60:	001f001f 	.word	0x001f001f
  21ad64:	001f001f 	.word	0x001f001f
  21ad68:	011300e4 	.word	0x011300e4
  21ad6c:	005c005c 	.word	0x005c005c
  21ad70:	001f001f 	.word	0x001f001f
  21ad74:	001f001f 	.word	0x001f001f
  21ad78:	0064001f 	.word	0x0064001f
  21ad7c:	0064006e 	.word	0x0064006e
  21ad80:	0064      	.short	0x0064
  21ad82:	f641 70b8 	movw	r0, #8120	; 0x1fb8
  21ad86:	b011      	add	sp, #68	; 0x44
  21ad88:	bd30      	pop	{r4, r5, pc}
  21ad8a:	2100      	movs	r1, #0
  21ad8c:	2400      	movs	r4, #0
  21ad8e:	4a89      	ldr	r2, [pc, #548]	; (21afb4 <app_bt_management_callback+0x280>)
  21ad90:	4608      	mov	r0, r1
  21ad92:	f5f2 fc63 	bl	d65c <wiced_va_printf+0x1f7>
  21ad96:	2100      	movs	r1, #0
  21ad98:	2001      	movs	r0, #1
  21ad9a:	f6c2 fa24 	bl	dd1e6 <wiced_bt_register_le_num_complete_cback+0x5>
  21ad9e:	4886      	ldr	r0, [pc, #536]	; (21afb8 <app_bt_management_callback+0x284>)
  21ada0:	2302      	movs	r3, #2
  21ada2:	4622      	mov	r2, r4
  21ada4:	4985      	ldr	r1, [pc, #532]	; (21afbc <app_bt_management_callback+0x288>)
  21ada6:	f800 4b0c 	strb.w	r4, [r0], #12
  21adaa:	f6c3 fc47 	bl	de63c <wiced_hal_rand_gen_num_array+0x7f>
  21adae:	f7ff fecd 	bl	21ab4c <a2dp_sink_hf_write_eir>
  21adb2:	f7ff fb41 	bl	21a438 <wiced_app_cfg_sdp_record_get_size>
  21adb6:	4601      	mov	r1, r0
  21adb8:	4881      	ldr	r0, [pc, #516]	; (21afc0 <app_bt_management_callback+0x28c>)
  21adba:	f6bd fb27 	bl	d840c <wiced_bt_gatt_disconnect+0x231>
  21adbe:	f7ff fb27 	bl	21a410 <av_app_init>
  21adc2:	f7ff fe8b 	bl	21aadc <handsfree_hfp_init>
  21adc6:	f44f 6200 	mov.w	r2, #2048	; 0x800
  21adca:	2112      	movs	r1, #18
  21adcc:	2002      	movs	r0, #2
  21adce:	f6bc fcf2 	bl	d77b6 <wiced_bt_dev_vendor_specific_command+0x4>
  21add2:	f44f 6200 	mov.w	r2, #2048	; 0x800
  21add6:	2112      	movs	r1, #18
  21add8:	2001      	movs	r0, #1
  21adda:	f6bc fcee 	bl	d77ba <wiced_bt_dev_set_discoverability+0x3>
  21adde:	210a      	movs	r1, #10
  21ade0:	2091      	movs	r0, #145	; 0x91
  21ade2:	f6b6 f9fb 	bl	d11dc <wiced_bt_get_buffer_size+0x3>
  21ade6:	4a77      	ldr	r2, [pc, #476]	; (21afc4 <app_bt_management_callback+0x290>)
  21ade8:	4603      	mov	r3, r0
  21adea:	6010      	str	r0, [r2, #0]
  21adec:	4621      	mov	r1, r4
  21adee:	4620      	mov	r0, r4
  21adf0:	4a75      	ldr	r2, [pc, #468]	; (21afc8 <app_bt_management_callback+0x294>)
  21adf2:	f5f2 fc33 	bl	d65c <wiced_va_printf+0x1f7>
  21adf6:	4875      	ldr	r0, [pc, #468]	; (21afcc <app_bt_management_callback+0x298>)
  21adf8:	f6bc fdc6 	bl	d7988 <wiced_bt_nvram_access_register+0x5>
  21adfc:	2000      	movs	r0, #0
  21adfe:	e7c2      	b.n	21ad86 <app_bt_management_callback+0x52>
  21ae00:	2100      	movs	r1, #0
  21ae02:	4a6c      	ldr	r2, [pc, #432]	; (21afb4 <app_bt_management_callback+0x280>)
  21ae04:	4608      	mov	r0, r1
  21ae06:	f5f2 fc29 	bl	d65c <wiced_va_printf+0x1f7>
  21ae0a:	e7f7      	b.n	21adfc <app_bt_management_callback+0xc8>
  21ae0c:	2100      	movs	r1, #0
  21ae0e:	4a70      	ldr	r2, [pc, #448]	; (21afd0 <app_bt_management_callback+0x29c>)
  21ae10:	4608      	mov	r0, r1
  21ae12:	f5f2 fc23 	bl	d65c <wiced_va_printf+0x1f7>
  21ae16:	4621      	mov	r1, r4
  21ae18:	4628      	mov	r0, r5
  21ae1a:	f7ff fedd 	bl	21abd8 <hf_sco_management_callback>
  21ae1e:	e7ed      	b.n	21adfc <app_bt_management_callback+0xc8>
  21ae20:	2100      	movs	r1, #0
  21ae22:	4a6c      	ldr	r2, [pc, #432]	; (21afd4 <app_bt_management_callback+0x2a0>)
  21ae24:	4608      	mov	r0, r1
  21ae26:	f5f2 fc19 	bl	d65c <wiced_va_printf+0x1f7>
  21ae2a:	4621      	mov	r1, r4
  21ae2c:	201c      	movs	r0, #28
  21ae2e:	e7f4      	b.n	21ae1a <app_bt_management_callback+0xe6>
  21ae30:	2100      	movs	r1, #0
  21ae32:	4a69      	ldr	r2, [pc, #420]	; (21afd8 <app_bt_management_callback+0x2a4>)
  21ae34:	4608      	mov	r0, r1
  21ae36:	f5f2 fc11 	bl	d65c <wiced_va_printf+0x1f7>
  21ae3a:	4b5f      	ldr	r3, [pc, #380]	; (21afb8 <app_bt_management_callback+0x284>)
  21ae3c:	781b      	ldrb	r3, [r3, #0]
  21ae3e:	2b00      	cmp	r3, #0
  21ae40:	f000 80a4 	beq.w	21af8c <app_bt_management_callback+0x258>
  21ae44:	2100      	movs	r1, #0
  21ae46:	4620      	mov	r0, r4
  21ae48:	f636 ff04 	bl	51c54 <wiced_bt_ble_scan+0x3>
  21ae4c:	e7d6      	b.n	21adfc <app_bt_management_callback+0xc8>
  21ae4e:	2100      	movs	r1, #0
  21ae50:	4a62      	ldr	r2, [pc, #392]	; (21afdc <app_bt_management_callback+0x2a8>)
  21ae52:	4608      	mov	r0, r1
  21ae54:	f5f2 fc02 	bl	d65c <wiced_va_printf+0x1f7>
  21ae58:	2100      	movs	r1, #0
  21ae5a:	6823      	ldr	r3, [r4, #0]
  21ae5c:	4608      	mov	r0, r1
  21ae5e:	4a60      	ldr	r2, [pc, #384]	; (21afe0 <app_bt_management_callback+0x2ac>)
  21ae60:	f5f2 fbfc 	bl	d65c <wiced_va_printf+0x1f7>
  21ae64:	2204      	movs	r2, #4
  21ae66:	2100      	movs	r1, #0
  21ae68:	4b5e      	ldr	r3, [pc, #376]	; (21afe4 <app_bt_management_callback+0x2b0>)
  21ae6a:	6820      	ldr	r0, [r4, #0]
  21ae6c:	f636 fefc 	bl	51c68 <wiced_bt_dev_set_advanced_connection_params+0x3>
  21ae70:	e7c4      	b.n	21adfc <app_bt_management_callback+0xc8>
  21ae72:	2100      	movs	r1, #0
  21ae74:	4a5c      	ldr	r2, [pc, #368]	; (21afe8 <app_bt_management_callback+0x2b4>)
  21ae76:	4608      	mov	r0, r1
  21ae78:	f5f2 fbf0 	bl	d65c <wiced_va_printf+0x1f7>
  21ae7c:	4621      	mov	r1, r4
  21ae7e:	2000      	movs	r0, #0
  21ae80:	f6bc fce9 	bl	d7856 <wiced_bt_dev_set_encryption+0x3>
  21ae84:	e7ba      	b.n	21adfc <app_bt_management_callback+0xc8>
  21ae86:	2100      	movs	r1, #0
  21ae88:	4a58      	ldr	r2, [pc, #352]	; (21afec <app_bt_management_callback+0x2b8>)
  21ae8a:	4608      	mov	r0, r1
  21ae8c:	f5f2 fbe6 	bl	d65c <wiced_va_printf+0x1f7>
  21ae90:	68a3      	ldr	r3, [r4, #8]
  21ae92:	4a57      	ldr	r2, [pc, #348]	; (21aff0 <app_bt_management_callback+0x2bc>)
  21ae94:	9300      	str	r3, [sp, #0]
  21ae96:	4623      	mov	r3, r4
  21ae98:	2100      	movs	r1, #0
  21ae9a:	4608      	mov	r0, r1
  21ae9c:	f5f2 fbde 	bl	d65c <wiced_va_printf+0x1f7>
  21aea0:	e7ac      	b.n	21adfc <app_bt_management_callback+0xc8>
  21aea2:	460b      	mov	r3, r1
  21aea4:	2100      	movs	r1, #0
  21aea6:	4a53      	ldr	r2, [pc, #332]	; (21aff4 <app_bt_management_callback+0x2c0>)
  21aea8:	4608      	mov	r0, r1
  21aeaa:	f5f2 fbd7 	bl	d65c <wiced_va_printf+0x1f7>
  21aeae:	2303      	movs	r3, #3
  21aeb0:	80e3      	strh	r3, [r4, #6]
  21aeb2:	4b51      	ldr	r3, [pc, #324]	; (21aff8 <app_bt_management_callback+0x2c4>)
  21aeb4:	60a3      	str	r3, [r4, #8]
  21aeb6:	e7a1      	b.n	21adfc <app_bt_management_callback+0xc8>
  21aeb8:	2100      	movs	r1, #0
  21aeba:	4a50      	ldr	r2, [pc, #320]	; (21affc <app_bt_management_callback+0x2c8>)
  21aebc:	4608      	mov	r0, r1
  21aebe:	f5f2 fbcd 	bl	d65c <wiced_va_printf+0x1f7>
  21aec2:	2100      	movs	r1, #0
  21aec4:	4623      	mov	r3, r4
  21aec6:	4608      	mov	r0, r1
  21aec8:	4a4d      	ldr	r2, [pc, #308]	; (21b000 <app_bt_management_callback+0x2cc>)
  21aeca:	f5f2 fbc7 	bl	d65c <wiced_va_printf+0x1f7>
  21aece:	2303      	movs	r3, #3
  21aed0:	71a3      	strb	r3, [r4, #6]
  21aed2:	2304      	movs	r3, #4
  21aed4:	7223      	strb	r3, [r4, #8]
  21aed6:	e791      	b.n	21adfc <app_bt_management_callback+0xc8>
  21aed8:	2100      	movs	r1, #0
  21aeda:	4a4a      	ldr	r2, [pc, #296]	; (21b004 <app_bt_management_callback+0x2d0>)
  21aedc:	4608      	mov	r0, r1
  21aede:	f5f2 fbbd 	bl	d65c <wiced_va_printf+0x1f7>
  21aee2:	2100      	movs	r1, #0
  21aee4:	7923      	ldrb	r3, [r4, #4]
  21aee6:	4608      	mov	r0, r1
  21aee8:	2b01      	cmp	r3, #1
  21aeea:	bf0c      	ite	eq
  21aeec:	79a3      	ldrbeq	r3, [r4, #6]
  21aeee:	88e3      	ldrhne	r3, [r4, #6]
  21aef0:	4a45      	ldr	r2, [pc, #276]	; (21b008 <app_bt_management_callback+0x2d4>)
  21aef2:	f5f2 fbb3 	bl	d65c <wiced_va_printf+0x1f7>
  21aef6:	e781      	b.n	21adfc <app_bt_management_callback+0xc8>
  21aef8:	2100      	movs	r1, #0
  21aefa:	4a44      	ldr	r2, [pc, #272]	; (21b00c <app_bt_management_callback+0x2d8>)
  21aefc:	4608      	mov	r0, r1
  21aefe:	f5f2 fbad 	bl	d65c <wiced_va_printf+0x1f7>
  21af02:	89a3      	ldrh	r3, [r4, #12]
  21af04:	4a42      	ldr	r2, [pc, #264]	; (21b010 <app_bt_management_callback+0x2dc>)
  21af06:	9300      	str	r3, [sp, #0]
  21af08:	6823      	ldr	r3, [r4, #0]
  21af0a:	e7c5      	b.n	21ae98 <app_bt_management_callback+0x164>
  21af0c:	2100      	movs	r1, #0
  21af0e:	4a41      	ldr	r2, [pc, #260]	; (21b014 <app_bt_management_callback+0x2e0>)
  21af10:	4608      	mov	r0, r1
  21af12:	f5f2 fba3 	bl	d65c <wiced_va_printf+0x1f7>
  21af16:	4622      	mov	r2, r4
  21af18:	218a      	movs	r1, #138	; 0x8a
  21af1a:	2001      	movs	r0, #1
  21af1c:	f7ff fed0 	bl	21acc0 <handsfree_write_nvram>
  21af20:	7da3      	ldrb	r3, [r4, #22]
  21af22:	2100      	movs	r1, #0
  21af24:	930e      	str	r3, [sp, #56]	; 0x38
  21af26:	7d63      	ldrb	r3, [r4, #21]
  21af28:	4608      	mov	r0, r1
  21af2a:	930d      	str	r3, [sp, #52]	; 0x34
  21af2c:	7d23      	ldrb	r3, [r4, #20]
  21af2e:	4a3a      	ldr	r2, [pc, #232]	; (21b018 <app_bt_management_callback+0x2e4>)
  21af30:	930c      	str	r3, [sp, #48]	; 0x30
  21af32:	7ce3      	ldrb	r3, [r4, #19]
  21af34:	930b      	str	r3, [sp, #44]	; 0x2c
  21af36:	7ca3      	ldrb	r3, [r4, #18]
  21af38:	930a      	str	r3, [sp, #40]	; 0x28
  21af3a:	7c63      	ldrb	r3, [r4, #17]
  21af3c:	9309      	str	r3, [sp, #36]	; 0x24
  21af3e:	7c23      	ldrb	r3, [r4, #16]
  21af40:	9308      	str	r3, [sp, #32]
  21af42:	7be3      	ldrb	r3, [r4, #15]
  21af44:	9307      	str	r3, [sp, #28]
  21af46:	7ba3      	ldrb	r3, [r4, #14]
  21af48:	9306      	str	r3, [sp, #24]
  21af4a:	7b63      	ldrb	r3, [r4, #13]
  21af4c:	9305      	str	r3, [sp, #20]
  21af4e:	7b23      	ldrb	r3, [r4, #12]
  21af50:	9304      	str	r3, [sp, #16]
  21af52:	7ae3      	ldrb	r3, [r4, #11]
  21af54:	9303      	str	r3, [sp, #12]
  21af56:	7aa3      	ldrb	r3, [r4, #10]
  21af58:	9302      	str	r3, [sp, #8]
  21af5a:	7a63      	ldrb	r3, [r4, #9]
  21af5c:	9301      	str	r3, [sp, #4]
  21af5e:	7a23      	ldrb	r3, [r4, #8]
  21af60:	9300      	str	r3, [sp, #0]
  21af62:	79e3      	ldrb	r3, [r4, #7]
  21af64:	f5f2 fb7a 	bl	d65c <wiced_va_printf+0x1f7>
  21af68:	e748      	b.n	21adfc <app_bt_management_callback+0xc8>
  21af6a:	2100      	movs	r1, #0
  21af6c:	4a2b      	ldr	r2, [pc, #172]	; (21b01c <app_bt_management_callback+0x2e8>)
  21af6e:	4608      	mov	r0, r1
  21af70:	f5f2 fb74 	bl	d65c <wiced_va_printf+0x1f7>
  21af74:	4621      	mov	r1, r4
  21af76:	228a      	movs	r2, #138	; 0x8a
  21af78:	2001      	movs	r0, #1
  21af7a:	f7ff febb 	bl	21acf4 <handsfree_read_nvram>
  21af7e:	4601      	mov	r1, r0
  21af80:	2800      	cmp	r0, #0
  21af82:	f47f af3b 	bne.w	21adfc <app_bt_management_callback+0xc8>
  21af86:	4a26      	ldr	r2, [pc, #152]	; (21b020 <app_bt_management_callback+0x2ec>)
  21af88:	f5f2 fb68 	bl	d65c <wiced_va_printf+0x1f7>
  21af8c:	f641 70ad 	movw	r0, #8109	; 0x1fad
  21af90:	e6f9      	b.n	21ad86 <app_bt_management_callback+0x52>
  21af92:	2100      	movs	r1, #0
  21af94:	4a23      	ldr	r2, [pc, #140]	; (21b024 <app_bt_management_callback+0x2f0>)
  21af96:	4608      	mov	r0, r1
  21af98:	f5f2 fb60 	bl	d65c <wiced_va_printf+0x1f7>
  21af9c:	2100      	movs	r1, #0
  21af9e:	7a23      	ldrb	r3, [r4, #8]
  21afa0:	4608      	mov	r0, r1
  21afa2:	9301      	str	r3, [sp, #4]
  21afa4:	7923      	ldrb	r3, [r4, #4]
  21afa6:	4a20      	ldr	r2, [pc, #128]	; (21b028 <app_bt_management_callback+0x2f4>)
  21afa8:	9300      	str	r3, [sp, #0]
  21afaa:	6823      	ldr	r3, [r4, #0]
  21afac:	f5f2 fb56 	bl	d65c <wiced_va_printf+0x1f7>
  21afb0:	e724      	b.n	21adfc <app_bt_management_callback+0xc8>
  21afb2:	bf00      	nop
  21afb4:	0021efa4 	.word	0x0021efa4
  21afb8:	0021fd4c 	.word	0x0021fd4c
  21afbc:	0021a43d 	.word	0x0021a43d
  21afc0:	0021db80 	.word	0x0021db80
  21afc4:	0021fd88 	.word	0x0021fd88
  21afc8:	0021efd2 	.word	0x0021efd2
  21afcc:	0021b02d 	.word	0x0021b02d
  21afd0:	0021efec 	.word	0x0021efec
  21afd4:	0021f020 	.word	0x0021f020
  21afd8:	0021f057 	.word	0x0021f057
  21afdc:	0021f08e 	.word	0x0021f08e
  21afe0:	0021f0c0 	.word	0x0021f0c0
  21afe4:	0021f9a2 	.word	0x0021f9a2
  21afe8:	0021f0d5 	.word	0x0021f0d5
  21afec:	0021f115 	.word	0x0021f115
  21aff0:	0021f150 	.word	0x0021f150
  21aff4:	0021f178 	.word	0x0021f178
  21aff8:	1717100d 	.word	0x1717100d
  21affc:	0021f1c9 	.word	0x0021f1c9
  21b000:	0021f216 	.word	0x0021f216
  21b004:	0021f23d 	.word	0x0021f23d
  21b008:	0021f274 	.word	0x0021f274
  21b00c:	0021f28b 	.word	0x0021f28b
  21b010:	0021f2c3 	.word	0x0021f2c3
  21b014:	0021f2ef 	.word	0x0021f2ef
  21b018:	0021f334 	.word	0x0021f334
  21b01c:	0021f38e 	.word	0x0021f38e
  21b020:	0021f3d4 	.word	0x0021f3d4
  21b024:	0021f3ec 	.word	0x0021f3ec
  21b028:	0021f42a 	.word	0x0021f42a

0021b02c <hci_control_hci_trace_cback>:
  21b02c:	4613      	mov	r3, r2
  21b02e:	460a      	mov	r2, r1
  21b030:	4601      	mov	r1, r0
  21b032:	2000      	movs	r0, #0
  21b034:	f6c1 bd21 	b.w	dca7a <wiced_transport_send_data+0x41>

0021b038 <install_libs>:
  21b038:	b508      	push	{r3, lr}
  21b03a:	f005 f91b 	bl	220274 <wiced_audio_sink_init>
  21b03e:	f005 f95b 	bl	2202f8 <install_patch_lite_host_proc_route_config_req>
  21b042:	f005 f961 	bl	220308 <install_patch_addin_get_sbc_frame_len_in_jitter_buf>
  21b046:	f005 f969 	bl	22031c <install_patch_addin_Sample_volume_ctrl>
  21b04a:	f005 f971 	bl	220330 <install_patch_addin_Timer2DoneInt>
  21b04e:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
  21b052:	f005 b977 	b.w	220344 <install_patch_pcm_scoLinkDown>

0021b056 <wiced_bt_a2dp_sbc_cfg_in_cap>:
  21b056:	b513      	push	{r0, r1, r4, lr}
  21b058:	2200      	movs	r2, #0
  21b05a:	460c      	mov	r4, r1
  21b05c:	4601      	mov	r1, r0
  21b05e:	4668      	mov	r0, sp
  21b060:	f6c3 fa52 	bl	de508 <wiced_bt_a2d_bld_sbc_info+0x3>
  21b064:	bb18      	cbnz	r0, 21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b066:	f89d 2000 	ldrb.w	r2, [sp]
  21b06a:	7823      	ldrb	r3, [r4, #0]
  21b06c:	421a      	tst	r2, r3
  21b06e:	d020      	beq.n	21b0b2 <wiced_bt_a2dp_sbc_cfg_in_cap+0x5c>
  21b070:	f89d 2001 	ldrb.w	r2, [sp, #1]
  21b074:	7863      	ldrb	r3, [r4, #1]
  21b076:	421a      	tst	r2, r3
  21b078:	d01d      	beq.n	21b0b6 <wiced_bt_a2dp_sbc_cfg_in_cap+0x60>
  21b07a:	f89d 2002 	ldrb.w	r2, [sp, #2]
  21b07e:	78a3      	ldrb	r3, [r4, #2]
  21b080:	421a      	tst	r2, r3
  21b082:	d01a      	beq.n	21b0ba <wiced_bt_a2dp_sbc_cfg_in_cap+0x64>
  21b084:	f89d 2003 	ldrb.w	r2, [sp, #3]
  21b088:	78e3      	ldrb	r3, [r4, #3]
  21b08a:	421a      	tst	r2, r3
  21b08c:	d017      	beq.n	21b0be <wiced_bt_a2dp_sbc_cfg_in_cap+0x68>
  21b08e:	f89d 2004 	ldrb.w	r2, [sp, #4]
  21b092:	7923      	ldrb	r3, [r4, #4]
  21b094:	421a      	tst	r2, r3
  21b096:	d014      	beq.n	21b0c2 <wiced_bt_a2dp_sbc_cfg_in_cap+0x6c>
  21b098:	f89d 2005 	ldrb.w	r2, [sp, #5]
  21b09c:	7963      	ldrb	r3, [r4, #5]
  21b09e:	429a      	cmp	r2, r3
  21b0a0:	d811      	bhi.n	21b0c6 <wiced_bt_a2dp_sbc_cfg_in_cap+0x70>
  21b0a2:	f89d 2006 	ldrb.w	r2, [sp, #6]
  21b0a6:	79a3      	ldrb	r3, [r4, #6]
  21b0a8:	429a      	cmp	r2, r3
  21b0aa:	bf38      	it	cc
  21b0ac:	20cc      	movcc	r0, #204	; 0xcc
  21b0ae:	b002      	add	sp, #8
  21b0b0:	bd10      	pop	{r4, pc}
  21b0b2:	20c4      	movs	r0, #196	; 0xc4
  21b0b4:	e7fb      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b0b6:	20c6      	movs	r0, #198	; 0xc6
  21b0b8:	e7f9      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b0ba:	20dd      	movs	r0, #221	; 0xdd
  21b0bc:	e7f7      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b0be:	20c8      	movs	r0, #200	; 0xc8
  21b0c0:	e7f5      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b0c2:	20ca      	movs	r0, #202	; 0xca
  21b0c4:	e7f3      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
  21b0c6:	20ce      	movs	r0, #206	; 0xce
  21b0c8:	e7f1      	b.n	21b0ae <wiced_bt_a2dp_sbc_cfg_in_cap+0x58>
	...

0021b0cc <wiced_bt_a2dp_sink_sdp_complete_cback>:
  21b0cc:	b5f0      	push	{r4, r5, r6, r7, lr}
  21b0ce:	4604      	mov	r4, r0
  21b0d0:	b089      	sub	sp, #36	; 0x24
  21b0d2:	220a      	movs	r2, #10
  21b0d4:	2100      	movs	r1, #0
  21b0d6:	a805      	add	r0, sp, #20
  21b0d8:	f616 fef4 	bl	31ec4 <memcpy+0x7>
  21b0dc:	b13c      	cbz	r4, 21b0ee <wiced_bt_a2dp_sink_sdp_complete_cback+0x22>
  21b0de:	230b      	movs	r3, #11
  21b0e0:	f8ad 3014 	strh.w	r3, [sp, #20]
  21b0e4:	a805      	add	r0, sp, #20
  21b0e6:	f000 fe21 	bl	21bd2c <wiced_bt_a2dp_sink_hdl_event>
  21b0ea:	b009      	add	sp, #36	; 0x24
  21b0ec:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21b0ee:	4d15      	ldr	r5, [pc, #84]	; (21b144 <wiced_bt_a2dp_sink_sdp_complete_cback+0x78>)
  21b0f0:	462f      	mov	r7, r5
  21b0f2:	6de8      	ldr	r0, [r5, #92]	; 0x5c
  21b0f4:	b300      	cbz	r0, 21b138 <wiced_bt_a2dp_sink_sdp_complete_cback+0x6c>
  21b0f6:	2200      	movs	r2, #0
  21b0f8:	f241 110a 	movw	r1, #4362	; 0x110a
  21b0fc:	f6bd f9fe 	bl	d84fc <wiced_bt_sdp_find_attribute_in_rec+0x3>
  21b100:	4606      	mov	r6, r0
  21b102:	b1c8      	cbz	r0, 21b138 <wiced_bt_a2dp_sink_sdp_complete_cback+0x6c>
  21b104:	209c      	movs	r0, #156	; 0x9c
  21b106:	2100      	movs	r1, #0
  21b108:	fb00 7404 	mla	r4, r0, r4, r7
  21b10c:	34d0      	adds	r4, #208	; 0xd0
  21b10e:	4608      	mov	r0, r1
  21b110:	4b0d      	ldr	r3, [pc, #52]	; (21b148 <wiced_bt_a2dp_sink_sdp_complete_cback+0x7c>)
  21b112:	4a0e      	ldr	r2, [pc, #56]	; (21b14c <wiced_bt_a2dp_sink_sdp_complete_cback+0x80>)
  21b114:	9400      	str	r4, [sp, #0]
  21b116:	f5f2 faa1 	bl	d65c <wiced_va_printf+0x1f7>
  21b11a:	2119      	movs	r1, #25
  21b11c:	4630      	mov	r0, r6
  21b11e:	aa03      	add	r2, sp, #12
  21b120:	f6bd f9f0 	bl	d8504 <wiced_bt_sdp_find_service_uuid_in_db+0x3>
  21b124:	2801      	cmp	r0, #1
  21b126:	d1da      	bne.n	21b0de <wiced_bt_a2dp_sink_sdp_complete_cback+0x12>
  21b128:	220a      	movs	r2, #10
  21b12a:	f8bd 3010 	ldrh.w	r3, [sp, #16]
  21b12e:	f8ad 2014 	strh.w	r2, [sp, #20]
  21b132:	f8ad 301c 	strh.w	r3, [sp, #28]
  21b136:	e7d5      	b.n	21b0e4 <wiced_bt_a2dp_sink_sdp_complete_cback+0x18>
  21b138:	3401      	adds	r4, #1
  21b13a:	2c04      	cmp	r4, #4
  21b13c:	f105 059c 	add.w	r5, r5, #156	; 0x9c
  21b140:	d1d7      	bne.n	21b0f2 <wiced_bt_a2dp_sink_sdp_complete_cback+0x26>
  21b142:	e7cc      	b.n	21b0de <wiced_bt_a2dp_sink_sdp_complete_cback+0x12>
  21b144:	0021fd8c 	.word	0x0021fd8c
  21b148:	0021dd86 	.word	0x0021dd86
  21b14c:	0021f467 	.word	0x0021f467

0021b150 <wiced_bt_a2dp_sink_ctrl_cback>:
  21b150:	e92d 47ff 	stmdb	sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, sl, lr}
  21b154:	4682      	mov	sl, r0
  21b156:	4689      	mov	r9, r1
  21b158:	4617      	mov	r7, r2
  21b15a:	461d      	mov	r5, r3
  21b15c:	b383      	cbz	r3, 21b1c0 <wiced_bt_a2dp_sink_ctrl_cback+0x70>
  21b15e:	2a0e      	cmp	r2, #14
  21b160:	d129      	bne.n	21b1b6 <wiced_bt_a2dp_sink_ctrl_cback+0x66>
  21b162:	89ae      	ldrh	r6, [r5, #12]
  21b164:	f5b6 7fc8 	cmp.w	r6, #400	; 0x190
  21b168:	bf28      	it	cs
  21b16a:	f44f 76c8 	movcs.w	r6, #400	; 0x190
  21b16e:	f106 008c 	add.w	r0, r6, #140	; 0x8c
  21b172:	b280      	uxth	r0, r0
  21b174:	f6b6 f82c 	bl	d11d0 <wiced_bt_get_buffer>
  21b178:	4604      	mov	r4, r0
  21b17a:	2800      	cmp	r0, #0
  21b17c:	d04d      	beq.n	21b21a <wiced_bt_a2dp_sink_ctrl_cback+0xca>
  21b17e:	f1b9 0f00 	cmp.w	r9, #0
  21b182:	d003      	beq.n	21b18c <wiced_bt_a2dp_sink_ctrl_cback+0x3c>
  21b184:	4649      	mov	r1, r9
  21b186:	3084      	adds	r0, #132	; 0x84
  21b188:	f002 fc4d 	bl	21da26 <utl_bdcpy>
  21b18c:	f8df 80a8 	ldr.w	r8, [pc, #168]	; 21b238 <wiced_bt_a2dp_sink_ctrl_cback+0xe8>
  21b190:	2d00      	cmp	r5, #0
  21b192:	d034      	beq.n	21b1fe <wiced_bt_a2dp_sink_ctrl_cback+0xae>
  21b194:	2210      	movs	r2, #16
  21b196:	4629      	mov	r1, r5
  21b198:	f104 0074 	add.w	r0, r4, #116	; 0x74
  21b19c:	f616 fe8e 	bl	31ebc <bsc_OpExtended+0x2d91>
  21b1a0:	1f3b      	subs	r3, r7, #4
  21b1a2:	2b0a      	cmp	r3, #10
  21b1a4:	d821      	bhi.n	21b1ea <wiced_bt_a2dp_sink_ctrl_cback+0x9a>
  21b1a6:	e8df f003 	tbb	[pc, r3]
  21b1aa:	200d      	.short	0x200d
  21b1ac:	20262020 	.word	0x20262020
  21b1b0:	1a0d2020 	.word	0x1a0d2020
  21b1b4:	14          	.byte	0x14
  21b1b5:	00          	.byte	0x00
  21b1b6:	2a0d      	cmp	r2, #13
  21b1b8:	d102      	bne.n	21b1c0 <wiced_bt_a2dp_sink_ctrl_cback+0x70>
  21b1ba:	781b      	ldrb	r3, [r3, #0]
  21b1bc:	2b00      	cmp	r3, #0
  21b1be:	d0d0      	beq.n	21b162 <wiced_bt_a2dp_sink_ctrl_cback+0x12>
  21b1c0:	2600      	movs	r6, #0
  21b1c2:	e7d4      	b.n	21b16e <wiced_bt_a2dp_sink_ctrl_cback+0x1e>
  21b1c4:	226c      	movs	r2, #108	; 0x6c
  21b1c6:	68a9      	ldr	r1, [r5, #8]
  21b1c8:	f104 0008 	add.w	r0, r4, #8
  21b1cc:	f616 fe76 	bl	31ebc <bsc_OpExtended+0x2d91>
  21b1d0:	e00b      	b.n	21b1ea <wiced_bt_a2dp_sink_ctrl_cback+0x9a>
  21b1d2:	f104 008c 	add.w	r0, r4, #140	; 0x8c
  21b1d6:	67e0      	str	r0, [r4, #124]	; 0x7c
  21b1d8:	4632      	mov	r2, r6
  21b1da:	68a9      	ldr	r1, [r5, #8]
  21b1dc:	e7f6      	b.n	21b1cc <wiced_bt_a2dp_sink_ctrl_cback+0x7c>
  21b1de:	782b      	ldrb	r3, [r5, #0]
  21b1e0:	f104 008c 	add.w	r0, r4, #140	; 0x8c
  21b1e4:	67e0      	str	r0, [r4, #124]	; 0x7c
  21b1e6:	2b00      	cmp	r3, #0
  21b1e8:	d0f6      	beq.n	21b1d8 <wiced_bt_a2dp_sink_ctrl_cback+0x88>
  21b1ea:	782b      	ldrb	r3, [r5, #0]
  21b1ec:	b14b      	cbz	r3, 21b202 <wiced_bt_a2dp_sink_ctrl_cback+0xb2>
  21b1ee:	44b8      	add	r8, r7
  21b1f0:	f898 303c 	ldrb.w	r3, [r8, #60]	; 0x3c
  21b1f4:	e008      	b.n	21b208 <wiced_bt_a2dp_sink_ctrl_cback+0xb8>
  21b1f6:	2300      	movs	r3, #0
  21b1f8:	f884 3074 	strb.w	r3, [r4, #116]	; 0x74
  21b1fc:	e7f5      	b.n	21b1ea <wiced_bt_a2dp_sink_ctrl_cback+0x9a>
  21b1fe:	f884 5074 	strb.w	r5, [r4, #116]	; 0x74
  21b202:	44b8      	add	r8, r7
  21b204:	f898 3026 	ldrb.w	r3, [r8, #38]	; 0x26
  21b208:	4620      	mov	r0, r4
  21b20a:	8023      	strh	r3, [r4, #0]
  21b20c:	f884 a08a 	strb.w	sl, [r4, #138]	; 0x8a
  21b210:	f000 fd8c 	bl	21bd2c <wiced_bt_a2dp_sink_hdl_event>
  21b214:	4620      	mov	r0, r4
  21b216:	f6b5 ffdd 	bl	d11d4 <wiced_bt_free_buffer>
  21b21a:	2f11      	cmp	r7, #17
  21b21c:	d108      	bne.n	21b230 <wiced_bt_a2dp_sink_ctrl_cback+0xe0>
  21b21e:	2300      	movs	r3, #0
  21b220:	463a      	mov	r2, r7
  21b222:	f88d 3001 	strb.w	r3, [sp, #1]
  21b226:	4649      	mov	r1, r9
  21b228:	466b      	mov	r3, sp
  21b22a:	4650      	mov	r0, sl
  21b22c:	f000 fdd4 	bl	21bdd8 <wiced_bt_a2dp_sink_conn_cback>
  21b230:	b004      	add	sp, #16
  21b232:	e8bd 87f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, pc}
  21b236:	bf00      	nop
  21b238:	0021dd86 	.word	0x0021dd86

0021b23c <wiced_bt_a2dp_sink_do_sdp>:
  21b23c:	4601      	mov	r1, r0
  21b23e:	b510      	push	{r4, lr}
  21b240:	4b1b      	ldr	r3, [pc, #108]	; (21b2b0 <wiced_bt_a2dp_sink_do_sdp+0x74>)
  21b242:	6944      	ldr	r4, [r0, #20]
  21b244:	6818      	ldr	r0, [r3, #0]
  21b246:	b08a      	sub	sp, #40	; 0x28
  21b248:	889b      	ldrh	r3, [r3, #4]
  21b24a:	9003      	str	r0, [sp, #12]
  21b24c:	3101      	adds	r1, #1
  21b24e:	4819      	ldr	r0, [pc, #100]	; (21b2b4 <wiced_bt_a2dp_sink_do_sdp+0x78>)
  21b250:	f8ad 3010 	strh.w	r3, [sp, #16]
  21b254:	f002 fbe7 	bl	21da26 <utl_bdcpy>
  21b258:	2302      	movs	r3, #2
  21b25a:	f8ad 3014 	strh.w	r3, [sp, #20]
  21b25e:	f241 130a 	movw	r3, #4362	; 0x110a
  21b262:	f8ad 3018 	strh.w	r3, [sp, #24]
  21b266:	6823      	ldr	r3, [r4, #0]
  21b268:	b923      	cbnz	r3, 21b274 <wiced_bt_a2dp_sink_do_sdp+0x38>
  21b26a:	f44f 707a 	mov.w	r0, #1000	; 0x3e8
  21b26e:	f6b5 ffaf 	bl	d11d0 <wiced_bt_get_buffer>
  21b272:	6020      	str	r0, [r4, #0]
  21b274:	6820      	ldr	r0, [r4, #0]
  21b276:	b1c0      	cbz	r0, 21b2aa <wiced_bt_a2dp_sink_do_sdp+0x6e>
  21b278:	ab03      	add	r3, sp, #12
  21b27a:	9301      	str	r3, [sp, #4]
  21b27c:	2303      	movs	r3, #3
  21b27e:	2201      	movs	r2, #1
  21b280:	9300      	str	r3, [sp, #0]
  21b282:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  21b286:	ab05      	add	r3, sp, #20
  21b288:	f6bd f922 	bl	d84d0 <wiced_bt_avrc_get_data_mtu+0x5f>
  21b28c:	b920      	cbnz	r0, 21b298 <wiced_bt_a2dp_sink_do_sdp+0x5c>
  21b28e:	f64f 70f3 	movw	r0, #65523	; 0xfff3
  21b292:	f7ff ff1b 	bl	21b0cc <wiced_bt_a2dp_sink_sdp_complete_cback>
  21b296:	e006      	b.n	21b2a6 <wiced_bt_a2dp_sink_do_sdp+0x6a>
  21b298:	4a07      	ldr	r2, [pc, #28]	; (21b2b8 <wiced_bt_a2dp_sink_do_sdp+0x7c>)
  21b29a:	6821      	ldr	r1, [r4, #0]
  21b29c:	4805      	ldr	r0, [pc, #20]	; (21b2b4 <wiced_bt_a2dp_sink_do_sdp+0x78>)
  21b29e:	f6bd f927 	bl	d84f0 <wiced_bt_sdp_service_search_request+0x3>
  21b2a2:	2800      	cmp	r0, #0
  21b2a4:	d0f3      	beq.n	21b28e <wiced_bt_a2dp_sink_do_sdp+0x52>
  21b2a6:	b00a      	add	sp, #40	; 0x28
  21b2a8:	bd10      	pop	{r4, pc}
  21b2aa:	2006      	movs	r0, #6
  21b2ac:	e7f1      	b.n	21b292 <wiced_bt_a2dp_sink_do_sdp+0x56>
  21b2ae:	bf00      	nop
  21b2b0:	0021ddd8 	.word	0x0021ddd8
  21b2b4:	0021fdb1 	.word	0x0021fdb1
  21b2b8:	0021b0cd 	.word	0x0021b0cd

0021b2bc <wiced_bt_a2dp_sink_delay_send>:
  21b2bc:	6943      	ldr	r3, [r0, #20]
  21b2be:	f8b3 2088 	ldrh.w	r2, [r3, #136]	; 0x88
  21b2c2:	05d2      	lsls	r2, r2, #23
  21b2c4:	d50e      	bpl.n	21b2e4 <wiced_bt_a2dp_sink_delay_send+0x28>
  21b2c6:	4a08      	ldr	r2, [pc, #32]	; (21b2e8 <wiced_bt_a2dp_sink_delay_send+0x2c>)
  21b2c8:	6991      	ldr	r1, [r2, #24]
  21b2ca:	8988      	ldrh	r0, [r1, #12]
  21b2cc:	7bca      	ldrb	r2, [r1, #15]
  21b2ce:	210a      	movs	r1, #10
  21b2d0:	4342      	muls	r2, r0
  21b2d2:	fbb2 f2f1 	udiv	r2, r2, r1
  21b2d6:	f893 008a 	ldrb.w	r0, [r3, #138]	; 0x8a
  21b2da:	f893 108b 	ldrb.w	r1, [r3, #139]	; 0x8b
  21b2de:	b292      	uxth	r2, r2
  21b2e0:	f6bd b9a3 	b.w	d862a <wiced_bt_avdt_get_all_cap_req+0x3>
  21b2e4:	4770      	bx	lr
  21b2e6:	bf00      	nop
  21b2e8:	0021fd8c 	.word	0x0021fd8c

0021b2ec <wiced_bt_a2dp_sink_clean_sep_record>:
  21b2ec:	2300      	movs	r3, #0
  21b2ee:	4619      	mov	r1, r3
  21b2f0:	b510      	push	{r4, lr}
  21b2f2:	4a05      	ldr	r2, [pc, #20]	; (21b308 <wiced_bt_a2dp_sink_clean_sep_record+0x1c>)
  21b2f4:	7854      	ldrb	r4, [r2, #1]
  21b2f6:	3301      	adds	r3, #1
  21b2f8:	4284      	cmp	r4, r0
  21b2fa:	bf08      	it	eq
  21b2fc:	7011      	strbeq	r1, [r2, #0]
  21b2fe:	2b04      	cmp	r3, #4
  21b300:	f102 0206 	add.w	r2, r2, #6
  21b304:	d1f6      	bne.n	21b2f4 <wiced_bt_a2dp_sink_clean_sep_record+0x8>
  21b306:	bd10      	pop	{r4, pc}
  21b308:	0021fd8c 	.word	0x0021fd8c

0021b30c <wiced_bt_a2dp_sink_cleanup>:
  21b30c:	b538      	push	{r3, r4, r5, lr}
  21b30e:	79c5      	ldrb	r5, [r0, #7]
  21b310:	2d00      	cmp	r5, #0
  21b312:	d141      	bne.n	21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b314:	6944      	ldr	r4, [r0, #20]
  21b316:	b344      	cbz	r4, 21b36a <wiced_bt_a2dp_sink_cleanup+0x5e>
  21b318:	6860      	ldr	r0, [r4, #4]
  21b31a:	b110      	cbz	r0, 21b322 <wiced_bt_a2dp_sink_cleanup+0x16>
  21b31c:	f6b5 ff5a 	bl	d11d4 <wiced_bt_free_buffer>
  21b320:	6065      	str	r5, [r4, #4]
  21b322:	6820      	ldr	r0, [r4, #0]
  21b324:	b118      	cbz	r0, 21b32e <wiced_bt_a2dp_sink_cleanup+0x22>
  21b326:	f6b5 ff55 	bl	d11d4 <wiced_bt_free_buffer>
  21b32a:	2300      	movs	r3, #0
  21b32c:	6023      	str	r3, [r4, #0]
  21b32e:	2300      	movs	r3, #0
  21b330:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b334:	f8a4 307e 	strh.w	r3, [r4, #126]	; 0x7e
  21b338:	f884 3095 	strb.w	r3, [r4, #149]	; 0x95
  21b33c:	f8a4 3086 	strh.w	r3, [r4, #134]	; 0x86
  21b340:	f884 3093 	strb.w	r3, [r4, #147]	; 0x93
  21b344:	f884 3091 	strb.w	r3, [r4, #145]	; 0x91
  21b348:	f8a4 3088 	strh.w	r3, [r4, #136]	; 0x88
  21b34c:	f884 3099 	strb.w	r3, [r4, #153]	; 0x99
  21b350:	f7ff ffcc 	bl	21b2ec <wiced_bt_a2dp_sink_clean_sep_record>
  21b354:	f894 3098 	ldrb.w	r3, [r4, #152]	; 0x98
  21b358:	b13b      	cbz	r3, 21b36a <wiced_bt_a2dp_sink_cleanup+0x5e>
  21b35a:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b35e:	b108      	cbz	r0, 21b364 <wiced_bt_a2dp_sink_cleanup+0x58>
  21b360:	f6bd f945 	bl	d85ee <wiced_bt_avdt_create_stream+0x37>
  21b364:	2300      	movs	r3, #0
  21b366:	f884 308a 	strb.w	r3, [r4, #138]	; 0x8a
  21b36a:	4b0c      	ldr	r3, [pc, #48]	; (21b39c <wiced_bt_a2dp_sink_cleanup+0x90>)
  21b36c:	f893 2033 	ldrb.w	r2, [r3, #51]	; 0x33
  21b370:	b992      	cbnz	r2, 21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b372:	6c1a      	ldr	r2, [r3, #64]	; 0x40
  21b374:	f892 108a 	ldrb.w	r1, [r2, #138]	; 0x8a
  21b378:	f893 204b 	ldrb.w	r2, [r3, #75]	; 0x4b
  21b37c:	430a      	orrs	r2, r1
  21b37e:	d10b      	bne.n	21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b380:	6d9b      	ldr	r3, [r3, #88]	; 0x58
  21b382:	f893 308a 	ldrb.w	r3, [r3, #138]	; 0x8a
  21b386:	b93b      	cbnz	r3, 21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b388:	b134      	cbz	r4, 21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b38a:	f894 3098 	ldrb.w	r3, [r4, #152]	; 0x98
  21b38e:	b11b      	cbz	r3, 21b398 <wiced_bt_a2dp_sink_cleanup+0x8c>
  21b390:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21b394:	f000 bcc0 	b.w	21bd18 <wiced_bt_a2dp_sink_dereg_comp>
  21b398:	bd38      	pop	{r3, r4, r5, pc}
  21b39a:	bf00      	nop
  21b39c:	0021fd8c 	.word	0x0021fd8c

0021b3a0 <wiced_bt_a2dp_sink_free_sdb>:
  21b3a0:	b510      	push	{r4, lr}
  21b3a2:	6944      	ldr	r4, [r0, #20]
  21b3a4:	6820      	ldr	r0, [r4, #0]
  21b3a6:	b118      	cbz	r0, 21b3b0 <wiced_bt_a2dp_sink_free_sdb+0x10>
  21b3a8:	f6b5 ff14 	bl	d11d4 <wiced_bt_free_buffer>
  21b3ac:	2300      	movs	r3, #0
  21b3ae:	6023      	str	r3, [r4, #0]
  21b3b0:	bd10      	pop	{r4, pc}
	...

0021b3b4 <wiced_bt_a2dp_sink_config_ind>:
  21b3b4:	e92d 43f0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, lr}
  21b3b8:	460c      	mov	r4, r1
  21b3ba:	6945      	ldr	r5, [r0, #20]
  21b3bc:	f891 3076 	ldrb.w	r3, [r1, #118]	; 0x76
  21b3c0:	b085      	sub	sp, #20
  21b3c2:	f885 308e 	strb.w	r3, [r5, #142]	; 0x8e
  21b3c6:	3108      	adds	r1, #8
  21b3c8:	4606      	mov	r6, r0
  21b3ca:	220a      	movs	r2, #10
  21b3cc:	f105 0008 	add.w	r0, r5, #8
  21b3d0:	f8b5 706e 	ldrh.w	r7, [r5, #110]	; 0x6e
  21b3d4:	f616 fd72 	bl	31ebc <bsc_OpExtended+0x2d91>
  21b3d8:	7aa3      	ldrb	r3, [r4, #10]
  21b3da:	f104 0884 	add.w	r8, r4, #132	; 0x84
  21b3de:	f105 0974 	add.w	r9, r5, #116	; 0x74
  21b3e2:	4641      	mov	r1, r8
  21b3e4:	4648      	mov	r0, r9
  21b3e6:	f885 3080 	strb.w	r3, [r5, #128]	; 0x80
  21b3ea:	f002 fb25 	bl	21da38 <utl_bdcmp>
  21b3ee:	b140      	cbz	r0, 21b402 <wiced_bt_a2dp_sink_config_ind+0x4e>
  21b3f0:	2100      	movs	r1, #0
  21b3f2:	4a20      	ldr	r2, [pc, #128]	; (21b474 <wiced_bt_a2dp_sink_config_ind+0xc0>)
  21b3f4:	4608      	mov	r0, r1
  21b3f6:	f5f2 f931 	bl	d65c <wiced_va_printf+0x1f7>
  21b3fa:	f240 1301 	movw	r3, #257	; 0x101
  21b3fe:	f8a5 3096 	strh.w	r3, [r5, #150]	; 0x96
  21b402:	4641      	mov	r1, r8
  21b404:	4648      	mov	r0, r9
  21b406:	f002 fb0e 	bl	21da26 <utl_bdcpy>
  21b40a:	f894 306c 	ldrb.w	r3, [r4, #108]	; 0x6c
  21b40e:	b143      	cbz	r3, 21b422 <wiced_bt_a2dp_sink_config_ind+0x6e>
  21b410:	f8b5 306e 	ldrh.w	r3, [r5, #110]	; 0x6e
  21b414:	b2fa      	uxtb	r2, r7
  21b416:	4293      	cmp	r3, r2
  21b418:	d013      	beq.n	21b442 <wiced_bt_a2dp_sink_config_ind+0x8e>
  21b41a:	f423 7380 	bic.w	r3, r3, #256	; 0x100
  21b41e:	429a      	cmp	r2, r3
  21b420:	d00f      	beq.n	21b442 <wiced_bt_a2dp_sink_config_ind+0x8e>
  21b422:	2329      	movs	r3, #41	; 0x29
  21b424:	4641      	mov	r1, r8
  21b426:	f10d 000a 	add.w	r0, sp, #10
  21b42a:	f88d 3008 	strb.w	r3, [sp, #8]
  21b42e:	f002 fafa 	bl	21da26 <utl_bdcpy>
  21b432:	2214      	movs	r2, #20
  21b434:	4669      	mov	r1, sp
  21b436:	4630      	mov	r0, r6
  21b438:	f000 fe96 	bl	21c168 <wiced_bt_a2dp_sink_ssm_execute>
  21b43c:	b005      	add	sp, #20
  21b43e:	e8bd 83f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, pc}
  21b442:	79f3      	ldrb	r3, [r6, #7]
  21b444:	4621      	mov	r1, r4
  21b446:	2b04      	cmp	r3, #4
  21b448:	bf04      	itt	eq
  21b44a:	2301      	moveq	r3, #1
  21b44c:	f885 3099 	strbeq.w	r3, [r5, #153]	; 0x99
  21b450:	f894 308a 	ldrb.w	r3, [r4, #138]	; 0x8a
  21b454:	f885 308a 	strb.w	r3, [r5, #138]	; 0x8a
  21b458:	f894 3080 	ldrb.w	r3, [r4, #128]	; 0x80
  21b45c:	f885 308b 	strb.w	r3, [r5, #139]	; 0x8b
  21b460:	f8b4 306e 	ldrh.w	r3, [r4, #110]	; 0x6e
  21b464:	f8a5 3088 	strh.w	r3, [r5, #136]	; 0x88
  21b468:	4b03      	ldr	r3, [pc, #12]	; (21b478 <wiced_bt_a2dp_sink_config_ind+0xc4>)
  21b46a:	6998      	ldr	r0, [r3, #24]
  21b46c:	3004      	adds	r0, #4
  21b46e:	f000 fb07 	bl	21ba80 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler>
  21b472:	e7e3      	b.n	21b43c <wiced_bt_a2dp_sink_config_ind+0x88>
  21b474:	0021f484 	.word	0x0021f484
  21b478:	0021fd8c 	.word	0x0021fd8c

0021b47c <wiced_bt_a2dp_sink_disconnect_req>:
  21b47c:	68c1      	ldr	r1, [r0, #12]
  21b47e:	3001      	adds	r0, #1
  21b480:	f6bd b8f3 	b.w	d866a <wiced_bt_avdt_connect_req+0x3>

0021b484 <wiced_bt_a2dp_sink_setconfig_rsp>:
  21b484:	b570      	push	{r4, r5, r6, lr}
  21b486:	460d      	mov	r5, r1
  21b488:	b100      	cbz	r0, 21b48c <wiced_bt_a2dp_sink_setconfig_rsp+0x8>
  21b48a:	b939      	cbnz	r1, 21b49c <wiced_bt_a2dp_sink_setconfig_rsp+0x18>
  21b48c:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21b490:	2100      	movs	r1, #0
  21b492:	4b14      	ldr	r3, [pc, #80]	; (21b4e4 <wiced_bt_a2dp_sink_setconfig_rsp+0x60>)
  21b494:	4608      	mov	r0, r1
  21b496:	4a14      	ldr	r2, [pc, #80]	; (21b4e8 <wiced_bt_a2dp_sink_setconfig_rsp+0x64>)
  21b498:	f5f2 b8e0 	b.w	d65c <wiced_va_printf+0x1f7>
  21b49c:	6944      	ldr	r4, [r0, #20]
  21b49e:	7a4b      	ldrb	r3, [r1, #9]
  21b4a0:	7a0a      	ldrb	r2, [r1, #8]
  21b4a2:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b4a6:	f894 108e 	ldrb.w	r1, [r4, #142]	; 0x8e
  21b4aa:	f6bd f8c2 	bl	d8632 <wiced_bt_avdt_open_req+0x3>
  21b4ae:	7a2b      	ldrb	r3, [r5, #8]
  21b4b0:	b9bb      	cbnz	r3, 21b4e2 <wiced_bt_a2dp_sink_setconfig_rsp+0x5e>
  21b4b2:	f8b4 3088 	ldrh.w	r3, [r4, #136]	; 0x88
  21b4b6:	05db      	lsls	r3, r3, #23
  21b4b8:	d513      	bpl.n	21b4e2 <wiced_bt_a2dp_sink_setconfig_rsp+0x5e>
  21b4ba:	2301      	movs	r3, #1
  21b4bc:	f884 3095 	strb.w	r3, [r4, #149]	; 0x95
  21b4c0:	4b0a      	ldr	r3, [pc, #40]	; (21b4ec <wiced_bt_a2dp_sink_setconfig_rsp+0x68>)
  21b4c2:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b4c6:	699b      	ldr	r3, [r3, #24]
  21b4c8:	8999      	ldrh	r1, [r3, #12]
  21b4ca:	7bda      	ldrb	r2, [r3, #15]
  21b4cc:	230a      	movs	r3, #10
  21b4ce:	434a      	muls	r2, r1
  21b4d0:	fbb2 f2f3 	udiv	r2, r2, r3
  21b4d4:	f894 108b 	ldrb.w	r1, [r4, #139]	; 0x8b
  21b4d8:	b292      	uxth	r2, r2
  21b4da:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21b4de:	f6bd b8a4 	b.w	d862a <wiced_bt_avdt_get_all_cap_req+0x3>
  21b4e2:	bd70      	pop	{r4, r5, r6, pc}
  21b4e4:	0021ddde 	.word	0x0021ddde
  21b4e8:	0021f492 	.word	0x0021f492
  21b4ec:	0021fd8c 	.word	0x0021fd8c

0021b4f0 <wiced_bt_a2dp_sink_reconfig_rsp>:
  21b4f0:	b570      	push	{r4, r5, r6, lr}
  21b4f2:	6944      	ldr	r4, [r0, #20]
  21b4f4:	460d      	mov	r5, r1
  21b4f6:	f894 3099 	ldrb.w	r3, [r4, #153]	; 0x99
  21b4fa:	b183      	cbz	r3, 21b51e <wiced_bt_a2dp_sink_reconfig_rsp+0x2e>
  21b4fc:	2300      	movs	r3, #0
  21b4fe:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b502:	f884 3099 	strb.w	r3, [r4, #153]	; 0x99
  21b506:	f7ff fef1 	bl	21b2ec <wiced_bt_a2dp_sink_clean_sep_record>
  21b50a:	7a6b      	ldrb	r3, [r5, #9]
  21b50c:	7a2a      	ldrb	r2, [r5, #8]
  21b50e:	f894 108e 	ldrb.w	r1, [r4, #142]	; 0x8e
  21b512:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b516:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21b51a:	f6bd b898 	b.w	d864e <wiced_bt_avdt_reconfig_req+0x3>
  21b51e:	bd70      	pop	{r4, r5, r6, pc}

0021b520 <wiced_bt_a2dp_sink_sig_opened>:
  21b520:	f891 308a 	ldrb.w	r3, [r1, #138]	; 0x8a
  21b524:	7243      	strb	r3, [r0, #9]
  21b526:	4770      	bx	lr

0021b528 <wiced_bt_a2dp_sink_sig_closed>:
  21b528:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b52a:	4604      	mov	r4, r0
  21b52c:	1c41      	adds	r1, r0, #1
  21b52e:	f10d 0006 	add.w	r0, sp, #6
  21b532:	f002 fa78 	bl	21da26 <utl_bdcpy>
  21b536:	2300      	movs	r3, #0
  21b538:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b53c:	6963      	ldr	r3, [r4, #20]
  21b53e:	2001      	movs	r0, #1
  21b540:	f893 308a 	ldrb.w	r3, [r3, #138]	; 0x8a
  21b544:	a901      	add	r1, sp, #4
  21b546:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b54a:	4b02      	ldr	r3, [pc, #8]	; (21b554 <wiced_bt_a2dp_sink_sig_closed+0x2c>)
  21b54c:	69db      	ldr	r3, [r3, #28]
  21b54e:	4798      	blx	r3
  21b550:	b004      	add	sp, #16
  21b552:	bd10      	pop	{r4, pc}
  21b554:	0021fd8c 	.word	0x0021fd8c

0021b558 <wiced_bt_a2dp_sink_str_opened>:
  21b558:	b530      	push	{r4, r5, lr}
  21b55a:	460d      	mov	r5, r1
  21b55c:	6944      	ldr	r4, [r0, #20]
  21b55e:	b089      	sub	sp, #36	; 0x24
  21b560:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b564:	f6bd f883 	bl	d866e <wiced_bt_avdt_disconnect_req+0x3>
  21b568:	f8b5 307a 	ldrh.w	r3, [r5, #122]	; 0x7a
  21b56c:	f8a4 007a 	strh.w	r0, [r4, #122]	; 0x7a
  21b570:	3b0c      	subs	r3, #12
  21b572:	f8a4 307c 	strh.w	r3, [r4, #124]	; 0x7c
  21b576:	f8b4 3088 	ldrh.w	r3, [r4, #136]	; 0x88
  21b57a:	05d9      	lsls	r1, r3, #23
  21b57c:	d505      	bpl.n	21b58a <wiced_bt_a2dp_sink_str_opened+0x32>
  21b57e:	f894 3095 	ldrb.w	r3, [r4, #149]	; 0x95
  21b582:	b913      	cbnz	r3, 21b58a <wiced_bt_a2dp_sink_str_opened+0x32>
  21b584:	2301      	movs	r3, #1
  21b586:	f884 3095 	strb.w	r3, [r4, #149]	; 0x95
  21b58a:	2300      	movs	r3, #0
  21b58c:	2500      	movs	r5, #0
  21b58e:	f884 3092 	strb.w	r3, [r4, #146]	; 0x92
  21b592:	f894 3091 	ldrb.w	r3, [r4, #145]	; 0x91
  21b596:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b59a:	07da      	lsls	r2, r3, #31
  21b59c:	bf44      	itt	mi
  21b59e:	f023 0301 	bicmi.w	r3, r3, #1
  21b5a2:	f884 3091 	strbmi.w	r3, [r4, #145]	; 0x91
  21b5a6:	f000 fd37 	bl	21c018 <wiced_bt_a2dp_sink_set_handle>
  21b5aa:	f104 0174 	add.w	r1, r4, #116	; 0x74
  21b5ae:	f10d 0006 	add.w	r0, sp, #6
  21b5b2:	f002 fa38 	bl	21da26 <utl_bdcpy>
  21b5b6:	f894 308a 	ldrb.w	r3, [r4, #138]	; 0x8a
  21b5ba:	2210      	movs	r2, #16
  21b5bc:	4628      	mov	r0, r5
  21b5be:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b5c2:	f10d 0106 	add.w	r1, sp, #6
  21b5c6:	ab04      	add	r3, sp, #16
  21b5c8:	f8ad 5004 	strh.w	r5, [sp, #4]
  21b5cc:	f88d 5011 	strb.w	r5, [sp, #17]
  21b5d0:	f000 fc02 	bl	21bdd8 <wiced_bt_a2dp_sink_conn_cback>
  21b5d4:	4b03      	ldr	r3, [pc, #12]	; (21b5e4 <wiced_bt_a2dp_sink_str_opened+0x8c>)
  21b5d6:	4628      	mov	r0, r5
  21b5d8:	69db      	ldr	r3, [r3, #28]
  21b5da:	a901      	add	r1, sp, #4
  21b5dc:	4798      	blx	r3
  21b5de:	b009      	add	sp, #36	; 0x24
  21b5e0:	bd30      	pop	{r4, r5, pc}
  21b5e2:	bf00      	nop
  21b5e4:	0021fd8c 	.word	0x0021fd8c

0021b5e8 <wiced_bt_a2dp_sink_str_open_fail>:
  21b5e8:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b5ea:	6944      	ldr	r4, [r0, #20]
  21b5ec:	f10d 0006 	add.w	r0, sp, #6
  21b5f0:	f104 0174 	add.w	r1, r4, #116	; 0x74
  21b5f4:	f002 fa17 	bl	21da26 <utl_bdcpy>
  21b5f8:	f641 73ad 	movw	r3, #8109	; 0x1fad
  21b5fc:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b600:	f894 308a 	ldrb.w	r3, [r4, #138]	; 0x8a
  21b604:	2000      	movs	r0, #0
  21b606:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b60a:	4b03      	ldr	r3, [pc, #12]	; (21b618 <wiced_bt_a2dp_sink_str_open_fail+0x30>)
  21b60c:	a901      	add	r1, sp, #4
  21b60e:	69db      	ldr	r3, [r3, #28]
  21b610:	4798      	blx	r3
  21b612:	b004      	add	sp, #16
  21b614:	bd10      	pop	{r4, pc}
  21b616:	bf00      	nop
  21b618:	0021fd8c 	.word	0x0021fd8c

0021b61c <wiced_bt_a2dp_sink_connect_req>:
  21b61c:	b5f7      	push	{r0, r1, r2, r4, r5, r6, r7, lr}
  21b61e:	460d      	mov	r5, r1
  21b620:	2100      	movs	r1, #0
  21b622:	4604      	mov	r4, r0
  21b624:	1c46      	adds	r6, r0, #1
  21b626:	6947      	ldr	r7, [r0, #20]
  21b628:	4a0d      	ldr	r2, [pc, #52]	; (21b660 <wiced_bt_a2dp_sink_connect_req+0x44>)
  21b62a:	4608      	mov	r0, r1
  21b62c:	4b0d      	ldr	r3, [pc, #52]	; (21b664 <wiced_bt_a2dp_sink_connect_req+0x48>)
  21b62e:	9600      	str	r6, [sp, #0]
  21b630:	f5f2 f814 	bl	d65c <wiced_va_printf+0x1f7>
  21b634:	892b      	ldrh	r3, [r5, #8]
  21b636:	4629      	mov	r1, r5
  21b638:	f8a7 307e 	strh.w	r3, [r7, #126]	; 0x7e
  21b63c:	4620      	mov	r0, r4
  21b63e:	f7ff feaf 	bl	21b3a0 <wiced_bt_a2dp_sink_free_sdb>
  21b642:	68e2      	ldr	r2, [r4, #12]
  21b644:	2100      	movs	r1, #0
  21b646:	4630      	mov	r0, r6
  21b648:	f6bd f80d 	bl	d8666 <wiced_bt_avdt_security_rsp+0xf>
  21b64c:	2100      	movs	r1, #0
  21b64e:	4603      	mov	r3, r0
  21b650:	4608      	mov	r0, r1
  21b652:	4a05      	ldr	r2, [pc, #20]	; (21b668 <wiced_bt_a2dp_sink_connect_req+0x4c>)
  21b654:	b003      	add	sp, #12
  21b656:	e8bd 40f0 	ldmia.w	sp!, {r4, r5, r6, r7, lr}
  21b65a:	f5f1 bfff 	b.w	d65c <wiced_va_printf+0x1f7>
  21b65e:	bf00      	nop
  21b660:	0021f4af 	.word	0x0021f4af
  21b664:	0021ddff 	.word	0x0021ddff
  21b668:	0021f4b9 	.word	0x0021f4b9

0021b66c <wiced_bt_a2dp_sink_sdp_failed>:
  21b66c:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b66e:	4604      	mov	r4, r0
  21b670:	1c41      	adds	r1, r0, #1
  21b672:	f10d 0006 	add.w	r0, sp, #6
  21b676:	f002 f9d6 	bl	21da26 <utl_bdcpy>
  21b67a:	f641 73ad 	movw	r3, #8109	; 0x1fad
  21b67e:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b682:	6963      	ldr	r3, [r4, #20]
  21b684:	4620      	mov	r0, r4
  21b686:	f893 308a 	ldrb.w	r3, [r3, #138]	; 0x8a
  21b68a:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b68e:	f000 fb16 	bl	21bcbe <wiced_bt_a2dp_sink_dealloc_ccb>
  21b692:	4b03      	ldr	r3, [pc, #12]	; (21b6a0 <wiced_bt_a2dp_sink_sdp_failed+0x34>)
  21b694:	2000      	movs	r0, #0
  21b696:	69db      	ldr	r3, [r3, #28]
  21b698:	a901      	add	r1, sp, #4
  21b69a:	4798      	blx	r3
  21b69c:	b004      	add	sp, #16
  21b69e:	bd10      	pop	{r4, pc}
  21b6a0:	0021fd8c 	.word	0x0021fd8c

0021b6a4 <wiced_bt_a2dp_sink_sig_open_fail>:
  21b6a4:	f7ff bfe2 	b.w	21b66c <wiced_bt_a2dp_sink_sdp_failed>

0021b6a8 <wiced_bt_a2dp_sink_rej_conn>:
  21b6a8:	4608      	mov	r0, r1
  21b6aa:	2300      	movs	r3, #0
  21b6ac:	2231      	movs	r2, #49	; 0x31
  21b6ae:	f891 1076 	ldrb.w	r1, [r1, #118]	; 0x76
  21b6b2:	f890 008a 	ldrb.w	r0, [r0, #138]	; 0x8a
  21b6b6:	f6bc bfbc 	b.w	d8632 <wiced_bt_avdt_open_req+0x3>

0021b6ba <wiced_bt_a2dp_sink_send_start_resp>:
  21b6ba:	b538      	push	{r3, r4, r5, lr}
  21b6bc:	7bca      	ldrb	r2, [r1, #15]
  21b6be:	6945      	ldr	r5, [r0, #20]
  21b6c0:	460c      	mov	r4, r1
  21b6c2:	f895 008a 	ldrb.w	r0, [r5, #138]	; 0x8a
  21b6c6:	b10a      	cbz	r2, 21b6cc <wiced_bt_a2dp_sink_send_start_resp+0x12>
  21b6c8:	2af1      	cmp	r2, #241	; 0xf1
  21b6ca:	d122      	bne.n	21b712 <wiced_bt_a2dp_sink_send_start_resp+0x58>
  21b6cc:	2301      	movs	r3, #1
  21b6ce:	f895 1091 	ldrb.w	r1, [r5, #145]	; 0x91
  21b6d2:	f885 3093 	strb.w	r3, [r5, #147]	; 0x93
  21b6d6:	f011 0f10 	tst.w	r1, #16
  21b6da:	bf1c      	itt	ne
  21b6dc:	f021 0110 	bicne.w	r1, r1, #16
  21b6e0:	f885 1091 	strbne.w	r1, [r5, #145]	; 0x91
  21b6e4:	2af1      	cmp	r2, #241	; 0xf1
  21b6e6:	f885 3094 	strb.w	r3, [r5, #148]	; 0x94
  21b6ea:	d109      	bne.n	21b700 <wiced_bt_a2dp_sink_send_start_resp+0x46>
  21b6ec:	2300      	movs	r3, #0
  21b6ee:	73e3      	strb	r3, [r4, #15]
  21b6f0:	7be2      	ldrb	r2, [r4, #15]
  21b6f2:	7ba1      	ldrb	r1, [r4, #14]
  21b6f4:	f895 008a 	ldrb.w	r0, [r5, #138]	; 0x8a
  21b6f8:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21b6fc:	f6bc bf9f 	b.w	d863e <wiced_bt_avdt_start_req+0x3>
  21b700:	f000 fcd0 	bl	21c0a4 <wiced_bt_a2dp_sink_streaming_configure_route>
  21b704:	f020 0010 	bic.w	r0, r0, #16
  21b708:	b280      	uxth	r0, r0
  21b70a:	2800      	cmp	r0, #0
  21b70c:	d0f0      	beq.n	21b6f0 <wiced_bt_a2dp_sink_send_start_resp+0x36>
  21b70e:	2231      	movs	r2, #49	; 0x31
  21b710:	e7ef      	b.n	21b6f2 <wiced_bt_a2dp_sink_send_start_resp+0x38>
  21b712:	7b89      	ldrb	r1, [r1, #14]
  21b714:	e7f0      	b.n	21b6f8 <wiced_bt_a2dp_sink_send_start_resp+0x3e>
	...

0021b718 <wiced_bt_a2dp_sink_str_stopped>:
  21b718:	b530      	push	{r4, r5, lr}
  21b71a:	460d      	mov	r5, r1
  21b71c:	6944      	ldr	r4, [r0, #20]
  21b71e:	b085      	sub	sp, #20
  21b720:	f10d 0006 	add.w	r0, sp, #6
  21b724:	f104 0174 	add.w	r1, r4, #116	; 0x74
  21b728:	f002 f97d 	bl	21da26 <utl_bdcpy>
  21b72c:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b730:	f8ad 000c 	strh.w	r0, [sp, #12]
  21b734:	b30d      	cbz	r5, 21b77a <wiced_bt_a2dp_sink_str_stopped+0x62>
  21b736:	f894 3093 	ldrb.w	r3, [r4, #147]	; 0x93
  21b73a:	b163      	cbz	r3, 21b756 <wiced_bt_a2dp_sink_str_stopped+0x3e>
  21b73c:	f894 3097 	ldrb.w	r3, [r4, #151]	; 0x97
  21b740:	b14b      	cbz	r3, 21b756 <wiced_bt_a2dp_sink_str_stopped+0x3e>
  21b742:	2300      	movs	r3, #0
  21b744:	2101      	movs	r1, #1
  21b746:	f884 3092 	strb.w	r3, [r4, #146]	; 0x92
  21b74a:	f104 008a 	add.w	r0, r4, #138	; 0x8a
  21b74e:	f6bc ff78 	bl	d8642 <wiced_bt_avdt_start_resp+0x3>
  21b752:	b005      	add	sp, #20
  21b754:	bd30      	pop	{r4, r5, pc}
  21b756:	f894 2094 	ldrb.w	r2, [r4, #148]	; 0x94
  21b75a:	2300      	movs	r3, #0
  21b75c:	b10a      	cbz	r2, 21b762 <wiced_bt_a2dp_sink_str_stopped+0x4a>
  21b75e:	f884 3094 	strb.w	r3, [r4, #148]	; 0x94
  21b762:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b766:	f000 fc85 	bl	21c074 <wiced_bt_a2dp_sink_streaming_stop>
  21b76a:	f8ad 0004 	strh.w	r0, [sp, #4]
  21b76e:	4b07      	ldr	r3, [pc, #28]	; (21b78c <wiced_bt_a2dp_sink_str_stopped+0x74>)
  21b770:	2004      	movs	r0, #4
  21b772:	69db      	ldr	r3, [r3, #28]
  21b774:	a901      	add	r1, sp, #4
  21b776:	4798      	blx	r3
  21b778:	e7eb      	b.n	21b752 <wiced_bt_a2dp_sink_str_stopped+0x3a>
  21b77a:	f894 3094 	ldrb.w	r3, [r4, #148]	; 0x94
  21b77e:	2b00      	cmp	r3, #0
  21b780:	d0e7      	beq.n	21b752 <wiced_bt_a2dp_sink_str_stopped+0x3a>
  21b782:	f884 5094 	strb.w	r5, [r4, #148]	; 0x94
  21b786:	f000 fc75 	bl	21c074 <wiced_bt_a2dp_sink_streaming_stop>
  21b78a:	e7f0      	b.n	21b76e <wiced_bt_a2dp_sink_str_stopped+0x56>
  21b78c:	0021fd8c 	.word	0x0021fd8c

0021b790 <wiced_bt_a2dp_sink_do_close>:
  21b790:	b510      	push	{r4, lr}
  21b792:	6944      	ldr	r4, [r0, #20]
  21b794:	f894 3094 	ldrb.w	r3, [r4, #148]	; 0x94
  21b798:	b113      	cbz	r3, 21b7a0 <wiced_bt_a2dp_sink_do_close+0x10>
  21b79a:	2100      	movs	r1, #0
  21b79c:	f7ff ffbc 	bl	21b718 <wiced_bt_a2dp_sink_str_stopped>
  21b7a0:	2300      	movs	r3, #0
  21b7a2:	f884 3093 	strb.w	r3, [r4, #147]	; 0x93
  21b7a6:	2301      	movs	r3, #1
  21b7a8:	f884 309a 	strb.w	r3, [r4, #154]	; 0x9a
  21b7ac:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b7b0:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21b7b4:	f6bc bf47 	b.w	d8646 <wiced_bt_avdt_suspend_req+0x3>

0021b7b8 <wiced_bt_a2dp_sink_start_ind>:
  21b7b8:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b7ba:	2300      	movs	r3, #0
  21b7bc:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b7c0:	f891 3076 	ldrb.w	r3, [r1, #118]	; 0x76
  21b7c4:	2206      	movs	r2, #6
  21b7c6:	f88d 3006 	strb.w	r3, [sp, #6]
  21b7ca:	6943      	ldr	r3, [r0, #20]
  21b7cc:	3184      	adds	r1, #132	; 0x84
  21b7ce:	f893 308a 	ldrb.w	r3, [r3, #138]	; 0x8a
  21b7d2:	f10d 000a 	add.w	r0, sp, #10
  21b7d6:	f8ad 3008 	strh.w	r3, [sp, #8]
  21b7da:	f616 fb6f 	bl	31ebc <bsc_OpExtended+0x2d91>
  21b7de:	4b04      	ldr	r3, [pc, #16]	; (21b7f0 <wiced_bt_a2dp_sink_start_ind+0x38>)
  21b7e0:	2002      	movs	r0, #2
  21b7e2:	69db      	ldr	r3, [r3, #28]
  21b7e4:	a901      	add	r1, sp, #4
  21b7e6:	4798      	blx	r3
  21b7e8:	b005      	add	sp, #20
  21b7ea:	f85d fb04 	ldr.w	pc, [sp], #4
  21b7ee:	bf00      	nop
  21b7f0:	0021fd8c 	.word	0x0021fd8c

0021b7f4 <wiced_bt_a2dp_sink_start_ok>:
  21b7f4:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b7f6:	2301      	movs	r3, #1
  21b7f8:	6941      	ldr	r1, [r0, #20]
  21b7fa:	f881 3093 	strb.w	r3, [r1, #147]	; 0x93
  21b7fe:	f881 3094 	strb.w	r3, [r1, #148]	; 0x94
  21b802:	2300      	movs	r3, #0
  21b804:	f891 2091 	ldrb.w	r2, [r1, #145]	; 0x91
  21b808:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b80c:	06d0      	lsls	r0, r2, #27
  21b80e:	f891 308a 	ldrb.w	r3, [r1, #138]	; 0x8a
  21b812:	bf44      	itt	mi
  21b814:	f022 0210 	bicmi.w	r2, r2, #16
  21b818:	f881 2091 	strbmi.w	r2, [r1, #145]	; 0x91
  21b81c:	f10d 0006 	add.w	r0, sp, #6
  21b820:	3174      	adds	r1, #116	; 0x74
  21b822:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b826:	f002 f8fe 	bl	21da26 <utl_bdcpy>
  21b82a:	4b04      	ldr	r3, [pc, #16]	; (21b83c <wiced_bt_a2dp_sink_start_ok+0x48>)
  21b82c:	2003      	movs	r0, #3
  21b82e:	69db      	ldr	r3, [r3, #28]
  21b830:	a901      	add	r1, sp, #4
  21b832:	4798      	blx	r3
  21b834:	b005      	add	sp, #20
  21b836:	f85d fb04 	ldr.w	pc, [sp], #4
  21b83a:	bf00      	nop
  21b83c:	0021fd8c 	.word	0x0021fd8c

0021b840 <wiced_bt_a2dp_sink_do_start>:
  21b840:	b510      	push	{r4, lr}
  21b842:	6944      	ldr	r4, [r0, #20]
  21b844:	f894 3093 	ldrb.w	r3, [r4, #147]	; 0x93
  21b848:	b98b      	cbnz	r3, 21b86e <wiced_bt_a2dp_sink_do_start+0x2e>
  21b84a:	f894 3091 	ldrb.w	r3, [r4, #145]	; 0x91
  21b84e:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b852:	f043 0310 	orr.w	r3, r3, #16
  21b856:	f884 3091 	strb.w	r3, [r4, #145]	; 0x91
  21b85a:	f000 fc23 	bl	21c0a4 <wiced_bt_a2dp_sink_streaming_configure_route>
  21b85e:	b950      	cbnz	r0, 21b876 <wiced_bt_a2dp_sink_do_start+0x36>
  21b860:	f104 008a 	add.w	r0, r4, #138	; 0x8a
  21b864:	2101      	movs	r1, #1
  21b866:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21b86a:	f6bc bee6 	b.w	d863a <wiced_bt_avdt_security_set_scms+0x3>
  21b86e:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21b872:	f7ff bfbf 	b.w	21b7f4 <wiced_bt_a2dp_sink_start_ok>
  21b876:	bd10      	pop	{r4, pc}

0021b878 <wiced_bt_a2dp_sink_start_failed>:
  21b878:	b51f      	push	{r0, r1, r2, r3, r4, lr}
  21b87a:	6944      	ldr	r4, [r0, #20]
  21b87c:	f894 3093 	ldrb.w	r3, [r4, #147]	; 0x93
  21b880:	b9c3      	cbnz	r3, 21b8b4 <wiced_bt_a2dp_sink_start_failed+0x3c>
  21b882:	f894 3094 	ldrb.w	r3, [r4, #148]	; 0x94
  21b886:	b9ab      	cbnz	r3, 21b8b4 <wiced_bt_a2dp_sink_start_failed+0x3c>
  21b888:	2304      	movs	r3, #4
  21b88a:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b88e:	f894 308a 	ldrb.w	r3, [r4, #138]	; 0x8a
  21b892:	f104 0174 	add.w	r1, r4, #116	; 0x74
  21b896:	f10d 0006 	add.w	r0, sp, #6
  21b89a:	f8ad 300c 	strh.w	r3, [sp, #12]
  21b89e:	f002 f8c2 	bl	21da26 <utl_bdcpy>
  21b8a2:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b8a6:	f000 fbe5 	bl	21c074 <wiced_bt_a2dp_sink_streaming_stop>
  21b8aa:	4b03      	ldr	r3, [pc, #12]	; (21b8b8 <wiced_bt_a2dp_sink_start_failed+0x40>)
  21b8ac:	2003      	movs	r0, #3
  21b8ae:	69db      	ldr	r3, [r3, #28]
  21b8b0:	a901      	add	r1, sp, #4
  21b8b2:	4798      	blx	r3
  21b8b4:	b004      	add	sp, #16
  21b8b6:	bd10      	pop	{r4, pc}
  21b8b8:	0021fd8c 	.word	0x0021fd8c

0021b8bc <wiced_bt_a2dp_sink_suspend_cfm>:
  21b8bc:	b530      	push	{r4, r5, lr}
  21b8be:	b085      	sub	sp, #20
  21b8c0:	6944      	ldr	r4, [r0, #20]
  21b8c2:	f891 5074 	ldrb.w	r5, [r1, #116]	; 0x74
  21b8c6:	f10d 0006 	add.w	r0, sp, #6
  21b8ca:	3184      	adds	r1, #132	; 0x84
  21b8cc:	f002 f8ab 	bl	21da26 <utl_bdcpy>
  21b8d0:	2300      	movs	r3, #0
  21b8d2:	f894 008a 	ldrb.w	r0, [r4, #138]	; 0x8a
  21b8d6:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b8da:	f8ad 000c 	strh.w	r0, [sp, #12]
  21b8de:	b1bd      	cbz	r5, 21b910 <wiced_bt_a2dp_sink_suspend_cfm+0x54>
  21b8e0:	2d08      	cmp	r5, #8
  21b8e2:	bf18      	it	ne
  21b8e4:	f884 3097 	strbne.w	r3, [r4, #151]	; 0x97
  21b8e8:	2304      	movs	r3, #4
  21b8ea:	f8ad 3004 	strh.w	r3, [sp, #4]
  21b8ee:	f894 3094 	ldrb.w	r3, [r4, #148]	; 0x94
  21b8f2:	b113      	cbz	r3, 21b8fa <wiced_bt_a2dp_sink_suspend_cfm+0x3e>
  21b8f4:	2300      	movs	r3, #0
  21b8f6:	f884 3094 	strb.w	r3, [r4, #148]	; 0x94
  21b8fa:	f000 fbbb 	bl	21c074 <wiced_bt_a2dp_sink_streaming_stop>
  21b8fe:	4b06      	ldr	r3, [pc, #24]	; (21b918 <wiced_bt_a2dp_sink_suspend_cfm+0x5c>)
  21b900:	f8ad 0004 	strh.w	r0, [sp, #4]
  21b904:	69db      	ldr	r3, [r3, #28]
  21b906:	2004      	movs	r0, #4
  21b908:	a901      	add	r1, sp, #4
  21b90a:	4798      	blx	r3
  21b90c:	b005      	add	sp, #20
  21b90e:	bd30      	pop	{r4, r5, pc}
  21b910:	f884 5093 	strb.w	r5, [r4, #147]	; 0x93
  21b914:	e7eb      	b.n	21b8ee <wiced_bt_a2dp_sink_suspend_cfm+0x32>
  21b916:	bf00      	nop
  21b918:	0021fd8c 	.word	0x0021fd8c

0021b91c <wiced_bt_a2dp_sink_hdl_str_close>:
  21b91c:	b538      	push	{r3, r4, r5, lr}
  21b91e:	6945      	ldr	r5, [r0, #20]
  21b920:	4604      	mov	r4, r0
  21b922:	f895 3094 	ldrb.w	r3, [r5, #148]	; 0x94
  21b926:	b113      	cbz	r3, 21b92e <wiced_bt_a2dp_sink_hdl_str_close+0x12>
  21b928:	2100      	movs	r1, #0
  21b92a:	f7ff fef5 	bl	21b718 <wiced_bt_a2dp_sink_str_stopped>
  21b92e:	f895 309a 	ldrb.w	r3, [r5, #154]	; 0x9a
  21b932:	2b01      	cmp	r3, #1
  21b934:	d106      	bne.n	21b944 <wiced_bt_a2dp_sink_hdl_str_close+0x28>
  21b936:	68e1      	ldr	r1, [r4, #12]
  21b938:	1c60      	adds	r0, r4, #1
  21b93a:	f6bc fe96 	bl	d866a <wiced_bt_avdt_connect_req+0x3>
  21b93e:	2300      	movs	r3, #0
  21b940:	f885 309a 	strb.w	r3, [r5, #154]	; 0x9a
  21b944:	bd38      	pop	{r3, r4, r5, pc}

0021b946 <wiced_bt_a2dp_sink_sig_hdl_ap_close_disconnect_req>:
  21b946:	f7ff bd99 	b.w	21b47c <wiced_bt_a2dp_sink_disconnect_req>

0021b94a <wiced_bt_a2dp_sink_sig_closed_cleanup>:
  21b94a:	b538      	push	{r3, r4, r5, lr}
  21b94c:	4604      	mov	r4, r0
  21b94e:	460d      	mov	r5, r1
  21b950:	6943      	ldr	r3, [r0, #20]
  21b952:	f893 008a 	ldrb.w	r0, [r3, #138]	; 0x8a
  21b956:	f000 fbc7 	bl	21c0e8 <wiced_bt_a2dp_sink_stream_close>
  21b95a:	4629      	mov	r1, r5
  21b95c:	4620      	mov	r0, r4
  21b95e:	f7ff fde3 	bl	21b528 <wiced_bt_a2dp_sink_sig_closed>
  21b962:	4620      	mov	r0, r4
  21b964:	4629      	mov	r1, r5
  21b966:	f7ff fcd1 	bl	21b30c <wiced_bt_a2dp_sink_cleanup>
  21b96a:	4620      	mov	r0, r4
  21b96c:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21b970:	f000 b9a5 	b.w	21bcbe <wiced_bt_a2dp_sink_dealloc_ccb>

0021b974 <wiced_bt_a2dp_sink_init>:
  21b974:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  21b976:	4e1d      	ldr	r6, [pc, #116]	; (21b9ec <wiced_bt_a2dp_sink_init+0x78>)
  21b978:	4605      	mov	r5, r0
  21b97a:	f896 3024 	ldrb.w	r3, [r6, #36]	; 0x24
  21b97e:	460c      	mov	r4, r1
  21b980:	2b01      	cmp	r3, #1
  21b982:	d109      	bne.n	21b998 <wiced_bt_a2dp_sink_init+0x24>
  21b984:	2100      	movs	r1, #0
  21b986:	4b1a      	ldr	r3, [pc, #104]	; (21b9f0 <wiced_bt_a2dp_sink_init+0x7c>)
  21b988:	4608      	mov	r0, r1
  21b98a:	4a1a      	ldr	r2, [pc, #104]	; (21b9f4 <wiced_bt_a2dp_sink_init+0x80>)
  21b98c:	f5f1 fe66 	bl	d65c <wiced_va_printf+0x1f7>
  21b990:	f44f 747a 	mov.w	r4, #1000	; 0x3e8
  21b994:	4620      	mov	r0, r4
  21b996:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
  21b998:	b930      	cbnz	r0, 21b9a8 <wiced_bt_a2dp_sink_init+0x34>
  21b99a:	4601      	mov	r1, r0
  21b99c:	4b14      	ldr	r3, [pc, #80]	; (21b9f0 <wiced_bt_a2dp_sink_init+0x7c>)
  21b99e:	4a16      	ldr	r2, [pc, #88]	; (21b9f8 <wiced_bt_a2dp_sink_init+0x84>)
  21b9a0:	f5f1 fe5c 	bl	d65c <wiced_va_printf+0x1f7>
  21b9a4:	2405      	movs	r4, #5
  21b9a6:	e7f5      	b.n	21b994 <wiced_bt_a2dp_sink_init+0x20>
  21b9a8:	f000 fc10 	bl	21c1cc <wiced_bt_a2dp_sink_init_state_machine>
  21b9ac:	4607      	mov	r7, r0
  21b9ae:	2800      	cmp	r0, #0
  21b9b0:	d1f8      	bne.n	21b9a4 <wiced_bt_a2dp_sink_init+0x30>
  21b9b2:	4601      	mov	r1, r0
  21b9b4:	f44f 7233 	mov.w	r2, #716	; 0x2cc
  21b9b8:	4630      	mov	r0, r6
  21b9ba:	f616 fa83 	bl	31ec4 <memcpy+0x7>
  21b9be:	e9c6 5406 	strd	r5, r4, [r6, #24]
  21b9c2:	f000 fa2d 	bl	21be20 <wiced_bt_a2dp_sink_register>
  21b9c6:	4604      	mov	r4, r0
  21b9c8:	b130      	cbz	r0, 21b9d8 <wiced_bt_a2dp_sink_init+0x64>
  21b9ca:	4639      	mov	r1, r7
  21b9cc:	4638      	mov	r0, r7
  21b9ce:	4b08      	ldr	r3, [pc, #32]	; (21b9f0 <wiced_bt_a2dp_sink_init+0x7c>)
  21b9d0:	4a0a      	ldr	r2, [pc, #40]	; (21b9fc <wiced_bt_a2dp_sink_init+0x88>)
  21b9d2:	f5f1 fe43 	bl	d65c <wiced_va_printf+0x1f7>
  21b9d6:	e7dd      	b.n	21b994 <wiced_bt_a2dp_sink_init+0x20>
  21b9d8:	2301      	movs	r3, #1
  21b9da:	f105 000c 	add.w	r0, r5, #12
  21b9de:	f886 3024 	strb.w	r3, [r6, #36]	; 0x24
  21b9e2:	e8bd 40f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, lr}
  21b9e6:	f7fc b9d6 	b.w	217d96 <wiced_audio_sink_config_init>
  21b9ea:	bf00      	nop
  21b9ec:	0021fd8c 	.word	0x0021fd8c
  21b9f0:	0021de1e 	.word	0x0021de1e
  21b9f4:	0021f4df 	.word	0x0021f4df
  21b9f8:	0021f4f9 	.word	0x0021f4f9
  21b9fc:	0021f51a 	.word	0x0021f51a

0021ba00 <wiced_bt_a2dp_sink_send_start_response>:
  21ba00:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  21ba02:	4b11      	ldr	r3, [pc, #68]	; (21ba48 <wiced_bt_a2dp_sink_send_start_response+0x48>)
  21ba04:	4607      	mov	r7, r0
  21ba06:	f893 3024 	ldrb.w	r3, [r3, #36]	; 0x24
  21ba0a:	460e      	mov	r6, r1
  21ba0c:	2b01      	cmp	r3, #1
  21ba0e:	4615      	mov	r5, r2
  21ba10:	d007      	beq.n	21ba22 <wiced_bt_a2dp_sink_send_start_response+0x22>
  21ba12:	2100      	movs	r1, #0
  21ba14:	4b0d      	ldr	r3, [pc, #52]	; (21ba4c <wiced_bt_a2dp_sink_send_start_response+0x4c>)
  21ba16:	4608      	mov	r0, r1
  21ba18:	4a0d      	ldr	r2, [pc, #52]	; (21ba50 <wiced_bt_a2dp_sink_send_start_response+0x50>)
  21ba1a:	f5f1 fe1f 	bl	d65c <wiced_va_printf+0x1f7>
  21ba1e:	2009      	movs	r0, #9
  21ba20:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
  21ba22:	2012      	movs	r0, #18
  21ba24:	f6b5 fbd4 	bl	d11d0 <wiced_bt_get_buffer>
  21ba28:	4604      	mov	r4, r0
  21ba2a:	b158      	cbz	r0, 21ba44 <wiced_bt_a2dp_sink_send_start_response+0x44>
  21ba2c:	2307      	movs	r3, #7
  21ba2e:	8207      	strh	r7, [r0, #16]
  21ba30:	8003      	strh	r3, [r0, #0]
  21ba32:	7386      	strb	r6, [r0, #14]
  21ba34:	73c5      	strb	r5, [r0, #15]
  21ba36:	f000 f979 	bl	21bd2c <wiced_bt_a2dp_sink_hdl_event>
  21ba3a:	4620      	mov	r0, r4
  21ba3c:	f6b5 fbca 	bl	d11d4 <wiced_bt_free_buffer>
  21ba40:	2000      	movs	r0, #0
  21ba42:	e7ed      	b.n	21ba20 <wiced_bt_a2dp_sink_send_start_response+0x20>
  21ba44:	2008      	movs	r0, #8
  21ba46:	e7eb      	b.n	21ba20 <wiced_bt_a2dp_sink_send_start_response+0x20>
  21ba48:	0021fd8c 	.word	0x0021fd8c
  21ba4c:	0021dea2 	.word	0x0021dea2
  21ba50:	0021f53a 	.word	0x0021f53a

0021ba54 <wiced_bt_a2dp_sink_cfg_init>:
  21ba54:	b510      	push	{r4, lr}
  21ba56:	7812      	ldrb	r2, [r2, #0]
  21ba58:	4604      	mov	r4, r0
  21ba5a:	b122      	cbz	r2, 21ba66 <wiced_bt_a2dp_sink_cfg_init+0x12>
  21ba5c:	2202      	movs	r2, #2
  21ba5e:	701a      	strb	r2, [r3, #0]
  21ba60:	705a      	strb	r2, [r3, #1]
  21ba62:	2200      	movs	r2, #0
  21ba64:	709a      	strb	r2, [r3, #2]
  21ba66:	7820      	ldrb	r0, [r4, #0]
  21ba68:	b938      	cbnz	r0, 21ba7a <wiced_bt_a2dp_sink_cfg_init+0x26>
  21ba6a:	460a      	mov	r2, r1
  21ba6c:	1d21      	adds	r1, r4, #4
  21ba6e:	f6c2 fd49 	bl	de504 <wiced_bt_a2d_sbc_descramble+0x3>
  21ba72:	fab0 f080 	clz	r0, r0
  21ba76:	0940      	lsrs	r0, r0, #5
  21ba78:	bd10      	pop	{r4, pc}
  21ba7a:	2000      	movs	r0, #0
  21ba7c:	e7fc      	b.n	21ba78 <wiced_bt_a2dp_sink_cfg_init+0x24>
	...

0021ba80 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler>:
  21ba80:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21ba84:	f04f 0900 	mov.w	r9, #0
  21ba88:	f891 206d 	ldrb.w	r2, [r1, #109]	; 0x6d
  21ba8c:	b091      	sub	sp, #68	; 0x44
  21ba8e:	9202      	str	r2, [sp, #8]
  21ba90:	4a3e      	ldr	r2, [pc, #248]	; (21bb8c <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0x10c>)
  21ba92:	464e      	mov	r6, r9
  21ba94:	f04f 0a07 	mov.w	sl, #7
  21ba98:	240a      	movs	r4, #10
  21ba9a:	4690      	mov	r8, r2
  21ba9c:	7a8b      	ldrb	r3, [r1, #10]
  21ba9e:	460d      	mov	r5, r1
  21baa0:	f101 0b08 	add.w	fp, r1, #8
  21baa4:	6991      	ldr	r1, [r2, #24]
  21baa6:	f88d 3014 	strb.w	r3, [sp, #20]
  21baaa:	688f      	ldr	r7, [r1, #8]
  21baac:	3710      	adds	r7, #16
  21baae:	f8d8 2018 	ldr.w	r2, [r8, #24]
  21bab2:	7911      	ldrb	r1, [r2, #4]
  21bab4:	fa5f f289 	uxtb.w	r2, r9
  21bab8:	4291      	cmp	r1, r2
  21baba:	d83b      	bhi.n	21bb34 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xb4>
  21babc:	2210      	movs	r2, #16
  21babe:	2100      	movs	r1, #0
  21bac0:	a809      	add	r0, sp, #36	; 0x24
  21bac2:	f616 f9ff 	bl	31ec4 <memcpy+0x7>
  21bac6:	2c00      	cmp	r4, #0
  21bac8:	bf0c      	ite	eq
  21baca:	2313      	moveq	r3, #19
  21bacc:	2314      	movne	r3, #20
  21bace:	f105 0784 	add.w	r7, r5, #132	; 0x84
  21bad2:	2206      	movs	r2, #6
  21bad4:	4639      	mov	r1, r7
  21bad6:	f10d 002e 	add.w	r0, sp, #46	; 0x2e
  21bada:	f8ad 3024 	strh.w	r3, [sp, #36]	; 0x24
  21bade:	f88d 402c 	strb.w	r4, [sp, #44]	; 0x2c
  21bae2:	f88d a02d 	strb.w	sl, [sp, #45]	; 0x2d
  21bae6:	f616 f9e9 	bl	31ebc <bsc_OpExtended+0x2d91>
  21baea:	a809      	add	r0, sp, #36	; 0x24
  21baec:	f000 f91e 	bl	21bd2c <wiced_bt_a2dp_sink_hdl_event>
  21baf0:	b9ec      	cbnz	r4, 21bb2e <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xae>
  21baf2:	2301      	movs	r3, #1
  21baf4:	4620      	mov	r0, r4
  21baf6:	f895 208a 	ldrb.w	r2, [r5, #138]	; 0x8a
  21bafa:	9300      	str	r3, [sp, #0]
  21bafc:	a905      	add	r1, sp, #20
  21bafe:	4633      	mov	r3, r6
  21bb00:	f000 fa56 	bl	21bfb0 <wiced_bt_a2dp_sink_set_route_codec_config>
  21bb04:	2210      	movs	r2, #16
  21bb06:	a905      	add	r1, sp, #20
  21bb08:	a80b      	add	r0, sp, #44	; 0x2c
  21bb0a:	f616 f9d7 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bb0e:	f895 308a 	ldrb.w	r3, [r5, #138]	; 0x8a
  21bb12:	4639      	mov	r1, r7
  21bb14:	2206      	movs	r2, #6
  21bb16:	a809      	add	r0, sp, #36	; 0x24
  21bb18:	f8ad 302a 	strh.w	r3, [sp, #42]	; 0x2a
  21bb1c:	f8ad 603c 	strh.w	r6, [sp, #60]	; 0x3c
  21bb20:	f616 f9cc 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bb24:	2005      	movs	r0, #5
  21bb26:	f8d8 301c 	ldr.w	r3, [r8, #28]
  21bb2a:	a909      	add	r1, sp, #36	; 0x24
  21bb2c:	4798      	blx	r3
  21bb2e:	b011      	add	sp, #68	; 0x44
  21bb30:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21bb34:	f817 2c10 	ldrb.w	r2, [r7, #-16]
  21bb38:	429a      	cmp	r2, r3
  21bb3a:	d11e      	bne.n	21bb7a <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xfa>
  21bb3c:	b9eb      	cbnz	r3, 21bb7a <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xfa>
  21bb3e:	4658      	mov	r0, fp
  21bb40:	f1a7 010c 	sub.w	r1, r7, #12
  21bb44:	9303      	str	r3, [sp, #12]
  21bb46:	f7ff fa86 	bl	21b056 <wiced_bt_a2dp_sbc_cfg_in_cap>
  21bb4a:	9b03      	ldr	r3, [sp, #12]
  21bb4c:	4604      	mov	r4, r0
  21bb4e:	b9a0      	cbnz	r0, 21bb7a <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xfa>
  21bb50:	9a02      	ldr	r2, [sp, #8]
  21bb52:	b142      	cbz	r2, 21bb66 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xe6>
  21bb54:	7d2a      	ldrb	r2, [r5, #20]
  21bb56:	7cee      	ldrb	r6, [r5, #19]
  21bb58:	ea46 2602 	orr.w	r6, r6, r2, lsl #8
  21bb5c:	7caa      	ldrb	r2, [r5, #18]
  21bb5e:	2a02      	cmp	r2, #2
  21bb60:	d10f      	bne.n	21bb82 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0x102>
  21bb62:	2e02      	cmp	r6, #2
  21bb64:	d10d      	bne.n	21bb82 <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0x102>
  21bb66:	2200      	movs	r2, #0
  21bb68:	4659      	mov	r1, fp
  21bb6a:	a806      	add	r0, sp, #24
  21bb6c:	9303      	str	r3, [sp, #12]
  21bb6e:	f6c2 fccb 	bl	de508 <wiced_bt_a2d_bld_sbc_info+0x3>
  21bb72:	9b03      	ldr	r3, [sp, #12]
  21bb74:	4604      	mov	r4, r0
  21bb76:	2800      	cmp	r0, #0
  21bb78:	d0a0      	beq.n	21babc <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0x3c>
  21bb7a:	f109 0901 	add.w	r9, r9, #1
  21bb7e:	3710      	adds	r7, #16
  21bb80:	e795      	b.n	21baae <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0x2e>
  21bb82:	f04f 0a04 	mov.w	sl, #4
  21bb86:	24e0      	movs	r4, #224	; 0xe0
  21bb88:	e7f7      	b.n	21bb7a <wiced_bt_a2dp_sink_cfg_setcfg_ind_handler+0xfa>
  21bb8a:	bf00      	nop
  21bb8c:	0021fd8c 	.word	0x0021fd8c

0021bb90 <wiced_bt_a2dp_sink_get_ccb_by_handle>:
  21bb90:	4602      	mov	r2, r0
  21bb92:	480e      	ldr	r0, [pc, #56]	; (21bbcc <wiced_bt_a2dp_sink_get_ccb_by_handle+0x3c>)
  21bb94:	f890 302c 	ldrb.w	r3, [r0, #44]	; 0x2c
  21bb98:	2b01      	cmp	r3, #1
  21bb9a:	4603      	mov	r3, r0
  21bb9c:	d104      	bne.n	21bba8 <wiced_bt_a2dp_sink_get_ccb_by_handle+0x18>
  21bb9e:	6c01      	ldr	r1, [r0, #64]	; 0x40
  21bba0:	f891 108a 	ldrb.w	r1, [r1, #138]	; 0x8a
  21bba4:	4291      	cmp	r1, r2
  21bba6:	d00d      	beq.n	21bbc4 <wiced_bt_a2dp_sink_get_ccb_by_handle+0x34>
  21bba8:	f893 0044 	ldrb.w	r0, [r3, #68]	; 0x44
  21bbac:	2801      	cmp	r0, #1
  21bbae:	d10b      	bne.n	21bbc8 <wiced_bt_a2dp_sink_get_ccb_by_handle+0x38>
  21bbb0:	6d99      	ldr	r1, [r3, #88]	; 0x58
  21bbb2:	f891 108a 	ldrb.w	r1, [r1, #138]	; 0x8a
  21bbb6:	4291      	cmp	r1, r2
  21bbb8:	d106      	bne.n	21bbc8 <wiced_bt_a2dp_sink_get_ccb_by_handle+0x38>
  21bbba:	2218      	movs	r2, #24
  21bbbc:	fb02 3000 	mla	r0, r2, r0, r3
  21bbc0:	302c      	adds	r0, #44	; 0x2c
  21bbc2:	4770      	bx	lr
  21bbc4:	2000      	movs	r0, #0
  21bbc6:	e7f8      	b.n	21bbba <wiced_bt_a2dp_sink_get_ccb_by_handle+0x2a>
  21bbc8:	2000      	movs	r0, #0
  21bbca:	4770      	bx	lr
  21bbcc:	0021fd8c 	.word	0x0021fd8c

0021bbd0 <wiced_bt_a2dp_sink_report_conn>:
  21bbd0:	4770      	bx	lr
	...

0021bbd4 <wiced_bt_a2dp_sink_alloc_ccb>:
  21bbd4:	b570      	push	{r4, r5, r6, lr}
  21bbd6:	4c12      	ldr	r4, [pc, #72]	; (21bc20 <wiced_bt_a2dp_sink_alloc_ccb+0x4c>)
  21bbd8:	4601      	mov	r1, r0
  21bbda:	f894 202c 	ldrb.w	r2, [r4, #44]	; 0x2c
  21bbde:	b11a      	cbz	r2, 21bbe8 <wiced_bt_a2dp_sink_alloc_ccb+0x14>
  21bbe0:	f894 3044 	ldrb.w	r3, [r4, #68]	; 0x44
  21bbe4:	b9d3      	cbnz	r3, 21bc1c <wiced_bt_a2dp_sink_alloc_ccb+0x48>
  21bbe6:	2201      	movs	r2, #1
  21bbe8:	2518      	movs	r5, #24
  21bbea:	4355      	muls	r5, r2
  21bbec:	1966      	adds	r6, r4, r5
  21bbee:	4613      	mov	r3, r2
  21bbf0:	f886 2034 	strb.w	r2, [r6, #52]	; 0x34
  21bbf4:	229c      	movs	r2, #156	; 0x9c
  21bbf6:	2001      	movs	r0, #1
  21bbf8:	fb02 4303 	mla	r3, r2, r3, r4
  21bbfc:	335c      	adds	r3, #92	; 0x5c
  21bbfe:	6433      	str	r3, [r6, #64]	; 0x40
  21bc00:	4b08      	ldr	r3, [pc, #32]	; (21bc24 <wiced_bt_a2dp_sink_alloc_ccb+0x50>)
  21bc02:	f886 002c 	strb.w	r0, [r6, #44]	; 0x2c
  21bc06:	f105 002d 	add.w	r0, r5, #45	; 0x2d
  21bc0a:	4420      	add	r0, r4
  21bc0c:	63b3      	str	r3, [r6, #56]	; 0x38
  21bc0e:	3108      	adds	r1, #8
  21bc10:	f001 ff09 	bl	21da26 <utl_bdcpy>
  21bc14:	f105 002c 	add.w	r0, r5, #44	; 0x2c
  21bc18:	4420      	add	r0, r4
  21bc1a:	bd70      	pop	{r4, r5, r6, pc}
  21bc1c:	2000      	movs	r0, #0
  21bc1e:	e7fc      	b.n	21bc1a <wiced_bt_a2dp_sink_alloc_ccb+0x46>
  21bc20:	0021fd8c 	.word	0x0021fd8c
  21bc24:	0021b151 	.word	0x0021b151

0021bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>:
  21bc28:	b570      	push	{r4, r5, r6, lr}
  21bc2a:	4d0f      	ldr	r5, [pc, #60]	; (21bc68 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x40>)
  21bc2c:	4606      	mov	r6, r0
  21bc2e:	f895 302c 	ldrb.w	r3, [r5, #44]	; 0x2c
  21bc32:	2b01      	cmp	r3, #1
  21bc34:	d00a      	beq.n	21bc4c <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x24>
  21bc36:	f895 4044 	ldrb.w	r4, [r5, #68]	; 0x44
  21bc3a:	2c01      	cmp	r4, #1
  21bc3c:	d112      	bne.n	21bc64 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x3c>
  21bc3e:	4631      	mov	r1, r6
  21bc40:	480a      	ldr	r0, [pc, #40]	; (21bc6c <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x44>)
  21bc42:	f001 fef9 	bl	21da38 <utl_bdcmp>
  21bc46:	b968      	cbnz	r0, 21bc64 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x3c>
  21bc48:	4620      	mov	r0, r4
  21bc4a:	e006      	b.n	21bc5a <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x32>
  21bc4c:	4601      	mov	r1, r0
  21bc4e:	f105 002d 	add.w	r0, r5, #45	; 0x2d
  21bc52:	f001 fef1 	bl	21da38 <utl_bdcmp>
  21bc56:	2800      	cmp	r0, #0
  21bc58:	d1ed      	bne.n	21bc36 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0xe>
  21bc5a:	2318      	movs	r3, #24
  21bc5c:	fb03 5000 	mla	r0, r3, r0, r5
  21bc60:	302c      	adds	r0, #44	; 0x2c
  21bc62:	e000      	b.n	21bc66 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr+0x3e>
  21bc64:	2000      	movs	r0, #0
  21bc66:	bd70      	pop	{r4, r5, r6, pc}
  21bc68:	0021fd8c 	.word	0x0021fd8c
  21bc6c:	0021fdd1 	.word	0x0021fdd1

0021bc70 <wiced_bt_a2dp_sink_sig_change>:
  21bc70:	b538      	push	{r3, r4, r5, lr}
  21bc72:	88c3      	ldrh	r3, [r0, #6]
  21bc74:	4604      	mov	r4, r0
  21bc76:	2b10      	cmp	r3, #16
  21bc78:	d119      	bne.n	21bcae <wiced_bt_a2dp_sink_sig_change+0x3e>
  21bc7a:	8883      	ldrh	r3, [r0, #4]
  21bc7c:	2b01      	cmp	r3, #1
  21bc7e:	d115      	bne.n	21bcac <wiced_bt_a2dp_sink_sig_change+0x3c>
  21bc80:	f100 0584 	add.w	r5, r0, #132	; 0x84
  21bc84:	4628      	mov	r0, r5
  21bc86:	f7ff ffcf 	bl	21bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>
  21bc8a:	b128      	cbz	r0, 21bc98 <wiced_bt_a2dp_sink_sig_change+0x28>
  21bc8c:	2217      	movs	r2, #23
  21bc8e:	4621      	mov	r1, r4
  21bc90:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21bc94:	f000 ba68 	b.w	21c168 <wiced_bt_a2dp_sink_ssm_execute>
  21bc98:	f104 0008 	add.w	r0, r4, #8
  21bc9c:	4629      	mov	r1, r5
  21bc9e:	f001 fec2 	bl	21da26 <utl_bdcpy>
  21bca2:	4620      	mov	r0, r4
  21bca4:	f7ff ff96 	bl	21bbd4 <wiced_bt_a2dp_sink_alloc_ccb>
  21bca8:	2800      	cmp	r0, #0
  21bcaa:	d1ef      	bne.n	21bc8c <wiced_bt_a2dp_sink_sig_change+0x1c>
  21bcac:	bd38      	pop	{r3, r4, r5, pc}
  21bcae:	3084      	adds	r0, #132	; 0x84
  21bcb0:	f7ff ffba 	bl	21bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>
  21bcb4:	2800      	cmp	r0, #0
  21bcb6:	d0f9      	beq.n	21bcac <wiced_bt_a2dp_sink_sig_change+0x3c>
  21bcb8:	2218      	movs	r2, #24
  21bcba:	2100      	movs	r1, #0
  21bcbc:	e7e8      	b.n	21bc90 <wiced_bt_a2dp_sink_sig_change+0x20>

0021bcbe <wiced_bt_a2dp_sink_dealloc_ccb>:
  21bcbe:	2218      	movs	r2, #24
  21bcc0:	2100      	movs	r1, #0
  21bcc2:	f616 b8ff 	b.w	31ec4 <memcpy+0x7>

0021bcc6 <wiced_bt_a2dp_sink_dealloc_scb>:
  21bcc6:	b510      	push	{r4, lr}
  21bcc8:	229c      	movs	r2, #156	; 0x9c
  21bcca:	2100      	movs	r1, #0
  21bccc:	4604      	mov	r4, r0
  21bcce:	f616 f8f9 	bl	31ec4 <memcpy+0x7>
  21bcd2:	229c      	movs	r2, #156	; 0x9c
  21bcd4:	2100      	movs	r1, #0
  21bcd6:	18a0      	adds	r0, r4, r2
  21bcd8:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21bcdc:	f616 b8f2 	b.w	31ec4 <memcpy+0x7>

0021bce0 <wiced_bt_a2dp_sink_api_deinit>:
  21bce0:	b538      	push	{r3, r4, r5, lr}
  21bce2:	2301      	movs	r3, #1
  21bce4:	4605      	mov	r5, r0
  21bce6:	4c0b      	ldr	r4, [pc, #44]	; (21bd14 <wiced_bt_a2dp_sink_api_deinit+0x34>)
  21bce8:	4601      	mov	r1, r0
  21bcea:	2205      	movs	r2, #5
  21bcec:	f104 002c 	add.w	r0, r4, #44	; 0x2c
  21bcf0:	f884 30f4 	strb.w	r3, [r4, #244]	; 0xf4
  21bcf4:	f000 fa38 	bl	21c168 <wiced_bt_a2dp_sink_ssm_execute>
  21bcf8:	2205      	movs	r2, #5
  21bcfa:	4629      	mov	r1, r5
  21bcfc:	f104 0044 	add.w	r0, r4, #68	; 0x44
  21bd00:	f000 fa32 	bl	21c168 <wiced_bt_a2dp_sink_ssm_execute>
  21bd04:	f104 005c 	add.w	r0, r4, #92	; 0x5c
  21bd08:	f7ff ffdd 	bl	21bcc6 <wiced_bt_a2dp_sink_dealloc_scb>
  21bd0c:	2300      	movs	r3, #0
  21bd0e:	f884 3024 	strb.w	r3, [r4, #36]	; 0x24
  21bd12:	bd38      	pop	{r3, r4, r5, pc}
  21bd14:	0021fd8c 	.word	0x0021fd8c

0021bd18 <wiced_bt_a2dp_sink_dereg_comp>:
  21bd18:	b508      	push	{r3, lr}
  21bd1a:	f6bc fbfd 	bl	d8518 <wiced_bt_avdt_register+0x3>
  21bd1e:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
  21bd22:	4801      	ldr	r0, [pc, #4]	; (21bd28 <wiced_bt_a2dp_sink_dereg_comp+0x10>)
  21bd24:	f7ff bfcf 	b.w	21bcc6 <wiced_bt_a2dp_sink_dealloc_scb>
  21bd28:	0021fde8 	.word	0x0021fde8

0021bd2c <wiced_bt_a2dp_sink_hdl_event>:
  21bd2c:	7803      	ldrb	r3, [r0, #0]
  21bd2e:	b570      	push	{r4, r5, r6, lr}
  21bd30:	1e5a      	subs	r2, r3, #1
  21bd32:	2a17      	cmp	r2, #23
  21bd34:	4605      	mov	r5, r0
  21bd36:	d81b      	bhi.n	21bd70 <wiced_bt_a2dp_sink_hdl_event+0x44>
  21bd38:	2b03      	cmp	r3, #3
  21bd3a:	d805      	bhi.n	21bd48 <wiced_bt_a2dp_sink_hdl_event+0x1c>
  21bd3c:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21bd40:	4a23      	ldr	r2, [pc, #140]	; (21bdd0 <wiced_bt_a2dp_sink_hdl_event+0xa4>)
  21bd42:	f852 3023 	ldr.w	r3, [r2, r3, lsl #2]
  21bd46:	4718      	bx	r3
  21bd48:	3b04      	subs	r3, #4
  21bd4a:	2b14      	cmp	r3, #20
  21bd4c:	d810      	bhi.n	21bd70 <wiced_bt_a2dp_sink_hdl_event+0x44>
  21bd4e:	e8df f003 	tbb	[pc, r3]
  21bd52:	100b      	.short	0x100b
  21bd54:	10101010 	.word	0x10101010
  21bd58:	18181414 	.word	0x18181414
  21bd5c:	29181818 	.word	0x29181818
  21bd60:	1831311a 	.word	0x1831311a
  21bd64:	1833      	.short	0x1833
  21bd66:	18          	.byte	0x18
  21bd67:	00          	.byte	0x00
  21bd68:	f7ff ff34 	bl	21bbd4 <wiced_bt_a2dp_sink_alloc_ccb>
  21bd6c:	4604      	mov	r4, r0
  21bd6e:	b994      	cbnz	r4, 21bd96 <wiced_bt_a2dp_sink_hdl_event+0x6a>
  21bd70:	bd70      	pop	{r4, r5, r6, pc}
  21bd72:	8a00      	ldrh	r0, [r0, #16]
  21bd74:	f7ff ff0c 	bl	21bb90 <wiced_bt_a2dp_sink_get_ccb_by_handle>
  21bd78:	e7f8      	b.n	21bd6c <wiced_bt_a2dp_sink_hdl_event+0x40>
  21bd7a:	4816      	ldr	r0, [pc, #88]	; (21bdd4 <wiced_bt_a2dp_sink_hdl_event+0xa8>)
  21bd7c:	f7ff ff54 	bl	21bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>
  21bd80:	e7f4      	b.n	21bd6c <wiced_bt_a2dp_sink_hdl_event+0x40>
  21bd82:	3084      	adds	r0, #132	; 0x84
  21bd84:	e7fa      	b.n	21bd7c <wiced_bt_a2dp_sink_hdl_event+0x50>
  21bd86:	3084      	adds	r0, #132	; 0x84
  21bd88:	f7ff ff4e 	bl	21bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>
  21bd8c:	4604      	mov	r4, r0
  21bd8e:	2800      	cmp	r0, #0
  21bd90:	d0ee      	beq.n	21bd70 <wiced_bt_a2dp_sink_hdl_event+0x44>
  21bd92:	2301      	movs	r3, #1
  21bd94:	7423      	strb	r3, [r4, #16]
  21bd96:	4629      	mov	r1, r5
  21bd98:	4620      	mov	r0, r4
  21bd9a:	782a      	ldrb	r2, [r5, #0]
  21bd9c:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21bda0:	f000 b9e2 	b.w	21c168 <wiced_bt_a2dp_sink_ssm_execute>
  21bda4:	3084      	adds	r0, #132	; 0x84
  21bda6:	f7ff ff3f 	bl	21bc28 <wiced_bt_a2dp_sink_get_ccb_by_bd_addr>
  21bdaa:	4604      	mov	r4, r0
  21bdac:	2800      	cmp	r0, #0
  21bdae:	d0df      	beq.n	21bd70 <wiced_bt_a2dp_sink_hdl_event+0x44>
  21bdb0:	2300      	movs	r3, #0
  21bdb2:	e7ef      	b.n	21bd94 <wiced_bt_a2dp_sink_hdl_event+0x68>
  21bdb4:	300a      	adds	r0, #10
  21bdb6:	e7e1      	b.n	21bd7c <wiced_bt_a2dp_sink_hdl_event+0x50>
  21bdb8:	f890 008a 	ldrb.w	r0, [r0, #138]	; 0x8a
  21bdbc:	f7ff fee8 	bl	21bb90 <wiced_bt_a2dp_sink_get_ccb_by_handle>
  21bdc0:	2206      	movs	r2, #6
  21bdc2:	4604      	mov	r4, r0
  21bdc4:	1c41      	adds	r1, r0, #1
  21bdc6:	f105 0084 	add.w	r0, r5, #132	; 0x84
  21bdca:	f616 f877 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bdce:	e7ce      	b.n	21bd6e <wiced_bt_a2dp_sink_hdl_event+0x42>
  21bdd0:	0021df50 	.word	0x0021df50
  21bdd4:	0021fdb1 	.word	0x0021fdb1

0021bdd8 <wiced_bt_a2dp_sink_conn_cback>:
  21bdd8:	b5f0      	push	{r4, r5, r6, r7, lr}
  21bdda:	4614      	mov	r4, r2
  21bddc:	b0a5      	sub	sp, #148	; 0x94
  21bdde:	4606      	mov	r6, r0
  21bde0:	460d      	mov	r5, r1
  21bde2:	228c      	movs	r2, #140	; 0x8c
  21bde4:	2100      	movs	r1, #0
  21bde6:	a801      	add	r0, sp, #4
  21bde8:	461f      	mov	r7, r3
  21bdea:	f616 f86b 	bl	31ec4 <memcpy+0x7>
  21bdee:	f1a4 0310 	sub.w	r3, r4, #16
  21bdf2:	2b01      	cmp	r3, #1
  21bdf4:	d811      	bhi.n	21be1a <wiced_bt_a2dp_sink_conn_cback+0x42>
  21bdf6:	2302      	movs	r3, #2
  21bdf8:	f8ad 3004 	strh.w	r3, [sp, #4]
  21bdfc:	787b      	ldrb	r3, [r7, #1]
  21bdfe:	2206      	movs	r2, #6
  21be00:	4629      	mov	r1, r5
  21be02:	a822      	add	r0, sp, #136	; 0x88
  21be04:	f8ad 400a 	strh.w	r4, [sp, #10]
  21be08:	f8ad 3008 	strh.w	r3, [sp, #8]
  21be0c:	f88d 608e 	strb.w	r6, [sp, #142]	; 0x8e
  21be10:	f616 f854 	bl	31ebc <bsc_OpExtended+0x2d91>
  21be14:	a801      	add	r0, sp, #4
  21be16:	f7ff ff89 	bl	21bd2c <wiced_bt_a2dp_sink_hdl_event>
  21be1a:	b025      	add	sp, #148	; 0x94
  21be1c:	bdf0      	pop	{r4, r5, r6, r7, pc}
	...

0021be20 <wiced_bt_a2dp_sink_register>:
  21be20:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21be24:	4b4d      	ldr	r3, [pc, #308]	; (21bf5c <wiced_bt_a2dp_sink_register+0x13c>)
  21be26:	b0a1      	sub	sp, #132	; 0x84
  21be28:	2500      	movs	r5, #0
  21be2a:	9300      	str	r3, [sp, #0]
  21be2c:	230a      	movs	r3, #10
  21be2e:	4c4c      	ldr	r4, [pc, #304]	; (21bf60 <wiced_bt_a2dp_sink_register+0x140>)
  21be30:	4e4c      	ldr	r6, [pc, #304]	; (21bf64 <wiced_bt_a2dp_sink_register+0x144>)
  21be32:	4668      	mov	r0, sp
  21be34:	494c      	ldr	r1, [pc, #304]	; (21bf68 <wiced_bt_a2dp_sink_register+0x148>)
  21be36:	f8ad 3004 	strh.w	r3, [sp, #4]
  21be3a:	f884 502c 	strb.w	r5, [r4, #44]	; 0x2c
  21be3e:	63a6      	str	r6, [r4, #56]	; 0x38
  21be40:	f884 5044 	strb.w	r5, [r4, #68]	; 0x44
  21be44:	6526      	str	r6, [r4, #80]	; 0x50
  21be46:	f6bc fb65 	bl	d8514 <wiced_bt_sdp_find_service_uuid_in_rec+0x3>
  21be4a:	2278      	movs	r2, #120	; 0x78
  21be4c:	4629      	mov	r1, r5
  21be4e:	a802      	add	r0, sp, #8
  21be50:	f616 f838 	bl	31ec4 <memcpy+0x7>
  21be54:	2301      	movs	r3, #1
  21be56:	f88d 306c 	strb.w	r3, [sp, #108]	; 0x6c
  21be5a:	f88d 307c 	strb.w	r3, [sp, #124]	; 0x7c
  21be5e:	69a3      	ldr	r3, [r4, #24]
  21be60:	781a      	ldrb	r2, [r3, #0]
  21be62:	f884 50e0 	strb.w	r5, [r4, #224]	; 0xe0
  21be66:	f012 0101 	ands.w	r1, r2, #1
  21be6a:	bf14      	ite	ne
  21be6c:	462b      	movne	r3, r5
  21be6e:	2304      	moveq	r3, #4
  21be70:	f8ad 307e 	strh.w	r3, [sp, #126]	; 0x7e
  21be74:	f240 1301 	movw	r3, #257	; 0x101
  21be78:	f8a4 30f2 	strh.w	r3, [r4, #242]	; 0xf2
  21be7c:	f8a4 318e 	strh.w	r3, [r4, #398]	; 0x18e
  21be80:	f8a4 322a 	strh.w	r3, [r4, #554]	; 0x22a
  21be84:	f8a4 32c6 	strh.w	r3, [r4, #710]	; 0x2c6
  21be88:	2302      	movs	r3, #2
  21be8a:	f8ad 306e 	strh.w	r3, [sp, #110]	; 0x6e
  21be8e:	0793      	lsls	r3, r2, #30
  21be90:	bf48      	it	mi
  21be92:	f44f 7381 	movmi.w	r3, #258	; 0x102
  21be96:	f884 517c 	strb.w	r5, [r4, #380]	; 0x17c
  21be9a:	f884 5218 	strb.w	r5, [r4, #536]	; 0x218
  21be9e:	f884 52b4 	strb.w	r5, [r4, #692]	; 0x2b4
  21bea2:	f88d 507d 	strb.w	r5, [sp, #125]	; 0x7d
  21bea6:	961d      	str	r6, [sp, #116]	; 0x74
  21bea8:	bf48      	it	mi
  21beaa:	f8ad 306e 	strhmi.w	r3, [sp, #110]	; 0x6e
  21beae:	b141      	cbz	r1, 21bec2 <wiced_bt_a2dp_sink_register+0xa2>
  21beb0:	f8bd 306e 	ldrh.w	r3, [sp, #110]	; 0x6e
  21beb4:	f043 0310 	orr.w	r3, r3, #16
  21beb8:	f8ad 306e 	strh.w	r3, [sp, #110]	; 0x6e
  21bebc:	2301      	movs	r3, #1
  21bebe:	f88d 306d 	strb.w	r3, [sp, #109]	; 0x6d
  21bec2:	f04f 0802 	mov.w	r8, #2
  21bec6:	2500      	movs	r5, #0
  21bec8:	226c      	movs	r2, #108	; 0x6c
  21beca:	a902      	add	r1, sp, #8
  21becc:	4827      	ldr	r0, [pc, #156]	; (21bf6c <wiced_bt_a2dp_sink_register+0x14c>)
  21bece:	f615 fff5 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bed2:	226c      	movs	r2, #108	; 0x6c
  21bed4:	a902      	add	r1, sp, #8
  21bed6:	4826      	ldr	r0, [pc, #152]	; (21bf70 <wiced_bt_a2dp_sink_register+0x150>)
  21bed8:	f615 fff0 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bedc:	226c      	movs	r2, #108	; 0x6c
  21bede:	a902      	add	r1, sp, #8
  21bee0:	4824      	ldr	r0, [pc, #144]	; (21bf74 <wiced_bt_a2dp_sink_register+0x154>)
  21bee2:	f615 ffeb 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bee6:	226c      	movs	r2, #108	; 0x6c
  21bee8:	4823      	ldr	r0, [pc, #140]	; (21bf78 <wiced_bt_a2dp_sink_register+0x158>)
  21beea:	a902      	add	r1, sp, #8
  21beec:	f615 ffe6 	bl	31ebc <bsc_OpExtended+0x2d91>
  21bef0:	69a3      	ldr	r3, [r4, #24]
  21bef2:	f893 a004 	ldrb.w	sl, [r3, #4]
  21bef6:	2600      	movs	r6, #0
  21bef8:	f04f 0906 	mov.w	r9, #6
  21befc:	e021      	b.n	21bf42 <wiced_bt_a2dp_sink_register+0x122>
  21befe:	69a3      	ldr	r3, [r4, #24]
  21bf00:	f10d 026d 	add.w	r2, sp, #109	; 0x6d
  21bf04:	6898      	ldr	r0, [r3, #8]
  21bf06:	a902      	add	r1, sp, #8
  21bf08:	f10d 0312 	add.w	r3, sp, #18
  21bf0c:	eb00 1006 	add.w	r0, r0, r6, lsl #4
  21bf10:	f7ff fda0 	bl	21ba54 <wiced_bt_a2dp_sink_cfg_init>
  21bf14:	2801      	cmp	r0, #1
  21bf16:	ea4f 1b06 	mov.w	fp, r6, lsl #4
  21bf1a:	d111      	bne.n	21bf40 <wiced_bt_a2dp_sink_register+0x120>
  21bf1c:	2d03      	cmp	r5, #3
  21bf1e:	d80f      	bhi.n	21bf40 <wiced_bt_a2dp_sink_register+0x120>
  21bf20:	fb09 f705 	mul.w	r7, r9, r5
  21bf24:	1c78      	adds	r0, r7, #1
  21bf26:	a902      	add	r1, sp, #8
  21bf28:	4420      	add	r0, r4
  21bf2a:	f6bc fb44 	bl	d85b6 <wiced_avdt_stream_ctrl_cback+0x4f>
  21bf2e:	b938      	cbnz	r0, 21bf40 <wiced_bt_a2dp_sink_register+0x120>
  21bf30:	69a3      	ldr	r3, [r4, #24]
  21bf32:	4427      	add	r7, r4
  21bf34:	689b      	ldr	r3, [r3, #8]
  21bf36:	3501      	adds	r5, #1
  21bf38:	f813 300b 	ldrb.w	r3, [r3, fp]
  21bf3c:	b2ed      	uxtb	r5, r5
  21bf3e:	70bb      	strb	r3, [r7, #2]
  21bf40:	3601      	adds	r6, #1
  21bf42:	b2f3      	uxtb	r3, r6
  21bf44:	459a      	cmp	sl, r3
  21bf46:	d8da      	bhi.n	21befe <wiced_bt_a2dp_sink_register+0xde>
  21bf48:	f1b8 0f01 	cmp.w	r8, #1
  21bf4c:	d002      	beq.n	21bf54 <wiced_bt_a2dp_sink_register+0x134>
  21bf4e:	f04f 0801 	mov.w	r8, #1
  21bf52:	e7d0      	b.n	21bef6 <wiced_bt_a2dp_sink_register+0xd6>
  21bf54:	2000      	movs	r0, #0
  21bf56:	b021      	add	sp, #132	; 0x84
  21bf58:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21bf5c:	040402a0 	.word	0x040402a0
  21bf60:	0021fd8c 	.word	0x0021fd8c
  21bf64:	0021b151 	.word	0x0021b151
  21bf68:	0021bdd9 	.word	0x0021bdd9
  21bf6c:	0021fdf0 	.word	0x0021fdf0
  21bf70:	0021fe8c 	.word	0x0021fe8c
  21bf74:	0021ff28 	.word	0x0021ff28
  21bf78:	0021ffc4 	.word	0x0021ffc4

0021bf7c <get_context_index>:
  21bf7c:	b510      	push	{r4, lr}
  21bf7e:	2400      	movs	r4, #0
  21bf80:	4b09      	ldr	r3, [pc, #36]	; (21bfa8 <get_context_index+0x2c>)
  21bf82:	7eda      	ldrb	r2, [r3, #27]
  21bf84:	2a01      	cmp	r2, #1
  21bf86:	d102      	bne.n	21bf8e <get_context_index+0x12>
  21bf88:	881a      	ldrh	r2, [r3, #0]
  21bf8a:	4282      	cmp	r2, r0
  21bf8c:	d009      	beq.n	21bfa2 <get_context_index+0x26>
  21bf8e:	3401      	adds	r4, #1
  21bf90:	2c04      	cmp	r4, #4
  21bf92:	f103 0320 	add.w	r3, r3, #32
  21bf96:	d1f4      	bne.n	21bf82 <get_context_index+0x6>
  21bf98:	2100      	movs	r1, #0
  21bf9a:	4a04      	ldr	r2, [pc, #16]	; (21bfac <get_context_index+0x30>)
  21bf9c:	4608      	mov	r0, r1
  21bf9e:	f5f1 fb5d 	bl	d65c <wiced_va_printf+0x1f7>
  21bfa2:	4620      	mov	r0, r4
  21bfa4:	bd10      	pop	{r4, pc}
  21bfa6:	bf00      	nop
  21bfa8:	0021fbcc 	.word	0x0021fbcc
  21bfac:	0021f60d 	.word	0x0021f60d

0021bfb0 <wiced_bt_a2dp_sink_set_route_codec_config>:
  21bfb0:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
  21bfb4:	4d16      	ldr	r5, [pc, #88]	; (21c010 <wiced_bt_a2dp_sink_set_route_codec_config+0x60>)
  21bfb6:	4680      	mov	r8, r0
  21bfb8:	461f      	mov	r7, r3
  21bfba:	2400      	movs	r4, #0
  21bfbc:	462e      	mov	r6, r5
  21bfbe:	f89d 9020 	ldrb.w	r9, [sp, #32]
  21bfc2:	7eeb      	ldrb	r3, [r5, #27]
  21bfc4:	2b01      	cmp	r3, #1
  21bfc6:	d10e      	bne.n	21bfe6 <wiced_bt_a2dp_sink_set_route_codec_config+0x36>
  21bfc8:	882b      	ldrh	r3, [r5, #0]
  21bfca:	4293      	cmp	r3, r2
  21bfcc:	d00f      	beq.n	21bfee <wiced_bt_a2dp_sink_set_route_codec_config+0x3e>
  21bfce:	3401      	adds	r4, #1
  21bfd0:	2c04      	cmp	r4, #4
  21bfd2:	f105 0520 	add.w	r5, r5, #32
  21bfd6:	d1f4      	bne.n	21bfc2 <wiced_bt_a2dp_sink_set_route_codec_config+0x12>
  21bfd8:	e8bd 43f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
  21bfdc:	2100      	movs	r1, #0
  21bfde:	4a0d      	ldr	r2, [pc, #52]	; (21c014 <wiced_bt_a2dp_sink_set_route_codec_config+0x64>)
  21bfe0:	4608      	mov	r0, r1
  21bfe2:	f5f1 bb3b 	b.w	d65c <wiced_va_printf+0x1f7>
  21bfe6:	2001      	movs	r0, #1
  21bfe8:	eb06 1344 	add.w	r3, r6, r4, lsl #5
  21bfec:	76d8      	strb	r0, [r3, #27]
  21bfee:	0165      	lsls	r5, r4, #5
  21bff0:	5372      	strh	r2, [r6, r5]
  21bff2:	3508      	adds	r5, #8
  21bff4:	eb06 1444 	add.w	r4, r6, r4, lsl #5
  21bff8:	2210      	movs	r2, #16
  21bffa:	1970      	adds	r0, r6, r5
  21bffc:	f615 ff5e 	bl	31ebc <bsc_OpExtended+0x2d91>
  21c000:	f884 901c 	strb.w	r9, [r4, #28]
  21c004:	f8c4 8004 	str.w	r8, [r4, #4]
  21c008:	8327      	strh	r7, [r4, #24]
  21c00a:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
  21c00e:	bf00      	nop
  21c010:	0021fbcc 	.word	0x0021fbcc
  21c014:	0021f62a 	.word	0x0021f62a

0021c018 <wiced_bt_a2dp_sink_set_handle>:
  21c018:	b510      	push	{r4, lr}
  21c01a:	4604      	mov	r4, r0
  21c01c:	f7ff ffae 	bl	21bf7c <get_context_index>
  21c020:	2804      	cmp	r0, #4
  21c022:	bf1e      	ittt	ne
  21c024:	4b01      	ldrne	r3, [pc, #4]	; (21c02c <wiced_bt_a2dp_sink_set_handle+0x14>)
  21c026:	0140      	lslne	r0, r0, #5
  21c028:	521c      	strhne	r4, [r3, r0]
  21c02a:	bd10      	pop	{r4, pc}
  21c02c:	0021fbcc 	.word	0x0021fbcc

0021c030 <wiced_bt_a2dp_sink_streaming_start>:
  21c030:	b573      	push	{r0, r1, r4, r5, r6, lr}
  21c032:	f7ff ffa3 	bl	21bf7c <get_context_index>
  21c036:	2804      	cmp	r0, #4
  21c038:	d015      	beq.n	21c066 <wiced_bt_a2dp_sink_streaming_start+0x36>
  21c03a:	4d0d      	ldr	r5, [pc, #52]	; (21c070 <wiced_bt_a2dp_sink_streaming_start+0x40>)
  21c03c:	0146      	lsls	r6, r0, #5
  21c03e:	eb05 1440 	add.w	r4, r5, r0, lsl #5
  21c042:	7ea3      	ldrb	r3, [r4, #26]
  21c044:	b98b      	cbnz	r3, 21c06a <wiced_bt_a2dp_sink_streaming_start+0x3a>
  21c046:	f106 0308 	add.w	r3, r6, #8
  21c04a:	442b      	add	r3, r5
  21c04c:	9300      	str	r3, [sp, #0]
  21c04e:	8b23      	ldrh	r3, [r4, #24]
  21c050:	6862      	ldr	r2, [r4, #4]
  21c052:	7f21      	ldrb	r1, [r4, #28]
  21c054:	5ba8      	ldrh	r0, [r5, r6]
  21c056:	f7fb feb4 	bl	217dc2 <wiced_audio_sink_configure>
  21c05a:	fab0 f380 	clz	r3, r0
  21c05e:	095b      	lsrs	r3, r3, #5
  21c060:	76a3      	strb	r3, [r4, #26]
  21c062:	b002      	add	sp, #8
  21c064:	bd70      	pop	{r4, r5, r6, pc}
  21c066:	200c      	movs	r0, #12
  21c068:	e7fb      	b.n	21c062 <wiced_bt_a2dp_sink_streaming_start+0x32>
  21c06a:	2000      	movs	r0, #0
  21c06c:	e7f9      	b.n	21c062 <wiced_bt_a2dp_sink_streaming_start+0x32>
  21c06e:	bf00      	nop
  21c070:	0021fbcc 	.word	0x0021fbcc

0021c074 <wiced_bt_a2dp_sink_streaming_stop>:
  21c074:	b538      	push	{r3, r4, r5, lr}
  21c076:	4605      	mov	r5, r0
  21c078:	f7ff ff80 	bl	21bf7c <get_context_index>
  21c07c:	2803      	cmp	r0, #3
  21c07e:	dc0c      	bgt.n	21c09a <wiced_bt_a2dp_sink_streaming_stop+0x26>
  21c080:	4c07      	ldr	r4, [pc, #28]	; (21c0a0 <wiced_bt_a2dp_sink_streaming_stop+0x2c>)
  21c082:	eb04 1440 	add.w	r4, r4, r0, lsl #5
  21c086:	7ea3      	ldrb	r3, [r4, #26]
  21c088:	b13b      	cbz	r3, 21c09a <wiced_bt_a2dp_sink_streaming_stop+0x26>
  21c08a:	4628      	mov	r0, r5
  21c08c:	f7fc f8ac 	bl	2181e8 <wiced_audio_sink_reset>
  21c090:	1e03      	subs	r3, r0, #0
  21c092:	bf18      	it	ne
  21c094:	2301      	movne	r3, #1
  21c096:	76a3      	strb	r3, [r4, #26]
  21c098:	bd38      	pop	{r3, r4, r5, pc}
  21c09a:	200c      	movs	r0, #12
  21c09c:	e7fc      	b.n	21c098 <wiced_bt_a2dp_sink_streaming_stop+0x24>
  21c09e:	bf00      	nop
  21c0a0:	0021fbcc 	.word	0x0021fbcc

0021c0a4 <wiced_bt_a2dp_sink_streaming_configure_route>:
  21c0a4:	b570      	push	{r4, r5, r6, lr}
  21c0a6:	4605      	mov	r5, r0
  21c0a8:	f7ff ff68 	bl	21bf7c <get_context_index>
  21c0ac:	2804      	cmp	r0, #4
  21c0ae:	d016      	beq.n	21c0de <wiced_bt_a2dp_sink_streaming_configure_route+0x3a>
  21c0b0:	2600      	movs	r6, #0
  21c0b2:	4c0c      	ldr	r4, [pc, #48]	; (21c0e4 <wiced_bt_a2dp_sink_streaming_configure_route+0x40>)
  21c0b4:	7ee3      	ldrb	r3, [r4, #27]
  21c0b6:	2b01      	cmp	r3, #1
  21c0b8:	d107      	bne.n	21c0ca <wiced_bt_a2dp_sink_streaming_configure_route+0x26>
  21c0ba:	8820      	ldrh	r0, [r4, #0]
  21c0bc:	42a8      	cmp	r0, r5
  21c0be:	d004      	beq.n	21c0ca <wiced_bt_a2dp_sink_streaming_configure_route+0x26>
  21c0c0:	7ea3      	ldrb	r3, [r4, #26]
  21c0c2:	2b01      	cmp	r3, #1
  21c0c4:	d101      	bne.n	21c0ca <wiced_bt_a2dp_sink_streaming_configure_route+0x26>
  21c0c6:	f7ff ffd5 	bl	21c074 <wiced_bt_a2dp_sink_streaming_stop>
  21c0ca:	3601      	adds	r6, #1
  21c0cc:	2e04      	cmp	r6, #4
  21c0ce:	f104 0420 	add.w	r4, r4, #32
  21c0d2:	d1ef      	bne.n	21c0b4 <wiced_bt_a2dp_sink_streaming_configure_route+0x10>
  21c0d4:	4628      	mov	r0, r5
  21c0d6:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21c0da:	f7ff bfa9 	b.w	21c030 <wiced_bt_a2dp_sink_streaming_start>
  21c0de:	200c      	movs	r0, #12
  21c0e0:	bd70      	pop	{r4, r5, r6, pc}
  21c0e2:	bf00      	nop
  21c0e4:	0021fbcc 	.word	0x0021fbcc

0021c0e8 <wiced_bt_a2dp_sink_stream_close>:
  21c0e8:	b508      	push	{r3, lr}
  21c0ea:	f7ff ff47 	bl	21bf7c <get_context_index>
  21c0ee:	2803      	cmp	r0, #3
  21c0f0:	dc07      	bgt.n	21c102 <wiced_bt_a2dp_sink_stream_close+0x1a>
  21c0f2:	4b04      	ldr	r3, [pc, #16]	; (21c104 <wiced_bt_a2dp_sink_stream_close+0x1c>)
  21c0f4:	eb03 1040 	add.w	r0, r3, r0, lsl #5
  21c0f8:	7ec3      	ldrb	r3, [r0, #27]
  21c0fa:	2b01      	cmp	r3, #1
  21c0fc:	bf04      	itt	eq
  21c0fe:	2300      	moveq	r3, #0
  21c100:	76c3      	strbeq	r3, [r0, #27]
  21c102:	bd08      	pop	{r3, pc}
  21c104:	0021fbcc 	.word	0x0021fbcc

0021c108 <wiced_bt_a2dp_sink_update_route_config>:
  21c108:	b5f7      	push	{r0, r1, r2, r4, r5, r6, r7, lr}
  21c10a:	460c      	mov	r4, r1
  21c10c:	f7ff ff36 	bl	21bf7c <get_context_index>
  21c110:	2803      	cmp	r0, #3
  21c112:	dc19      	bgt.n	21c148 <wiced_bt_a2dp_sink_update_route_config+0x40>
  21c114:	4a10      	ldr	r2, [pc, #64]	; (21c158 <wiced_bt_a2dp_sink_update_route_config+0x50>)
  21c116:	0147      	lsls	r7, r0, #5
  21c118:	eb02 1340 	add.w	r3, r2, r0, lsl #5
  21c11c:	7ed8      	ldrb	r0, [r3, #27]
  21c11e:	2801      	cmp	r0, #1
  21c120:	d112      	bne.n	21c148 <wiced_bt_a2dp_sink_update_route_config+0x40>
  21c122:	7825      	ldrb	r5, [r4, #0]
  21c124:	2d01      	cmp	r5, #1
  21c126:	d011      	beq.n	21c14c <wiced_bt_a2dp_sink_update_route_config+0x44>
  21c128:	2d04      	cmp	r5, #4
  21c12a:	d00f      	beq.n	21c14c <wiced_bt_a2dp_sink_update_route_config+0x44>
  21c12c:	b965      	cbnz	r5, 21c148 <wiced_bt_a2dp_sink_update_route_config+0x40>
  21c12e:	7a19      	ldrb	r1, [r3, #8]
  21c130:	b161      	cbz	r1, 21c14c <wiced_bt_a2dp_sink_update_route_config+0x44>
  21c132:	4b0a      	ldr	r3, [pc, #40]	; (21c15c <wiced_bt_a2dp_sink_update_route_config+0x54>)
  21c134:	699b      	ldr	r3, [r3, #24]
  21c136:	8e1e      	ldrh	r6, [r3, #48]	; 0x30
  21c138:	b946      	cbnz	r6, 21c14c <wiced_bt_a2dp_sink_update_route_config+0x44>
  21c13a:	9100      	str	r1, [sp, #0]
  21c13c:	4630      	mov	r0, r6
  21c13e:	4631      	mov	r1, r6
  21c140:	4b07      	ldr	r3, [pc, #28]	; (21c160 <wiced_bt_a2dp_sink_update_route_config+0x58>)
  21c142:	4a08      	ldr	r2, [pc, #32]	; (21c164 <wiced_bt_a2dp_sink_update_route_config+0x5c>)
  21c144:	f5f1 fa8a 	bl	d65c <wiced_va_printf+0x1f7>
  21c148:	2000      	movs	r0, #0
  21c14a:	e003      	b.n	21c154 <wiced_bt_a2dp_sink_update_route_config+0x4c>
  21c14c:	19d3      	adds	r3, r2, r7
  21c14e:	605d      	str	r5, [r3, #4]
  21c150:	7a22      	ldrb	r2, [r4, #8]
  21c152:	771a      	strb	r2, [r3, #28]
  21c154:	b003      	add	sp, #12
  21c156:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21c158:	0021fbcc 	.word	0x0021fbcc
  21c15c:	0021fd8c 	.word	0x0021fd8c
  21c160:	0021df60 	.word	0x0021df60
  21c164:	0021f647 	.word	0x0021f647

0021c168 <wiced_bt_a2dp_sink_ssm_execute>:
  21c168:	b4f0      	push	{r4, r5, r6, r7}
  21c16a:	6944      	ldr	r4, [r0, #20]
  21c16c:	b934      	cbnz	r4, 21c17c <wiced_bt_a2dp_sink_ssm_execute+0x14>
  21c16e:	4621      	mov	r1, r4
  21c170:	4620      	mov	r0, r4
  21c172:	bcf0      	pop	{r4, r5, r6, r7}
  21c174:	4b12      	ldr	r3, [pc, #72]	; (21c1c0 <wiced_bt_a2dp_sink_ssm_execute+0x58>)
  21c176:	4a13      	ldr	r2, [pc, #76]	; (21c1c4 <wiced_bt_a2dp_sink_ssm_execute+0x5c>)
  21c178:	f5f1 ba70 	b.w	d65c <wiced_va_printf+0x1f7>
  21c17c:	79c4      	ldrb	r4, [r0, #7]
  21c17e:	4b12      	ldr	r3, [pc, #72]	; (21c1c8 <wiced_bt_a2dp_sink_ssm_execute+0x60>)
  21c180:	eb03 05c4 	add.w	r5, r3, r4, lsl #3
  21c184:	f853 7034 	ldr.w	r7, [r3, r4, lsl #3]
  21c188:	792c      	ldrb	r4, [r5, #4]
  21c18a:	2500      	movs	r5, #0
  21c18c:	3c01      	subs	r4, #1
  21c18e:	42ac      	cmp	r4, r5
  21c190:	da01      	bge.n	21c196 <wiced_bt_a2dp_sink_ssm_execute+0x2e>
  21c192:	bcf0      	pop	{r4, r5, r6, r7}
  21c194:	4770      	bx	lr
  21c196:	1b63      	subs	r3, r4, r5
  21c198:	eb05 0363 	add.w	r3, r5, r3, asr #1
  21c19c:	f817 c033 	ldrb.w	ip, [r7, r3, lsl #3]
  21c1a0:	eb07 06c3 	add.w	r6, r7, r3, lsl #3
  21c1a4:	4562      	cmp	r2, ip
  21c1a6:	d004      	beq.n	21c1b2 <wiced_bt_a2dp_sink_ssm_execute+0x4a>
  21c1a8:	bf8c      	ite	hi
  21c1aa:	1c5d      	addhi	r5, r3, #1
  21c1ac:	f103 34ff 	addls.w	r4, r3, #4294967295
  21c1b0:	e7ed      	b.n	21c18e <wiced_bt_a2dp_sink_ssm_execute+0x26>
  21c1b2:	6873      	ldr	r3, [r6, #4]
  21c1b4:	7872      	ldrb	r2, [r6, #1]
  21c1b6:	71c2      	strb	r2, [r0, #7]
  21c1b8:	2b00      	cmp	r3, #0
  21c1ba:	d0ea      	beq.n	21c192 <wiced_bt_a2dp_sink_ssm_execute+0x2a>
  21c1bc:	bcf0      	pop	{r4, r5, r6, r7}
  21c1be:	4718      	bx	r3
  21c1c0:	0021dfe8 	.word	0x0021dfe8
  21c1c4:	0021f712 	.word	0x0021f712
  21c1c8:	0021e008 	.word	0x0021e008

0021c1cc <wiced_bt_a2dp_sink_init_state_machine>:
  21c1cc:	b5f7      	push	{r0, r1, r2, r4, r5, r6, r7, lr}
  21c1ce:	2200      	movs	r2, #0
  21c1d0:	4910      	ldr	r1, [pc, #64]	; (21c214 <wiced_bt_a2dp_sink_init_state_machine+0x48>)
  21c1d2:	2301      	movs	r3, #1
  21c1d4:	6808      	ldr	r0, [r1, #0]
  21c1d6:	790c      	ldrb	r4, [r1, #4]
  21c1d8:	f1a0 0508 	sub.w	r5, r0, #8
  21c1dc:	429c      	cmp	r4, r3
  21c1de:	dc06      	bgt.n	21c1ee <wiced_bt_a2dp_sink_init_state_machine+0x22>
  21c1e0:	3201      	adds	r2, #1
  21c1e2:	2a06      	cmp	r2, #6
  21c1e4:	f101 0108 	add.w	r1, r1, #8
  21c1e8:	d1f3      	bne.n	21c1d2 <wiced_bt_a2dp_sink_init_state_machine+0x6>
  21c1ea:	2000      	movs	r0, #0
  21c1ec:	e00e      	b.n	21c20c <wiced_bt_a2dp_sink_init_state_machine+0x40>
  21c1ee:	f810 7033 	ldrb.w	r7, [r0, r3, lsl #3]
  21c1f2:	f815 6033 	ldrb.w	r6, [r5, r3, lsl #3]
  21c1f6:	42b7      	cmp	r7, r6
  21c1f8:	d20a      	bcs.n	21c210 <wiced_bt_a2dp_sink_init_state_machine+0x44>
  21c1fa:	2100      	movs	r1, #0
  21c1fc:	e9cd 2300 	strd	r2, r3, [sp]
  21c200:	4608      	mov	r0, r1
  21c202:	4b05      	ldr	r3, [pc, #20]	; (21c218 <wiced_bt_a2dp_sink_init_state_machine+0x4c>)
  21c204:	4a05      	ldr	r2, [pc, #20]	; (21c21c <wiced_bt_a2dp_sink_init_state_machine+0x50>)
  21c206:	f5f1 fa29 	bl	d65c <wiced_va_printf+0x1f7>
  21c20a:	2005      	movs	r0, #5
  21c20c:	b003      	add	sp, #12
  21c20e:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21c210:	3301      	adds	r3, #1
  21c212:	e7e3      	b.n	21c1dc <wiced_bt_a2dp_sink_init_state_machine+0x10>
  21c214:	0021e008 	.word	0x0021e008
  21c218:	0021e038 	.word	0x0021e038
  21c21c:	0021f736 	.word	0x0021f736

0021c220 <wiced_bt_hfp_hf_rfcomm_mgmt_cback>:
  21c220:	b530      	push	{r4, r5, lr}
  21c222:	4605      	mov	r5, r0
  21c224:	460c      	mov	r4, r1
  21c226:	2100      	movs	r1, #0
  21c228:	b087      	sub	sp, #28
  21c22a:	2208      	movs	r2, #8
  21c22c:	a804      	add	r0, sp, #16
  21c22e:	f8ad 1006 	strh.w	r1, [sp, #6]
  21c232:	f615 fe47 	bl	31ec4 <memcpy+0x7>
  21c236:	bb05      	cbnz	r5, 21c27a <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x5a>
  21c238:	4620      	mov	r0, r4
  21c23a:	f10d 0206 	add.w	r2, sp, #6
  21c23e:	a902      	add	r1, sp, #8
  21c240:	f6bd f805 	bl	d924e <wiced_bt_rfcomm_write_data+0x1>
  21c244:	b280      	uxth	r0, r0
  21c246:	b9b0      	cbnz	r0, 21c276 <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x56>
  21c248:	4620      	mov	r0, r4
  21c24a:	f001 f9f9 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21c24e:	b930      	cbnz	r0, 21c25e <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x3e>
  21c250:	f001 f9da 	bl	21d608 <wiced_bt_hfp_hf_scb_alloc>
  21c254:	b178      	cbz	r0, 21c276 <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x56>
  21c256:	2301      	movs	r3, #1
  21c258:	8044      	strh	r4, [r0, #2]
  21c25a:	f880 3022 	strb.w	r3, [r0, #34]	; 0x22
  21c25e:	a902      	add	r1, sp, #8
  21c260:	3004      	adds	r0, #4
  21c262:	f001 fbe0 	bl	21da26 <utl_bdcpy>
  21c266:	2307      	movs	r3, #7
  21c268:	a804      	add	r0, sp, #16
  21c26a:	f8ad 3010 	strh.w	r3, [sp, #16]
  21c26e:	f8ad 4016 	strh.w	r4, [sp, #22]
  21c272:	f001 fadd 	bl	21d830 <wiced_bt_hfp_hf_hdl_event>
  21c276:	b007      	add	sp, #28
  21c278:	bd30      	pop	{r4, r5, pc}
  21c27a:	2d10      	cmp	r5, #16
  21c27c:	d001      	beq.n	21c282 <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x62>
  21c27e:	2d13      	cmp	r5, #19
  21c280:	d1f9      	bne.n	21c276 <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x56>
  21c282:	2309      	movs	r3, #9
  21c284:	e7f0      	b.n	21c268 <wiced_bt_hfp_hf_rfcomm_mgmt_cback+0x48>
	...

0021c288 <wiced_bt_hfp_hf_sdp_complete_cback>:
  21c288:	b570      	push	{r4, r5, r6, lr}
  21c28a:	b088      	sub	sp, #32
  21c28c:	4605      	mov	r5, r0
  21c28e:	220e      	movs	r2, #14
  21c290:	2100      	movs	r1, #0
  21c292:	a804      	add	r0, sp, #16
  21c294:	f615 fe16 	bl	31ec4 <memcpy+0x7>
  21c298:	2208      	movs	r2, #8
  21c29a:	2100      	movs	r1, #0
  21c29c:	eb0d 0002 	add.w	r0, sp, r2
  21c2a0:	f615 fe10 	bl	31ec4 <memcpy+0x7>
  21c2a4:	b1cd      	cbz	r5, 21c2da <wiced_bt_hfp_hf_sdp_complete_cback+0x52>
  21c2a6:	230c      	movs	r3, #12
  21c2a8:	482f      	ldr	r0, [pc, #188]	; (21c368 <wiced_bt_hfp_hf_sdp_complete_cback+0xe0>)
  21c2aa:	f8ad 3010 	strh.w	r3, [sp, #16]
  21c2ae:	f001 f9e3 	bl	21d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>
  21c2b2:	4604      	mov	r4, r0
  21c2b4:	f8bd 3010 	ldrh.w	r3, [sp, #16]
  21c2b8:	a804      	add	r0, sp, #16
  21c2ba:	2b0c      	cmp	r3, #12
  21c2bc:	bf02      	ittt	eq
  21c2be:	2200      	moveq	r2, #0
  21c2c0:	4b2a      	ldreq	r3, [pc, #168]	; (21c36c <wiced_bt_hfp_hf_sdp_complete_cback+0xe4>)
  21c2c2:	f8a3 220e 	strheq.w	r2, [r3, #526]	; 0x20e
  21c2c6:	f001 fab3 	bl	21d830 <wiced_bt_hfp_hf_hdl_event>
  21c2ca:	b1a4      	cbz	r4, 21c2f6 <wiced_bt_hfp_hf_sdp_complete_cback+0x6e>
  21c2cc:	69e0      	ldr	r0, [r4, #28]
  21c2ce:	b190      	cbz	r0, 21c2f6 <wiced_bt_hfp_hf_sdp_complete_cback+0x6e>
  21c2d0:	f6b4 ff80 	bl	d11d4 <wiced_bt_free_buffer>
  21c2d4:	2300      	movs	r3, #0
  21c2d6:	61e3      	str	r3, [r4, #28]
  21c2d8:	e00d      	b.n	21c2f6 <wiced_bt_hfp_hf_sdp_complete_cback+0x6e>
  21c2da:	4823      	ldr	r0, [pc, #140]	; (21c368 <wiced_bt_hfp_hf_sdp_complete_cback+0xe0>)
  21c2dc:	f001 f9cc 	bl	21d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>
  21c2e0:	4e22      	ldr	r6, [pc, #136]	; (21c36c <wiced_bt_hfp_hf_sdp_complete_cback+0xe4>)
  21c2e2:	4604      	mov	r4, r0
  21c2e4:	b948      	cbnz	r0, 21c2fa <wiced_bt_hfp_hf_sdp_complete_cback+0x72>
  21c2e6:	230c      	movs	r3, #12
  21c2e8:	f8a6 020e 	strh.w	r0, [r6, #526]	; 0x20e
  21c2ec:	a804      	add	r0, sp, #16
  21c2ee:	f8ad 3010 	strh.w	r3, [sp, #16]
  21c2f2:	f001 fa9d 	bl	21d830 <wiced_bt_hfp_hf_hdl_event>
  21c2f6:	b008      	add	sp, #32
  21c2f8:	bd70      	pop	{r4, r5, r6, pc}
  21c2fa:	462a      	mov	r2, r5
  21c2fc:	f8b6 120e 	ldrh.w	r1, [r6, #526]	; 0x20e
  21c300:	69c0      	ldr	r0, [r0, #28]
  21c302:	f6bc f8fb 	bl	d84fc <wiced_bt_sdp_find_attribute_in_rec+0x3>
  21c306:	4605      	mov	r5, r0
  21c308:	b918      	cbnz	r0, 21c312 <wiced_bt_hfp_hf_sdp_complete_cback+0x8a>
  21c30a:	230c      	movs	r3, #12
  21c30c:	f8ad 3010 	strh.w	r3, [sp, #16]
  21c310:	e7d0      	b.n	21c2b4 <wiced_bt_hfp_hf_sdp_complete_cback+0x2c>
  21c312:	230b      	movs	r3, #11
  21c314:	aa02      	add	r2, sp, #8
  21c316:	2103      	movs	r1, #3
  21c318:	f8ad 3010 	strh.w	r3, [sp, #16]
  21c31c:	f6bc f8f2 	bl	d8504 <wiced_bt_sdp_find_service_uuid_in_db+0x3>
  21c320:	2801      	cmp	r0, #1
  21c322:	bf08      	it	eq
  21c324:	f8bd 300c 	ldrheq.w	r3, [sp, #12]
  21c328:	f8b6 220e 	ldrh.w	r2, [r6, #526]	; 0x20e
  21c32c:	bf08      	it	eq
  21c32e:	f88d 3018 	strbeq.w	r3, [sp, #24]
  21c332:	f241 131f 	movw	r3, #4383	; 0x111f
  21c336:	429a      	cmp	r2, r3
  21c338:	d1bc      	bne.n	21c2b4 <wiced_bt_hfp_hf_sdp_complete_cback+0x2c>
  21c33a:	f240 3111 	movw	r1, #785	; 0x311
  21c33e:	4628      	mov	r0, r5
  21c340:	f6bc f8da 	bl	d84f8 <wiced_bt_sdp_find_attribute_in_db+0x3>
  21c344:	b110      	cbz	r0, 21c34c <wiced_bt_hfp_hf_sdp_complete_cback+0xc4>
  21c346:	8903      	ldrh	r3, [r0, #8]
  21c348:	f8ad 301a 	strh.w	r3, [sp, #26]
  21c34c:	f241 111e 	movw	r1, #4382	; 0x111e
  21c350:	4628      	mov	r0, r5
  21c352:	f10d 0206 	add.w	r2, sp, #6
  21c356:	f6bc f8d9 	bl	d850c <wiced_bt_sdp_find_protocol_lists_elem_in_rec+0x3>
  21c35a:	2801      	cmp	r0, #1
  21c35c:	bf04      	itt	eq
  21c35e:	f8bd 3006 	ldrheq.w	r3, [sp, #6]
  21c362:	f8ad 301c 	strheq.w	r3, [sp, #28]
  21c366:	e7a5      	b.n	21c2b4 <wiced_bt_hfp_hf_sdp_complete_cback+0x2c>
  21c368:	00220260 	.word	0x00220260
  21c36c:	00220058 	.word	0x00220058

0021c370 <wiced_bt_hfp_hf_rfcomm_data_cback>:
  21c370:	b57f      	push	{r0, r1, r2, r3, r4, r5, r6, lr}
  21c372:	4606      	mov	r6, r0
  21c374:	460d      	mov	r5, r1
  21c376:	4614      	mov	r4, r2
  21c378:	f001 f962 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21c37c:	b150      	cbz	r0, 21c394 <wiced_bt_hfp_hf_rfcomm_data_cback+0x24>
  21c37e:	230a      	movs	r3, #10
  21c380:	4668      	mov	r0, sp
  21c382:	f8ad 3000 	strh.w	r3, [sp]
  21c386:	f8ad 6006 	strh.w	r6, [sp, #6]
  21c38a:	9502      	str	r5, [sp, #8]
  21c38c:	f8ad 400c 	strh.w	r4, [sp, #12]
  21c390:	f001 fa4e 	bl	21d830 <wiced_bt_hfp_hf_hdl_event>
  21c394:	2000      	movs	r0, #0
  21c396:	b004      	add	sp, #16
  21c398:	bd70      	pop	{r4, r5, r6, pc}
	...

0021c39c <wiced_bt_hfp_hf_do_sdp>:
  21c39c:	b510      	push	{r4, lr}
  21c39e:	4604      	mov	r4, r0
  21c3a0:	4a1a      	ldr	r2, [pc, #104]	; (21c40c <wiced_bt_hfp_hf_do_sdp+0x70>)
  21c3a2:	b08a      	sub	sp, #40	; 0x28
  21c3a4:	6810      	ldr	r0, [r2, #0]
  21c3a6:	6851      	ldr	r1, [r2, #4]
  21c3a8:	ab03      	add	r3, sp, #12
  21c3aa:	c303      	stmia	r3!, {r0, r1}
  21c3ac:	4818      	ldr	r0, [pc, #96]	; (21c410 <wiced_bt_hfp_hf_do_sdp+0x74>)
  21c3ae:	1d21      	adds	r1, r4, #4
  21c3b0:	f001 fb39 	bl	21da26 <utl_bdcpy>
  21c3b4:	2302      	movs	r3, #2
  21c3b6:	f8ad 3014 	strh.w	r3, [sp, #20]
  21c3ba:	4b16      	ldr	r3, [pc, #88]	; (21c414 <wiced_bt_hfp_hf_do_sdp+0x78>)
  21c3bc:	f8b3 320e 	ldrh.w	r3, [r3, #526]	; 0x20e
  21c3c0:	f8ad 3018 	strh.w	r3, [sp, #24]
  21c3c4:	69e3      	ldr	r3, [r4, #28]
  21c3c6:	b923      	cbnz	r3, 21c3d2 <wiced_bt_hfp_hf_do_sdp+0x36>
  21c3c8:	f44f 707a 	mov.w	r0, #1000	; 0x3e8
  21c3cc:	f6b4 ff00 	bl	d11d0 <wiced_bt_get_buffer>
  21c3d0:	61e0      	str	r0, [r4, #28]
  21c3d2:	69e0      	ldr	r0, [r4, #28]
  21c3d4:	b1c0      	cbz	r0, 21c408 <wiced_bt_hfp_hf_do_sdp+0x6c>
  21c3d6:	ab03      	add	r3, sp, #12
  21c3d8:	9301      	str	r3, [sp, #4]
  21c3da:	2304      	movs	r3, #4
  21c3dc:	2201      	movs	r2, #1
  21c3de:	9300      	str	r3, [sp, #0]
  21c3e0:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  21c3e4:	ab05      	add	r3, sp, #20
  21c3e6:	f6bc f873 	bl	d84d0 <wiced_bt_avrc_get_data_mtu+0x5f>
  21c3ea:	b920      	cbnz	r0, 21c3f6 <wiced_bt_hfp_hf_do_sdp+0x5a>
  21c3ec:	f64f 70f3 	movw	r0, #65523	; 0xfff3
  21c3f0:	f7ff ff4a 	bl	21c288 <wiced_bt_hfp_hf_sdp_complete_cback>
  21c3f4:	e006      	b.n	21c404 <wiced_bt_hfp_hf_do_sdp+0x68>
  21c3f6:	4a08      	ldr	r2, [pc, #32]	; (21c418 <wiced_bt_hfp_hf_do_sdp+0x7c>)
  21c3f8:	69e1      	ldr	r1, [r4, #28]
  21c3fa:	4805      	ldr	r0, [pc, #20]	; (21c410 <wiced_bt_hfp_hf_do_sdp+0x74>)
  21c3fc:	f6bc f878 	bl	d84f0 <wiced_bt_sdp_service_search_request+0x3>
  21c400:	2800      	cmp	r0, #0
  21c402:	d0f3      	beq.n	21c3ec <wiced_bt_hfp_hf_do_sdp+0x50>
  21c404:	b00a      	add	sp, #40	; 0x28
  21c406:	bd10      	pop	{r4, pc}
  21c408:	2006      	movs	r0, #6
  21c40a:	e7f1      	b.n	21c3f0 <wiced_bt_hfp_hf_do_sdp+0x54>
  21c40c:	0021e1e8 	.word	0x0021e1e8
  21c410:	00220260 	.word	0x00220260
  21c414:	00220058 	.word	0x00220058
  21c418:	0021c289 	.word	0x0021c289

0021c41c <wiced_bt_hfp_hf_sdp_failed>:
  21c41c:	b530      	push	{r4, r5, lr}
  21c41e:	4604      	mov	r4, r0
  21c420:	2500      	movs	r5, #0
  21c422:	b0c3      	sub	sp, #268	; 0x10c
  21c424:	1d01      	adds	r1, r0, #4
  21c426:	a802      	add	r0, sp, #8
  21c428:	f001 fafd 	bl	21da26 <utl_bdcpy>
  21c42c:	8863      	ldrh	r3, [r4, #2]
  21c42e:	4620      	mov	r0, r4
  21c430:	f8ad 3004 	strh.w	r3, [sp, #4]
  21c434:	f88d 500e 	strb.w	r5, [sp, #14]
  21c438:	f001 f95e 	bl	21d6f8 <wiced_bt_hfp_hf_scb_dealloc>
  21c43c:	4b03      	ldr	r3, [pc, #12]	; (21c44c <wiced_bt_hfp_hf_sdp_failed+0x30>)
  21c43e:	4628      	mov	r0, r5
  21c440:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21c444:	a901      	add	r1, sp, #4
  21c446:	4798      	blx	r3
  21c448:	b043      	add	sp, #268	; 0x10c
  21c44a:	bd30      	pop	{r4, r5, pc}
  21c44c:	00220058 	.word	0x00220058

0021c450 <wiced_bt_hfp_hf_rfc_connect_req>:
  21c450:	b5f0      	push	{r4, r5, r6, r7, lr}
  21c452:	2700      	movs	r7, #0
  21c454:	b08b      	sub	sp, #44	; 0x2c
  21c456:	4604      	mov	r4, r0
  21c458:	460d      	mov	r5, r1
  21c45a:	f8ad 7016 	strh.w	r7, [sp, #22]
  21c45e:	f001 f967 	bl	21d730 <wiced_bt_hfp_close_rfcomm_server_port>
  21c462:	1d26      	adds	r6, r4, #4
  21c464:	b320      	cbz	r0, 21c4b0 <wiced_bt_hfp_hf_rfc_connect_req+0x60>
  21c466:	4b1f      	ldr	r3, [pc, #124]	; (21c4e4 <wiced_bt_hfp_hf_rfc_connect_req+0x94>)
  21c468:	463a      	mov	r2, r7
  21c46a:	f8b3 c20e 	ldrh.w	ip, [r3, #526]	; 0x20e
  21c46e:	4b1e      	ldr	r3, [pc, #120]	; (21c4e8 <wiced_bt_hfp_hf_rfc_connect_req+0x98>)
  21c470:	f241 101f 	movw	r0, #4383	; 0x111f
  21c474:	9302      	str	r3, [sp, #8]
  21c476:	f10d 0316 	add.w	r3, sp, #22
  21c47a:	e9cd 6300 	strd	r6, r3, [sp]
  21c47e:	f241 171e 	movw	r7, #4382	; 0x111e
  21c482:	7a29      	ldrb	r1, [r5, #8]
  21c484:	f241 1508 	movw	r5, #4360	; 0x1108
  21c488:	23ff      	movs	r3, #255	; 0xff
  21c48a:	4584      	cmp	ip, r0
  21c48c:	bf0c      	ite	eq
  21c48e:	4638      	moveq	r0, r7
  21c490:	4628      	movne	r0, r5
  21c492:	f6bc ff51 	bl	d9338 <wiced_bt_rfcomm_control_callback+0x3d>
  21c496:	b958      	cbnz	r0, 21c4b0 <wiced_bt_hfp_hf_rfc_connect_req+0x60>
  21c498:	4914      	ldr	r1, [pc, #80]	; (21c4ec <wiced_bt_hfp_hf_rfc_connect_req+0x9c>)
  21c49a:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21c49e:	f6bc fe28 	bl	d90f2 <wiced_bt_rfcomm_set_event_callback+0x3>
  21c4a2:	4603      	mov	r3, r0
  21c4a4:	2101      	movs	r1, #1
  21c4a6:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21c4aa:	b163      	cbz	r3, 21c4c6 <wiced_bt_hfp_hf_rfc_connect_req+0x76>
  21c4ac:	f6bc fe1a 	bl	d90e4 <wiced_bt_app_install_patch+0x4f5>
  21c4b0:	2308      	movs	r3, #8
  21c4b2:	a808      	add	r0, sp, #32
  21c4b4:	4631      	mov	r1, r6
  21c4b6:	f8ad 3018 	strh.w	r3, [sp, #24]
  21c4ba:	f001 fab4 	bl	21da26 <utl_bdcpy>
  21c4be:	a806      	add	r0, sp, #24
  21c4c0:	f001 f9b6 	bl	21d830 <wiced_bt_hfp_hf_hdl_event>
  21c4c4:	e00b      	b.n	21c4de <wiced_bt_hfp_hf_rfc_connect_req+0x8e>
  21c4c6:	f6bc fe16 	bl	d90f6 <wiced_bt_rfcomm_set_data_callback+0x3>
  21c4ca:	b118      	cbz	r0, 21c4d4 <wiced_bt_hfp_hf_rfc_connect_req+0x84>
  21c4cc:	2101      	movs	r1, #1
  21c4ce:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21c4d2:	e7eb      	b.n	21c4ac <wiced_bt_hfp_hf_rfc_connect_req+0x5c>
  21c4d4:	f8bd 3016 	ldrh.w	r3, [sp, #22]
  21c4d8:	f884 0022 	strb.w	r0, [r4, #34]	; 0x22
  21c4dc:	8063      	strh	r3, [r4, #2]
  21c4de:	b00b      	add	sp, #44	; 0x2c
  21c4e0:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21c4e2:	bf00      	nop
  21c4e4:	00220058 	.word	0x00220058
  21c4e8:	0021c221 	.word	0x0021c221
  21c4ec:	0021c371 	.word	0x0021c371

0021c4f0 <wiced_bt_hfp_hf_rfc_connection_fail>:
  21c4f0:	b510      	push	{r4, lr}
  21c4f2:	b0c2      	sub	sp, #264	; 0x108
  21c4f4:	4604      	mov	r4, r0
  21c4f6:	1d01      	adds	r1, r0, #4
  21c4f8:	a802      	add	r0, sp, #8
  21c4fa:	f001 fa94 	bl	21da26 <utl_bdcpy>
  21c4fe:	2300      	movs	r3, #0
  21c500:	f88d 300e 	strb.w	r3, [sp, #14]
  21c504:	8863      	ldrh	r3, [r4, #2]
  21c506:	f8ad 3004 	strh.w	r3, [sp, #4]
  21c50a:	f894 3022 	ldrb.w	r3, [r4, #34]	; 0x22
  21c50e:	b90b      	cbnz	r3, 21c514 <wiced_bt_hfp_hf_rfc_connection_fail+0x24>
  21c510:	f001 f932 	bl	21d778 <wiced_bt_hfp_open_rfcomm_server_port>
  21c514:	4620      	mov	r0, r4
  21c516:	f001 f8ef 	bl	21d6f8 <wiced_bt_hfp_hf_scb_dealloc>
  21c51a:	4b04      	ldr	r3, [pc, #16]	; (21c52c <wiced_bt_hfp_hf_rfc_connection_fail+0x3c>)
  21c51c:	2000      	movs	r0, #0
  21c51e:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21c522:	a901      	add	r1, sp, #4
  21c524:	4798      	blx	r3
  21c526:	b042      	add	sp, #264	; 0x108
  21c528:	bd10      	pop	{r4, pc}
  21c52a:	bf00      	nop
  21c52c:	00220058 	.word	0x00220058

0021c530 <wiced_bt_hfp_hf_rfc_connected>:
  21c530:	b5f0      	push	{r4, r5, r6, r7, lr}
  21c532:	f890 3022 	ldrb.w	r3, [r0, #34]	; 0x22
  21c536:	4604      	mov	r4, r0
  21c538:	4e22      	ldr	r6, [pc, #136]	; (21c5c4 <wiced_bt_hfp_hf_rfc_connected+0x94>)
  21c53a:	b0c3      	sub	sp, #268	; 0x10c
  21c53c:	b393      	cbz	r3, 21c5a4 <wiced_bt_hfp_hf_rfc_connected+0x74>
  21c53e:	8840      	ldrh	r0, [r0, #2]
  21c540:	f001 f8be 	bl	21d6c0 <wiced_bt_hfp_hf_get_uuid_by_handle>
  21c544:	4605      	mov	r5, r0
  21c546:	23ff      	movs	r3, #255	; 0xff
  21c548:	f104 000c 	add.w	r0, r4, #12
  21c54c:	82e3      	strh	r3, [r4, #22]
  21c54e:	60e4      	str	r4, [r4, #12]
  21c550:	f000 f968 	bl	21c824 <wiced_bt_hfp_hf_at_init>
  21c554:	f8d6 31f8 	ldr.w	r3, [r6, #504]	; 0x1f8
  21c558:	1d21      	adds	r1, r4, #4
  21c55a:	66e3      	str	r3, [r4, #108]	; 0x6c
  21c55c:	f896 31f4 	ldrb.w	r3, [r6, #500]	; 0x1f4
  21c560:	a802      	add	r0, sp, #8
  21c562:	f884 3070 	strb.w	r3, [r4, #112]	; 0x70
  21c566:	f896 31f5 	ldrb.w	r3, [r6, #501]	; 0x1f5
  21c56a:	f241 1708 	movw	r7, #4360	; 0x1108
  21c56e:	f884 3071 	strb.w	r3, [r4, #113]	; 0x71
  21c572:	f001 fa58 	bl	21da26 <utl_bdcpy>
  21c576:	2301      	movs	r3, #1
  21c578:	f88d 300e 	strb.w	r3, [sp, #14]
  21c57c:	8863      	ldrh	r3, [r4, #2]
  21c57e:	1bea      	subs	r2, r5, r7
  21c580:	f8ad 3004 	strh.w	r3, [sp, #4]
  21c584:	4253      	negs	r3, r2
  21c586:	4153      	adcs	r3, r2
  21c588:	2000      	movs	r0, #0
  21c58a:	f88d 300f 	strb.w	r3, [sp, #15]
  21c58e:	a901      	add	r1, sp, #4
  21c590:	f8d6 3204 	ldr.w	r3, [r6, #516]	; 0x204
  21c594:	4798      	blx	r3
  21c596:	42bd      	cmp	r5, r7
  21c598:	4620      	mov	r0, r4
  21c59a:	d10f      	bne.n	21c5bc <wiced_bt_hfp_hf_rfc_connected+0x8c>
  21c59c:	f000 fb56 	bl	21cc4c <wiced_bt_hsp_at_slc>
  21c5a0:	b043      	add	sp, #268	; 0x10c
  21c5a2:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21c5a4:	f241 101f 	movw	r0, #4383	; 0x111f
  21c5a8:	f241 151e 	movw	r5, #4382	; 0x111e
  21c5ac:	f241 1308 	movw	r3, #4360	; 0x1108
  21c5b0:	f8b6 220e 	ldrh.w	r2, [r6, #526]	; 0x20e
  21c5b4:	4282      	cmp	r2, r0
  21c5b6:	bf18      	it	ne
  21c5b8:	461d      	movne	r5, r3
  21c5ba:	e7c4      	b.n	21c546 <wiced_bt_hfp_hf_rfc_connected+0x16>
  21c5bc:	f000 fb6a 	bl	21cc94 <wiced_bt_hfp_hf_at_slc>
  21c5c0:	e7ee      	b.n	21c5a0 <wiced_bt_hfp_hf_rfc_connected+0x70>
  21c5c2:	bf00      	nop
  21c5c4:	00220058 	.word	0x00220058

0021c5c8 <wiced_bt_hfp_hf_rfc_disconnect_req>:
  21c5c8:	2100      	movs	r1, #0
  21c5ca:	8840      	ldrh	r0, [r0, #2]
  21c5cc:	f6bc bd8a 	b.w	d90e4 <wiced_bt_app_install_patch+0x4f5>

0021c5d0 <wiced_bt_hfp_hf_rfc_disconnected>:
  21c5d0:	b570      	push	{r4, r5, r6, lr}
  21c5d2:	2500      	movs	r5, #0
  21c5d4:	4604      	mov	r4, r0
  21c5d6:	b0c2      	sub	sp, #264	; 0x108
  21c5d8:	84c5      	strh	r5, [r0, #38]	; 0x26
  21c5da:	f880 5021 	strb.w	r5, [r0, #33]	; 0x21
  21c5de:	f880 502b 	strb.w	r5, [r0, #43]	; 0x2b
  21c5e2:	f880 5072 	strb.w	r5, [r0, #114]	; 0x72
  21c5e6:	300c      	adds	r0, #12
  21c5e8:	f000 f922 	bl	21c830 <wiced_bt_hfp_hf_at_reinit>
  21c5ec:	1d26      	adds	r6, r4, #4
  21c5ee:	2278      	movs	r2, #120	; 0x78
  21c5f0:	4629      	mov	r1, r5
  21c5f2:	f104 0080 	add.w	r0, r4, #128	; 0x80
  21c5f6:	f615 fc65 	bl	31ec4 <memcpy+0x7>
  21c5fa:	4631      	mov	r1, r6
  21c5fc:	a802      	add	r0, sp, #8
  21c5fe:	f001 fa12 	bl	21da26 <utl_bdcpy>
  21c602:	8863      	ldrh	r3, [r4, #2]
  21c604:	2206      	movs	r2, #6
  21c606:	4629      	mov	r1, r5
  21c608:	4630      	mov	r0, r6
  21c60a:	f8ad 3004 	strh.w	r3, [sp, #4]
  21c60e:	f88d 500e 	strb.w	r5, [sp, #14]
  21c612:	f615 fc57 	bl	31ec4 <memcpy+0x7>
  21c616:	4620      	mov	r0, r4
  21c618:	f001 f86e 	bl	21d6f8 <wiced_bt_hfp_hf_scb_dealloc>
  21c61c:	f894 3022 	ldrb.w	r3, [r4, #34]	; 0x22
  21c620:	b90b      	cbnz	r3, 21c626 <wiced_bt_hfp_hf_rfc_disconnected+0x56>
  21c622:	f001 f8a9 	bl	21d778 <wiced_bt_hfp_open_rfcomm_server_port>
  21c626:	4b04      	ldr	r3, [pc, #16]	; (21c638 <wiced_bt_hfp_hf_rfc_disconnected+0x68>)
  21c628:	2000      	movs	r0, #0
  21c62a:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21c62e:	a901      	add	r1, sp, #4
  21c630:	4798      	blx	r3
  21c632:	b042      	add	sp, #264	; 0x108
  21c634:	bd70      	pop	{r4, r5, r6, pc}
  21c636:	bf00      	nop
  21c638:	00220058 	.word	0x00220058

0021c63c <wiced_bt_hfp_hf_rfc_data>:
  21c63c:	898a      	ldrh	r2, [r1, #12]
  21c63e:	300c      	adds	r0, #12
  21c640:	6889      	ldr	r1, [r1, #8]
  21c642:	f000 b905 	b.w	21c850 <wiced_bt_hfp_hf_at_parse>

0021c646 <wiced_bt_hfp_hf_cmd_timeout>:
  21c646:	f000 ba17 	b.w	21ca78 <wiced_bt_hfp_hf_at_cmd_queue_timeout>
	...

0021c64c <wiced_bt_hfp_hf_slc_open>:
  21c64c:	b510      	push	{r4, lr}
  21c64e:	f890 3021 	ldrb.w	r3, [r0, #33]	; 0x21
  21c652:	4604      	mov	r4, r0
  21c654:	2b01      	cmp	r3, #1
  21c656:	b0c2      	sub	sp, #264	; 0x108
  21c658:	d012      	beq.n	21c680 <wiced_bt_hfp_hf_slc_open+0x34>
  21c65a:	2301      	movs	r3, #1
  21c65c:	1d01      	adds	r1, r0, #4
  21c65e:	f880 3021 	strb.w	r3, [r0, #33]	; 0x21
  21c662:	a802      	add	r0, sp, #8
  21c664:	f001 f9df 	bl	21da26 <utl_bdcpy>
  21c668:	2302      	movs	r3, #2
  21c66a:	f88d 300e 	strb.w	r3, [sp, #14]
  21c66e:	8863      	ldrh	r3, [r4, #2]
  21c670:	2000      	movs	r0, #0
  21c672:	f8ad 3004 	strh.w	r3, [sp, #4]
  21c676:	4b03      	ldr	r3, [pc, #12]	; (21c684 <wiced_bt_hfp_hf_slc_open+0x38>)
  21c678:	a901      	add	r1, sp, #4
  21c67a:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21c67e:	4798      	blx	r3
  21c680:	b042      	add	sp, #264	; 0x108
  21c682:	bd10      	pop	{r4, pc}
  21c684:	00220058 	.word	0x00220058

0021c688 <wiced_bt_hfp_hf_do_call_action>:
  21c688:	b5f0      	push	{r4, r5, r6, r7, lr}
  21c68a:	7a8b      	ldrb	r3, [r1, #10]
  21c68c:	4606      	mov	r6, r0
  21c68e:	460c      	mov	r4, r1
  21c690:	b089      	sub	sp, #36	; 0x24
  21c692:	2b07      	cmp	r3, #7
  21c694:	d82c      	bhi.n	21c6f0 <wiced_bt_hfp_hf_do_call_action+0x68>
  21c696:	e8df f003 	tbb	[pc, r3]
  21c69a:	3304      	.short	0x3304
  21c69c:	3f3f3f39 	.word	0x3f3f3f39
  21c6a0:	3f3f      	.short	0x3f3f
  21c6a2:	340b      	adds	r4, #11
  21c6a4:	4620      	mov	r0, r4
  21c6a6:	f65d fc2d 	bl	79f04 <_printf_int_dec+0x73>
  21c6aa:	4605      	mov	r5, r0
  21c6ac:	b310      	cbz	r0, 21c6f4 <wiced_bt_hfp_hf_do_call_action+0x6c>
  21c6ae:	2813      	cmp	r0, #19
  21c6b0:	dc20      	bgt.n	21c6f4 <wiced_bt_hfp_hf_do_call_action+0x6c>
  21c6b2:	af02      	add	r7, sp, #8
  21c6b4:	2215      	movs	r2, #21
  21c6b6:	2100      	movs	r1, #0
  21c6b8:	4638      	mov	r0, r7
  21c6ba:	f615 fc03 	bl	31ec4 <memcpy+0x7>
  21c6be:	4621      	mov	r1, r4
  21c6c0:	4638      	mov	r0, r7
  21c6c2:	f001 f94e 	bl	21d962 <utl_strcpy>
  21c6c6:	ab08      	add	r3, sp, #32
  21c6c8:	441d      	add	r5, r3
  21c6ca:	f815 3c19 	ldrb.w	r3, [r5, #-25]
  21c6ce:	210a      	movs	r1, #10
  21c6d0:	2b3b      	cmp	r3, #59	; 0x3b
  21c6d2:	bf1f      	itttt	ne
  21c6d4:	233b      	movne	r3, #59	; 0x3b
  21c6d6:	f805 3c18 	strbne.w	r3, [r5, #-24]
  21c6da:	2300      	movne	r3, #0
  21c6dc:	f805 3c17 	strbne.w	r3, [r5, #-23]
  21c6e0:	2300      	movs	r3, #0
  21c6e2:	e9cd 7300 	strd	r7, r3, [sp]
  21c6e6:	2301      	movs	r3, #1
  21c6e8:	461a      	mov	r2, r3
  21c6ea:	4630      	mov	r0, r6
  21c6ec:	f000 fa54 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21c6f0:	b009      	add	sp, #36	; 0x24
  21c6f2:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21c6f4:	2300      	movs	r3, #0
  21c6f6:	2201      	movs	r2, #1
  21c6f8:	2105      	movs	r1, #5
  21c6fa:	e9cd 3300 	strd	r3, r3, [sp]
  21c6fe:	e7f4      	b.n	21c6ea <wiced_bt_hfp_hf_do_call_action+0x62>
  21c700:	2300      	movs	r3, #0
  21c702:	2201      	movs	r2, #1
  21c704:	2102      	movs	r1, #2
  21c706:	e9cd 3300 	strd	r3, r3, [sp]
  21c70a:	e7ee      	b.n	21c6ea <wiced_bt_hfp_hf_do_call_action+0x62>
  21c70c:	2300      	movs	r3, #0
  21c70e:	2201      	movs	r2, #1
  21c710:	2107      	movs	r1, #7
  21c712:	e9cd 3300 	strd	r3, r3, [sp]
  21c716:	e7e8      	b.n	21c6ea <wiced_bt_hfp_hf_do_call_action+0x62>
  21c718:	ad02      	add	r5, sp, #8
  21c71a:	2215      	movs	r2, #21
  21c71c:	2100      	movs	r1, #0
  21c71e:	4628      	mov	r0, r5
  21c720:	f615 fbd0 	bl	31ec4 <memcpy+0x7>
  21c724:	7aa3      	ldrb	r3, [r4, #10]
  21c726:	f104 010b 	add.w	r1, r4, #11
  21c72a:	332d      	adds	r3, #45	; 0x2d
  21c72c:	f10d 0009 	add.w	r0, sp, #9
  21c730:	f88d 3008 	strb.w	r3, [sp, #8]
  21c734:	f001 f915 	bl	21d962 <utl_strcpy>
  21c738:	2300      	movs	r3, #0
  21c73a:	2202      	movs	r2, #2
  21c73c:	e9cd 5300 	strd	r5, r3, [sp]
  21c740:	2106      	movs	r1, #6
  21c742:	2301      	movs	r3, #1
  21c744:	e7d1      	b.n	21c6ea <wiced_bt_hfp_hf_do_call_action+0x62>

0021c746 <wiced_bt_hfp_hf_do_notify_volume>:
  21c746:	b537      	push	{r0, r1, r2, r4, r5, lr}
  21c748:	460c      	mov	r4, r1
  21c74a:	7a89      	ldrb	r1, [r1, #10]
  21c74c:	4605      	mov	r5, r0
  21c74e:	b959      	cbnz	r1, 21c768 <wiced_bt_hfp_hf_do_notify_volume+0x22>
  21c750:	7ae3      	ldrb	r3, [r4, #11]
  21c752:	e9cd 1300 	strd	r1, r3, [sp]
  21c756:	2302      	movs	r3, #2
  21c758:	461a      	mov	r2, r3
  21c75a:	f000 fa1d 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21c75e:	7ae3      	ldrb	r3, [r4, #11]
  21c760:	f885 3071 	strb.w	r3, [r5, #113]	; 0x71
  21c764:	b003      	add	sp, #12
  21c766:	bd30      	pop	{r4, r5, pc}
  21c768:	2901      	cmp	r1, #1
  21c76a:	d1fb      	bne.n	21c764 <wiced_bt_hfp_hf_do_notify_volume+0x1e>
  21c76c:	7ae3      	ldrb	r3, [r4, #11]
  21c76e:	9301      	str	r3, [sp, #4]
  21c770:	2300      	movs	r3, #0
  21c772:	9300      	str	r3, [sp, #0]
  21c774:	2302      	movs	r3, #2
  21c776:	461a      	mov	r2, r3
  21c778:	f000 fa0e 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21c77c:	7ae3      	ldrb	r3, [r4, #11]
  21c77e:	f885 3070 	strb.w	r3, [r5, #112]	; 0x70
  21c782:	e7ef      	b.n	21c764 <wiced_bt_hfp_hf_do_notify_volume+0x1e>

0021c784 <wiced_bt_hfp_hf_do_send_at_cmd>:
  21c784:	b570      	push	{r4, r5, r6, lr}
  21c786:	f101 040a 	add.w	r4, r1, #10
  21c78a:	4605      	mov	r5, r0
  21c78c:	4620      	mov	r0, r4
  21c78e:	f65d fbb9 	bl	79f04 <_printf_int_dec+0x73>
  21c792:	4622      	mov	r2, r4
  21c794:	b283      	uxth	r3, r0
  21c796:	2100      	movs	r1, #0
  21c798:	4628      	mov	r0, r5
  21c79a:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
  21c79e:	f000 b922 	b.w	21c9e6 <wiced_bt_hfp_hf_at_cmd_queue_enqueue>
	...

0021c7a4 <wiced_bt_hfp_hf_init>:
  21c7a4:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
  21c7a6:	4e1a      	ldr	r6, [pc, #104]	; (21c810 <wiced_bt_hfp_hf_init+0x6c>)
  21c7a8:	4605      	mov	r5, r0
  21c7aa:	7833      	ldrb	r3, [r6, #0]
  21c7ac:	460f      	mov	r7, r1
  21c7ae:	2b01      	cmp	r3, #1
  21c7b0:	d02b      	beq.n	21c80a <wiced_bt_hfp_hf_init+0x66>
  21c7b2:	b908      	cbnz	r0, 21c7b8 <wiced_bt_hfp_hf_init+0x14>
  21c7b4:	2005      	movs	r0, #5
  21c7b6:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
  21c7b8:	7a03      	ldrb	r3, [r0, #8]
  21c7ba:	2b02      	cmp	r3, #2
  21c7bc:	d8fa      	bhi.n	21c7b4 <wiced_bt_hfp_hf_init+0x10>
  21c7be:	f001 f897 	bl	21d8f0 <wiced_bt_hfp_hf_init_state_machine>
  21c7c2:	4604      	mov	r4, r0
  21c7c4:	2800      	cmp	r0, #0
  21c7c6:	d1f5      	bne.n	21c7b4 <wiced_bt_hfp_hf_init+0x10>
  21c7c8:	4601      	mov	r1, r0
  21c7ca:	f44f 7204 	mov.w	r2, #528	; 0x210
  21c7ce:	4630      	mov	r0, r6
  21c7d0:	f615 fb78 	bl	31ec4 <memcpy+0x7>
  21c7d4:	2210      	movs	r2, #16
  21c7d6:	4629      	mov	r1, r5
  21c7d8:	f506 70fa 	add.w	r0, r6, #500	; 0x1f4
  21c7dc:	f615 fb6e 	bl	31ebc <bsc_OpExtended+0x2d91>
  21c7e0:	f8c6 7204 	str.w	r7, [r6, #516]	; 0x204
  21c7e4:	7a2a      	ldrb	r2, [r5, #8]
  21c7e6:	b2e3      	uxtb	r3, r4
  21c7e8:	429a      	cmp	r2, r3
  21c7ea:	d803      	bhi.n	21c7f4 <wiced_bt_hfp_hf_init+0x50>
  21c7ec:	2301      	movs	r3, #1
  21c7ee:	2000      	movs	r0, #0
  21c7f0:	7033      	strb	r3, [r6, #0]
  21c7f2:	e7e0      	b.n	21c7b6 <wiced_bt_hfp_hf_init+0x12>
  21c7f4:	eb05 0243 	add.w	r2, r5, r3, lsl #1
  21c7f8:	442b      	add	r3, r5
  21c7fa:	8991      	ldrh	r1, [r2, #12]
  21c7fc:	7a58      	ldrb	r0, [r3, #9]
  21c7fe:	f000 ffd1 	bl	21d7a4 <wiced_bt_hfp_hf_register>
  21c802:	3401      	adds	r4, #1
  21c804:	2800      	cmp	r0, #0
  21c806:	d0ed      	beq.n	21c7e4 <wiced_bt_hfp_hf_init+0x40>
  21c808:	e7d5      	b.n	21c7b6 <wiced_bt_hfp_hf_init+0x12>
  21c80a:	f44f 707a 	mov.w	r0, #1000	; 0x3e8
  21c80e:	e7d2      	b.n	21c7b6 <wiced_bt_hfp_hf_init+0x12>
  21c810:	00220058 	.word	0x00220058

0021c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>:
  21c814:	2201      	movs	r2, #1
  21c816:	2100      	movs	r1, #0
  21c818:	4801      	ldr	r0, [pc, #4]	; (21c820 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset+0xc>)
  21c81a:	f615 bb53 	b.w	31ec4 <memcpy+0x7>
  21c81e:	bf00      	nop
  21c820:	0021fc4c 	.word	0x0021fc4c

0021c824 <wiced_bt_hfp_hf_at_init>:
  21c824:	2300      	movs	r3, #0
  21c826:	6043      	str	r3, [r0, #4]
  21c828:	8103      	strh	r3, [r0, #8]
  21c82a:	7303      	strb	r3, [r0, #12]
  21c82c:	f7ff bff2 	b.w	21c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>

0021c830 <wiced_bt_hfp_hf_at_reinit>:
  21c830:	b510      	push	{r4, lr}
  21c832:	4604      	mov	r4, r0
  21c834:	6840      	ldr	r0, [r0, #4]
  21c836:	b118      	cbz	r0, 21c840 <wiced_bt_hfp_hf_at_reinit+0x10>
  21c838:	f6b4 fccc 	bl	d11d4 <wiced_bt_free_buffer>
  21c83c:	2300      	movs	r3, #0
  21c83e:	6063      	str	r3, [r4, #4]
  21c840:	2300      	movs	r3, #0
  21c842:	8123      	strh	r3, [r4, #8]
  21c844:	7323      	strb	r3, [r4, #12]
  21c846:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21c84a:	f7ff bfe3 	b.w	21c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>
	...

0021c850 <wiced_bt_hfp_hf_at_parse>:
  21c850:	e92d 4ff7 	stmdb	sp!, {r0, r1, r2, r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21c854:	4604      	mov	r4, r0
  21c856:	460f      	mov	r7, r1
  21c858:	4616      	mov	r6, r2
  21c85a:	2500      	movs	r5, #0
  21c85c:	f8df 80e4 	ldr.w	r8, [pc, #228]	; 21c944 <wiced_bt_hfp_hf_at_parse+0xf4>
  21c860:	f102 39ff 	add.w	r9, r2, #4294967295
  21c864:	45a9      	cmp	r9, r5
  21c866:	dc01      	bgt.n	21c86c <wiced_bt_hfp_hf_at_parse+0x1c>
  21c868:	2001      	movs	r0, #1
  21c86a:	e021      	b.n	21c8b0 <wiced_bt_hfp_hf_at_parse+0x60>
  21c86c:	7b23      	ldrb	r3, [r4, #12]
  21c86e:	2b01      	cmp	r3, #1
  21c870:	d011      	beq.n	21c896 <wiced_bt_hfp_hf_at_parse+0x46>
  21c872:	2b02      	cmp	r3, #2
  21c874:	d05e      	beq.n	21c934 <wiced_bt_hfp_hf_at_parse+0xe4>
  21c876:	2b00      	cmp	r3, #0
  21c878:	d1f4      	bne.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c87a:	42ae      	cmp	r6, r5
  21c87c:	dd02      	ble.n	21c884 <wiced_bt_hfp_hf_at_parse+0x34>
  21c87e:	5d7b      	ldrb	r3, [r7, r5]
  21c880:	2b1f      	cmp	r3, #31
  21c882:	d918      	bls.n	21c8b6 <wiced_bt_hfp_hf_at_parse+0x66>
  21c884:	45a9      	cmp	r9, r5
  21c886:	ddef      	ble.n	21c868 <wiced_bt_hfp_hf_at_parse+0x18>
  21c888:	2301      	movs	r3, #1
  21c88a:	8960      	ldrh	r0, [r4, #10]
  21c88c:	7323      	strb	r3, [r4, #12]
  21c88e:	f6b4 fc9f 	bl	d11d0 <wiced_bt_get_buffer>
  21c892:	6060      	str	r0, [r4, #4]
  21c894:	e7e6      	b.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c896:	1979      	adds	r1, r7, r5
  21c898:	42ae      	cmp	r6, r5
  21c89a:	8923      	ldrh	r3, [r4, #8]
  21c89c:	8962      	ldrh	r2, [r4, #10]
  21c89e:	dd01      	ble.n	21c8a4 <wiced_bt_hfp_hf_at_parse+0x54>
  21c8a0:	429a      	cmp	r2, r3
  21c8a2:	d80a      	bhi.n	21c8ba <wiced_bt_hfp_hf_at_parse+0x6a>
  21c8a4:	429a      	cmp	r2, r3
  21c8a6:	d1dd      	bne.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c8a8:	4620      	mov	r0, r4
  21c8aa:	f7ff ffc1 	bl	21c830 <wiced_bt_hfp_hf_at_reinit>
  21c8ae:	2000      	movs	r0, #0
  21c8b0:	b003      	add	sp, #12
  21c8b2:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21c8b6:	3501      	adds	r5, #1
  21c8b8:	e7df      	b.n	21c87a <wiced_bt_hfp_hf_at_parse+0x2a>
  21c8ba:	f811 0b01 	ldrb.w	r0, [r1], #1
  21c8be:	6862      	ldr	r2, [r4, #4]
  21c8c0:	280d      	cmp	r0, #13
  21c8c2:	54d0      	strb	r0, [r2, r3]
  21c8c4:	8923      	ldrh	r3, [r4, #8]
  21c8c6:	f105 0501 	add.w	r5, r5, #1
  21c8ca:	d130      	bne.n	21c92e <wiced_bt_hfp_hf_at_parse+0xde>
  21c8cc:	f04f 0100 	mov.w	r1, #0
  21c8d0:	6862      	ldr	r2, [r4, #4]
  21c8d2:	54d1      	strb	r1, [r2, r3]
  21c8d4:	2200      	movs	r2, #0
  21c8d6:	fa1f fb82 	uxth.w	fp, r2
  21c8da:	f858 003b 	ldr.w	r0, [r8, fp, lsl #3]
  21c8de:	46da      	mov	sl, fp
  21c8e0:	7803      	ldrb	r3, [r0, #0]
  21c8e2:	6861      	ldr	r1, [r4, #4]
  21c8e4:	b1d3      	cbz	r3, 21c91c <wiced_bt_hfp_hf_at_parse+0xcc>
  21c8e6:	9201      	str	r2, [sp, #4]
  21c8e8:	f001 f887 	bl	21d9fa <utl_strucmp>
  21c8ec:	9a01      	ldr	r2, [sp, #4]
  21c8ee:	3201      	adds	r2, #1
  21c8f0:	2800      	cmp	r0, #0
  21c8f2:	d1f0      	bne.n	21c8d6 <wiced_bt_hfp_hf_at_parse+0x86>
  21c8f4:	6862      	ldr	r2, [r4, #4]
  21c8f6:	f858 003a 	ldr.w	r0, [r8, sl, lsl #3]
  21c8fa:	9201      	str	r2, [sp, #4]
  21c8fc:	f65d fb02 	bl	79f04 <_printf_int_dec+0x73>
  21c900:	9a01      	ldr	r2, [sp, #4]
  21c902:	4659      	mov	r1, fp
  21c904:	4402      	add	r2, r0
  21c906:	6820      	ldr	r0, [r4, #0]
  21c908:	f000 fabc 	bl	21ce84 <wiced_bt_hfp_hf_at_cback>
  21c90c:	4620      	mov	r0, r4
  21c90e:	f7ff ff8f 	bl	21c830 <wiced_bt_hfp_hf_at_reinit>
  21c912:	f858 303a 	ldr.w	r3, [r8, sl, lsl #3]
  21c916:	781b      	ldrb	r3, [r3, #0]
  21c918:	2b00      	cmp	r3, #0
  21c91a:	d1a3      	bne.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c91c:	2101      	movs	r1, #1
  21c91e:	6862      	ldr	r2, [r4, #4]
  21c920:	6820      	ldr	r0, [r4, #0]
  21c922:	f000 fe69 	bl	21d5f8 <wiced_bt_hfp_hf_at_err_cback>
  21c926:	4620      	mov	r0, r4
  21c928:	f7ff ff82 	bl	21c830 <wiced_bt_hfp_hf_at_reinit>
  21c92c:	e79a      	b.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c92e:	3301      	adds	r3, #1
  21c930:	8123      	strh	r3, [r4, #8]
  21c932:	e7b1      	b.n	21c898 <wiced_bt_hfp_hf_at_parse+0x48>
  21c934:	42ae      	cmp	r6, r5
  21c936:	dd95      	ble.n	21c864 <wiced_bt_hfp_hf_at_parse+0x14>
  21c938:	5d7b      	ldrb	r3, [r7, r5]
  21c93a:	3501      	adds	r5, #1
  21c93c:	2b0a      	cmp	r3, #10
  21c93e:	d1f9      	bne.n	21c934 <wiced_bt_hfp_hf_at_parse+0xe4>
  21c940:	e7f1      	b.n	21c926 <wiced_bt_hfp_hf_at_parse+0xd6>
  21c942:	bf00      	nop
  21c944:	0021f9a8 	.word	0x0021f9a8

0021c948 <wiced_bt_hfp_hf_at_cmd_queue_init>:
  21c948:	b510      	push	{r4, lr}
  21c94a:	4604      	mov	r4, r0
  21c94c:	302c      	adds	r0, #44	; 0x2c
  21c94e:	f652 fe12 	bl	6f576 <wiced_create_pool+0x95>
  21c952:	2300      	movs	r3, #0
  21c954:	4622      	mov	r2, r4
  21c956:	f884 3068 	strb.w	r3, [r4, #104]	; 0x68
  21c95a:	f104 0038 	add.w	r0, r4, #56	; 0x38
  21c95e:	2302      	movs	r3, #2
  21c960:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21c964:	4901      	ldr	r1, [pc, #4]	; (21c96c <wiced_bt_hfp_hf_at_cmd_queue_init+0x24>)
  21c966:	f6c1 be69 	b.w	de63c <wiced_hal_rand_gen_num_array+0x7f>
  21c96a:	bf00      	nop
  21c96c:	0021cad1 	.word	0x0021cad1

0021c970 <wiced_bt_hfp_hf_at_cmd_queue_free>:
  21c970:	b538      	push	{r3, r4, r5, lr}
  21c972:	4604      	mov	r4, r0
  21c974:	3038      	adds	r0, #56	; 0x38
  21c976:	f6c1 fe7b 	bl	de670 <wiced_init_timer+0x33>
  21c97a:	f104 052c 	add.w	r5, r4, #44	; 0x2c
  21c97e:	4628      	mov	r0, r5
  21c980:	f652 ff21 	bl	6f7c6 <GKI_enqueue_head+0x57>
  21c984:	b910      	cbnz	r0, 21c98c <wiced_bt_hfp_hf_at_cmd_queue_free+0x1c>
  21c986:	f884 0068 	strb.w	r0, [r4, #104]	; 0x68
  21c98a:	bd38      	pop	{r3, r4, r5, pc}
  21c98c:	f6b4 fc22 	bl	d11d4 <wiced_bt_free_buffer>
  21c990:	e7f5      	b.n	21c97e <wiced_bt_hfp_hf_at_cmd_queue_free+0xe>

0021c992 <wiced_bt_hfp_hf_at_cmd_queue_send>:
  21c992:	b530      	push	{r4, r5, lr}
  21c994:	2100      	movs	r1, #0
  21c996:	4604      	mov	r4, r0
  21c998:	b0c7      	sub	sp, #284	; 0x11c
  21c99a:	f240 120b 	movw	r2, #267	; 0x10b
  21c99e:	a803      	add	r0, sp, #12
  21c9a0:	9102      	str	r1, [sp, #8]
  21c9a2:	f615 fa8f 	bl	31ec4 <memcpy+0x7>
  21c9a6:	f104 002c 	add.w	r0, r4, #44	; 0x2c
  21c9aa:	f652 fff7 	bl	6f99c <GKI_dequeue+0x1d5>
  21c9ae:	4605      	mov	r5, r0
  21c9b0:	b1a0      	cbz	r0, 21c9dc <wiced_bt_hfp_hf_at_cmd_queue_send+0x4a>
  21c9b2:	8842      	ldrh	r2, [r0, #2]
  21c9b4:	f100 0108 	add.w	r1, r0, #8
  21c9b8:	a802      	add	r0, sp, #8
  21c9ba:	f615 fa7f 	bl	31ebc <bsc_OpExtended+0x2d91>
  21c9be:	f10d 0306 	add.w	r3, sp, #6
  21c9c2:	886a      	ldrh	r2, [r5, #2]
  21c9c4:	8860      	ldrh	r0, [r4, #2]
  21c9c6:	a902      	add	r1, sp, #8
  21c9c8:	f6bc fc40 	bl	d924c <wiced_bt_rfcomm_flow_control+0x14d>
  21c9cc:	4603      	mov	r3, r0
  21c9ce:	f104 0038 	add.w	r0, r4, #56	; 0x38
  21c9d2:	b92b      	cbnz	r3, 21c9e0 <wiced_bt_hfp_hf_at_cmd_queue_send+0x4e>
  21c9d4:	f241 3188 	movw	r1, #5000	; 0x1388
  21c9d8:	f6c1 fe80 	bl	de6dc <wiced_deinit_timer+0x19>
  21c9dc:	b047      	add	sp, #284	; 0x11c
  21c9de:	bd30      	pop	{r4, r5, pc}
  21c9e0:	f44f 717a 	mov.w	r1, #1000	; 0x3e8
  21c9e4:	e7f8      	b.n	21c9d8 <wiced_bt_hfp_hf_at_cmd_queue_send+0x46>

0021c9e6 <wiced_bt_hfp_hf_at_cmd_queue_enqueue>:
  21c9e6:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  21c9ea:	4605      	mov	r5, r0
  21c9ec:	f103 0008 	add.w	r0, r3, #8
  21c9f0:	b280      	uxth	r0, r0
  21c9f2:	4688      	mov	r8, r1
  21c9f4:	4617      	mov	r7, r2
  21c9f6:	461e      	mov	r6, r3
  21c9f8:	f6b4 fbea 	bl	d11d0 <wiced_bt_get_buffer>
  21c9fc:	4604      	mov	r4, r0
  21c9fe:	b1c8      	cbz	r0, 21ca34 <wiced_bt_hfp_hf_at_cmd_queue_enqueue+0x4e>
  21ca00:	4632      	mov	r2, r6
  21ca02:	4639      	mov	r1, r7
  21ca04:	8046      	strh	r6, [r0, #2]
  21ca06:	f8a0 8006 	strh.w	r8, [r0, #6]
  21ca0a:	3008      	adds	r0, #8
  21ca0c:	f615 fa56 	bl	31ebc <bsc_OpExtended+0x2d91>
  21ca10:	4621      	mov	r1, r4
  21ca12:	f105 002c 	add.w	r0, r5, #44	; 0x2c
  21ca16:	f652 fe7f 	bl	6f718 <GKI_freebuf+0x71>
  21ca1a:	f895 3068 	ldrb.w	r3, [r5, #104]	; 0x68
  21ca1e:	3301      	adds	r3, #1
  21ca20:	b2db      	uxtb	r3, r3
  21ca22:	2b01      	cmp	r3, #1
  21ca24:	f885 3068 	strb.w	r3, [r5, #104]	; 0x68
  21ca28:	d104      	bne.n	21ca34 <wiced_bt_hfp_hf_at_cmd_queue_enqueue+0x4e>
  21ca2a:	4628      	mov	r0, r5
  21ca2c:	e8bd 41f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, lr}
  21ca30:	f7ff bfaf 	b.w	21c992 <wiced_bt_hfp_hf_at_cmd_queue_send>
  21ca34:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}

0021ca38 <wiced_bt_hfp_hf_at_cmd_queue_handle_res>:
  21ca38:	b570      	push	{r4, r5, r6, lr}
  21ca3a:	4604      	mov	r4, r0
  21ca3c:	3038      	adds	r0, #56	; 0x38
  21ca3e:	f104 062c 	add.w	r6, r4, #44	; 0x2c
  21ca42:	f6c1 fe15 	bl	de670 <wiced_init_timer+0x33>
  21ca46:	f7ff fee5 	bl	21c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>
  21ca4a:	4630      	mov	r0, r6
  21ca4c:	f652 febb 	bl	6f7c6 <GKI_enqueue_head+0x57>
  21ca50:	b180      	cbz	r0, 21ca74 <wiced_bt_hfp_hf_at_cmd_queue_handle_res+0x3c>
  21ca52:	f894 2068 	ldrb.w	r2, [r4, #104]	; 0x68
  21ca56:	3a01      	subs	r2, #1
  21ca58:	f884 2068 	strb.w	r2, [r4, #104]	; 0x68
  21ca5c:	7985      	ldrb	r5, [r0, #6]
  21ca5e:	f6b4 fbb9 	bl	d11d4 <wiced_bt_free_buffer>
  21ca62:	4630      	mov	r0, r6
  21ca64:	f652 ffa2 	bl	6f9ac <GKI_getfirst+0xf>
  21ca68:	b910      	cbnz	r0, 21ca70 <wiced_bt_hfp_hf_at_cmd_queue_handle_res+0x38>
  21ca6a:	4620      	mov	r0, r4
  21ca6c:	f7ff ff91 	bl	21c992 <wiced_bt_hfp_hf_at_cmd_queue_send>
  21ca70:	4628      	mov	r0, r5
  21ca72:	bd70      	pop	{r4, r5, r6, pc}
  21ca74:	25ff      	movs	r5, #255	; 0xff
  21ca76:	e7f4      	b.n	21ca62 <wiced_bt_hfp_hf_at_cmd_queue_handle_res+0x2a>

0021ca78 <wiced_bt_hfp_hf_at_cmd_queue_timeout>:
  21ca78:	b538      	push	{r3, r4, r5, lr}
  21ca7a:	f100 052c 	add.w	r5, r0, #44	; 0x2c
  21ca7e:	4604      	mov	r4, r0
  21ca80:	4628      	mov	r0, r5
  21ca82:	f652 ff93 	bl	6f9ac <GKI_getfirst+0xf>
  21ca86:	b118      	cbz	r0, 21ca90 <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x18>
  21ca88:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21ca8c:	f7ff bec2 	b.w	21c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>
  21ca90:	4a0e      	ldr	r2, [pc, #56]	; (21cacc <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x54>)
  21ca92:	7813      	ldrb	r3, [r2, #0]
  21ca94:	2b01      	cmp	r3, #1
  21ca96:	d915      	bls.n	21cac4 <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x4c>
  21ca98:	4628      	mov	r0, r5
  21ca9a:	f652 fe94 	bl	6f7c6 <GKI_enqueue_head+0x57>
  21ca9e:	b130      	cbz	r0, 21caae <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x36>
  21caa0:	f894 3068 	ldrb.w	r3, [r4, #104]	; 0x68
  21caa4:	3b01      	subs	r3, #1
  21caa6:	f884 3068 	strb.w	r3, [r4, #104]	; 0x68
  21caaa:	f6b4 fb93 	bl	d11d4 <wiced_bt_free_buffer>
  21caae:	f7ff feb1 	bl	21c814 <wiced_bt_hfp_hf_at_cmd_retrans_info_reset>
  21cab2:	4628      	mov	r0, r5
  21cab4:	f652 ff7a 	bl	6f9ac <GKI_getfirst+0xf>
  21cab8:	b938      	cbnz	r0, 21caca <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x52>
  21caba:	4620      	mov	r0, r4
  21cabc:	e8bd 4038 	ldmia.w	sp!, {r3, r4, r5, lr}
  21cac0:	f7ff bf67 	b.w	21c992 <wiced_bt_hfp_hf_at_cmd_queue_send>
  21cac4:	3301      	adds	r3, #1
  21cac6:	7013      	strb	r3, [r2, #0]
  21cac8:	e7f7      	b.n	21caba <wiced_bt_hfp_hf_at_cmd_queue_timeout+0x42>
  21caca:	bd38      	pop	{r3, r4, r5, pc}
  21cacc:	0021fc4c 	.word	0x0021fc4c

0021cad0 <wiced_bt_hfp_hf_at_timer_cback>:
  21cad0:	4806      	ldr	r0, [pc, #24]	; (21caec <wiced_bt_hfp_hf_at_timer_cback+0x1c>)
  21cad2:	7903      	ldrb	r3, [r0, #4]
  21cad4:	2b01      	cmp	r3, #1
  21cad6:	d102      	bne.n	21cade <wiced_bt_hfp_hf_at_timer_cback+0xe>
  21cad8:	3004      	adds	r0, #4
  21cada:	f7ff bfcd 	b.w	21ca78 <wiced_bt_hfp_hf_at_cmd_queue_timeout>
  21cade:	f890 30fc 	ldrb.w	r3, [r0, #252]	; 0xfc
  21cae2:	2b01      	cmp	r3, #1
  21cae4:	d101      	bne.n	21caea <wiced_bt_hfp_hf_at_timer_cback+0x1a>
  21cae6:	30fc      	adds	r0, #252	; 0xfc
  21cae8:	e7f7      	b.n	21cada <wiced_bt_hfp_hf_at_timer_cback+0xa>
  21caea:	4770      	bx	lr
  21caec:	00220058 	.word	0x00220058

0021caf0 <wiced_bt_hfp_check_ind_status>:
  21caf0:	e92d 41f0 	stmdb	sp!, {r4, r5, r6, r7, r8, lr}
  21caf4:	4606      	mov	r6, r0
  21caf6:	4617      	mov	r7, r2
  21caf8:	4698      	mov	r8, r3
  21cafa:	460c      	mov	r4, r1
  21cafc:	2500      	movs	r5, #0
  21cafe:	4630      	mov	r0, r6
  21cb00:	f65d fa00 	bl	79f04 <_printf_int_dec+0x73>
  21cb04:	4285      	cmp	r5, r0
  21cb06:	d301      	bcc.n	21cb0c <wiced_bt_hfp_check_ind_status+0x1c>
  21cb08:	2400      	movs	r4, #0
  21cb0a:	e007      	b.n	21cb1c <wiced_bt_hfp_check_ind_status+0x2c>
  21cb0c:	5d71      	ldrb	r1, [r6, r5]
  21cb0e:	b944      	cbnz	r4, 21cb22 <wiced_bt_hfp_check_ind_status+0x32>
  21cb10:	3930      	subs	r1, #48	; 0x30
  21cb12:	42b9      	cmp	r1, r7
  21cb14:	dc02      	bgt.n	21cb1c <wiced_bt_hfp_check_ind_status+0x2c>
  21cb16:	2401      	movs	r4, #1
  21cb18:	f888 1000 	strb.w	r1, [r8]
  21cb1c:	4620      	mov	r0, r4
  21cb1e:	e8bd 81f0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, pc}
  21cb22:	292c      	cmp	r1, #44	; 0x2c
  21cb24:	bf04      	itt	eq
  21cb26:	f104 34ff 	addeq.w	r4, r4, #4294967295
  21cb2a:	b2e4      	uxtbeq	r4, r4
  21cb2c:	3501      	adds	r5, #1
  21cb2e:	e7e6      	b.n	21cafe <wiced_bt_hfp_check_ind_status+0xe>

0021cb30 <wiced_bt_hfp_hf_find_indicator_id>:
  21cb30:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
  21cb34:	2500      	movs	r5, #0
  21cb36:	4606      	mov	r6, r0
  21cb38:	460f      	mov	r7, r1
  21cb3a:	f65d f9e3 	bl	79f04 <_printf_int_dec+0x73>
  21cb3e:	462c      	mov	r4, r5
  21cb40:	462a      	mov	r2, r5
  21cb42:	fa1f f880 	uxth.w	r8, r0
  21cb46:	b2eb      	uxtb	r3, r5
  21cb48:	4543      	cmp	r3, r8
  21cb4a:	d303      	bcc.n	21cb54 <wiced_bt_hfp_hf_find_indicator_id+0x24>
  21cb4c:	2400      	movs	r4, #0
  21cb4e:	4620      	mov	r0, r4
  21cb50:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
  21cb54:	5cf1      	ldrb	r1, [r6, r3]
  21cb56:	2922      	cmp	r1, #34	; 0x22
  21cb58:	d11b      	bne.n	21cb92 <wiced_bt_hfp_hf_find_indicator_id+0x62>
  21cb5a:	3301      	adds	r3, #1
  21cb5c:	eb06 0903 	add.w	r9, r6, r3
  21cb60:	5cf3      	ldrb	r3, [r6, r3]
  21cb62:	2b22      	cmp	r3, #34	; 0x22
  21cb64:	d015      	beq.n	21cb92 <wiced_bt_hfp_hf_find_indicator_id+0x62>
  21cb66:	b99a      	cbnz	r2, 21cb90 <wiced_bt_hfp_hf_find_indicator_id+0x60>
  21cb68:	4638      	mov	r0, r7
  21cb6a:	f65d f9cb 	bl	79f04 <_printf_int_dec+0x73>
  21cb6e:	3401      	adds	r4, #1
  21cb70:	4602      	mov	r2, r0
  21cb72:	4639      	mov	r1, r7
  21cb74:	4648      	mov	r0, r9
  21cb76:	b2e4      	uxtb	r4, r4
  21cb78:	f652 f8bc 	bl	6ecf4 <BTM_SetLinkSuperTout+0x7df>
  21cb7c:	b930      	cbnz	r0, 21cb8c <wiced_bt_hfp_hf_find_indicator_id+0x5c>
  21cb7e:	4638      	mov	r0, r7
  21cb80:	f65d f9c0 	bl	79f04 <_printf_int_dec+0x73>
  21cb84:	f819 3000 	ldrb.w	r3, [r9, r0]
  21cb88:	2b22      	cmp	r3, #34	; 0x22
  21cb8a:	d0e0      	beq.n	21cb4e <wiced_bt_hfp_hf_find_indicator_id+0x1e>
  21cb8c:	2201      	movs	r2, #1
  21cb8e:	e000      	b.n	21cb92 <wiced_bt_hfp_hf_find_indicator_id+0x62>
  21cb90:	2200      	movs	r2, #0
  21cb92:	3501      	adds	r5, #1
  21cb94:	e7d7      	b.n	21cb46 <wiced_bt_hfp_hf_find_indicator_id+0x16>
	...

0021cb98 <wiced_bt_hfp_hf_at_send_cmd>:
  21cb98:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21cb9c:	b0c5      	sub	sp, #276	; 0x114
  21cb9e:	466f      	mov	r7, sp
  21cba0:	460e      	mov	r6, r1
  21cba2:	4691      	mov	r9, r2
  21cba4:	2100      	movs	r1, #0
  21cba6:	f240 120f 	movw	r2, #271	; 0x10f
  21cbaa:	4680      	mov	r8, r0
  21cbac:	4638      	mov	r0, r7
  21cbae:	469b      	mov	fp, r3
  21cbb0:	f8dd a138 	ldr.w	sl, [sp, #312]	; 0x138
  21cbb4:	f615 f986 	bl	31ec4 <memcpy+0x7>
  21cbb8:	f245 4341 	movw	r3, #21569	; 0x5441
  21cbbc:	803b      	strh	r3, [r7, #0]
  21cbbe:	4b22      	ldr	r3, [pc, #136]	; (21cc48 <wiced_bt_hfp_hf_at_send_cmd+0xb0>)
  21cbc0:	f10d 0502 	add.w	r5, sp, #2
  21cbc4:	f853 4026 	ldr.w	r4, [r3, r6, lsl #2]
  21cbc8:	22ff      	movs	r2, #255	; 0xff
  21cbca:	4621      	mov	r1, r4
  21cbcc:	4628      	mov	r0, r5
  21cbce:	f695 fe7b 	bl	b28c8 <A30fb2800+0x52b0>
  21cbd2:	4620      	mov	r0, r4
  21cbd4:	f65d f996 	bl	79f04 <_printf_int_dec+0x73>
  21cbd8:	f1b9 0f02 	cmp.w	r9, #2
  21cbdc:	eb05 0400 	add.w	r4, r5, r0
  21cbe0:	d118      	bne.n	21cc14 <wiced_bt_hfp_hf_at_send_cmd+0x7c>
  21cbe2:	233d      	movs	r3, #61	; 0x3d
  21cbe4:	542b      	strb	r3, [r5, r0]
  21cbe6:	3401      	adds	r4, #1
  21cbe8:	f1bb 0f02 	cmp.w	fp, #2
  21cbec:	d121      	bne.n	21cc32 <wiced_bt_hfp_hf_at_send_cmd+0x9a>
  21cbee:	4621      	mov	r1, r4
  21cbf0:	f8bd 013c 	ldrh.w	r0, [sp, #316]	; 0x13c
  21cbf4:	f000 fedc 	bl	21d9b0 <utl_itoa>
  21cbf8:	4404      	add	r4, r0
  21cbfa:	230d      	movs	r3, #13
  21cbfc:	f804 3b01 	strb.w	r3, [r4], #1
  21cc00:	1be4      	subs	r4, r4, r7
  21cc02:	463a      	mov	r2, r7
  21cc04:	4631      	mov	r1, r6
  21cc06:	4640      	mov	r0, r8
  21cc08:	b2a3      	uxth	r3, r4
  21cc0a:	f7ff feec 	bl	21c9e6 <wiced_bt_hfp_hf_at_cmd_queue_enqueue>
  21cc0e:	b045      	add	sp, #276	; 0x114
  21cc10:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21cc14:	f1b9 0f04 	cmp.w	r9, #4
  21cc18:	d101      	bne.n	21cc1e <wiced_bt_hfp_hf_at_send_cmd+0x86>
  21cc1a:	233f      	movs	r3, #63	; 0x3f
  21cc1c:	e7e2      	b.n	21cbe4 <wiced_bt_hfp_hf_at_send_cmd+0x4c>
  21cc1e:	f1b9 0f08 	cmp.w	r9, #8
  21cc22:	d1e1      	bne.n	21cbe8 <wiced_bt_hfp_hf_at_send_cmd+0x50>
  21cc24:	233d      	movs	r3, #61	; 0x3d
  21cc26:	542b      	strb	r3, [r5, r0]
  21cc28:	233f      	movs	r3, #63	; 0x3f
  21cc2a:	3402      	adds	r4, #2
  21cc2c:	f804 3c01 	strb.w	r3, [r4, #-1]
  21cc30:	e7da      	b.n	21cbe8 <wiced_bt_hfp_hf_at_send_cmd+0x50>
  21cc32:	f1bb 0f01 	cmp.w	fp, #1
  21cc36:	d1e0      	bne.n	21cbfa <wiced_bt_hfp_hf_at_send_cmd+0x62>
  21cc38:	4651      	mov	r1, sl
  21cc3a:	4620      	mov	r0, r4
  21cc3c:	f000 fe91 	bl	21d962 <utl_strcpy>
  21cc40:	4650      	mov	r0, sl
  21cc42:	f65d f95f 	bl	79f04 <_printf_int_dec+0x73>
  21cc46:	e7d7      	b.n	21cbf8 <wiced_bt_hfp_hf_at_send_cmd+0x60>
  21cc48:	0021e1f0 	.word	0x0021e1f0

0021cc4c <wiced_bt_hsp_at_slc>:
  21cc4c:	b537      	push	{r0, r1, r2, r4, r5, lr}
  21cc4e:	f890 3022 	ldrb.w	r3, [r0, #34]	; 0x22
  21cc52:	4604      	mov	r4, r0
  21cc54:	b93b      	cbnz	r3, 21cc66 <wiced_bt_hsp_at_slc+0x1a>
  21cc56:	22c8      	movs	r2, #200	; 0xc8
  21cc58:	e9cd 3200 	strd	r3, r2, [sp]
  21cc5c:	2302      	movs	r3, #2
  21cc5e:	2117      	movs	r1, #23
  21cc60:	461a      	mov	r2, r3
  21cc62:	f7ff ff99 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21cc66:	f894 3071 	ldrb.w	r3, [r4, #113]	; 0x71
  21cc6a:	2500      	movs	r5, #0
  21cc6c:	9301      	str	r3, [sp, #4]
  21cc6e:	2302      	movs	r3, #2
  21cc70:	4629      	mov	r1, r5
  21cc72:	461a      	mov	r2, r3
  21cc74:	4620      	mov	r0, r4
  21cc76:	9500      	str	r5, [sp, #0]
  21cc78:	f7ff ff8e 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21cc7c:	f894 3070 	ldrb.w	r3, [r4, #112]	; 0x70
  21cc80:	2101      	movs	r1, #1
  21cc82:	e9cd 5300 	strd	r5, r3, [sp]
  21cc86:	2302      	movs	r3, #2
  21cc88:	4620      	mov	r0, r4
  21cc8a:	461a      	mov	r2, r3
  21cc8c:	f7ff ff84 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21cc90:	b003      	add	sp, #12
  21cc92:	bd30      	pop	{r4, r5, pc}

0021cc94 <wiced_bt_hfp_hf_at_slc>:
  21cc94:	b513      	push	{r0, r1, r4, lr}
  21cc96:	f890 302b 	ldrb.w	r3, [r0, #43]	; 0x2b
  21cc9a:	4604      	mov	r4, r0
  21cc9c:	2b0e      	cmp	r3, #14
  21cc9e:	d81b      	bhi.n	21ccd8 <wiced_bt_hfp_hf_at_slc+0x44>
  21cca0:	e8df f013 	tbh	[pc, r3, lsl #1]
  21cca4:	002a000f 	.word	0x002a000f
  21cca8:	0041003b 	.word	0x0041003b
  21ccac:	004d0046 	.word	0x004d0046
  21ccb0:	006f005c 	.word	0x006f005c
  21ccb4:	008f007f 	.word	0x008f007f
  21ccb8:	00b000a0 	.word	0x00b000a0
  21ccbc:	00d100c0 	.word	0x00d100c0
  21ccc0:	00df      	.short	0x00df
  21ccc2:	f9b0 306c 	ldrsh.w	r3, [r0, #108]	; 0x6c
  21ccc6:	2118      	movs	r1, #24
  21ccc8:	9301      	str	r3, [sp, #4]
  21ccca:	2300      	movs	r3, #0
  21cccc:	9300      	str	r3, [sp, #0]
  21ccce:	2302      	movs	r3, #2
  21ccd0:	461a      	mov	r2, r3
  21ccd2:	4620      	mov	r0, r4
  21ccd4:	f7ff ff60 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21ccd8:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21ccdc:	3301      	adds	r3, #1
  21ccde:	b2db      	uxtb	r3, r3
  21cce0:	2b10      	cmp	r3, #16
  21cce2:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cce6:	f040 80c4 	bne.w	21ce72 <wiced_bt_hfp_hf_at_slc+0x1de>
  21ccea:	2100      	movs	r1, #0
  21ccec:	4620      	mov	r0, r4
  21ccee:	b002      	add	sp, #8
  21ccf0:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
  21ccf4:	f7ff bcaa 	b.w	21c64c <wiced_bt_hfp_hf_slc_open>
  21ccf8:	6ec3      	ldr	r3, [r0, #108]	; 0x6c
  21ccfa:	0619      	lsls	r1, r3, #24
  21ccfc:	d50a      	bpl.n	21cd14 <wiced_bt_hfp_hf_at_slc+0x80>
  21ccfe:	8cc3      	ldrh	r3, [r0, #38]	; 0x26
  21cd00:	059a      	lsls	r2, r3, #22
  21cd02:	d507      	bpl.n	21cd14 <wiced_bt_hfp_hf_at_slc+0x80>
  21cd04:	2300      	movs	r3, #0
  21cd06:	9301      	str	r3, [sp, #4]
  21cd08:	4b5b      	ldr	r3, [pc, #364]	; (21ce78 <wiced_bt_hfp_hf_at_slc+0x1e4>)
  21cd0a:	2202      	movs	r2, #2
  21cd0c:	9300      	str	r3, [sp, #0]
  21cd0e:	2115      	movs	r1, #21
  21cd10:	2301      	movs	r3, #1
  21cd12:	e7de      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cd14:	2302      	movs	r3, #2
  21cd16:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cd1a:	2300      	movs	r3, #0
  21cd1c:	2208      	movs	r2, #8
  21cd1e:	e9cd 3300 	strd	r3, r3, [sp]
  21cd22:	2108      	movs	r1, #8
  21cd24:	e7d5      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cd26:	2300      	movs	r3, #0
  21cd28:	2204      	movs	r2, #4
  21cd2a:	e9cd 3300 	strd	r3, r3, [sp]
  21cd2e:	e7f8      	b.n	21cd22 <wiced_bt_hfp_hf_at_slc+0x8e>
  21cd30:	2301      	movs	r3, #1
  21cd32:	4a52      	ldr	r2, [pc, #328]	; (21ce7c <wiced_bt_hfp_hf_at_slc+0x1e8>)
  21cd34:	211a      	movs	r1, #26
  21cd36:	9200      	str	r2, [sp, #0]
  21cd38:	9301      	str	r3, [sp, #4]
  21cd3a:	2202      	movs	r2, #2
  21cd3c:	e7c9      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cd3e:	6ec3      	ldr	r3, [r0, #108]	; 0x6c
  21cd40:	079b      	lsls	r3, r3, #30
  21cd42:	d508      	bpl.n	21cd56 <wiced_bt_hfp_hf_at_slc+0xc2>
  21cd44:	8cc3      	ldrh	r3, [r0, #38]	; 0x26
  21cd46:	07d8      	lsls	r0, r3, #31
  21cd48:	d505      	bpl.n	21cd56 <wiced_bt_hfp_hf_at_slc+0xc2>
  21cd4a:	2300      	movs	r3, #0
  21cd4c:	2208      	movs	r2, #8
  21cd4e:	2106      	movs	r1, #6
  21cd50:	e9cd 3300 	strd	r3, r3, [sp]
  21cd54:	e7bd      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cd56:	2306      	movs	r3, #6
  21cd58:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cd5c:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21cd5e:	05d9      	lsls	r1, r3, #23
  21cd60:	d50a      	bpl.n	21cd78 <wiced_bt_hfp_hf_at_slc+0xe4>
  21cd62:	8ce3      	ldrh	r3, [r4, #38]	; 0x26
  21cd64:	055a      	lsls	r2, r3, #21
  21cd66:	d507      	bpl.n	21cd78 <wiced_bt_hfp_hf_at_slc+0xe4>
  21cd68:	2300      	movs	r3, #0
  21cd6a:	9301      	str	r3, [sp, #4]
  21cd6c:	4b42      	ldr	r3, [pc, #264]	; (21ce78 <wiced_bt_hfp_hf_at_slc+0x1e4>)
  21cd6e:	2202      	movs	r2, #2
  21cd70:	9300      	str	r3, [sp, #0]
  21cd72:	2301      	movs	r3, #1
  21cd74:	2116      	movs	r1, #22
  21cd76:	e7ac      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cd78:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cd7c:	3301      	adds	r3, #1
  21cd7e:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cd82:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21cd84:	05db      	lsls	r3, r3, #23
  21cd86:	d507      	bpl.n	21cd98 <wiced_bt_hfp_hf_at_slc+0x104>
  21cd88:	8ce3      	ldrh	r3, [r4, #38]	; 0x26
  21cd8a:	0558      	lsls	r0, r3, #21
  21cd8c:	d504      	bpl.n	21cd98 <wiced_bt_hfp_hf_at_slc+0x104>
  21cd8e:	2300      	movs	r3, #0
  21cd90:	2208      	movs	r2, #8
  21cd92:	e9cd 3300 	strd	r3, r3, [sp]
  21cd96:	e7ed      	b.n	21cd74 <wiced_bt_hfp_hf_at_slc+0xe0>
  21cd98:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cd9c:	3301      	adds	r3, #1
  21cd9e:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cda2:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21cda4:	05d9      	lsls	r1, r3, #23
  21cda6:	d507      	bpl.n	21cdb8 <wiced_bt_hfp_hf_at_slc+0x124>
  21cda8:	8ce3      	ldrh	r3, [r4, #38]	; 0x26
  21cdaa:	055a      	lsls	r2, r3, #21
  21cdac:	d504      	bpl.n	21cdb8 <wiced_bt_hfp_hf_at_slc+0x124>
  21cdae:	2300      	movs	r3, #0
  21cdb0:	2204      	movs	r2, #4
  21cdb2:	e9cd 3300 	strd	r3, r3, [sp]
  21cdb6:	e7dd      	b.n	21cd74 <wiced_bt_hfp_hf_at_slc+0xe0>
  21cdb8:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cdbc:	3301      	adds	r3, #1
  21cdbe:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cdc2:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21cdc4:	06db      	lsls	r3, r3, #27
  21cdc6:	d508      	bpl.n	21cdda <wiced_bt_hfp_hf_at_slc+0x146>
  21cdc8:	f894 3070 	ldrb.w	r3, [r4, #112]	; 0x70
  21cdcc:	2101      	movs	r1, #1
  21cdce:	9301      	str	r3, [sp, #4]
  21cdd0:	2300      	movs	r3, #0
  21cdd2:	9300      	str	r3, [sp, #0]
  21cdd4:	2302      	movs	r3, #2
  21cdd6:	461a      	mov	r2, r3
  21cdd8:	e77b      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cdda:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cdde:	3301      	adds	r3, #1
  21cde0:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21cde4:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21cde6:	06d8      	lsls	r0, r3, #27
  21cde8:	d507      	bpl.n	21cdfa <wiced_bt_hfp_hf_at_slc+0x166>
  21cdea:	f894 3071 	ldrb.w	r3, [r4, #113]	; 0x71
  21cdee:	2100      	movs	r1, #0
  21cdf0:	9301      	str	r3, [sp, #4]
  21cdf2:	2302      	movs	r3, #2
  21cdf4:	9100      	str	r1, [sp, #0]
  21cdf6:	461a      	mov	r2, r3
  21cdf8:	e76b      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21cdfa:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cdfe:	3301      	adds	r3, #1
  21ce00:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21ce04:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21ce06:	0759      	lsls	r1, r3, #29
  21ce08:	d507      	bpl.n	21ce1a <wiced_bt_hfp_hf_at_slc+0x186>
  21ce0a:	2300      	movs	r3, #0
  21ce0c:	2201      	movs	r2, #1
  21ce0e:	e9cd 3200 	strd	r3, r2, [sp]
  21ce12:	2302      	movs	r3, #2
  21ce14:	2119      	movs	r1, #25
  21ce16:	461a      	mov	r2, r3
  21ce18:	e75b      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21ce1a:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21ce1e:	3301      	adds	r3, #1
  21ce20:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21ce24:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21ce26:	079b      	lsls	r3, r3, #30
  21ce28:	d515      	bpl.n	21ce56 <wiced_bt_hfp_hf_at_slc+0x1c2>
  21ce2a:	2300      	movs	r3, #0
  21ce2c:	2101      	movs	r1, #1
  21ce2e:	e9cd 3100 	strd	r3, r1, [sp]
  21ce32:	2302      	movs	r3, #2
  21ce34:	4620      	mov	r0, r4
  21ce36:	461a      	mov	r2, r3
  21ce38:	211b      	movs	r1, #27
  21ce3a:	f7ff fead 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21ce3e:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21ce40:	0798      	lsls	r0, r3, #30
  21ce42:	f53f af49 	bmi.w	21ccd8 <wiced_bt_hfp_hf_at_slc+0x44>
  21ce46:	2300      	movs	r3, #0
  21ce48:	9301      	str	r3, [sp, #4]
  21ce4a:	4b0d      	ldr	r3, [pc, #52]	; (21ce80 <wiced_bt_hfp_hf_at_slc+0x1ec>)
  21ce4c:	2202      	movs	r2, #2
  21ce4e:	9300      	str	r3, [sp, #0]
  21ce50:	210e      	movs	r1, #14
  21ce52:	2301      	movs	r3, #1
  21ce54:	e73d      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21ce56:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21ce5a:	3301      	adds	r3, #1
  21ce5c:	f884 302b 	strb.w	r3, [r4, #43]	; 0x2b
  21ce60:	e7f1      	b.n	21ce46 <wiced_bt_hfp_hf_at_slc+0x1b2>
  21ce62:	2300      	movs	r3, #0
  21ce64:	2201      	movs	r2, #1
  21ce66:	e9cd 3200 	strd	r3, r2, [sp]
  21ce6a:	2302      	movs	r3, #2
  21ce6c:	210f      	movs	r1, #15
  21ce6e:	461a      	mov	r2, r3
  21ce70:	e72f      	b.n	21ccd2 <wiced_bt_hfp_hf_at_slc+0x3e>
  21ce72:	b002      	add	sp, #8
  21ce74:	bd10      	pop	{r4, pc}
  21ce76:	bf00      	nop
  21ce78:	0021f7b9 	.word	0x0021f7b9
  21ce7c:	0021f7bd 	.word	0x0021f7bd
  21ce80:	0021f7c5 	.word	0x0021f7c5

0021ce84 <wiced_bt_hfp_hf_at_cback>:
  21ce84:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
  21ce88:	f890 3022 	ldrb.w	r3, [r0, #34]	; 0x22
  21ce8c:	4604      	mov	r4, r0
  21ce8e:	460e      	mov	r6, r1
  21ce90:	4615      	mov	r5, r2
  21ce92:	f5ad 7d07 	sub.w	sp, sp, #540	; 0x21c
  21ce96:	b3ab      	cbz	r3, 21cf04 <wiced_bt_hfp_hf_at_cback+0x80>
  21ce98:	8840      	ldrh	r0, [r0, #2]
  21ce9a:	f000 fc11 	bl	21d6c0 <wiced_bt_hfp_hf_get_uuid_by_handle>
  21ce9e:	4607      	mov	r7, r0
  21cea0:	2e18      	cmp	r6, #24
  21cea2:	bf98      	it	ls
  21cea4:	4baa      	ldrls	r3, [pc, #680]	; (21d150 <wiced_bt_hfp_hf_at_cback+0x2cc>)
  21cea6:	f44f 7282 	mov.w	r2, #260	; 0x104
  21ceaa:	bf98      	it	ls
  21ceac:	eb03 03c6 	addls.w	r3, r3, r6, lsl #3
  21ceb0:	f04f 0100 	mov.w	r1, #0
  21ceb4:	a804      	add	r0, sp, #16
  21ceb6:	bf94      	ite	ls
  21ceb8:	f893 8005 	ldrbls.w	r8, [r3, #5]
  21cebc:	f04f 0800 	movhi.w	r8, #0
  21cec0:	f615 f800 	bl	31ec4 <memcpy+0x7>
  21cec4:	8863      	ldrh	r3, [r4, #2]
  21cec6:	f8ad 3010 	strh.w	r3, [sp, #16]
  21ceca:	2e18      	cmp	r6, #24
  21cecc:	d833      	bhi.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21cece:	e8df f016 	tbh	[pc, r6, lsl #1]
  21ced2:	0026      	.short	0x0026
  21ced4:	004c0026 	.word	0x004c0026
  21ced8:	006a0032 	.word	0x006a0032
  21cedc:	0032005b 	.word	0x0032005b
  21cee0:	01430032 	.word	0x01430032
  21cee4:	020400a7 	.word	0x020400a7
  21cee8:	02a900a7 	.word	0x02a900a7
  21ceec:	012f0295 	.word	0x012f0295
  21cef0:	00320295 	.word	0x00320295
  21cef4:	02b9011b 	.word	0x02b9011b
  21cef8:	00320299 	.word	0x00320299
  21cefc:	005b006a 	.word	0x005b006a
  21cf00:	02b20295 	.word	0x02b20295
  21cf04:	4b93      	ldr	r3, [pc, #588]	; (21d154 <wiced_bt_hfp_hf_at_cback+0x2d0>)
  21cf06:	f241 101f 	movw	r0, #4383	; 0x111f
  21cf0a:	f8b3 220e 	ldrh.w	r2, [r3, #526]	; 0x20e
  21cf0e:	f241 171e 	movw	r7, #4382	; 0x111e
  21cf12:	f241 1308 	movw	r3, #4360	; 0x1108
  21cf16:	4282      	cmp	r2, r0
  21cf18:	bf18      	it	ne
  21cf1a:	461f      	movne	r7, r3
  21cf1c:	e7c0      	b.n	21cea0 <wiced_bt_hfp_hf_at_cback+0x1c>
  21cf1e:	4620      	mov	r0, r4
  21cf20:	f7ff fd8a 	bl	21ca38 <wiced_bt_hfp_hf_at_cmd_queue_handle_res>
  21cf24:	28ff      	cmp	r0, #255	; 0xff
  21cf26:	d102      	bne.n	21cf2e <wiced_bt_hfp_hf_at_cback+0xaa>
  21cf28:	f04f 0800 	mov.w	r8, #0
  21cf2c:	e003      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21cf2e:	f894 3021 	ldrb.w	r3, [r4, #33]	; 0x21
  21cf32:	2b00      	cmp	r3, #0
  21cf34:	d0f8      	beq.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21cf36:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21cf3a:	2b10      	cmp	r3, #16
  21cf3c:	d008      	beq.n	21cf50 <wiced_bt_hfp_hf_at_cback+0xcc>
  21cf3e:	2e01      	cmp	r6, #1
  21cf40:	d806      	bhi.n	21cf50 <wiced_bt_hfp_hf_at_cback+0xcc>
  21cf42:	f241 1308 	movw	r3, #4360	; 0x1108
  21cf46:	429f      	cmp	r7, r3
  21cf48:	d002      	beq.n	21cf50 <wiced_bt_hfp_hf_at_cback+0xcc>
  21cf4a:	4620      	mov	r0, r4
  21cf4c:	f7ff fea2 	bl	21cc94 <wiced_bt_hfp_hf_at_slc>
  21cf50:	f1b8 0f00 	cmp.w	r8, #0
  21cf54:	d005      	beq.n	21cf62 <wiced_bt_hfp_hf_at_cback+0xde>
  21cf56:	4b7f      	ldr	r3, [pc, #508]	; (21d154 <wiced_bt_hfp_hf_at_cback+0x2d0>)
  21cf58:	4640      	mov	r0, r8
  21cf5a:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21cf5e:	a904      	add	r1, sp, #16
  21cf60:	4798      	blx	r3
  21cf62:	f50d 7d07 	add.w	sp, sp, #540	; 0x21c
  21cf66:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
  21cf6a:	4620      	mov	r0, r4
  21cf6c:	f7ff fd64 	bl	21ca38 <wiced_bt_hfp_hf_at_cmd_queue_handle_res>
  21cf70:	28ff      	cmp	r0, #255	; 0xff
  21cf72:	d0d9      	beq.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21cf74:	f894 3021 	ldrb.w	r3, [r4, #33]	; 0x21
  21cf78:	2b00      	cmp	r3, #0
  21cf7a:	d0d5      	beq.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21cf7c:	4628      	mov	r0, r5
  21cf7e:	f000 fcfa 	bl	21d976 <utl_str2int>
  21cf82:	f88d 0014 	strb.w	r0, [sp, #20]
  21cf86:	e7d6      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21cf88:	4628      	mov	r0, r5
  21cf8a:	f000 fcf4 	bl	21d976 <utl_str2int>
  21cf8e:	4b71      	ldr	r3, [pc, #452]	; (21d154 <wiced_bt_hfp_hf_at_cback+0x2d0>)
  21cf90:	b2c0      	uxtb	r0, r0
  21cf92:	f884 0070 	strb.w	r0, [r4, #112]	; 0x70
  21cf96:	f883 01f4 	strb.w	r0, [r3, #500]	; 0x1f4
  21cf9a:	2301      	movs	r3, #1
  21cf9c:	f88d 0015 	strb.w	r0, [sp, #21]
  21cfa0:	f88d 3014 	strb.w	r3, [sp, #20]
  21cfa4:	e7c7      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21cfa6:	4628      	mov	r0, r5
  21cfa8:	f000 fce5 	bl	21d976 <utl_str2int>
  21cfac:	4b69      	ldr	r3, [pc, #420]	; (21d154 <wiced_bt_hfp_hf_at_cback+0x2d0>)
  21cfae:	b2c0      	uxtb	r0, r0
  21cfb0:	f884 0071 	strb.w	r0, [r4, #113]	; 0x71
  21cfb4:	f88d 0015 	strb.w	r0, [sp, #21]
  21cfb8:	f883 01f5 	strb.w	r0, [r3, #501]	; 0x1f5
  21cfbc:	2300      	movs	r3, #0
  21cfbe:	e7ef      	b.n	21cfa0 <wiced_bt_hfp_hf_at_cback+0x11c>
  21cfc0:	f80a 3009 	strb.w	r3, [sl, r9]
  21cfc4:	46d9      	mov	r9, fp
  21cfc6:	4628      	mov	r0, r5
  21cfc8:	f65c ff9c 	bl	79f04 <_printf_int_dec+0x73>
  21cfcc:	4548      	cmp	r0, r9
  21cfce:	f109 0b01 	add.w	fp, r9, #1
  21cfd2:	d903      	bls.n	21cfdc <wiced_bt_hfp_hf_at_cback+0x158>
  21cfd4:	f815 3009 	ldrb.w	r3, [r5, r9]
  21cfd8:	2b2c      	cmp	r3, #44	; 0x2c
  21cfda:	d1f1      	bne.n	21cfc0 <wiced_bt_hfp_hf_at_cback+0x13c>
  21cfdc:	f04f 0300 	mov.w	r3, #0
  21cfe0:	4650      	mov	r0, sl
  21cfe2:	f80a 3009 	strb.w	r3, [sl, r9]
  21cfe6:	f65c ff8d 	bl	79f04 <_printf_int_dec+0x73>
  21cfea:	f04f 0900 	mov.w	r9, #0
  21cfee:	1c42      	adds	r2, r0, #1
  21cff0:	4651      	mov	r1, sl
  21cff2:	a805      	add	r0, sp, #20
  21cff4:	f695 fc68 	bl	b28c8 <A30fb2800+0x52b0>
  21cff8:	eb05 030b 	add.w	r3, r5, fp
  21cffc:	4628      	mov	r0, r5
  21cffe:	9302      	str	r3, [sp, #8]
  21d000:	f65c ff80 	bl	79f04 <_printf_int_dec+0x73>
  21d004:	eb09 020b 	add.w	r2, r9, fp
  21d008:	4290      	cmp	r0, r2
  21d00a:	9b02      	ldr	r3, [sp, #8]
  21d00c:	d80d      	bhi.n	21d02a <wiced_bt_hfp_hf_at_cback+0x1a6>
  21d00e:	2300      	movs	r3, #0
  21d010:	4650      	mov	r0, sl
  21d012:	f80a 3009 	strb.w	r3, [sl, r9]
  21d016:	f000 fcae 	bl	21d976 <utl_str2int>
  21d01a:	f88d 0034 	strb.w	r0, [sp, #52]	; 0x34
  21d01e:	e78a      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d020:	f04f 0900 	mov.w	r9, #0
  21d024:	f50d 7a8a 	add.w	sl, sp, #276	; 0x114
  21d028:	e7cd      	b.n	21cfc6 <wiced_bt_hfp_hf_at_cback+0x142>
  21d02a:	f813 2b01 	ldrb.w	r2, [r3], #1
  21d02e:	f80a 2009 	strb.w	r2, [sl, r9]
  21d032:	f109 0901 	add.w	r9, r9, #1
  21d036:	e7e1      	b.n	21cffc <wiced_bt_hfp_hf_at_cback+0x178>
  21d038:	f109 0901 	add.w	r9, r9, #1
  21d03c:	f815 3009 	ldrb.w	r3, [r5, r9]
  21d040:	3b30      	subs	r3, #48	; 0x30
  21d042:	b2db      	uxtb	r3, r3
  21d044:	2b09      	cmp	r3, #9
  21d046:	d8f7      	bhi.n	21d038 <wiced_bt_hfp_hf_at_cback+0x1b4>
  21d048:	f88d 3014 	strb.w	r3, [sp, #20]
  21d04c:	eb05 0309 	add.w	r3, r5, r9
  21d050:	789a      	ldrb	r2, [r3, #2]
  21d052:	4628      	mov	r0, r5
  21d054:	3a30      	subs	r2, #48	; 0x30
  21d056:	f88d 2015 	strb.w	r2, [sp, #21]
  21d05a:	791a      	ldrb	r2, [r3, #4]
  21d05c:	f109 090a 	add.w	r9, r9, #10
  21d060:	3a30      	subs	r2, #48	; 0x30
  21d062:	f88d 2016 	strb.w	r2, [sp, #22]
  21d066:	799a      	ldrb	r2, [r3, #6]
  21d068:	7a1b      	ldrb	r3, [r3, #8]
  21d06a:	3a30      	subs	r2, #48	; 0x30
  21d06c:	3b30      	subs	r3, #48	; 0x30
  21d06e:	f88d 2017 	strb.w	r2, [sp, #23]
  21d072:	f88d 3018 	strb.w	r3, [sp, #24]
  21d076:	f65c ff45 	bl	79f04 <_printf_int_dec+0x73>
  21d07a:	4548      	cmp	r0, r9
  21d07c:	f67f af5b 	bls.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d080:	f815 3009 	ldrb.w	r3, [r5, r9]
  21d084:	3b30      	subs	r3, #48	; 0x30
  21d086:	2b09      	cmp	r3, #9
  21d088:	d841      	bhi.n	21d10e <wiced_bt_hfp_hf_at_cback+0x28a>
  21d08a:	f50d 7a8a 	add.w	sl, sp, #276	; 0x114
  21d08e:	4651      	mov	r1, sl
  21d090:	2300      	movs	r3, #0
  21d092:	46d3      	mov	fp, sl
  21d094:	4628      	mov	r0, r5
  21d096:	e9cd 3102 	strd	r3, r1, [sp, #8]
  21d09a:	f65c ff33 	bl	79f04 <_printf_int_dec+0x73>
  21d09e:	9b02      	ldr	r3, [sp, #8]
  21d0a0:	4548      	cmp	r0, r9
  21d0a2:	f103 0201 	add.w	r2, r3, #1
  21d0a6:	d907      	bls.n	21d0b8 <wiced_bt_hfp_hf_at_cback+0x234>
  21d0a8:	f815 0009 	ldrb.w	r0, [r5, r9]
  21d0ac:	9903      	ldr	r1, [sp, #12]
  21d0ae:	f1a0 0c30 	sub.w	ip, r0, #48	; 0x30
  21d0b2:	f1bc 0f09 	cmp.w	ip, #9
  21d0b6:	d92d      	bls.n	21d114 <wiced_bt_hfp_hf_at_cback+0x290>
  21d0b8:	2100      	movs	r1, #0
  21d0ba:	f10d 0019 	add.w	r0, sp, #25
  21d0be:	f80a 1003 	strb.w	r1, [sl, r3]
  21d0c2:	4651      	mov	r1, sl
  21d0c4:	f695 fc00 	bl	b28c8 <A30fb2800+0x52b0>
  21d0c8:	f815 3009 	ldrb.w	r3, [r5, r9]
  21d0cc:	464a      	mov	r2, r9
  21d0ce:	3b30      	subs	r3, #48	; 0x30
  21d0d0:	2b09      	cmp	r3, #9
  21d0d2:	d825      	bhi.n	21d120 <wiced_bt_hfp_hf_at_cback+0x29c>
  21d0d4:	eba9 0102 	sub.w	r1, r9, r2
  21d0d8:	4628      	mov	r0, r5
  21d0da:	e9cd 1202 	strd	r1, r2, [sp, #8]
  21d0de:	f65c ff11 	bl	79f04 <_printf_int_dec+0x73>
  21d0e2:	4548      	cmp	r0, r9
  21d0e4:	9902      	ldr	r1, [sp, #8]
  21d0e6:	d906      	bls.n	21d0f6 <wiced_bt_hfp_hf_at_cback+0x272>
  21d0e8:	f815 3009 	ldrb.w	r3, [r5, r9]
  21d0ec:	9a03      	ldr	r2, [sp, #12]
  21d0ee:	f1a3 0030 	sub.w	r0, r3, #48	; 0x30
  21d0f2:	2809      	cmp	r0, #9
  21d0f4:	d917      	bls.n	21d126 <wiced_bt_hfp_hf_at_cback+0x2a2>
  21d0f6:	2300      	movs	r3, #0
  21d0f8:	4658      	mov	r0, fp
  21d0fa:	f80b 3001 	strb.w	r3, [fp, r1]
  21d0fe:	f000 fc3a 	bl	21d976 <utl_str2int>
  21d102:	f88d 0039 	strb.w	r0, [sp, #57]	; 0x39
  21d106:	e716      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d108:	f04f 0900 	mov.w	r9, #0
  21d10c:	e796      	b.n	21d03c <wiced_bt_hfp_hf_at_cback+0x1b8>
  21d10e:	f109 0901 	add.w	r9, r9, #1
  21d112:	e7b5      	b.n	21d080 <wiced_bt_hfp_hf_at_cback+0x1fc>
  21d114:	4613      	mov	r3, r2
  21d116:	f801 0b01 	strb.w	r0, [r1], #1
  21d11a:	f109 0901 	add.w	r9, r9, #1
  21d11e:	e7b9      	b.n	21d094 <wiced_bt_hfp_hf_at_cback+0x210>
  21d120:	f109 0901 	add.w	r9, r9, #1
  21d124:	e7d0      	b.n	21d0c8 <wiced_bt_hfp_hf_at_cback+0x244>
  21d126:	f80a 3b01 	strb.w	r3, [sl], #1
  21d12a:	f109 0901 	add.w	r9, r9, #1
  21d12e:	e7d1      	b.n	21d0d4 <wiced_bt_hfp_hf_at_cback+0x250>
  21d130:	4628      	mov	r0, r5
  21d132:	f65c fee7 	bl	79f04 <_printf_int_dec+0x73>
  21d136:	f5b0 7f80 	cmp.w	r0, #256	; 0x100
  21d13a:	f63f aef5 	bhi.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d13e:	4628      	mov	r0, r5
  21d140:	f65c fee0 	bl	79f04 <_printf_int_dec+0x73>
  21d144:	4629      	mov	r1, r5
  21d146:	4602      	mov	r2, r0
  21d148:	a805      	add	r0, sp, #20
  21d14a:	f695 fbbd 	bl	b28c8 <A30fb2800+0x52b0>
  21d14e:	e6f2      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d150:	0021f9a8 	.word	0x0021f9a8
  21d154:	00220058 	.word	0x00220058
  21d158:	f894 3072 	ldrb.w	r3, [r4, #114]	; 0x72
  21d15c:	2b00      	cmp	r3, #0
  21d15e:	f000 8087 	beq.w	21d270 <wiced_bt_hfp_hf_at_cback+0x3ec>
  21d162:	2100      	movs	r1, #0
  21d164:	f44f 7282 	mov.w	r2, #260	; 0x104
  21d168:	f8a4 107a 	strh.w	r1, [r4, #122]	; 0x7a
  21d16c:	f884 107c 	strb.w	r1, [r4, #124]	; 0x7c
  21d170:	a845      	add	r0, sp, #276	; 0x114
  21d172:	f614 fea7 	bl	31ec4 <memcpy+0x7>
  21d176:	8863      	ldrh	r3, [r4, #2]
  21d178:	f8ad 3114 	strh.w	r3, [sp, #276]	; 0x114
  21d17c:	46a9      	mov	r9, r5
  21d17e:	f815 3b01 	ldrb.w	r3, [r5], #1
  21d182:	2b20      	cmp	r3, #32
  21d184:	d0fa      	beq.n	21d17c <wiced_bt_hfp_hf_at_cback+0x2f8>
  21d186:	f894 1073 	ldrb.w	r1, [r4, #115]	; 0x73
  21d18a:	2201      	movs	r2, #1
  21d18c:	3901      	subs	r1, #1
  21d18e:	4648      	mov	r0, r9
  21d190:	f50d 738d 	add.w	r3, sp, #282	; 0x11a
  21d194:	b2c9      	uxtb	r1, r1
  21d196:	f7ff fcab 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d19a:	b118      	cbz	r0, 21d1a4 <wiced_bt_hfp_hf_at_cback+0x320>
  21d19c:	f89d 311a 	ldrb.w	r3, [sp, #282]	; 0x11a
  21d1a0:	f884 307a 	strb.w	r3, [r4, #122]	; 0x7a
  21d1a4:	f894 1074 	ldrb.w	r1, [r4, #116]	; 0x74
  21d1a8:	2204      	movs	r2, #4
  21d1aa:	3901      	subs	r1, #1
  21d1ac:	4648      	mov	r0, r9
  21d1ae:	ab46      	add	r3, sp, #280	; 0x118
  21d1b0:	b2c9      	uxtb	r1, r1
  21d1b2:	f7ff fc9d 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d1b6:	b118      	cbz	r0, 21d1c0 <wiced_bt_hfp_hf_at_cback+0x33c>
  21d1b8:	f89d 3118 	ldrb.w	r3, [sp, #280]	; 0x118
  21d1bc:	f884 307b 	strb.w	r3, [r4, #123]	; 0x7b
  21d1c0:	f894 1075 	ldrb.w	r1, [r4, #117]	; 0x75
  21d1c4:	2201      	movs	r2, #1
  21d1c6:	3901      	subs	r1, #1
  21d1c8:	4648      	mov	r0, r9
  21d1ca:	f20d 1319 	addw	r3, sp, #281	; 0x119
  21d1ce:	b2c9      	uxtb	r1, r1
  21d1d0:	f7ff fc8e 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d1d4:	b118      	cbz	r0, 21d1de <wiced_bt_hfp_hf_at_cback+0x35a>
  21d1d6:	f89d 3119 	ldrb.w	r3, [sp, #281]	; 0x119
  21d1da:	f884 307c 	strb.w	r3, [r4, #124]	; 0x7c
  21d1de:	4dbe      	ldr	r5, [pc, #760]	; (21d4d8 <wiced_bt_hfp_hf_at_cback+0x654>)
  21d1e0:	2004      	movs	r0, #4
  21d1e2:	f8d5 3204 	ldr.w	r3, [r5, #516]	; 0x204
  21d1e6:	a945      	add	r1, sp, #276	; 0x114
  21d1e8:	4798      	blx	r3
  21d1ea:	f894 1076 	ldrb.w	r1, [r4, #118]	; 0x76
  21d1ee:	2201      	movs	r2, #1
  21d1f0:	3901      	subs	r1, #1
  21d1f2:	4648      	mov	r0, r9
  21d1f4:	ab46      	add	r3, sp, #280	; 0x118
  21d1f6:	b2c9      	uxtb	r1, r1
  21d1f8:	f7ff fc7a 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d1fc:	b140      	cbz	r0, 21d210 <wiced_bt_hfp_hf_at_cback+0x38c>
  21d1fe:	f89d 3118 	ldrb.w	r3, [sp, #280]	; 0x118
  21d202:	2002      	movs	r0, #2
  21d204:	f884 3076 	strb.w	r3, [r4, #118]	; 0x76
  21d208:	f8d5 3204 	ldr.w	r3, [r5, #516]	; 0x204
  21d20c:	a945      	add	r1, sp, #276	; 0x114
  21d20e:	4798      	blx	r3
  21d210:	f894 1077 	ldrb.w	r1, [r4, #119]	; 0x77
  21d214:	2201      	movs	r2, #1
  21d216:	3901      	subs	r1, #1
  21d218:	4648      	mov	r0, r9
  21d21a:	ab46      	add	r3, sp, #280	; 0x118
  21d21c:	b2c9      	uxtb	r1, r1
  21d21e:	f7ff fc67 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d222:	b120      	cbz	r0, 21d22e <wiced_bt_hfp_hf_at_cback+0x3aa>
  21d224:	2003      	movs	r0, #3
  21d226:	f8d5 3204 	ldr.w	r3, [r5, #516]	; 0x204
  21d22a:	a945      	add	r1, sp, #276	; 0x114
  21d22c:	4798      	blx	r3
  21d22e:	f894 1079 	ldrb.w	r1, [r4, #121]	; 0x79
  21d232:	2205      	movs	r2, #5
  21d234:	3901      	subs	r1, #1
  21d236:	4648      	mov	r0, r9
  21d238:	ab46      	add	r3, sp, #280	; 0x118
  21d23a:	b2c9      	uxtb	r1, r1
  21d23c:	f7ff fc58 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d240:	b120      	cbz	r0, 21d24c <wiced_bt_hfp_hf_at_cback+0x3c8>
  21d242:	2007      	movs	r0, #7
  21d244:	f8d5 3204 	ldr.w	r3, [r5, #516]	; 0x204
  21d248:	a945      	add	r1, sp, #276	; 0x114
  21d24a:	4798      	blx	r3
  21d24c:	f894 1078 	ldrb.w	r1, [r4, #120]	; 0x78
  21d250:	2205      	movs	r2, #5
  21d252:	3901      	subs	r1, #1
  21d254:	4648      	mov	r0, r9
  21d256:	ab46      	add	r3, sp, #280	; 0x118
  21d258:	b2c9      	uxtb	r1, r1
  21d25a:	f7ff fc49 	bl	21caf0 <wiced_bt_hfp_check_ind_status>
  21d25e:	2800      	cmp	r0, #0
  21d260:	f43f ae69 	beq.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d264:	2008      	movs	r0, #8
  21d266:	f8d5 3204 	ldr.w	r3, [r5, #516]	; 0x204
  21d26a:	a945      	add	r1, sp, #276	; 0x114
  21d26c:	4798      	blx	r3
  21d26e:	e662      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d270:	2301      	movs	r3, #1
  21d272:	499a      	ldr	r1, [pc, #616]	; (21d4dc <wiced_bt_hfp_hf_at_cback+0x658>)
  21d274:	f884 3072 	strb.w	r3, [r4, #114]	; 0x72
  21d278:	4628      	mov	r0, r5
  21d27a:	f7ff fc59 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d27e:	4998      	ldr	r1, [pc, #608]	; (21d4e0 <wiced_bt_hfp_hf_at_cback+0x65c>)
  21d280:	f884 0073 	strb.w	r0, [r4, #115]	; 0x73
  21d284:	4628      	mov	r0, r5
  21d286:	f7ff fc53 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d28a:	f884 0074 	strb.w	r0, [r4, #116]	; 0x74
  21d28e:	b928      	cbnz	r0, 21d29c <wiced_bt_hfp_hf_at_cback+0x418>
  21d290:	4628      	mov	r0, r5
  21d292:	4994      	ldr	r1, [pc, #592]	; (21d4e4 <wiced_bt_hfp_hf_at_cback+0x660>)
  21d294:	f7ff fc4c 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d298:	f884 0074 	strb.w	r0, [r4, #116]	; 0x74
  21d29c:	4992      	ldr	r1, [pc, #584]	; (21d4e8 <wiced_bt_hfp_hf_at_cback+0x664>)
  21d29e:	4628      	mov	r0, r5
  21d2a0:	f7ff fc46 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d2a4:	4991      	ldr	r1, [pc, #580]	; (21d4ec <wiced_bt_hfp_hf_at_cback+0x668>)
  21d2a6:	f884 0076 	strb.w	r0, [r4, #118]	; 0x76
  21d2aa:	4628      	mov	r0, r5
  21d2ac:	f7ff fc40 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d2b0:	498f      	ldr	r1, [pc, #572]	; (21d4f0 <wiced_bt_hfp_hf_at_cback+0x66c>)
  21d2b2:	f884 0078 	strb.w	r0, [r4, #120]	; 0x78
  21d2b6:	4628      	mov	r0, r5
  21d2b8:	f7ff fc3a 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d2bc:	498d      	ldr	r1, [pc, #564]	; (21d4f4 <wiced_bt_hfp_hf_at_cback+0x670>)
  21d2be:	f884 0075 	strb.w	r0, [r4, #117]	; 0x75
  21d2c2:	4628      	mov	r0, r5
  21d2c4:	f7ff fc34 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d2c8:	498b      	ldr	r1, [pc, #556]	; (21d4f8 <wiced_bt_hfp_hf_at_cback+0x674>)
  21d2ca:	f884 0079 	strb.w	r0, [r4, #121]	; 0x79
  21d2ce:	4628      	mov	r0, r5
  21d2d0:	f7ff fc2e 	bl	21cb30 <wiced_bt_hfp_hf_find_indicator_id>
  21d2d4:	f884 0077 	strb.w	r0, [r4, #119]	; 0x77
  21d2d8:	e62d      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d2da:	f44f 7282 	mov.w	r2, #260	; 0x104
  21d2de:	2100      	movs	r1, #0
  21d2e0:	a845      	add	r0, sp, #276	; 0x114
  21d2e2:	f614 fdef 	bl	31ec4 <memcpy+0x7>
  21d2e6:	8863      	ldrh	r3, [r4, #2]
  21d2e8:	f8ad 3114 	strh.w	r3, [sp, #276]	; 0x114
  21d2ec:	782a      	ldrb	r2, [r5, #0]
  21d2ee:	4629      	mov	r1, r5
  21d2f0:	2a20      	cmp	r2, #32
  21d2f2:	f105 0501 	add.w	r5, r5, #1
  21d2f6:	d0f9      	beq.n	21d2ec <wiced_bt_hfp_hf_at_cback+0x468>
  21d2f8:	3a30      	subs	r2, #48	; 0x30
  21d2fa:	b2d2      	uxtb	r2, r2
  21d2fc:	f811 3f01 	ldrb.w	r3, [r1, #1]!
  21d300:	2b20      	cmp	r3, #32
  21d302:	d0fb      	beq.n	21d2fc <wiced_bt_hfp_hf_at_cback+0x478>
  21d304:	2b2c      	cmp	r3, #44	; 0x2c
  21d306:	d0f9      	beq.n	21d2fc <wiced_bt_hfp_hf_at_cback+0x478>
  21d308:	f894 1073 	ldrb.w	r1, [r4, #115]	; 0x73
  21d30c:	3b30      	subs	r3, #48	; 0x30
  21d30e:	4291      	cmp	r1, r2
  21d310:	b2db      	uxtb	r3, r3
  21d312:	d11d      	bne.n	21d350 <wiced_bt_hfp_hf_at_cback+0x4cc>
  21d314:	2b01      	cmp	r3, #1
  21d316:	d116      	bne.n	21d346 <wiced_bt_hfp_hf_at_cback+0x4c2>
  21d318:	2200      	movs	r2, #0
  21d31a:	f884 207b 	strb.w	r2, [r4, #123]	; 0x7b
  21d31e:	f884 307a 	strb.w	r3, [r4, #122]	; 0x7a
  21d322:	f894 307a 	ldrb.w	r3, [r4, #122]	; 0x7a
  21d326:	2004      	movs	r0, #4
  21d328:	f88d 311a 	strb.w	r3, [sp, #282]	; 0x11a
  21d32c:	f894 307c 	ldrb.w	r3, [r4, #124]	; 0x7c
  21d330:	a945      	add	r1, sp, #276	; 0x114
  21d332:	f88d 3119 	strb.w	r3, [sp, #281]	; 0x119
  21d336:	f894 307b 	ldrb.w	r3, [r4, #123]	; 0x7b
  21d33a:	f88d 3118 	strb.w	r3, [sp, #280]	; 0x118
  21d33e:	4b66      	ldr	r3, [pc, #408]	; (21d4d8 <wiced_bt_hfp_hf_at_cback+0x654>)
  21d340:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21d344:	e792      	b.n	21d26c <wiced_bt_hfp_hf_at_cback+0x3e8>
  21d346:	2b00      	cmp	r3, #0
  21d348:	d1eb      	bne.n	21d322 <wiced_bt_hfp_hf_at_cback+0x49e>
  21d34a:	f884 307c 	strb.w	r3, [r4, #124]	; 0x7c
  21d34e:	e7e6      	b.n	21d31e <wiced_bt_hfp_hf_at_cback+0x49a>
  21d350:	f894 1074 	ldrb.w	r1, [r4, #116]	; 0x74
  21d354:	4291      	cmp	r1, r2
  21d356:	d104      	bne.n	21d362 <wiced_bt_hfp_hf_at_cback+0x4de>
  21d358:	2b04      	cmp	r3, #4
  21d35a:	d8e2      	bhi.n	21d322 <wiced_bt_hfp_hf_at_cback+0x49e>
  21d35c:	f884 307b 	strb.w	r3, [r4, #123]	; 0x7b
  21d360:	e7df      	b.n	21d322 <wiced_bt_hfp_hf_at_cback+0x49e>
  21d362:	f894 1075 	ldrb.w	r1, [r4, #117]	; 0x75
  21d366:	4291      	cmp	r1, r2
  21d368:	d117      	bne.n	21d39a <wiced_bt_hfp_hf_at_cback+0x516>
  21d36a:	b94b      	cbnz	r3, 21d380 <wiced_bt_hfp_hf_at_cback+0x4fc>
  21d36c:	f894 207c 	ldrb.w	r2, [r4, #124]	; 0x7c
  21d370:	1e51      	subs	r1, r2, #1
  21d372:	424a      	negs	r2, r1
  21d374:	414a      	adcs	r2, r1
  21d376:	f884 207a 	strb.w	r2, [r4, #122]	; 0x7a
  21d37a:	f884 307c 	strb.w	r3, [r4, #124]	; 0x7c
  21d37e:	e7d0      	b.n	21d322 <wiced_bt_hfp_hf_at_cback+0x49e>
  21d380:	2b01      	cmp	r3, #1
  21d382:	d102      	bne.n	21d38a <wiced_bt_hfp_hf_at_cback+0x506>
  21d384:	f884 307a 	strb.w	r3, [r4, #122]	; 0x7a
  21d388:	e7f7      	b.n	21d37a <wiced_bt_hfp_hf_at_cback+0x4f6>
  21d38a:	2b02      	cmp	r3, #2
  21d38c:	f47f add3 	bne.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d390:	2300      	movs	r3, #0
  21d392:	f884 307a 	strb.w	r3, [r4, #122]	; 0x7a
  21d396:	2301      	movs	r3, #1
  21d398:	e7ef      	b.n	21d37a <wiced_bt_hfp_hf_at_cback+0x4f6>
  21d39a:	f894 1076 	ldrb.w	r1, [r4, #118]	; 0x76
  21d39e:	4291      	cmp	r1, r2
  21d3a0:	d10a      	bne.n	21d3b8 <wiced_bt_hfp_hf_at_cback+0x534>
  21d3a2:	2b01      	cmp	r3, #1
  21d3a4:	f63f adc7 	bhi.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d3a8:	2002      	movs	r0, #2
  21d3aa:	f88d 3118 	strb.w	r3, [sp, #280]	; 0x118
  21d3ae:	4b4a      	ldr	r3, [pc, #296]	; (21d4d8 <wiced_bt_hfp_hf_at_cback+0x654>)
  21d3b0:	a945      	add	r1, sp, #276	; 0x114
  21d3b2:	f8d3 3204 	ldr.w	r3, [r3, #516]	; 0x204
  21d3b6:	e759      	b.n	21d26c <wiced_bt_hfp_hf_at_cback+0x3e8>
  21d3b8:	f894 1077 	ldrb.w	r1, [r4, #119]	; 0x77
  21d3bc:	4291      	cmp	r1, r2
  21d3be:	d106      	bne.n	21d3ce <wiced_bt_hfp_hf_at_cback+0x54a>
  21d3c0:	2b01      	cmp	r3, #1
  21d3c2:	f63f adb8 	bhi.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d3c6:	2003      	movs	r0, #3
  21d3c8:	f88d 3118 	strb.w	r3, [sp, #280]	; 0x118
  21d3cc:	e7ef      	b.n	21d3ae <wiced_bt_hfp_hf_at_cback+0x52a>
  21d3ce:	f894 1078 	ldrb.w	r1, [r4, #120]	; 0x78
  21d3d2:	4291      	cmp	r1, r2
  21d3d4:	d106      	bne.n	21d3e4 <wiced_bt_hfp_hf_at_cback+0x560>
  21d3d6:	2b05      	cmp	r3, #5
  21d3d8:	f63f adad 	bhi.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d3dc:	2008      	movs	r0, #8
  21d3de:	f88d 3118 	strb.w	r3, [sp, #280]	; 0x118
  21d3e2:	e7e4      	b.n	21d3ae <wiced_bt_hfp_hf_at_cback+0x52a>
  21d3e4:	f894 1079 	ldrb.w	r1, [r4, #121]	; 0x79
  21d3e8:	4291      	cmp	r1, r2
  21d3ea:	f47f ada4 	bne.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d3ee:	2b05      	cmp	r3, #5
  21d3f0:	f63f ada1 	bhi.w	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d3f4:	2007      	movs	r0, #7
  21d3f6:	f88d 3118 	strb.w	r3, [sp, #280]	; 0x118
  21d3fa:	e7d8      	b.n	21d3ae <wiced_bt_hfp_hf_at_cback+0x52a>
  21d3fc:	4628      	mov	r0, r5
  21d3fe:	f000 faba 	bl	21d976 <utl_str2int>
  21d402:	e598      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d404:	4628      	mov	r0, r5
  21d406:	f000 fab6 	bl	21d976 <utl_str2int>
  21d40a:	2300      	movs	r3, #0
  21d40c:	9300      	str	r3, [sp, #0]
  21d40e:	2302      	movs	r3, #2
  21d410:	f88d 0014 	strb.w	r0, [sp, #20]
  21d414:	b2c0      	uxtb	r0, r0
  21d416:	9001      	str	r0, [sp, #4]
  21d418:	461a      	mov	r2, r3
  21d41a:	2114      	movs	r1, #20
  21d41c:	4620      	mov	r0, r4
  21d41e:	f7ff fbbb 	bl	21cb98 <wiced_bt_hfp_hf_at_send_cmd>
  21d422:	e588      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d424:	6ee3      	ldr	r3, [r4, #108]	; 0x6c
  21d426:	071b      	lsls	r3, r3, #28
  21d428:	f57f ad7e 	bpl.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d42c:	8ce3      	ldrh	r3, [r4, #38]	; 0x26
  21d42e:	0758      	lsls	r0, r3, #29
  21d430:	f57f ad7a 	bpl.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d434:	e5a2      	b.n	21cf7c <wiced_bt_hfp_hf_at_cback+0xf8>
  21d436:	4628      	mov	r0, r5
  21d438:	f000 fa9d 	bl	21d976 <utl_str2int>
  21d43c:	b280      	uxth	r0, r0
  21d43e:	84e0      	strh	r0, [r4, #38]	; 0x26
  21d440:	9005      	str	r0, [sp, #20]
  21d442:	e578      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d444:	f894 302b 	ldrb.w	r3, [r4, #43]	; 0x2b
  21d448:	2b08      	cmp	r3, #8
  21d44a:	d157      	bne.n	21d4fc <wiced_bt_hfp_hf_at_cback+0x678>
  21d44c:	f04f 0800 	mov.w	r8, #0
  21d450:	f815 2008 	ldrb.w	r2, [r5, r8]
  21d454:	f1a2 0320 	sub.w	r3, r2, #32
  21d458:	b2db      	uxtb	r3, r3
  21d45a:	2b0c      	cmp	r3, #12
  21d45c:	d930      	bls.n	21d4c0 <wiced_bt_hfp_hf_at_cback+0x63c>
  21d45e:	2a00      	cmp	r2, #0
  21d460:	f43f ad62 	beq.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d464:	2100      	movs	r1, #0
  21d466:	2206      	movs	r2, #6
  21d468:	a845      	add	r0, sp, #276	; 0x114
  21d46a:	f614 fd2b 	bl	31ec4 <memcpy+0x7>
  21d46e:	ab45      	add	r3, sp, #276	; 0x114
  21d470:	f108 0105 	add.w	r1, r8, #5
  21d474:	f1b8 0ffe 	cmp.w	r8, #254	; 0xfe
  21d478:	dc0b      	bgt.n	21d492 <wiced_bt_hfp_hf_at_cback+0x60e>
  21d47a:	f815 2008 	ldrb.w	r2, [r5, r8]
  21d47e:	f1a2 0030 	sub.w	r0, r2, #48	; 0x30
  21d482:	2809      	cmp	r0, #9
  21d484:	d805      	bhi.n	21d492 <wiced_bt_hfp_hf_at_cback+0x60e>
  21d486:	f108 0801 	add.w	r8, r8, #1
  21d48a:	4588      	cmp	r8, r1
  21d48c:	f803 2b01 	strb.w	r2, [r3], #1
  21d490:	d1f0      	bne.n	21d474 <wiced_bt_hfp_hf_at_cback+0x5f0>
  21d492:	a845      	add	r0, sp, #276	; 0x114
  21d494:	f000 fa4a 	bl	21d92c <wiced_bt_hfp_hf_utils_str2uint16>
  21d498:	b200      	sxth	r0, r0
  21d49a:	2800      	cmp	r0, #0
  21d49c:	f6ff ad44 	blt.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d4a0:	2100      	movs	r1, #0
  21d4a2:	f04f 0e0c 	mov.w	lr, #12
  21d4a6:	f104 0c80 	add.w	ip, r4, #128	; 0x80
  21d4aa:	fb0e f301 	mul.w	r3, lr, r1
  21d4ae:	f85c 2003 	ldr.w	r2, [ip, r3]
  21d4b2:	2a00      	cmp	r2, #0
  21d4b4:	f000 809c 	beq.w	21d5f0 <wiced_bt_hfp_hf_at_cback+0x76c>
  21d4b8:	3101      	adds	r1, #1
  21d4ba:	290a      	cmp	r1, #10
  21d4bc:	d1f5      	bne.n	21d4aa <wiced_bt_hfp_hf_at_cback+0x626>
  21d4be:	e533      	b.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d4c0:	f241 3201 	movw	r2, #4865	; 0x1301
  21d4c4:	fa22 f303 	lsr.w	r3, r2, r3
  21d4c8:	07d9      	lsls	r1, r3, #31
  21d4ca:	d5cb      	bpl.n	21d464 <wiced_bt_hfp_hf_at_cback+0x5e0>
  21d4cc:	f108 0801 	add.w	r8, r8, #1
  21d4d0:	f1b8 0ffe 	cmp.w	r8, #254	; 0xfe
  21d4d4:	ddbc      	ble.n	21d450 <wiced_bt_hfp_hf_at_cback+0x5cc>
  21d4d6:	e527      	b.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d4d8:	00220058 	.word	0x00220058
  21d4dc:	0021f7c9 	.word	0x0021f7c9
  21d4e0:	0021f7ce 	.word	0x0021f7ce
  21d4e4:	0021f7d8 	.word	0x0021f7d8
  21d4e8:	0021f7e3 	.word	0x0021f7e3
  21d4ec:	0021f7eb 	.word	0x0021f7eb
  21d4f0:	0021f7f3 	.word	0x0021f7f3
  21d4f4:	0021f7fc 	.word	0x0021f7fc
  21d4f8:	0021f803 	.word	0x0021f803
  21d4fc:	f04f 0b00 	mov.w	fp, #0
  21d500:	f815 200b 	ldrb.w	r2, [r5, fp]
  21d504:	2a20      	cmp	r2, #32
  21d506:	d137      	bne.n	21d578 <wiced_bt_hfp_hf_at_cback+0x6f4>
  21d508:	f10b 0b01 	add.w	fp, fp, #1
  21d50c:	f1bb 0fff 	cmp.w	fp, #255	; 0xff
  21d510:	d1f6      	bne.n	21d500 <wiced_bt_hfp_hf_at_cback+0x67c>
  21d512:	2206      	movs	r2, #6
  21d514:	2100      	movs	r1, #0
  21d516:	a845      	add	r0, sp, #276	; 0x114
  21d518:	f614 fcd4 	bl	31ec4 <memcpy+0x7>
  21d51c:	ab45      	add	r3, sp, #276	; 0x114
  21d51e:	461a      	mov	r2, r3
  21d520:	9302      	str	r3, [sp, #8]
  21d522:	f10b 0005 	add.w	r0, fp, #5
  21d526:	f1bb 0fff 	cmp.w	fp, #255	; 0xff
  21d52a:	d00c      	beq.n	21d546 <wiced_bt_hfp_hf_at_cback+0x6c2>
  21d52c:	f815 100b 	ldrb.w	r1, [r5, fp]
  21d530:	f1a1 0c30 	sub.w	ip, r1, #48	; 0x30
  21d534:	f1bc 0f09 	cmp.w	ip, #9
  21d538:	d805      	bhi.n	21d546 <wiced_bt_hfp_hf_at_cback+0x6c2>
  21d53a:	f10b 0b01 	add.w	fp, fp, #1
  21d53e:	4583      	cmp	fp, r0
  21d540:	f802 1b01 	strb.w	r1, [r2], #1
  21d544:	d1ef      	bne.n	21d526 <wiced_bt_hfp_hf_at_cback+0x6a2>
  21d546:	a845      	add	r0, sp, #276	; 0x114
  21d548:	f000 f9f0 	bl	21d92c <wiced_bt_hfp_hf_utils_str2uint16>
  21d54c:	b202      	sxth	r2, r0
  21d54e:	2a00      	cmp	r2, #0
  21d550:	4681      	mov	r9, r0
  21d552:	f6ff ace9 	blt.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d556:	f04f 0a00 	mov.w	sl, #0
  21d55a:	200c      	movs	r0, #12
  21d55c:	f104 0180 	add.w	r1, r4, #128	; 0x80
  21d560:	fb00 fc0a 	mul.w	ip, r0, sl
  21d564:	f851 300c 	ldr.w	r3, [r1, ip]
  21d568:	429a      	cmp	r2, r3
  21d56a:	d010      	beq.n	21d58e <wiced_bt_hfp_hf_at_cback+0x70a>
  21d56c:	f10a 0a01 	add.w	sl, sl, #1
  21d570:	f1ba 0f0a 	cmp.w	sl, #10
  21d574:	d1f4      	bne.n	21d560 <wiced_bt_hfp_hf_at_cback+0x6dc>
  21d576:	e4d7      	b.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d578:	2a00      	cmp	r2, #0
  21d57a:	d1ca      	bne.n	21d512 <wiced_bt_hfp_hf_at_cback+0x68e>
  21d57c:	e4d4      	b.n	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d57e:	f815 200b 	ldrb.w	r2, [r5, fp]
  21d582:	2a20      	cmp	r2, #32
  21d584:	d001      	beq.n	21d58a <wiced_bt_hfp_hf_at_cback+0x706>
  21d586:	2a2c      	cmp	r2, #44	; 0x2c
  21d588:	d105      	bne.n	21d596 <wiced_bt_hfp_hf_at_cback+0x712>
  21d58a:	f10b 0b01 	add.w	fp, fp, #1
  21d58e:	f1bb 0ffe 	cmp.w	fp, #254	; 0xfe
  21d592:	ddf4      	ble.n	21d57e <wiced_bt_hfp_hf_at_cback+0x6fa>
  21d594:	e002      	b.n	21d59c <wiced_bt_hfp_hf_at_cback+0x718>
  21d596:	2a00      	cmp	r2, #0
  21d598:	f43f acc6 	beq.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d59c:	2100      	movs	r1, #0
  21d59e:	2206      	movs	r2, #6
  21d5a0:	a845      	add	r0, sp, #276	; 0x114
  21d5a2:	f614 fc8f 	bl	31ec4 <memcpy+0x7>
  21d5a6:	f10b 0105 	add.w	r1, fp, #5
  21d5aa:	f1bb 0ffe 	cmp.w	fp, #254	; 0xfe
  21d5ae:	dc05      	bgt.n	21d5bc <wiced_bt_hfp_hf_at_cback+0x738>
  21d5b0:	f815 200b 	ldrb.w	r2, [r5, fp]
  21d5b4:	f1a2 0030 	sub.w	r0, r2, #48	; 0x30
  21d5b8:	2809      	cmp	r0, #9
  21d5ba:	d910      	bls.n	21d5de <wiced_bt_hfp_hf_at_cback+0x75a>
  21d5bc:	a845      	add	r0, sp, #276	; 0x114
  21d5be:	f000 f9b5 	bl	21d92c <wiced_bt_hfp_hf_utils_str2uint16>
  21d5c2:	0402      	lsls	r2, r0, #16
  21d5c4:	f53f acb0 	bmi.w	21cf28 <wiced_bt_hfp_hf_at_cback+0xa4>
  21d5c8:	220c      	movs	r2, #12
  21d5ca:	fb02 4a0a 	mla	sl, r2, sl, r4
  21d5ce:	b2c3      	uxtb	r3, r0
  21d5d0:	f88a 3084 	strb.w	r3, [sl, #132]	; 0x84
  21d5d4:	f88d 9014 	strb.w	r9, [sp, #20]
  21d5d8:	f88d 3015 	strb.w	r3, [sp, #21]
  21d5dc:	e4ab      	b.n	21cf36 <wiced_bt_hfp_hf_at_cback+0xb2>
  21d5de:	9b02      	ldr	r3, [sp, #8]
  21d5e0:	f10b 0b01 	add.w	fp, fp, #1
  21d5e4:	f803 2b01 	strb.w	r2, [r3], #1
  21d5e8:	458b      	cmp	fp, r1
  21d5ea:	9302      	str	r3, [sp, #8]
  21d5ec:	d1dd      	bne.n	21d5aa <wiced_bt_hfp_hf_at_cback+0x726>
  21d5ee:	e7e5      	b.n	21d5bc <wiced_bt_hfp_hf_at_cback+0x738>
  21d5f0:	4423      	add	r3, r4
  21d5f2:	f8c3 0080 	str.w	r0, [r3, #128]	; 0x80
  21d5f6:	e769      	b.n	21d4cc <wiced_bt_hfp_hf_at_cback+0x648>

0021d5f8 <wiced_bt_hfp_hf_at_err_cback>:
  21d5f8:	4770      	bx	lr
	...

0021d5fc <wiced_bt_hfp_hf_api_deinit>:
  21d5fc:	2200      	movs	r2, #0
  21d5fe:	4b01      	ldr	r3, [pc, #4]	; (21d604 <wiced_bt_hfp_hf_api_deinit+0x8>)
  21d600:	701a      	strb	r2, [r3, #0]
  21d602:	4770      	bx	lr
  21d604:	00220058 	.word	0x00220058

0021d608 <wiced_bt_hfp_hf_scb_alloc>:
  21d608:	b570      	push	{r4, r5, r6, lr}
  21d60a:	4c0c      	ldr	r4, [pc, #48]	; (21d63c <wiced_bt_hfp_hf_scb_alloc+0x34>)
  21d60c:	7925      	ldrb	r5, [r4, #4]
  21d60e:	b11d      	cbz	r5, 21d618 <wiced_bt_hfp_hf_scb_alloc+0x10>
  21d610:	f894 30fc 	ldrb.w	r3, [r4, #252]	; 0xfc
  21d614:	b983      	cbnz	r3, 21d638 <wiced_bt_hfp_hf_scb_alloc+0x30>
  21d616:	2501      	movs	r5, #1
  21d618:	22f8      	movs	r2, #248	; 0xf8
  21d61a:	4355      	muls	r5, r2
  21d61c:	1d2e      	adds	r6, r5, #4
  21d61e:	4426      	add	r6, r4
  21d620:	2100      	movs	r1, #0
  21d622:	4630      	mov	r0, r6
  21d624:	f614 fc4e 	bl	31ec4 <memcpy+0x7>
  21d628:	4630      	mov	r0, r6
  21d62a:	f7ff f98d 	bl	21c948 <wiced_bt_hfp_hf_at_cmd_queue_init>
  21d62e:	2301      	movs	r3, #1
  21d630:	442c      	add	r4, r5
  21d632:	7123      	strb	r3, [r4, #4]
  21d634:	4630      	mov	r0, r6
  21d636:	bd70      	pop	{r4, r5, r6, pc}
  21d638:	2600      	movs	r6, #0
  21d63a:	e7fb      	b.n	21d634 <wiced_bt_hfp_hf_scb_alloc+0x2c>
  21d63c:	00220058 	.word	0x00220058

0021d640 <wiced_bt_hfp_hf_get_scb_by_handle>:
  21d640:	4602      	mov	r2, r0
  21d642:	480c      	ldr	r0, [pc, #48]	; (21d674 <wiced_bt_hfp_hf_get_scb_by_handle+0x34>)
  21d644:	7903      	ldrb	r3, [r0, #4]
  21d646:	2b01      	cmp	r3, #1
  21d648:	4603      	mov	r3, r0
  21d64a:	d102      	bne.n	21d652 <wiced_bt_hfp_hf_get_scb_by_handle+0x12>
  21d64c:	88c1      	ldrh	r1, [r0, #6]
  21d64e:	4291      	cmp	r1, r2
  21d650:	d00c      	beq.n	21d66c <wiced_bt_hfp_hf_get_scb_by_handle+0x2c>
  21d652:	f893 00fc 	ldrb.w	r0, [r3, #252]	; 0xfc
  21d656:	2801      	cmp	r0, #1
  21d658:	d10a      	bne.n	21d670 <wiced_bt_hfp_hf_get_scb_by_handle+0x30>
  21d65a:	f8b3 10fe 	ldrh.w	r1, [r3, #254]	; 0xfe
  21d65e:	4291      	cmp	r1, r2
  21d660:	d106      	bne.n	21d670 <wiced_bt_hfp_hf_get_scb_by_handle+0x30>
  21d662:	22f8      	movs	r2, #248	; 0xf8
  21d664:	fb02 3000 	mla	r0, r2, r0, r3
  21d668:	3004      	adds	r0, #4
  21d66a:	4770      	bx	lr
  21d66c:	2000      	movs	r0, #0
  21d66e:	e7f8      	b.n	21d662 <wiced_bt_hfp_hf_get_scb_by_handle+0x22>
  21d670:	2000      	movs	r0, #0
  21d672:	4770      	bx	lr
  21d674:	00220058 	.word	0x00220058

0021d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>:
  21d678:	b570      	push	{r4, r5, r6, lr}
  21d67a:	4d0f      	ldr	r5, [pc, #60]	; (21d6b8 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x40>)
  21d67c:	4606      	mov	r6, r0
  21d67e:	792b      	ldrb	r3, [r5, #4]
  21d680:	2b01      	cmp	r3, #1
  21d682:	d00a      	beq.n	21d69a <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x22>
  21d684:	f895 40fc 	ldrb.w	r4, [r5, #252]	; 0xfc
  21d688:	2c01      	cmp	r4, #1
  21d68a:	d112      	bne.n	21d6b2 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x3a>
  21d68c:	4631      	mov	r1, r6
  21d68e:	480b      	ldr	r0, [pc, #44]	; (21d6bc <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x44>)
  21d690:	f000 f9d2 	bl	21da38 <utl_bdcmp>
  21d694:	b968      	cbnz	r0, 21d6b2 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x3a>
  21d696:	4620      	mov	r0, r4
  21d698:	e006      	b.n	21d6a8 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x30>
  21d69a:	4601      	mov	r1, r0
  21d69c:	f105 0008 	add.w	r0, r5, #8
  21d6a0:	f000 f9ca 	bl	21da38 <utl_bdcmp>
  21d6a4:	2800      	cmp	r0, #0
  21d6a6:	d1ed      	bne.n	21d684 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0xc>
  21d6a8:	23f8      	movs	r3, #248	; 0xf8
  21d6aa:	fb03 5000 	mla	r0, r3, r0, r5
  21d6ae:	3004      	adds	r0, #4
  21d6b0:	e000      	b.n	21d6b4 <wiced_bt_hfp_hf_get_scb_by_bd_addr+0x3c>
  21d6b2:	2000      	movs	r0, #0
  21d6b4:	bd70      	pop	{r4, r5, r6, pc}
  21d6b6:	bf00      	nop
  21d6b8:	00220058 	.word	0x00220058
  21d6bc:	00220158 	.word	0x00220158

0021d6c0 <wiced_bt_hfp_hf_get_uuid_by_handle>:
  21d6c0:	4602      	mov	r2, r0
  21d6c2:	4b05      	ldr	r3, [pc, #20]	; (21d6d8 <wiced_bt_hfp_hf_get_uuid_by_handle+0x18>)
  21d6c4:	8819      	ldrh	r1, [r3, #0]
  21d6c6:	4281      	cmp	r1, r0
  21d6c8:	88d9      	ldrh	r1, [r3, #6]
  21d6ca:	bf0c      	ite	eq
  21d6cc:	8898      	ldrheq	r0, [r3, #4]
  21d6ce:	2000      	movne	r0, #0
  21d6d0:	4291      	cmp	r1, r2
  21d6d2:	bf08      	it	eq
  21d6d4:	8958      	ldrheq	r0, [r3, #10]
  21d6d6:	4770      	bx	lr
  21d6d8:	00220268 	.word	0x00220268

0021d6dc <wiced_bt_hfp_hf_get_uuid_by_scn>:
  21d6dc:	4602      	mov	r2, r0
  21d6de:	4b05      	ldr	r3, [pc, #20]	; (21d6f4 <wiced_bt_hfp_hf_get_uuid_by_scn+0x18>)
  21d6e0:	7899      	ldrb	r1, [r3, #2]
  21d6e2:	4281      	cmp	r1, r0
  21d6e4:	7a19      	ldrb	r1, [r3, #8]
  21d6e6:	bf0c      	ite	eq
  21d6e8:	8898      	ldrheq	r0, [r3, #4]
  21d6ea:	2000      	movne	r0, #0
  21d6ec:	4291      	cmp	r1, r2
  21d6ee:	bf08      	it	eq
  21d6f0:	8958      	ldrheq	r0, [r3, #10]
  21d6f2:	4770      	bx	lr
  21d6f4:	00220268 	.word	0x00220268

0021d6f8 <wiced_bt_hfp_hf_scb_dealloc>:
  21d6f8:	b538      	push	{r3, r4, r5, lr}
  21d6fa:	4604      	mov	r4, r0
  21d6fc:	2500      	movs	r5, #0
  21d6fe:	f7ff f937 	bl	21c970 <wiced_bt_hfp_hf_at_cmd_queue_free>
  21d702:	69e0      	ldr	r0, [r4, #28]
  21d704:	7025      	strb	r5, [r4, #0]
  21d706:	b110      	cbz	r0, 21d70e <wiced_bt_hfp_hf_scb_dealloc+0x16>
  21d708:	f6b3 fd64 	bl	d11d4 <wiced_bt_free_buffer>
  21d70c:	61e5      	str	r5, [r4, #28]
  21d70e:	bd38      	pop	{r3, r4, r5, pc}

0021d710 <wiced_bt_hfp_set_rfcomm_server_data>:
  21d710:	4b06      	ldr	r3, [pc, #24]	; (21d72c <wiced_bt_hfp_set_rfcomm_server_data+0x1c>)
  21d712:	b530      	push	{r4, r5, lr}
  21d714:	881c      	ldrh	r4, [r3, #0]
  21d716:	b114      	cbz	r4, 21d71e <wiced_bt_hfp_set_rfcomm_server_data+0xe>
  21d718:	88dc      	ldrh	r4, [r3, #6]
  21d71a:	b934      	cbnz	r4, 21d72a <wiced_bt_hfp_set_rfcomm_server_data+0x1a>
  21d71c:	2401      	movs	r4, #1
  21d71e:	2506      	movs	r5, #6
  21d720:	436c      	muls	r4, r5
  21d722:	191d      	adds	r5, r3, r4
  21d724:	5318      	strh	r0, [r3, r4]
  21d726:	70a9      	strb	r1, [r5, #2]
  21d728:	80aa      	strh	r2, [r5, #4]
  21d72a:	bd30      	pop	{r4, r5, pc}
  21d72c:	00220268 	.word	0x00220268

0021d730 <wiced_bt_hfp_close_rfcomm_server_port>:
  21d730:	b538      	push	{r3, r4, r5, lr}
  21d732:	4c10      	ldr	r4, [pc, #64]	; (21d774 <wiced_bt_hfp_close_rfcomm_server_port+0x44>)
  21d734:	8825      	ldrh	r5, [r4, #0]
  21d736:	b145      	cbz	r5, 21d74a <wiced_bt_hfp_close_rfcomm_server_port+0x1a>
  21d738:	4628      	mov	r0, r5
  21d73a:	f7ff ff81 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21d73e:	b920      	cbnz	r0, 21d74a <wiced_bt_hfp_close_rfcomm_server_port+0x1a>
  21d740:	2101      	movs	r1, #1
  21d742:	4628      	mov	r0, r5
  21d744:	f6bb fcce 	bl	d90e4 <wiced_bt_app_install_patch+0x4f5>
  21d748:	b158      	cbz	r0, 21d762 <wiced_bt_hfp_close_rfcomm_server_port+0x32>
  21d74a:	88e5      	ldrh	r5, [r4, #6]
  21d74c:	b17d      	cbz	r5, 21d76e <wiced_bt_hfp_close_rfcomm_server_port+0x3e>
  21d74e:	4628      	mov	r0, r5
  21d750:	f7ff ff76 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21d754:	b958      	cbnz	r0, 21d76e <wiced_bt_hfp_close_rfcomm_server_port+0x3e>
  21d756:	2101      	movs	r1, #1
  21d758:	4628      	mov	r0, r5
  21d75a:	f6bb fcc3 	bl	d90e4 <wiced_bt_app_install_patch+0x4f5>
  21d75e:	b930      	cbnz	r0, 21d76e <wiced_bt_hfp_close_rfcomm_server_port+0x3e>
  21d760:	2001      	movs	r0, #1
  21d762:	2306      	movs	r3, #6
  21d764:	4358      	muls	r0, r3
  21d766:	2300      	movs	r3, #0
  21d768:	5223      	strh	r3, [r4, r0]
  21d76a:	2001      	movs	r0, #1
  21d76c:	bd38      	pop	{r3, r4, r5, pc}
  21d76e:	2000      	movs	r0, #0
  21d770:	e7fc      	b.n	21d76c <wiced_bt_hfp_close_rfcomm_server_port+0x3c>
  21d772:	bf00      	nop
  21d774:	00220268 	.word	0x00220268

0021d778 <wiced_bt_hfp_open_rfcomm_server_port>:
  21d778:	b508      	push	{r3, lr}
  21d77a:	4b08      	ldr	r3, [pc, #32]	; (21d79c <wiced_bt_hfp_open_rfcomm_server_port+0x24>)
  21d77c:	8819      	ldrh	r1, [r3, #0]
  21d77e:	b111      	cbz	r1, 21d786 <wiced_bt_hfp_open_rfcomm_server_port+0xe>
  21d780:	88da      	ldrh	r2, [r3, #6]
  21d782:	b94a      	cbnz	r2, 21d798 <wiced_bt_hfp_open_rfcomm_server_port+0x20>
  21d784:	2101      	movs	r1, #1
  21d786:	2206      	movs	r2, #6
  21d788:	fb02 3101 	mla	r1, r2, r1, r3
  21d78c:	4804      	ldr	r0, [pc, #16]	; (21d7a0 <wiced_bt_hfp_open_rfcomm_server_port+0x28>)
  21d78e:	3102      	adds	r1, #2
  21d790:	f672 fd2e 	bl	901f0 <wiced_app_event_serialize>
  21d794:	2001      	movs	r0, #1
  21d796:	bd08      	pop	{r3, pc}
  21d798:	2000      	movs	r0, #0
  21d79a:	e7fc      	b.n	21d796 <wiced_bt_hfp_open_rfcomm_server_port+0x1e>
  21d79c:	00220268 	.word	0x00220268
  21d7a0:	0021d819 	.word	0x0021d819

0021d7a4 <wiced_bt_hfp_hf_register>:
  21d7a4:	2300      	movs	r3, #0
  21d7a6:	b570      	push	{r4, r5, r6, lr}
  21d7a8:	460e      	mov	r6, r1
  21d7aa:	b086      	sub	sp, #24
  21d7ac:	f8ad 3016 	strh.w	r3, [sp, #22]
  21d7b0:	4b16      	ldr	r3, [pc, #88]	; (21d80c <wiced_bt_hfp_hf_register+0x68>)
  21d7b2:	4605      	mov	r5, r0
  21d7b4:	9302      	str	r3, [sp, #8]
  21d7b6:	f10d 0316 	add.w	r3, sp, #22
  21d7ba:	9301      	str	r3, [sp, #4]
  21d7bc:	4b14      	ldr	r3, [pc, #80]	; (21d810 <wiced_bt_hfp_hf_register+0x6c>)
  21d7be:	4601      	mov	r1, r0
  21d7c0:	9300      	str	r3, [sp, #0]
  21d7c2:	2201      	movs	r2, #1
  21d7c4:	23ff      	movs	r3, #255	; 0xff
  21d7c6:	4630      	mov	r0, r6
  21d7c8:	f6bb fdb6 	bl	d9338 <wiced_bt_rfcomm_control_callback+0x3d>
  21d7cc:	b110      	cbz	r0, 21d7d4 <wiced_bt_hfp_hf_register+0x30>
  21d7ce:	b280      	uxth	r0, r0
  21d7d0:	b006      	add	sp, #24
  21d7d2:	bd70      	pop	{r4, r5, r6, pc}
  21d7d4:	490f      	ldr	r1, [pc, #60]	; (21d814 <wiced_bt_hfp_hf_register+0x70>)
  21d7d6:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21d7da:	f6bb fc8a 	bl	d90f2 <wiced_bt_rfcomm_set_event_callback+0x3>
  21d7de:	4604      	mov	r4, r0
  21d7e0:	2101      	movs	r1, #1
  21d7e2:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21d7e6:	b11c      	cbz	r4, 21d7f0 <wiced_bt_hfp_hf_register+0x4c>
  21d7e8:	f6bb fc7c 	bl	d90e4 <wiced_bt_app_install_patch+0x4f5>
  21d7ec:	b2a0      	uxth	r0, r4
  21d7ee:	e7ef      	b.n	21d7d0 <wiced_bt_hfp_hf_register+0x2c>
  21d7f0:	f6bb fc81 	bl	d90f6 <wiced_bt_rfcomm_set_data_callback+0x3>
  21d7f4:	4604      	mov	r4, r0
  21d7f6:	f8bd 0016 	ldrh.w	r0, [sp, #22]
  21d7fa:	b10c      	cbz	r4, 21d800 <wiced_bt_hfp_hf_register+0x5c>
  21d7fc:	2101      	movs	r1, #1
  21d7fe:	e7f3      	b.n	21d7e8 <wiced_bt_hfp_hf_register+0x44>
  21d800:	4632      	mov	r2, r6
  21d802:	4629      	mov	r1, r5
  21d804:	f7ff ff84 	bl	21d710 <wiced_bt_hfp_set_rfcomm_server_data>
  21d808:	4620      	mov	r0, r4
  21d80a:	e7e1      	b.n	21d7d0 <wiced_bt_hfp_hf_register+0x2c>
  21d80c:	0021c221 	.word	0x0021c221
  21d810:	0021fa78 	.word	0x0021fa78
  21d814:	0021c371 	.word	0x0021c371

0021d818 <wiced_bt_hfp_serialize_hf_register>:
  21d818:	b510      	push	{r4, lr}
  21d81a:	b138      	cbz	r0, 21d82c <wiced_bt_hfp_serialize_hf_register+0x14>
  21d81c:	7804      	ldrb	r4, [r0, #0]
  21d81e:	4620      	mov	r0, r4
  21d820:	f7ff ff5c 	bl	21d6dc <wiced_bt_hfp_hf_get_uuid_by_scn>
  21d824:	4601      	mov	r1, r0
  21d826:	4620      	mov	r0, r4
  21d828:	f7ff ffbc 	bl	21d7a4 <wiced_bt_hfp_hf_register>
  21d82c:	2000      	movs	r0, #0
  21d82e:	bd10      	pop	{r4, pc}

0021d830 <wiced_bt_hfp_hf_hdl_event>:
  21d830:	b538      	push	{r3, r4, r5, lr}
  21d832:	7803      	ldrb	r3, [r0, #0]
  21d834:	4605      	mov	r5, r0
  21d836:	1e5a      	subs	r2, r3, #1
  21d838:	2a0c      	cmp	r2, #12
  21d83a:	d803      	bhi.n	21d844 <wiced_bt_hfp_hf_hdl_event+0x14>
  21d83c:	2b01      	cmp	r3, #1
  21d83e:	d103      	bne.n	21d848 <wiced_bt_hfp_hf_hdl_event+0x18>
  21d840:	f7ff fedc 	bl	21d5fc <wiced_bt_hfp_hf_api_deinit>
  21d844:	2001      	movs	r0, #1
  21d846:	bd38      	pop	{r3, r4, r5, pc}
  21d848:	3b02      	subs	r3, #2
  21d84a:	2b0a      	cmp	r3, #10
  21d84c:	d8fa      	bhi.n	21d844 <wiced_bt_hfp_hf_hdl_event+0x14>
  21d84e:	e8df f003 	tbb	[pc, r3]
  21d852:	2306      	.short	0x2306
  21d854:	1d232323 	.word	0x1d232323
  21d858:	161d1d21 	.word	0x161d1d21
  21d85c:	16          	.byte	0x16
  21d85d:	00          	.byte	0x00
  21d85e:	f7ff fed3 	bl	21d608 <wiced_bt_hfp_hf_scb_alloc>
  21d862:	4604      	mov	r4, r0
  21d864:	2800      	cmp	r0, #0
  21d866:	d0ed      	beq.n	21d844 <wiced_bt_hfp_hf_hdl_event+0x14>
  21d868:	f105 0108 	add.w	r1, r5, #8
  21d86c:	3004      	adds	r0, #4
  21d86e:	f000 f8da 	bl	21da26 <utl_bdcpy>
  21d872:	4629      	mov	r1, r5
  21d874:	4620      	mov	r0, r4
  21d876:	782a      	ldrb	r2, [r5, #0]
  21d878:	f000 f812 	bl	21d8a0 <wiced_bt_hfp_hf_ssm_execute>
  21d87c:	e7e2      	b.n	21d844 <wiced_bt_hfp_hf_hdl_event+0x14>
  21d87e:	4807      	ldr	r0, [pc, #28]	; (21d89c <wiced_bt_hfp_hf_hdl_event+0x6c>)
  21d880:	f7ff fefa 	bl	21d678 <wiced_bt_hfp_hf_get_scb_by_bd_addr>
  21d884:	4604      	mov	r4, r0
  21d886:	2800      	cmp	r0, #0
  21d888:	d1f3      	bne.n	21d872 <wiced_bt_hfp_hf_hdl_event+0x42>
  21d88a:	e7db      	b.n	21d844 <wiced_bt_hfp_hf_hdl_event+0x14>
  21d88c:	88c0      	ldrh	r0, [r0, #6]
  21d88e:	f7ff fed7 	bl	21d640 <wiced_bt_hfp_hf_get_scb_by_handle>
  21d892:	e7f7      	b.n	21d884 <wiced_bt_hfp_hf_hdl_event+0x54>
  21d894:	3008      	adds	r0, #8
  21d896:	e7f3      	b.n	21d880 <wiced_bt_hfp_hf_hdl_event+0x50>
  21d898:	8900      	ldrh	r0, [r0, #8]
  21d89a:	e7f8      	b.n	21d88e <wiced_bt_hfp_hf_hdl_event+0x5e>
  21d89c:	00220260 	.word	0x00220260

0021d8a0 <wiced_bt_hfp_hf_ssm_execute>:
  21d8a0:	b4f0      	push	{r4, r5, r6, r7}
  21d8a2:	b158      	cbz	r0, 21d8bc <wiced_bt_hfp_hf_ssm_execute+0x1c>
  21d8a4:	f890 4023 	ldrb.w	r4, [r0, #35]	; 0x23
  21d8a8:	4b10      	ldr	r3, [pc, #64]	; (21d8ec <wiced_bt_hfp_hf_ssm_execute+0x4c>)
  21d8aa:	eb03 05c4 	add.w	r5, r3, r4, lsl #3
  21d8ae:	f853 7034 	ldr.w	r7, [r3, r4, lsl #3]
  21d8b2:	792c      	ldrb	r4, [r5, #4]
  21d8b4:	2500      	movs	r5, #0
  21d8b6:	3c01      	subs	r4, #1
  21d8b8:	42ac      	cmp	r4, r5
  21d8ba:	da01      	bge.n	21d8c0 <wiced_bt_hfp_hf_ssm_execute+0x20>
  21d8bc:	bcf0      	pop	{r4, r5, r6, r7}
  21d8be:	4770      	bx	lr
  21d8c0:	1b63      	subs	r3, r4, r5
  21d8c2:	eb05 0363 	add.w	r3, r5, r3, asr #1
  21d8c6:	f817 c033 	ldrb.w	ip, [r7, r3, lsl #3]
  21d8ca:	eb07 06c3 	add.w	r6, r7, r3, lsl #3
  21d8ce:	4562      	cmp	r2, ip
  21d8d0:	d004      	beq.n	21d8dc <wiced_bt_hfp_hf_ssm_execute+0x3c>
  21d8d2:	bf8c      	ite	hi
  21d8d4:	1c5d      	addhi	r5, r3, #1
  21d8d6:	f103 34ff 	addls.w	r4, r3, #4294967295
  21d8da:	e7ed      	b.n	21d8b8 <wiced_bt_hfp_hf_ssm_execute+0x18>
  21d8dc:	6873      	ldr	r3, [r6, #4]
  21d8de:	7872      	ldrb	r2, [r6, #1]
  21d8e0:	f880 2023 	strb.w	r2, [r0, #35]	; 0x23
  21d8e4:	2b00      	cmp	r3, #0
  21d8e6:	d0e9      	beq.n	21d8bc <wiced_bt_hfp_hf_ssm_execute+0x1c>
  21d8e8:	bcf0      	pop	{r4, r5, r6, r7}
  21d8ea:	4718      	bx	r3
  21d8ec:	0021e260 	.word	0x0021e260

0021d8f0 <wiced_bt_hfp_hf_init_state_machine>:
  21d8f0:	2100      	movs	r1, #0
  21d8f2:	b5f0      	push	{r4, r5, r6, r7, lr}
  21d8f4:	4a0c      	ldr	r2, [pc, #48]	; (21d928 <wiced_bt_hfp_hf_init_state_machine+0x38>)
  21d8f6:	2301      	movs	r3, #1
  21d8f8:	6810      	ldr	r0, [r2, #0]
  21d8fa:	7914      	ldrb	r4, [r2, #4]
  21d8fc:	f1a0 0508 	sub.w	r5, r0, #8
  21d900:	429c      	cmp	r4, r3
  21d902:	dc06      	bgt.n	21d912 <wiced_bt_hfp_hf_init_state_machine+0x22>
  21d904:	3101      	adds	r1, #1
  21d906:	2904      	cmp	r1, #4
  21d908:	f102 0208 	add.w	r2, r2, #8
  21d90c:	d1f3      	bne.n	21d8f6 <wiced_bt_hfp_hf_init_state_machine+0x6>
  21d90e:	2000      	movs	r0, #0
  21d910:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21d912:	f810 7033 	ldrb.w	r7, [r0, r3, lsl #3]
  21d916:	f815 6033 	ldrb.w	r6, [r5, r3, lsl #3]
  21d91a:	42b7      	cmp	r7, r6
  21d91c:	d301      	bcc.n	21d922 <wiced_bt_hfp_hf_init_state_machine+0x32>
  21d91e:	3301      	adds	r3, #1
  21d920:	e7ee      	b.n	21d900 <wiced_bt_hfp_hf_init_state_machine+0x10>
  21d922:	2005      	movs	r0, #5
  21d924:	e7f4      	b.n	21d910 <wiced_bt_hfp_hf_init_state_machine+0x20>
  21d926:	bf00      	nop
  21d928:	0021e260 	.word	0x0021e260

0021d92c <wiced_bt_hfp_hf_utils_str2uint16>:
  21d92c:	b510      	push	{r4, lr}
  21d92e:	7802      	ldrb	r2, [r0, #0]
  21d930:	4603      	mov	r3, r0
  21d932:	2a20      	cmp	r2, #32
  21d934:	f100 0001 	add.w	r0, r0, #1
  21d938:	d0f9      	beq.n	21d92e <wiced_bt_hfp_hf_utils_str2uint16+0x2>
  21d93a:	b17a      	cbz	r2, 21d95c <wiced_bt_hfp_hf_utils_str2uint16+0x30>
  21d93c:	2000      	movs	r0, #0
  21d93e:	210a      	movs	r1, #10
  21d940:	f813 2b01 	ldrb.w	r2, [r3], #1
  21d944:	3a30      	subs	r2, #48	; 0x30
  21d946:	b2d4      	uxtb	r4, r2
  21d948:	2c09      	cmp	r4, #9
  21d94a:	d807      	bhi.n	21d95c <wiced_bt_hfp_hf_utils_str2uint16+0x30>
  21d94c:	4410      	add	r0, r2
  21d94e:	f5b0 3f80 	cmp.w	r0, #65536	; 0x10000
  21d952:	da03      	bge.n	21d95c <wiced_bt_hfp_hf_utils_str2uint16+0x30>
  21d954:	781a      	ldrb	r2, [r3, #0]
  21d956:	b11a      	cbz	r2, 21d960 <wiced_bt_hfp_hf_utils_str2uint16+0x34>
  21d958:	4348      	muls	r0, r1
  21d95a:	e7f1      	b.n	21d940 <wiced_bt_hfp_hf_utils_str2uint16+0x14>
  21d95c:	f04f 30ff 	mov.w	r0, #4294967295
  21d960:	bd10      	pop	{r4, pc}

0021d962 <utl_strcpy>:
  21d962:	4603      	mov	r3, r0
  21d964:	3901      	subs	r1, #1
  21d966:	f811 2f01 	ldrb.w	r2, [r1, #1]!
  21d96a:	b90a      	cbnz	r2, 21d970 <utl_strcpy+0xe>
  21d96c:	701a      	strb	r2, [r3, #0]
  21d96e:	4770      	bx	lr
  21d970:	f803 2b01 	strb.w	r2, [r3], #1
  21d974:	e7f7      	b.n	21d966 <utl_strcpy+0x4>

0021d976 <utl_str2int>:
  21d976:	b510      	push	{r4, lr}
  21d978:	7802      	ldrb	r2, [r0, #0]
  21d97a:	4603      	mov	r3, r0
  21d97c:	2a20      	cmp	r2, #32
  21d97e:	f100 0001 	add.w	r0, r0, #1
  21d982:	d0f9      	beq.n	21d978 <utl_str2int+0x2>
  21d984:	b18a      	cbz	r2, 21d9aa <utl_str2int+0x34>
  21d986:	2000      	movs	r0, #0
  21d988:	210a      	movs	r1, #10
  21d98a:	f813 2b01 	ldrb.w	r2, [r3], #1
  21d98e:	3a30      	subs	r2, #48	; 0x30
  21d990:	b2d4      	uxtb	r4, r2
  21d992:	2c09      	cmp	r4, #9
  21d994:	d809      	bhi.n	21d9aa <utl_str2int+0x34>
  21d996:	4410      	add	r0, r2
  21d998:	f5b0 4f00 	cmp.w	r0, #32768	; 0x8000
  21d99c:	da05      	bge.n	21d9aa <utl_str2int+0x34>
  21d99e:	781a      	ldrb	r2, [r3, #0]
  21d9a0:	b90a      	cbnz	r2, 21d9a6 <utl_str2int+0x30>
  21d9a2:	b200      	sxth	r0, r0
  21d9a4:	bd10      	pop	{r4, pc}
  21d9a6:	4348      	muls	r0, r1
  21d9a8:	e7ef      	b.n	21d98a <utl_str2int+0x14>
  21d9aa:	f04f 30ff 	mov.w	r0, #4294967295
  21d9ae:	e7f9      	b.n	21d9a4 <utl_str2int+0x2e>

0021d9b0 <utl_itoa>:
  21d9b0:	460b      	mov	r3, r1
  21d9b2:	b5f0      	push	{r4, r5, r6, r7, lr}
  21d9b4:	b938      	cbnz	r0, 21d9c6 <utl_itoa+0x16>
  21d9b6:	2230      	movs	r2, #48	; 0x30
  21d9b8:	f803 2b01 	strb.w	r2, [r3], #1
  21d9bc:	2200      	movs	r2, #0
  21d9be:	1a58      	subs	r0, r3, r1
  21d9c0:	701a      	strb	r2, [r3, #0]
  21d9c2:	b2c0      	uxtb	r0, r0
  21d9c4:	bdf0      	pop	{r4, r5, r6, r7, pc}
  21d9c6:	2505      	movs	r5, #5
  21d9c8:	2700      	movs	r7, #0
  21d9ca:	f242 7210 	movw	r2, #10000	; 0x2710
  21d9ce:	f04f 0e0a 	mov.w	lr, #10
  21d9d2:	fbb0 f4f2 	udiv	r4, r0, r2
  21d9d6:	4684      	mov	ip, r0
  21d9d8:	fb02 0014 	mls	r0, r2, r4, r0
  21d9dc:	4594      	cmp	ip, r2
  21d9de:	b2a6      	uxth	r6, r4
  21d9e0:	b280      	uxth	r0, r0
  21d9e2:	d200      	bcs.n	21d9e6 <utl_itoa+0x36>
  21d9e4:	b127      	cbz	r7, 21d9f0 <utl_itoa+0x40>
  21d9e6:	2701      	movs	r7, #1
  21d9e8:	f106 0430 	add.w	r4, r6, #48	; 0x30
  21d9ec:	f803 4b01 	strb.w	r4, [r3], #1
  21d9f0:	3d01      	subs	r5, #1
  21d9f2:	fbb2 f2fe 	udiv	r2, r2, lr
  21d9f6:	d1ec      	bne.n	21d9d2 <utl_itoa+0x22>
  21d9f8:	e7e0      	b.n	21d9bc <utl_itoa+0xc>

0021d9fa <utl_strucmp>:
  21d9fa:	b510      	push	{r4, lr}
  21d9fc:	1e42      	subs	r2, r0, #1
  21d9fe:	3901      	subs	r1, #1
  21da00:	f812 0f01 	ldrb.w	r0, [r2, #1]!
  21da04:	b118      	cbz	r0, 21da0e <utl_strucmp+0x14>
  21da06:	f811 3f01 	ldrb.w	r3, [r1, #1]!
  21da0a:	b90b      	cbnz	r3, 21da10 <utl_strucmp+0x16>
  21da0c:	2001      	movs	r0, #1
  21da0e:	bd10      	pop	{r4, pc}
  21da10:	f1a3 0461 	sub.w	r4, r3, #97	; 0x61
  21da14:	2c19      	cmp	r4, #25
  21da16:	bf9c      	itt	ls
  21da18:	3b20      	subls	r3, #32
  21da1a:	b2db      	uxtbls	r3, r3
  21da1c:	4298      	cmp	r0, r3
  21da1e:	d0ef      	beq.n	21da00 <utl_strucmp+0x6>
  21da20:	f04f 30ff 	mov.w	r0, #4294967295
  21da24:	e7f3      	b.n	21da0e <utl_strucmp+0x14>

0021da26 <utl_bdcpy>:
  21da26:	3801      	subs	r0, #1
  21da28:	1d8b      	adds	r3, r1, #6
  21da2a:	f811 2b01 	ldrb.w	r2, [r1], #1
  21da2e:	4299      	cmp	r1, r3
  21da30:	f800 2f01 	strb.w	r2, [r0, #1]!
  21da34:	d1f9      	bne.n	21da2a <utl_bdcpy+0x4>
  21da36:	4770      	bx	lr

0021da38 <utl_bdcmp>:
  21da38:	b510      	push	{r4, lr}
  21da3a:	1d83      	adds	r3, r0, #6
  21da3c:	f810 4b01 	ldrb.w	r4, [r0], #1
  21da40:	f811 2b01 	ldrb.w	r2, [r1], #1
  21da44:	4294      	cmp	r4, r2
  21da46:	d103      	bne.n	21da50 <utl_bdcmp+0x18>
  21da48:	4283      	cmp	r3, r0
  21da4a:	d1f7      	bne.n	21da3c <utl_bdcmp+0x4>
  21da4c:	2000      	movs	r0, #0
  21da4e:	bd10      	pop	{r4, pc}
  21da50:	f04f 30ff 	mov.w	r0, #4294967295
  21da54:	e7fb      	b.n	21da4e <utl_bdcmp+0x16>
	...

0021da58 <unbond.part.0>:
  21da58:	4b08      	ldr	r3, [pc, #32]	; (21da7c <unbond.part.0+0x24>)
  21da5a:	681a      	ldr	r2, [r3, #0]
  21da5c:	f022 427f 	bic.w	r2, r2, #4278190080	; 0xff000000
  21da60:	601a      	str	r2, [r3, #0]
  21da62:	681a      	ldr	r2, [r3, #0]
  21da64:	f042 4282 	orr.w	r2, r2, #1090519040	; 0x41000000
  21da68:	601a      	str	r2, [r3, #0]
  21da6a:	69da      	ldr	r2, [r3, #28]
  21da6c:	f022 4270 	bic.w	r2, r2, #4026531840	; 0xf0000000
  21da70:	61da      	str	r2, [r3, #28]
  21da72:	69da      	ldr	r2, [r3, #28]
  21da74:	f042 5280 	orr.w	r2, r2, #268435456	; 0x10000000
  21da78:	61da      	str	r2, [r3, #28]
  21da7a:	4770      	bx	lr
  21da7c:	0032006c 	.word	0x0032006c

0021da80 <unbond>:
  21da80:	280b      	cmp	r0, #11
  21da82:	d111      	bne.n	21daa8 <unbond+0x28>
  21da84:	4b0b      	ldr	r3, [pc, #44]	; (21dab4 <unbond+0x34>)
  21da86:	681a      	ldr	r2, [r3, #0]
  21da88:	f422 027f 	bic.w	r2, r2, #16711680	; 0xff0000
  21da8c:	601a      	str	r2, [r3, #0]
  21da8e:	681a      	ldr	r2, [r3, #0]
  21da90:	f442 0282 	orr.w	r2, r2, #4259840	; 0x410000
  21da94:	601a      	str	r2, [r3, #0]
  21da96:	69da      	ldr	r2, [r3, #28]
  21da98:	f022 6270 	bic.w	r2, r2, #251658240	; 0xf000000
  21da9c:	61da      	str	r2, [r3, #28]
  21da9e:	69da      	ldr	r2, [r3, #28]
  21daa0:	f042 7280 	orr.w	r2, r2, #16777216	; 0x1000000
  21daa4:	61da      	str	r2, [r3, #28]
  21daa6:	4770      	bx	lr
  21daa8:	281e      	cmp	r0, #30
  21daaa:	d101      	bne.n	21dab0 <unbond+0x30>
  21daac:	f7ff bfd4 	b.w	21da58 <unbond.part.0>
  21dab0:	4770      	bx	lr
  21dab2:	bf00      	nop
  21dab4:	0032006c 	.word	0x0032006c

0021dab8 <wiced_configure_swd_pins>:
  21dab8:	1c42      	adds	r2, r0, #1
  21daba:	b510      	push	{r4, lr}
  21dabc:	4604      	mov	r4, r0
  21dabe:	d001      	beq.n	21dac4 <wiced_configure_swd_pins+0xc>
  21dac0:	1c4b      	adds	r3, r1, #1
  21dac2:	d109      	bne.n	21dad8 <wiced_configure_swd_pins+0x20>
  21dac4:	4b1c      	ldr	r3, [pc, #112]	; (21db38 <wiced_configure_swd_pins+0x80>)
  21dac6:	681a      	ldr	r2, [r3, #0]
  21dac8:	f422 72fc 	bic.w	r2, r2, #504	; 0x1f8
  21dacc:	601a      	str	r2, [r3, #0]
  21dace:	681a      	ldr	r2, [r3, #0]
  21dad0:	f042 0240 	orr.w	r2, r2, #64	; 0x40
  21dad4:	601a      	str	r2, [r3, #0]
  21dad6:	bd10      	pop	{r4, pc}
  21dad8:	200b      	movs	r0, #11
  21dada:	f7ff ffd1 	bl	21da80 <unbond>
  21dade:	f7ff ffbb 	bl	21da58 <unbond.part.0>
  21dae2:	1ea3      	subs	r3, r4, #2
  21dae4:	b2db      	uxtb	r3, r3
  21dae6:	2b02      	cmp	r3, #2
  21dae8:	d806      	bhi.n	21daf8 <wiced_configure_swd_pins+0x40>
  21daea:	4b14      	ldr	r3, [pc, #80]	; (21db3c <wiced_configure_swd_pins+0x84>)
  21daec:	f853 2024 	ldr.w	r2, [r3, r4, lsl #2]
  21daf0:	6813      	ldr	r3, [r2, #0]
  21daf2:	f423 63c0 	bic.w	r3, r3, #1536	; 0x600
  21daf6:	6013      	str	r3, [r2, #0]
  21daf8:	1e8b      	subs	r3, r1, #2
  21dafa:	b2db      	uxtb	r3, r3
  21dafc:	2b02      	cmp	r3, #2
  21dafe:	d80f      	bhi.n	21db20 <wiced_configure_swd_pins+0x68>
  21db00:	4b0e      	ldr	r3, [pc, #56]	; (21db3c <wiced_configure_swd_pins+0x84>)
  21db02:	eb03 0381 	add.w	r3, r3, r1, lsl #2
  21db06:	695b      	ldr	r3, [r3, #20]
  21db08:	681a      	ldr	r2, [r3, #0]
  21db0a:	f422 62c0 	bic.w	r2, r2, #1536	; 0x600
  21db0e:	601a      	str	r2, [r3, #0]
  21db10:	681a      	ldr	r2, [r3, #0]
  21db12:	f022 0230 	bic.w	r2, r2, #48	; 0x30
  21db16:	601a      	str	r2, [r3, #0]
  21db18:	681a      	ldr	r2, [r3, #0]
  21db1a:	f042 0220 	orr.w	r2, r2, #32
  21db1e:	601a      	str	r2, [r3, #0]
  21db20:	4b05      	ldr	r3, [pc, #20]	; (21db38 <wiced_configure_swd_pins+0x80>)
  21db22:	0189      	lsls	r1, r1, #6
  21db24:	681a      	ldr	r2, [r3, #0]
  21db26:	ea41 01c4 	orr.w	r1, r1, r4, lsl #3
  21db2a:	f422 72fc 	bic.w	r2, r2, #504	; 0x1f8
  21db2e:	601a      	str	r2, [r3, #0]
  21db30:	681a      	ldr	r2, [r3, #0]
  21db32:	4311      	orrs	r1, r2
  21db34:	6019      	str	r1, [r3, #0]
  21db36:	e7ce      	b.n	21dad6 <wiced_configure_swd_pins+0x1e>
  21db38:	0032014c 	.word	0x0032014c
  21db3c:	0021e300 	.word	0x0021e300

Disassembly of section .setup:

00220274 <wiced_audio_sink_init>:
  220274:	4813      	ldr	r0, [pc, #76]	; (2202c4 <wiced_audio_sink_init+0x50>)
  220276:	6881      	ldr	r1, [r0, #8]
  220278:	f441 3120 	orr.w	r1, r1, #163840	; 0x28000
  22027c:	6081      	str	r1, [r0, #8]
  22027e:	6901      	ldr	r1, [r0, #16]
  220280:	f041 0138 	orr.w	r1, r1, #56	; 0x38
  220284:	6101      	str	r1, [r0, #16]
  220286:	4911      	ldr	r1, [pc, #68]	; (2202cc <wiced_audio_sink_init+0x58>)
  220288:	480f      	ldr	r0, [pc, #60]	; (2202c8 <wiced_audio_sink_init+0x54>)
  22028a:	6008      	str	r0, [r1, #0]
  22028c:	4911      	ldr	r1, [pc, #68]	; (2202d4 <wiced_audio_sink_init+0x60>)
  22028e:	4810      	ldr	r0, [pc, #64]	; (2202d0 <wiced_audio_sink_init+0x5c>)
  220290:	6008      	str	r0, [r1, #0]
  220292:	4912      	ldr	r1, [pc, #72]	; (2202dc <wiced_audio_sink_init+0x68>)
  220294:	4810      	ldr	r0, [pc, #64]	; (2202d8 <wiced_audio_sink_init+0x64>)
  220296:	6008      	str	r0, [r1, #0]
  220298:	4912      	ldr	r1, [pc, #72]	; (2202e4 <wiced_audio_sink_init+0x70>)
  22029a:	4811      	ldr	r0, [pc, #68]	; (2202e0 <wiced_audio_sink_init+0x6c>)
  22029c:	6008      	str	r0, [r1, #0]
  22029e:	4913      	ldr	r1, [pc, #76]	; (2202ec <wiced_audio_sink_init+0x78>)
  2202a0:	4811      	ldr	r0, [pc, #68]	; (2202e8 <wiced_audio_sink_init+0x74>)
  2202a2:	6008      	str	r0, [r1, #0]
  2202a4:	4812      	ldr	r0, [pc, #72]	; (2202f0 <wiced_audio_sink_init+0x7c>)
  2202a6:	f242 7110 	movw	r1, #10000	; 0x2710
  2202aa:	6001      	str	r1, [r0, #0]
  2202ac:	1049      	asrs	r1, r1, #1
  2202ae:	6041      	str	r1, [r0, #4]
  2202b0:	6081      	str	r1, [r0, #8]
  2202b2:	60c1      	str	r1, [r0, #12]
  2202b4:	480f      	ldr	r0, [pc, #60]	; (2202f4 <wiced_audio_sink_init+0x80>)
  2202b6:	7880      	ldrb	r0, [r0, #2]
  2202b8:	2800      	cmp	r0, #0
  2202ba:	d001      	beq.n	2202c0 <wiced_audio_sink_init+0x4c>
  2202bc:	f7f9 bbfe 	b.w	219abc <create_pll_tuner>
  2202c0:	4770      	bx	lr
  2202c2:	0000      	.short	0x0000
  2202c4:	0020af84 	.word	0x0020af84
  2202c8:	00217aa5 	.word	0x00217aa5
  2202cc:	00216b10 	.word	0x00216b10
  2202d0:	002191c1 	.word	0x002191c1
  2202d4:	00216b0c 	.word	0x00216b0c
  2202d8:	00218abb 	.word	0x00218abb
  2202dc:	00216b14 	.word	0x00216b14
  2202e0:	00218bb1 	.word	0x00218bb1
  2202e4:	00216b18 	.word	0x00216b18
  2202e8:	00218e8f 	.word	0x00218e8f
  2202ec:	00216b1c 	.word	0x00216b1c
  2202f0:	00201d30 	.word	0x00201d30
  2202f4:	0021fa80 	.word	0x0021fa80

002202f8 <install_patch_lite_host_proc_route_config_req>:
  2202f8:	4901      	ldr	r1, [pc, #4]	; (220300 <install_patch_lite_host_proc_route_config_req+0x8>)
  2202fa:	4802      	ldr	r0, [pc, #8]	; (220304 <install_patch_lite_host_proc_route_config_req+0xc>)
  2202fc:	f6b0 bfd6 	b.w	d12ac <A2a0bea7c>
  220300:	002183e9 	.word	0x002183e9
  220304:	000820a5 	.word	0x000820a5

00220308 <install_patch_addin_get_sbc_frame_len_in_jitter_buf>:
  220308:	4803      	ldr	r0, [pc, #12]	; (220318 <install_patch_addin_get_sbc_frame_len_in_jitter_buf+0x10>)
  22030a:	4902      	ldr	r1, [pc, #8]	; (220314 <install_patch_addin_get_sbc_frame_len_in_jitter_buf+0xc>)
  22030c:	1c80      	adds	r0, r0, #2
  22030e:	f6b0 bfcd 	b.w	d12ac <A2a0bea7c>
  220312:	0000      	.short	0x0000
  220314:	00217a95 	.word	0x00217a95
  220318:	00069c8b 	.word	0x00069c8b

0022031c <install_patch_addin_Sample_volume_ctrl>:
  22031c:	4803      	ldr	r0, [pc, #12]	; (22032c <install_patch_addin_Sample_volume_ctrl+0x10>)
  22031e:	4902      	ldr	r1, [pc, #8]	; (220328 <install_patch_addin_Sample_volume_ctrl+0xc>)
  220320:	1c80      	adds	r0, r0, #2
  220322:	f6b0 bfc3 	b.w	d12ac <A2a0bea7c>
  220326:	0000      	.short	0x0000
  220328:	00219e21 	.word	0x00219e21
  22032c:	000681bb 	.word	0x000681bb

00220330 <install_patch_addin_Timer2DoneInt>:
  220330:	4803      	ldr	r0, [pc, #12]	; (220340 <install_patch_addin_Timer2DoneInt+0x10>)
  220332:	4902      	ldr	r1, [pc, #8]	; (22033c <install_patch_addin_Timer2DoneInt+0xc>)
  220334:	1d80      	adds	r0, r0, #6
  220336:	f6b0 bfb9 	b.w	d12ac <A2a0bea7c>
  22033a:	0000      	.short	0x0000
  22033c:	00217a9d 	.word	0x00217a9d
  220340:	0001a7d7 	.word	0x0001a7d7

00220344 <install_patch_pcm_scoLinkDown>:
  220344:	4901      	ldr	r1, [pc, #4]	; (22034c <install_patch_pcm_scoLinkDown+0x8>)
  220346:	4802      	ldr	r0, [pc, #8]	; (220350 <install_patch_pcm_scoLinkDown+0xc>)
  220348:	f6b0 bfb0 	b.w	d12ac <A2a0bea7c>
  22034c:	0021a22d 	.word	0x0021a22d
  220350:	000ad619 	.word	0x000ad619

00220354 <spar_crt_setup>:
  220354:	480b      	ldr	r0, [pc, #44]	; (220384 <spar_crt_setup+0x30>)
  220356:	490c      	ldr	r1, [pc, #48]	; (220388 <spar_crt_setup+0x34>)
  220358:	b508      	push	{r3, lr}
  22035a:	4288      	cmp	r0, r1
  22035c:	d002      	beq.n	220364 <spar_crt_setup+0x10>
  22035e:	4a0b      	ldr	r2, [pc, #44]	; (22038c <spar_crt_setup+0x38>)
  220360:	f611 fdac 	bl	31ebc <bsc_OpExtended+0x2d91>
  220364:	4a0a      	ldr	r2, [pc, #40]	; (220390 <spar_crt_setup+0x3c>)
  220366:	2100      	movs	r1, #0
  220368:	480a      	ldr	r0, [pc, #40]	; (220394 <spar_crt_setup+0x40>)
  22036a:	f611 fdab 	bl	31ec4 <memcpy+0x7>
  22036e:	f7fa fe63 	bl	21b038 <install_libs>
  220372:	4b09      	ldr	r3, [pc, #36]	; (220398 <spar_crt_setup+0x44>)
  220374:	4a09      	ldr	r2, [pc, #36]	; (22039c <spar_crt_setup+0x48>)
  220376:	f023 030f 	bic.w	r3, r3, #15
  22037a:	6013      	str	r3, [r2, #0]
  22037c:	e8bd 4008 	ldmia.w	sp!, {r3, lr}
  220380:	f000 b80e 	b.w	2203a0 <application_setup>
  220384:	0021f948 	.word	0x0021f948
  220388:	0021f948 	.word	0x0021f948
  22038c:	00000268 	.word	0x00000268
  220390:	000006c0 	.word	0x000006c0
  220394:	0021fbb4 	.word	0x0021fbb4
  220398:	00220674 	.word	0x00220674
  22039c:	0020241c 	.word	0x0020241c

002203a0 <application_setup>:
  2203a0:	f04f 31ff 	mov.w	r1, #4294967295
  2203a4:	b508      	push	{r3, lr}
  2203a6:	4608      	mov	r0, r1
  2203a8:	f7fd fb86 	bl	21dab8 <wiced_configure_swd_pins>
  2203ac:	4b01      	ldr	r3, [pc, #4]	; (2203b4 <application_setup+0x14>)
  2203ae:	4a02      	ldr	r2, [pc, #8]	; (2203b8 <application_setup+0x18>)
  2203b0:	601a      	str	r2, [r3, #0]
  2203b2:	bd08      	pop	{r3, pc}
  2203b4:	00201d54 	.word	0x00201d54
  2203b8:	00217a39 	.word	0x00217a39
