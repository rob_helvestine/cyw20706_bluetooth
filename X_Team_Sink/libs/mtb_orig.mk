################################################
# Auto-generated as part of running getlibs 
################################################

# List of shared libraries 
SEARCH_TARGET_CYW920706WCDEVAL=../mtb_shared/wiced_btsdk/dev-kit/bsp/TARGET_CYW920706WCDEVAL/release-v2.8.0
SEARCH_btsdk-host-apps-bt-ble=../mtb_shared/wiced_btsdk/tools/btsdk-host-apps-bt-ble/release-v2.8.0
SEARCH_20706A2=../mtb_shared/wiced_btsdk/dev-kit/baselib/20706A2/release-v2.8.0
SEARCH_btsdk-common=../mtb_shared/wiced_btsdk/dev-kit/libraries/btsdk-common/release-v2.8.0
SEARCH_btsdk-include=../mtb_shared/wiced_btsdk/dev-kit/btsdk-include/release-v2.8.0
SEARCH_btsdk-tools=../mtb_shared/wiced_btsdk/dev-kit/btsdk-tools/release-v2.8.0
SEARCH_btsdk-utils=../mtb_shared/wiced_btsdk/tools/btsdk-utils/release-v2.8.0
SEARCH_core-make=../mtb_shared/core-make/release-v1.5.0

# Shared libraries added to build 
SEARCH+=$(SEARCH_TARGET_CYW920706WCDEVAL)
SEARCH+=$(SEARCH_btsdk-host-apps-bt-ble)
SEARCH+=$(SEARCH_20706A2)
SEARCH+=$(SEARCH_btsdk-common)
SEARCH+=$(SEARCH_btsdk-include)
SEARCH+=$(SEARCH_btsdk-tools)
SEARCH+=$(SEARCH_btsdk-utils)
SEARCH+=$(SEARCH_core-make)
